require("source-map-support").install();
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  var _this2 = this;
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  __webpack_require__(122);
  
  var _lodash = __webpack_require__(133);
  
  var _lodash2 = _interopRequireDefault(_lodash);
  
  var _fs = __webpack_require__(11);
  
  var _fs2 = _interopRequireDefault(_fs);
  
  var _path = __webpack_require__(12);
  
  var _path2 = _interopRequireDefault(_path);
  
  var _express = __webpack_require__(9);
  
  var _express2 = _interopRequireDefault(_express);
  
  var _reactDomServer = __webpack_require__(135);
  
  var _reactDomServer2 = _interopRequireDefault(_reactDomServer);
  
  var _router = __webpack_require__(118);
  
  var _router2 = _interopRequireDefault(_router);
  
  var _coreReactCookie = __webpack_require__(6);
  
  var _coreReactCookie2 = _interopRequireDefault(_coreReactCookie);
  
  var _superagent = __webpack_require__(13);
  
  var _superagent2 = _interopRequireDefault(_superagent);
  
  var server = global.server = (0, _express2['default'])();
  
  server.set('port', process.env.PORT || 5000);
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, 'public')));
  
  // Include static assets. Not advised for production
  /* CSS */
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/bootstrap/dist/fonts')));
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/bootstrap/dist/css')));
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/metisMenu/dist')));
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/font-awesome/fonts')));
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/font-awesome/css')));
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/bootstrap-social')));
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../dist/css')));
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/bootstrap-social')));
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/morrisjs')));
  
  /* JS */
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/jquery/dist')));
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/bootstrap/dist/js')));
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/typeaheadjs/dist')));
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/metisMenu/dist')));
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/raphael')));
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/morrisjs')));
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/datatables/media/js')));
  server.use(_express2['default']['static'](_path2['default'].join(__dirname, '../bower_components/datatables-plugins/integration/bootstrap/3')));
  
  //
  // Register API middleware
  // -----------------------------------------------------------------------------
  server.use('/api/content', __webpack_require__(64));
  //
  // Register server-side rendering middleware
  // -----------------------------------------------------------------------------
  
  server.get('/sales-invoice/:salesInvoiceId/print', function (req, res, next) {
  	var salesInvoiceId = req.params.salesInvoiceId;
  
  	_coreReactCookie2['default'].plugToRequest(req, res);
  	var auth = _coreReactCookie2['default'].load('auth');
  
  	_superagent2['default'].get('https://ceramic-api.herokuapp.com/v1/sales-invoice/' + salesInvoiceId + '/print').set('Authorization', auth).pipe(res);
  
  	// prompt to download
  	res.attachment('invoice-' + Date.now() + '.pdf');
  });
  
  server.get('/shipping-order/:shippingOrderId/print', function (req, res, next) {
  	var shippingOrderId = req.params.shippingOrderId;
  
  	_coreReactCookie2['default'].plugToRequest(req, res);
  	var auth = _coreReactCookie2['default'].load('auth');
  
  	_superagent2['default'].get('https://ceramic-api.herokuapp.com/v1/shipping-order/' + shippingOrderId + '/print').set('Authorization', auth).pipe(res);
  
  	// prompt to download
  	res.attachment('shipping-' + Date.now() + '.pdf');
  });
  
  // The top-level React component + HTML template for it
  var templateFile = _path2['default'].join(__dirname, 'templates/index.html');
  var template = _lodash2['default'].template(_fs2['default'].readFileSync(templateFile, 'utf8'));
  
  server.get('*', function callee$0$0(req, res, next) {
  	return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
  		var _this = this;
  
  		while (1) switch (context$1$0.prev = context$1$0.next) {
  			case 0:
  				context$1$0.prev = 0;
  				context$1$0.next = 3;
  				return regeneratorRuntime.awrap((function callee$1$0() {
  					var statusCode, data, css, context, html;
  					return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  						while (1) switch (context$2$0.prev = context$2$0.next) {
  							case 0:
  								statusCode = 200;
  								data = { title: '', description: '', css: '', body: '' };
  								css = [];
  								context = {
  									onInsertCss: function onInsertCss(value) {
  										return css.push(value);
  									},
  									onSetTitle: function onSetTitle(value) {
  										return data.title = value;
  									},
  									onSetMeta: function onSetMeta(key, value) {
  										return data[key] = value;
  									},
  									onPageNotFound: function onPageNotFound() {
  										return statusCode = 404;
  									}
  								};
  
  								_coreReactCookie2['default'].plugToRequest(req, res);
  
  								context$2$0.next = 7;
  								return regeneratorRuntime.awrap(_router2['default'].dispatch({ path: req.path, context: context }, function (state, component) {
  									data.body = _reactDomServer2['default'].renderToString(component);
  									data.css = css.join('');
  								}));
  
  							case 7:
  								html = template(data);
  
  								res.status(statusCode).send(html);
  
  							case 9:
  							case 'end':
  								return context$2$0.stop();
  						}
  					}, null, _this);
  				})());
  
  			case 3:
  				context$1$0.next = 8;
  				break;
  
  			case 5:
  				context$1$0.prev = 5;
  				context$1$0.t0 = context$1$0['catch'](0);
  
  				next(context$1$0.t0);
  
  			case 8:
  			case 'end':
  				return context$1$0.stop();
  		}
  	}, null, _this2, [[0, 5]]);
  });
  
  //
  // Launch the server
  // -----------------------------------------------------------------------------
  
  server.listen(server.get('port'), function () {
  	if (process.send) {
  		process.send('online');
  	} else {
  		console.log('The server is running at http://localhost:' + server.get('port'));
  	}
  });
  
  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "server.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 1 */
/***/ function(module, exports) {

  module.exports = require("react");

/***/ },
/* 2 */
/***/ function(module, exports) {

  /*
  	MIT License http://www.opensource.org/licenses/mit-license.php
  	Author Tobias Koppers @sokra
  */
  // css base code, injected by the css-loader
  module.exports = function() {
  	var list = [];
  
  	// return the list of modules as css string
  	list.toString = function toString() {
  		var result = [];
  		for(var i = 0; i < this.length; i++) {
  			var item = this[i];
  			if(item[2]) {
  				result.push("@media " + item[2] + "{" + item[1] + "}");
  			} else {
  				result.push(item[1]);
  			}
  		}
  		return result.join("");
  	};
  
  	// import a list of modules into the list
  	list.i = function(modules, mediaQuery) {
  		if(typeof modules === "string")
  			modules = [[null, modules, ""]];
  		var alreadyImportedModules = {};
  		for(var i = 0; i < this.length; i++) {
  			var id = this[i][0];
  			if(typeof id === "number")
  				alreadyImportedModules[id] = true;
  		}
  		for(i = 0; i < modules.length; i++) {
  			var item = modules[i];
  			// skip already imported module
  			// this implementation is not 100% perfect for weird media query combinations
  			//  when a module is imported multiple times with different media queries.
  			//  I hope this will never occur (Hey this way we have smaller bundles)
  			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
  				if(mediaQuery && !item[2]) {
  					item[2] = mediaQuery;
  				} else if(mediaQuery) {
  					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
  				}
  				list.push(item);
  			}
  		}
  	};
  	return list;
  };


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _fbjsLibInvariant = __webpack_require__(10);
  
  var _fbjsLibInvariant2 = _interopRequireDefault(_fbjsLibInvariant);
  
  var _fbjsLibExecutionEnvironment = __webpack_require__(7);
  
  var count = 0;
  
  function withStyles(styles) {
  	return function (ComposedComponent) {
  		return (function () {
  			_createClass(WithStyles, null, [{
  				key: 'contextTypes',
  				value: {
  					onInsertCss: _react.PropTypes.func
  				},
  				enumerable: true
  			}]);
  
  			function WithStyles() {
  				_classCallCheck(this, WithStyles);
  
  				this.refCount = 0;
  				ComposedComponent.prototype.renderCss = (function (css) {
  					var style = undefined;
  					if (_fbjsLibExecutionEnvironment.canUseDOM) {
  						if (this.styleId && (style = document.getElementById(this.styleId))) {
  							if ('textContent' in style) {
  								style.textContent = css;
  							} else {
  								style.styleSheet.cssText = css;
  							}
  						} else {
  							this.styleId = 'dynamic-css-' + count++;
  							style = document.createElement('style');
  							style.setAttribute('id', this.styleId);
  							style.setAttribute('type', 'text/css');
  
  							if ('textContent' in style) {
  								style.textContent = css;
  							} else {
  								style.styleSheet.cssText = css;
  							}
  
  							document.getElementsByTagName('head')[0].appendChild(style);
  							this.refCount++;
  						}
  					} else {
  						this.context.onInsertCss(css);
  					}
  				}).bind(this);
  			}
  
  			_createClass(WithStyles, [{
  				key: 'componentWillMount',
  				value: function componentWillMount() {
  					if (_fbjsLibExecutionEnvironment.canUseDOM) {
  						(0, _fbjsLibInvariant2['default'])(styles.use, 'The style-loader must be configured with reference-counted API.');
  						styles.use();
  					} else {
  						this.context.onInsertCss(styles.toString());
  					}
  				}
  			}, {
  				key: 'componentWillUnmount',
  				value: function componentWillUnmount() {
  					styles.unuse();
  					if (this.styleId) {
  						this.refCount--;
  						if (this.refCount < 1) {
  							var style = document.getElementById(this.styleId);
  							if (style) {
  								style.parentNode.removeChild(style);
  							}
  						}
  					}
  				}
  			}, {
  				key: 'render',
  				value: function render() {
  					return _react2['default'].createElement(ComposedComponent, this.props);
  				}
  			}]);
  
  			return WithStyles;
  		})();
  	};
  }
  
  exports['default'] = withStyles;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "withStyles.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  var isNode = typeof module !== 'undefined' && module.exports,
      React = isNode ? __webpack_require__(1) : window.React;
  
  var reactCookie = __webpack_require__(6);
  var RestCall = __webpack_require__(5);
  var Apps = {
  	version: "1.0.0",
  	baseUrl: "/",
  	// host: "https://ceramic-api.herokuapp.com",
  	host: "http://api.ceramic.dev:8080",
  	selector: "body",
  	timeFormat: "hh:mm:ss",
  	dateFormat: "DD-MMM-YYYY",
  	dateTimeFormat: "DD-MMM-YYYY hh:mm:ss",
  	defaultDirectory: "/attachment/",
  	defaultInformationMessage: "Your data has been saved.",
  	defaultErrorMessage: "Sorry, we cannot processing your request.\nPlease, try again later!",
  	optionSelectDefault: "-- SELECT ONE OPTION --",
  	AllAlerts: "",
  	props: [],
  	item: {}
  };
  
  Apps.Widget = function (options) {
  	var selector = Apps.selector + " .main";
  	var dataMessages = options.dataMessages || [];
  	var dataTask = options.dataTask || [];
  	var dataNotif = options.dataNotif || [];
  	var dataAccount = options.dataAccount || [];
  	var dataSideBar = options.dataSideBar || [];
  	var dataPageHeading = options.dataPageHeading || [];
  	var dataContent1 = options.dataContent1 || [];
  	var dataContent2 = options.dataContent2 || [];
  	var sessionUser = reactCookie.load('username');
  
  	/* <!-- Top Menu Header --> */
  	var NavHeader = React.createClass({
  		displayName: 'NavHeader',
  
  		render: function render() {
  			return React.createElement(
  				'div',
  				{ className: 'navbar-header' },
  				React.createElement(
  					'button',
  					{ type: 'button', className: 'navbar-toggle', 'data-toggle': 'collapse', 'data-target': '.navbar-collapse' },
  					React.createElement(
  						'span',
  						{ className: 'sr-only' },
  						'Toggle navigation'
  					),
  					React.createElement('span', { className: 'icon-bar' }),
  					React.createElement('span', { className: 'icon-bar' }),
  					React.createElement('span', { className: 'icon-bar' })
  				),
  				React.createElement(
  					'a',
  					{ className: 'navbar-brand', href: '/' },
  					'SB Admin'
  				)
  			);
  		}
  	});
  
  	var Messages = React.createClass({
  		displayName: 'Messages',
  
  		componentDidMount: function componentDidMount() {
  			{
  				dataMessages.map(function (result, i) {
  					if (typeof result.componentDidMount != 'undefined') result.componentDidMount();
  				});
  			};
  		},
  		loadHTML: function loadHTML() {
  			var rows = [],
  			    _this = this;
  			{
  				dataMessages.map(function (result, i) {
  					result.id = "key-" + i;
  					rows.push(React.createElement(
  						'li',
  						{ key: result.id },
  						React.createElement(
  							'a',
  							{ href: '#' },
  							React.createElement(
  								'div',
  								null,
  								React.createElement(
  									'strong',
  									null,
  									result.text
  								),
  								React.createElement(
  									'span',
  									{ className: 'pull-right text-muted' },
  									React.createElement(
  										'em',
  										null,
  										result.time
  									)
  								)
  							),
  							React.createElement(
  								'div',
  								null,
  								result.desc
  							)
  						)
  					));
  					rows.push(React.createElement('li', { key: result.id + "-devider", className: 'divider' }));
  				});
  			};
  			rows.push(React.createElement(
  				'li',
  				{ key: "key-view" },
  				React.createElement(
  					'a',
  					{ className: 'text-center', href: '#' },
  					React.createElement(
  						'strong',
  						null,
  						'Read All Messages'
  					),
  					' ',
  					React.createElement('i', { className: 'fa fa-angle-right' })
  				)
  			));
  			return React.createElement(
  				'ul',
  				{ className: 'dropdown-menu dropdown-messages' },
  				' ',
  				rows,
  				' '
  			);
  		},
  
  		render: function render() {
  			return this.loadHTML();
  		}
  	});
  
  	var Task = React.createClass({
  		displayName: 'Task',
  
  		componentDidMount: function componentDidMount() {
  			{
  				dataTask.map(function (result, i) {
  					if (typeof result.componentDidMount != 'undefined') result.componentDidMount();
  				});
  			};
  		},
  		loadHTML: function loadHTML() {
  			var rows = [],
  			    style = [],
  			    className = "",
  			    _this = this;
  
  			{
  				dataTask.map(function (result, i) {
  					result.id = "key-" + i;
  					className = "progress-bar progress-bar-" + result.type;
  					style[i] = {};
  					style[i].width = result.percentage;
  					rows.push(React.createElement(
  						'li',
  						{ key: result.id },
  						React.createElement(
  							'a',
  							{ href: '#' },
  							React.createElement(
  								'div',
  								null,
  								React.createElement(
  									'p',
  									null,
  									React.createElement(
  										'strong',
  										null,
  										result.text
  									),
  									React.createElement(
  										'span',
  										{ className: 'pull-right text-muted' },
  										result.percentage,
  										' Complete'
  									)
  								),
  								React.createElement(
  									'div',
  									{ className: 'progress progress-striped active' },
  									React.createElement(
  										'div',
  										{ className: className, role: 'progressbar', 'aria-valuenow': '40', 'aria-valuemin': '0', 'aria-valuemax': '100', style: style[i] },
  										React.createElement(
  											'span',
  											{ className: 'sr-only' },
  											result.percentage,
  											' Complete (',
  											result.type,
  											')'
  										)
  									)
  								)
  							)
  						)
  					));
  					rows.push(React.createElement('li', { key: result.id + "-divider", className: 'divider' }));
  				});
  			};
  
  			rows.push(React.createElement(
  				'li',
  				{ key: "key-view" },
  				React.createElement(
  					'a',
  					{ className: 'text-center', href: '#' },
  					React.createElement(
  						'strong',
  						null,
  						'See All Tasks'
  					),
  					' ',
  					React.createElement('i', { className: 'fa fa-angle-right' })
  				)
  			));
  
  			return React.createElement(
  				'ul',
  				{ className: 'dropdown-menu dropdown-tasks' },
  				' ',
  				rows,
  				' '
  			);
  		},
  
  		render: function render() {
  			return this.loadHTML();
  		}
  	});
  
  	var Notif = React.createClass({
  		displayName: 'Notif',
  
  		componentDidMount: function componentDidMount() {
  			{
  				dataNotif.map(function (result, i) {
  					if (typeof result.componentDidMount != 'undefined') result.componentDidMount();
  				});
  			};
  		},
  		loadHTML: function loadHTML() {
  			var rows = [],
  			    className = "",
  			    _this = this;
  
  			{
  				dataNotif.map(function (result, i) {
  					var url = result.url || "#";
  					result.id = "key-" + i;
  					className = "fa fa-fw " + result.icon;
  					if (typeof result.event == 'function') {
  						_this.onHandler = result.event;
  					}
  					rows.push(React.createElement(
  						'li',
  						{ key: result.id },
  						React.createElement(
  							'a',
  							{ href: url, onClick: _this.onHandler },
  							React.createElement('i', { className: className }),
  							' ',
  							result.text,
  							' ',
  							React.createElement(
  								'span',
  								{ className: 'pull-right text-muted small' },
  								' ',
  								result.time,
  								' '
  							)
  						)
  					));
  					rows.push(React.createElement('li', { key: result.id + "-divider", className: 'divider' }));
  				});
  			};
  			rows.push(React.createElement(
  				'li',
  				{ key: "key-view" },
  				React.createElement(
  					'a',
  					{ className: 'text-center', href: Apps.AllAlerts },
  					React.createElement(
  						'strong',
  						null,
  						'See All Alerts'
  					),
  					' ',
  					React.createElement('i', { className: 'fa fa-angle-right' })
  				)
  			));
  			return React.createElement(
  				'ul',
  				{ className: 'dropdown-menu dropdown-alerts' },
  				' ',
  				rows,
  				' '
  			);
  		},
  
  		render: function render() {
  			return this.loadHTML();
  		}
  	});
  
  	var Account = React.createClass({
  		displayName: 'Account',
  
  		componentDidMount: function componentDidMount() {
  			{
  				dataAccount.map(function (result, i) {
  					if (typeof result.componentDidMount != 'undefined') result.componentDidMount();
  				});
  			};
  		},
  		loadHTML: function loadHTML() {
  			var rows = [],
  			    _this = this;
  			{
  				dataAccount.map(function (result, i) {
  					result.id = "key-" + i;
  					var cls = "fa fa-fw " + result.cls;
  					if (typeof result.event == 'function') {
  						_this.onHandler = result.event;
  					}
  					rows.push(React.createElement(
  						'li',
  						{ id: result.id, key: result.id },
  						React.createElement(
  							'a',
  							{ href: result.url, onClick: _this.onHandler },
  							React.createElement('i', { className: cls }),
  							' ',
  							result.text
  						)
  					));
  					if (result.separated == true) rows.push(React.createElement('li', { key: result.id + "-divider", className: 'divider' }));
  				});
  			};
  			return React.createElement(
  				'ul',
  				{ className: 'dropdown-menu' },
  				' ',
  				rows,
  				' '
  			);
  		},
  		render: function render() {
  			return this.loadHTML();
  		}
  	});
  
  	/* <!-- Top Menu Items --> */
  	var NavBar = React.createClass({
  		displayName: 'NavBar',
  
  		loadHTML: function loadHTML() {
  			var html = [];
  			var tab1 = React.createElement(
  				'li',
  				{ className: 'dropdown', key: "key1" },
  				React.createElement(
  					'a',
  					{ href: '#', className: 'dropdown-toggle', 'data-toggle': 'dropdown' },
  					React.createElement('i', { className: 'fa fa-envelope fa-fw' }),
  					' ',
  					React.createElement('i', { className: 'fa fa-caret-down' })
  				),
  				React.createElement(Messages, null)
  			);
  			// html.push(tab1);
  			var tab2 = React.createElement(
  				'li',
  				{ className: 'dropdown', key: "key2" },
  				React.createElement(
  					'a',
  					{ href: '#', className: 'dropdown-toggle', 'data-toggle': 'dropdown' },
  					React.createElement('i', { className: 'fa fa-tasks fa-fw' }),
  					' ',
  					React.createElement('i', { className: 'fa fa-caret-down' })
  				),
  				React.createElement(Task, null)
  			);
  			// html.push(tab2);
  			var tab3 = React.createElement(
  				'li',
  				{ className: 'dropdown', key: "key3" },
  				React.createElement(
  					'a',
  					{ href: '#', className: 'dropdown-toggle', 'data-toggle': 'dropdown' },
  					React.createElement('i', { className: 'fa fa-bell fa-fw' }),
  					' ',
  					React.createElement(
  						'span',
  						{ className: 'label label-success', id: 'total-notif' },
  						'10'
  					),
  					' ',
  					React.createElement('i', { className: 'fa fa-caret-down' })
  				),
  				React.createElement(Notif, null)
  			);
  			html.push(tab3);
  			var tab4 = React.createElement(
  				'li',
  				{ className: 'dropdown', key: "key4" },
  				React.createElement(
  					'a',
  					{ href: '#', className: 'dropdown-toggle', 'data-toggle': 'dropdown' },
  					React.createElement('i', { className: 'fa fa-user fa-fw' }),
  					' ',
  					sessionUser,
  					' ',
  					React.createElement('i', { className: 'fa fa-caret-down' })
  				),
  				React.createElement(Account, null)
  			);
  			html.push(tab4);
  			return React.createElement(
  				'ul',
  				{ className: 'nav navbar-top-links navbar-right' },
  				' ',
  				html,
  				' '
  			);
  		},
  		render: function render() {
  			return this.loadHTML();
  		}
  	});
  
  	/* <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens --> */
  	var Sidebar = React.createClass({
  		displayName: 'Sidebar',
  
  		getInitialState: function getInitialState() {
  			var _this = this;
  			{
  				dataSideBar.map(function (result, i) {
  					if (typeof result.data != 'undefined') _this.prop.data = result.data;
  				});
  			};
  			return null;
  		},
  		componentDidMount: function componentDidMount() {
  			{
  				dataSideBar.map(function (result, i) {
  					if (typeof result.componentDidMount != 'undefined') result.componentDidMount();
  				});
  			};
  			$('#side-menu').metisMenu();
  		},
  		loadHTML: function loadHTML() {
  			var rows = [],
  			    className = "",
  			    _this = this;
  			rows.push(React.createElement(
  				'li',
  				{ key: "key-search-bar", className: 'sidebar-search' },
  				React.createElement(
  					'div',
  					{ className: 'input-group custom-search-form' },
  					React.createElement('input', { type: 'text', className: 'form-control', placeholder: 'Search...' }),
  					React.createElement(
  						'span',
  						{ className: 'input-group-btn' },
  						React.createElement(
  							'button',
  							{ className: 'btn btn-default', type: 'button' },
  							React.createElement('i', { className: 'fa fa-search' })
  						)
  					)
  				)
  			));
  			{
  				dataSideBar.map(function (result, i) {
  					result.id = "key-" + i;
  					className = "fa fa-fw " + result.icon;
  					if (typeof result.items !== 'undefined') {
  						var Items = [],
  						    Items2 = [];
  						{
  							result.items.map(function (resultItem1, j) {
  								resultItem1.id = "key-child1-" + j;
  								if (typeof resultItem1.items !== 'undefined') {
  									{
  										resultItem1.items.map(function (resultItem2, k) {
  											resultItem2.id = "key-child2-" + k;
  											Items2.push(React.createElement(
  												'li',
  												{ key: resultItem2.id },
  												React.createElement(
  													'a',
  													{ href: resultItem2.url },
  													resultItem2.text
  												)
  											));
  										});
  									};
  									Items.push(React.createElement(
  										'li',
  										{ key: resultItem1.id },
  										React.createElement(
  											'a',
  											{ href: resultItem1.url },
  											resultItem1.text,
  											' ',
  											React.createElement('span', { className: 'fa arrow' })
  										),
  										React.createElement(
  											'ul',
  											{ className: 'nav nav-third-level' },
  											' ',
  											Items2,
  											' '
  										)
  									));
  								} else {
  									Items.push(React.createElement(
  										'li',
  										{ key: resultItem1.id },
  										React.createElement(
  											'a',
  											{ href: resultItem1.url },
  											resultItem1.text
  										)
  									));
  								}
  							});
  						};
  						rows.push(React.createElement(
  							'li',
  							{ key: result.id, className: result['class'] },
  							React.createElement(
  								'a',
  								{ href: '#' },
  								React.createElement('i', { className: className }),
  								' ',
  								result.title,
  								' ',
  								React.createElement('span', { className: 'fa arrow' })
  							),
  							React.createElement(
  								'ul',
  								{ className: 'nav nav-second-level' },
  								Items
  							)
  						));
  					} else {
  						rows.push(React.createElement(
  							'li',
  							{ key: result.id },
  							React.createElement(
  								'a',
  								{ href: result.url },
  								React.createElement('i', { className: className }),
  								' ',
  								result.title,
  								' '
  							)
  						));
  					}
  				});
  			};
  
  			return React.createElement(
  				'div',
  				{ className: 'navbar-default sidebar', role: 'navigation' },
  				React.createElement(
  					'div',
  					{ className: 'sidebar-nav navbar-collapse' },
  					React.createElement(
  						'ul',
  						{ className: 'nav', id: 'side-menu' },
  						' ',
  						rows,
  						' '
  					)
  				)
  			);
  		},
  		render: function render() {
  			return this.loadHTML();
  		}
  	});
  
  	var Header = React.createClass({
  		displayName: 'Header',
  
  		render: function render() {
  			return React.createElement(
  				'nav',
  				{ className: 'navbar navbar-default navbar-static-top', role: 'navigation' },
  				React.createElement(NavHeader, null),
  				React.createElement(NavBar, null),
  				React.createElement(Sidebar, null)
  			);
  		}
  	});
  
  	var PageHeading = React.createClass({
  		displayName: 'PageHeading',
  
  		componentDidMount: function componentDidMount() {
  			QueryString = (function () {
  				// This function is anonymous, is executed immediately and
  				// the return value is assigned to QueryString!
  				var query_string = {};
  				var query = window.location.search.substring(1);
  				var vars = query.split("&");
  				for (var i = 0; i < vars.length; i++) {
  					var pair = vars[i].split("=");
  					// If first entry with this name
  					if (typeof query_string[pair[0]] === "undefined") {
  						query_string[pair[0]] = decodeURIComponent(pair[1]);
  						// If second entry with this name
  					} else if (typeof query_string[pair[0]] === "string") {
  							var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
  							query_string[pair[0]] = arr;
  							// If third or later entry with this name
  						} else {
  								query_string[pair[0]].push(decodeURIComponent(pair[1]));
  							}
  				}
  				return query_string;
  			})();
  
  			$.ajaxSetup({
  				beforeSend: function beforeSend(xhr) {
  					xhr.setRequestHeader("Authorization", reactCookie.load('auth'));
  					/* Loading */
  					$("body").append('<div id="loading" class="center" style="display:block;"><img src="/loading-x.gif" alt="" class="iLoading"/></div>');
  					var width = (parseInt($(window).width()) - ($(".iLoading").width() == 0 ? 480 : parseInt($(".iLoading").width()))) / 2;
  					var height = (parseInt($(window).height()) - ($(".iLoading").height() == 0 ? 320 : parseInt($(".iLoading").height()))) / 2;
  					$(".iLoading").css({
  						'position': 'fixed',
  						'left': width,
  						'top': height,
  						'z-index': 1000
  					});
  					$("body").css({
  						opacity: .6,
  						background: '#daecf4'
  					});
  					/***********/
  				},
  				complete: function complete(xhr, status) {
  					$("#loading").remove();
  					$("body").css({
  						opacity: 1,
  						background: ''
  					});
  				},
  				statusCode: {
  					401: function _() {
  						alert("Sorry, session expired. You must login again");
  						location.reload();
  					}
  				}
  			});
  
  			$(document).ajaxComplete(function (event, xhr, request, settings) {
  				// console.log(xhr);
  				var result = xhr.responseJSON;
  			});
  		},
  		loadHTML: function loadHTML() {
  			var rows = [],
  			    className = "",
  			    data = dataPageHeading,
  			    _this = this,
  			    toolbar = [],
  			    pageHeader = [],
  			    breadcrumb = "";
  			var buttonCollection = data.toolbar || [];
  
  			{
  				buttonCollection.map(function (result, i) {
  					className = "btn " + result.cls;
  					if (typeof result.event == 'function') {
  						_this.onHandler = result.event;
  					}
  					if (result.type.toLowerCase() == "button") {
  						toolbar.push(React.createElement(
  							'button',
  							{ className: className, id: result.name, name: result.name, onClick: _this.onHandler },
  							result.text
  						));
  					}
  				});
  			}
  
  			if (buttonCollection.length > 0) {
  				pageHeader.push(React.createElement(
  					'h1',
  					{ className: 'page-header', style: { display: "flex" } },
  					data.title,
  					' ',
  					React.createElement(
  						'small',
  						null,
  						data.additionalTitle
  					),
  					' ',
  					React.createElement(
  						'div',
  						{ style: { position: "absolute", right: "20px" } },
  						toolbar
  					)
  				));
  			} else {
  				pageHeader.push(React.createElement(
  					'h1',
  					{ className: 'page-header' },
  					data.title,
  					' ',
  					React.createElement(
  						'small',
  						null,
  						data.additionalTitle
  					),
  					' '
  				));
  			}
  
  			className = "fa " + data.breadcrumbsIcon;
  
  			if (typeof data.subtitle != 'undefined' && data.subtitle.length != 0) {
  				var subtitle = data.subtitle || [];
  				subtitle.map(function (result, i) {
  					if (i == 0) breadcrumb += result;else breadcrumb += " / " + result;
  				});
  			};
  
  			rows.push(React.createElement(
  				'div',
  				{ key: "key-page-heading", className: 'col-lg-12' },
  				pageHeader,
  				React.createElement(
  					'ol',
  					{ className: 'breadcrumb' },
  					React.createElement(
  						'li',
  						null,
  						React.createElement('i', { className: className }),
  						'  ',
  						breadcrumb
  					)
  				)
  			));
  			return React.createElement(
  				'div',
  				{ className: 'row' },
  				' ',
  				rows,
  				' '
  			);
  		},
  		render: function render() {
  			return this.loadHTML();
  		}
  	});
  
  	var Content1 = React.createClass({
  		displayName: 'Content1',
  
  		componentDidMount: function componentDidMount() {
  			{
  				dataContent1.map(function (result, i) {
  					if (typeof result.componentDidMount != 'undefined') result.componentDidMount();
  				});
  			};
  		},
  		loadHTML: function loadHTML() {
  			var className = {},
  			    rows = [],
  			    _this = this;
  			{
  				dataContent1.map(function (result, i) {
  					result.id = "key-" + i;
  					className.panel = "panel " + result.panel;
  					className.type = "fa fa-5x " + result.type;
  					rows.push(React.createElement(
  						'div',
  						{ key: result.id, className: 'col-lg-3 col-md-6' },
  						React.createElement(
  							'div',
  							{ className: className.panel },
  							React.createElement(
  								'div',
  								{ className: 'panel-heading' },
  								React.createElement(
  									'div',
  									{ className: 'row' },
  									React.createElement(
  										'div',
  										{ className: 'col-xs-3' },
  										React.createElement('i', { className: className.type })
  									),
  									React.createElement(
  										'div',
  										{ className: 'col-xs-9 text-right' },
  										React.createElement(
  											'div',
  											{ className: 'huge' },
  											result.text
  										),
  										React.createElement(
  											'div',
  											null,
  											result.desc
  										)
  									)
  								)
  							),
  							React.createElement(
  								'a',
  								{ href: result.url },
  								React.createElement(
  									'div',
  									{ className: 'panel-footer', key: "view-all-" + i, onClick: _this.clickHandler },
  									React.createElement(
  										'span',
  										{ className: 'pull-left' },
  										'View Details'
  									),
  									React.createElement(
  										'span',
  										{ className: 'pull-right' },
  										React.createElement('i', { className: 'fa fa-arrow-circle-right' })
  									),
  									React.createElement('div', { className: 'clearfix' })
  								)
  							)
  						)
  					));
  				});
  			};
  			return React.createElement(
  				'div',
  				{ className: 'row' },
  				' ',
  				rows,
  				' '
  			);
  		},
  		render: function render() {
  			return this.loadHTML();
  		}
  	});
  
  	var Content2 = React.createClass({
  		displayName: 'Content2',
  
  		componentWillUnmount: function componentWillUnmount() {
  			{
  				dataContent2.map(function (result, i) {
  					if (typeof result.componentWillUnmount != 'undefined') result.componentWillUnmount();
  				});
  			};
  		},
  		componentWillMount: function componentWillMount() {
  			{
  				dataContent2.map(function (result, i) {
  					if (typeof result.componentWillMount != 'undefined') result.componentWillMount();
  				});
  			};
  		},
  		componentWillReceiveProps: function componentWillReceiveProps(nextProps) {},
  		shouldComponentUpdate: function shouldComponentUpdate(nextProps, nextState) {
  			return true;
  		},
  		componentWillUpdate: function componentWillUpdate(nextProps, nextState) {},
  		componentDidMount: function componentDidMount() {
  			$('#page-wrapper').append('<div class="alert alert-dismissible fade in" role="alert" id="alert" style="position: absolute; top: 50px; right: 30px; display:none; width:220px;">' + '<div style="display:inline-flex">' + '<h4 id="alert-title"></h4> &nbsp;' + '<p id="alert-content"></p>' + '</div>' + '</div>');
  
  			$('body').append('<div class="modal fade" id="PopUpConfirm" tabindex="-1" role="dialog">' + '<div class="modal-dialog modal-sm">' + '<div class="modal-content">' + '<div class="modal-header">' + '<h4 class="modal-title">Confirmation</h4>' + '</div>' + '<div class="modal-body">' + '</div>' + '<div class="modal-footer">' + '<button type="button" class="btn btn-default" data-dismiss="modal">No</button>' + '<button type="button" class="btn btn-primary" id="btnYes">Yes</button>' + '</div>' + '</div>' + '</div>' + '</div>');
  
  			$('body').append('<div class="modal fade" id="popup"></div>');
  			$('#popup').html('<div class="modal-dialog">' + '<div class="modal-content">' + '<div class="modal-header">' + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '<h4 class="modal-title">Modal title</h4>' + '</div>' + '<div class="modal-body">' + '<p>One fine body&hellip;</p>' + '</div>' + '<div class="modal-footer">' + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' + '<button type="button" id="btnSubmitPopup" class="btn btn-primary">Save changes</button>' + '</div>' + '</div>' + '</div>');
  
  			if (typeof $(".datepicker") !== 'undefined') {
  				$(".datepicker").datepicker({
  					autoclose: true,
  					format: "dd M yyyy"
  				});
  			}
  
  			if (typeof $(".input-daterange input") !== 'undefined') {
  				$(".input-daterange input").each(function () {
  					$(this).datepicker({
  						autoclose: true,
  						format: "dd M yyyy"
  					});
  				});
  			}
  
  			if (typeof $("input[type=radio]") !== 'undefined') {
  				if ($("input[type=radio]").length > 0) {
  					$("input[type=radio]").each(function () {
  						var id = $(this).attr('id');
  						$("#" + id + ":first").attr('checked', 'checked');
  					});
  				}
  			}
  
  			$(':input[required], select[required]').change(function (e) {
  				if ($(this).val() == "") {
  					if ($(this).siblings().length == 0) $(this).parent().append("<span className='errorInput' id='error-" + $(this).attr('id') + "' style='color:red;font-weight:bold;'>This Field is Required!</span>");
  				} else $("#error-" + $(this).attr('id')).remove();
  			}).trigger("change");
  
  			{
  				dataContent2.map(function (result, i) {
  					if (typeof result.componentDidMount != 'undefined') result.componentDidMount();
  				});
  			};
  		},
  
  		loadHTML: function loadHTML() {
  			var rows = [],
  			    cls = "",
  			    objects = [],
  			    object = {},
  			    classHeader = "",
  			    body = [],
  			    ColHead = 12,
  			    ColBody = 12,
  			    _this = this,
  			    chart = [];
  
  			{
  				dataContent2.map(function (result, i) {
  					if (result.loadAll == true) rows = rows;else rows = [];
  					result.id = "key-" + i;
  					cls = result.cls || "col-lg-12";
  					if (typeof result.type !== 'undefined') {
  						objects = [], chart = [];
  						if (typeof result.menu !== 'undefined') {
  							{
  								result.menu.map(function (resultItem1, j) {
  									resultItem1.id = "key-child1-menu-" + j;
  									if (resultItem1.separated == true) objects.push(React.createElement('li', { key: resultItem1.id + "-divider", className: 'divider' }));
  									objects.push(React.createElement(
  										'li',
  										{ key: resultItem1.id },
  										React.createElement(
  											'a',
  											{ href: resultItem1.url },
  											' ',
  											resultItem1.text,
  											' '
  										)
  									));
  								});
  							};
  						}
  						if (result.type.toUpperCase() == 'AREACHART') {
  							object.title = result.title || 'Area Chart';
  							object.chartName = 'morris-area-chart';
  						} else if (result.type.toUpperCase() == 'BARCHART') {
  							object.title = result.title || 'Bar Chart';
  							object.chartName = 'morris-bar-chart';
  						} else if (result.type.toUpperCase() == 'DONUTCHART') {
  							object.title = result.title || 'Donut Chart';
  							object.chartName = 'morris-donut-chart';
  						}
  
  						if (typeof result.table != 'undefined') {
  							var bodyTable = [],
  							    objectsTable = [],
  							    headerTable = [],
  							    cellTable = [],
  							    rowTable = [],
  							    classTable = "",
  							    classChart = "";
  							{
  								result.table.map(function (resultItem1, j) {
  									if (typeof resultItem1.header !== 'undefined') {
  										{
  											resultItem1.header.map(function (resultItem2, k) {
  												headerTable.push(React.createElement(
  													'th',
  													{ key: "key-child1-header-" + k },
  													' ',
  													resultItem2,
  													' '
  												));
  											});
  										};
  										objectsTable.push(React.createElement(
  											'thead',
  											null,
  											React.createElement(
  												'tr',
  												{ key: "key-child1-row-" + j },
  												headerTable
  											)
  										));
  									}
  									if (typeof resultItem1.body !== 'undefined') {
  										{
  											resultItem1.body.map(function (resultItem2, k) {
  												cellTable = [];
  												{
  													resultItem2.map(function (resultItem3, l) {
  														cellTable.push(React.createElement(
  															'td',
  															{ key: "key-child2-cell-" + k + "-" + l },
  															' ',
  															resultItem3,
  															' '
  														));
  													});
  												};
  												rowTable.push(React.createElement(
  													'tr',
  													{ id: result.name + "-key-child2-row-" + k, key: "key-child2-row-" + k },
  													cellTable
  												));
  											});
  										};
  										objectsTable.push(React.createElement(
  											'tbody',
  											null,
  											rowTable
  										));
  									}
  
  									bodyTable.push(React.createElement(
  										'div',
  										{ key: result.id + "table-body", className: resultItem1.typeTable },
  										React.createElement(
  											'table',
  											{ className: 'table table-bordered table-hover table-striped', id: resultItem1.name, name: resultItem1.name },
  											objectsTable
  										)
  									));
  								});
  							};
  							if (typeof result.cols == 'undefined') {
  								ColHead = 4;
  								ColBody = 8;
  							}
  							classTable = "col-lg-" + ColHead;
  							classChart = "col-lg-" + ColBody;
  							chart.push(React.createElement(
  								'div',
  								{ className: classTable },
  								bodyTable
  							));
  						} else {
  							classChart = "col-lg-12";
  						}
  						chart.push(React.createElement(
  							'div',
  							{ className: classChart },
  							React.createElement('div', { id: object.chartName })
  						));
  
  						rows.push(React.createElement(
  							'div',
  							{ key: result.id, className: cls },
  							React.createElement(
  								'div',
  								{ className: 'panel panel-default' },
  								React.createElement(
  									'div',
  									{ className: 'panel-heading' },
  									React.createElement('i', { className: 'fa fa-bar-chart-o fa-fw' }),
  									' ',
  									object.title,
  									React.createElement(
  										'div',
  										{ className: 'pull-right' },
  										React.createElement(
  											'div',
  											{ className: 'btn-group' },
  											React.createElement(
  												'button',
  												{ type: 'button', className: 'btn btn-default btn-xs dropdown-toggle', 'data-toggle': 'dropdown' },
  												'Actions',
  												React.createElement('span', { className: 'caret' })
  											),
  											React.createElement(
  												'ul',
  												{ className: 'dropdown-menu pull-right', role: 'menu' },
  												objects
  											)
  										)
  									)
  								),
  								React.createElement(
  									'div',
  									{ className: 'panel-body' },
  									React.createElement(
  										'div',
  										{ className: 'row' },
  										chart
  									)
  								)
  							)
  						));
  					} else if (typeof result.mode !== 'undefined') {
  						switch (result.mode.toUpperCase()) {
  							case 'TIMELINE':
  								objects = [], body = [];
  								if (typeof result.items !== 'undefined') {
  									{
  										result.items.map(function (resultItem1, j) {
  											resultItem1.id = "key-child1-timeline-" + j;
  											if (resultItem1.inverted) object.level1 = "timeline-inverted";else object.level1 = "";
  											object.level2 = resultItem1.typeTimeline + " " + resultItem1.typeAlert;
  											object.level3 = "fa " + resultItem1.iconBadge;
  
  											objects.push(React.createElement(
  												'li',
  												{ key: resultItem1.id, className: object.level1 },
  												React.createElement(
  													'div',
  													{ className: object.level2 },
  													React.createElement('i', { className: object.level3 })
  												),
  												React.createElement(
  													'div',
  													{ className: 'timeline-panel' },
  													React.createElement(
  														'div',
  														{ className: 'timeline-heading' },
  														React.createElement(
  															'h4',
  															{ className: 'timeline-title' },
  															resultItem1.header
  														),
  														React.createElement(
  															'p',
  															null,
  															React.createElement(
  																'small',
  																{ className: 'text-muted' },
  																React.createElement('i', { className: 'fa fa-clock-o' }),
  																' ',
  																resultItem1.time
  															)
  														)
  													),
  													React.createElement(
  														'div',
  														{ className: 'timeline-body' },
  														React.createElement(
  															'p',
  															null,
  															resultItem1.body
  														)
  													)
  												)
  											));
  										});
  									};
  									body.push(React.createElement(
  										'div',
  										{ className: 'panel panel-default' },
  										React.createElement(
  											'div',
  											{ className: 'panel-heading' },
  											React.createElement('i', { className: 'fa fa-bar-chart-o fa-fw' }),
  											' ',
  											result.title
  										),
  										React.createElement(
  											'div',
  											{ className: 'panel-body' },
  											React.createElement(
  												'ul',
  												{ className: 'timeline' },
  												objects
  											)
  										)
  									));
  								}
  								break;
  							case 'NOTIFICATION':
  								objects = [], body = [];
  								var notif = [];
  								if (typeof result.items !== 'undefined') {
  									{
  										result.items.map(function (resultItem1, j) {
  											resultItem1.id = "key-child1-notification-" + j;
  											object['class'] = "fa fa-fw " + resultItem1.icon;
  											objects.push(React.createElement(
  												'a',
  												{ key: resultItem1.id, href: resultItem1.url, className: 'list-group-item' },
  												React.createElement('i', { className: object['class'] }),
  												' ',
  												resultItem1.text,
  												React.createElement(
  													'span',
  													{ className: 'pull-right text-muted small' },
  													React.createElement(
  														'em',
  														null,
  														resultItem1.time
  													)
  												)
  											));
  										});
  									};
  									notif.push(React.createElement(
  										'div',
  										{ className: 'list-group' },
  										objects
  									));
  									notif.push(React.createElement(
  										'a',
  										{ key: "key-view-alert-" + i, href: '#', className: 'btn btn-default btn-block' },
  										'View All Alerts'
  									));
  									body.push(React.createElement(
  										'div',
  										{ className: 'panel panel-default' },
  										React.createElement(
  											'div',
  											{ className: 'panel-heading' },
  											React.createElement('i', { className: 'fa fa-bar-chart-o fa-fw' }),
  											' ',
  											result.title
  										),
  										React.createElement(
  											'div',
  											{ className: 'panel-body' },
  											notif
  										)
  									));
  								}
  								break;
  							case 'TABLE':
  								body = [], objects = [];
  								var headerTable = [],
  								    cellTable = [],
  								    rowTable = [],
  								    tableEl = [];
  								if (typeof result.header !== 'undefined') {
  									{
  										result.header.map(function (resultItem1, j) {
  											headerTable.push(React.createElement(
  												'th',
  												{ key: "key-child1-header-" + j },
  												' ',
  												resultItem1,
  												' '
  											));
  										});
  									};
  									objects.push(React.createElement(
  										'thead',
  										null,
  										React.createElement(
  											'tr',
  											{ key: "key-child1-row-" + i },
  											headerTable
  										)
  									));
  								}
  								if (typeof result.body !== 'undefined') {
  									{
  										result.body.map(function (resultItem1, j) {
  											cellTable = [];
  											{
  												resultItem1.map(function (resultItem2, k) {
  													cellTable.push(React.createElement(
  														'td',
  														{ key: "key-child2-cell-" + j + "-" + k },
  														' ',
  														resultItem2,
  														' '
  													));
  												});
  											};
  											rowTable.push(React.createElement(
  												'tr',
  												{ id: result.name + "-key-child2-row-" + j, key: "key-child2-row-" + j },
  												cellTable
  											));
  										});
  									};
  									objects.push(React.createElement(
  										'tbody',
  										null,
  										rowTable
  									));
  								}
  								if (result.panel == true) {
  									tableEl.push(React.createElement(
  										'div',
  										{ key: result.id + "table-body", className: result.typeTable },
  										React.createElement(
  											'table',
  											{ className: 'table table-bordered table-hover table-striped', id: result.name, name: result.name },
  											objects
  										)
  									));
  									body.push(React.createElement(
  										'div',
  										{ className: 'panel panel-default' },
  										React.createElement(
  											'div',
  											{ className: 'panel-heading' },
  											React.createElement('i', { className: 'fa fa-bar-chart-o fa-fw' }),
  											' ',
  											result.title
  										),
  										React.createElement(
  											'div',
  											{ className: 'panel-body' },
  											tableEl
  										)
  									));
  								} else {
  									body.push(React.createElement(
  										'div',
  										{ key: result.id + "table-body", className: result.typeTable },
  										React.createElement(
  											'table',
  											{ className: 'table table-bordered table-hover table-striped', id: result.name, name: result.name },
  											objects
  										)
  									));
  								}
  								break;
  							case 'INPUT':
  								var classNameHeader = [],
  								    classNameBody = [],
  								    style = [];
  								body = [], objects = [];
  								{
  									result.items.map(function (resultItem1, j) {
  										var placeholder = resultItem1.placeholder || resultItem1.text;
  										var required = resultItem1.required || "",
  										    disabled = resultItem1.disabled || "";
  										if (typeof resultItem1.cols == 'undefined') {
  											ColHead = 2;
  											ColBody = 10;
  										} else {
  											ColHead = resultItem1.cols[0];
  											if (resultItem1.cols.length > 1) ColBody = resultItem1.cols[1];else ColBody = 12 - ColHead;
  										}
  										style[j] = {};
  										style[j].color = resultItem1.color;
  										style[j].display = resultItem1.display;
  										classNameHeader.push("control-label col-sm-" + ColHead);
  										classNameBody.push("col-sm-" + ColBody);
  										if (resultItem1.type == 'hidden') {
  											objects.push(React.createElement('input', { type: resultItem1.type, name: resultItem1.name, id: resultItem1.name, value: resultItem1.value }));
  										} else if (resultItem1.type == 'label') {
  											objects.push(React.createElement(
  												'div',
  												{ className: 'form-group', key: "group-" + j },
  												React.createElement(
  													'label',
  													{ className: classNameHeader[j] },
  													resultItem1.text
  												),
  												React.createElement(
  													'div',
  													{ className: classNameBody[j] },
  													React.createElement(
  														'label',
  														{ ref: resultItem1.name, name: resultItem1.name, id: resultItem1.name, style: style[j] },
  														resultItem1.text
  													)
  												)
  											));
  										} else if (resultItem1.type == 'text' || resultItem1.type == 'email' || resultItem1.type == 'password' || resultItem1.type == 'number') {
  											objects.push(React.createElement(
  												'div',
  												{ className: 'form-group', key: "group-" + j, style: style[j] },
  												React.createElement(
  													'label',
  													{ className: classNameHeader[j] },
  													resultItem1.text
  												),
  												React.createElement(
  													'div',
  													{ className: classNameBody[j] },
  													React.createElement('input', { className: 'form-control', required: required, disabled: disabled, type: resultItem1.type, name: resultItem1.name, id: resultItem1.name, placeholder: placeholder })
  												)
  											));
  										} else if (resultItem1.type == 'autocomplete') {
  											objects.push(React.createElement(
  												'div',
  												{ className: 'form-group', key: "group-" + j },
  												React.createElement(
  													'label',
  													{ className: classNameHeader[j] },
  													resultItem1.text
  												),
  												React.createElement(
  													'div',
  													{ className: classNameBody[j] },
  													React.createElement('input', { className: 'form-control typeahead', required: required, type: 'text', name: resultItem1.name, id: resultItem1.name, placeholder: placeholder })
  												)
  											));
  										} else if (resultItem1.type == 'area') {
  											objects.push(React.createElement(
  												'div',
  												{ className: 'form-group', key: "group-" + j },
  												React.createElement(
  													'label',
  													{ className: classNameHeader[j] },
  													resultItem1.text
  												),
  												React.createElement(
  													'div',
  													{ className: classNameBody[j] },
  													React.createElement('textarea', { required: required, className: 'form-control', name: resultItem1.name, id: resultItem1.name, rows: resultItem1.rows == 'undefined' ? 3 : resultItem1.rows })
  												)
  											));
  										} else if (resultItem1.type == 'file') {
  											objects.push(React.createElement(
  												'div',
  												{ className: 'form-group', key: "group-" + j },
  												React.createElement(
  													'label',
  													{ className: classNameHeader[j] },
  													resultItem1.text
  												),
  												React.createElement(
  													'div',
  													{ className: classNameBody[j] },
  													React.createElement('input', { required: required, type: 'file', name: resultItem1.name, id: resultItem1.name, accept: 'image/*' })
  												)
  											));
  										} else if (resultItem1.type == 'checkbox') {
  											var checkbox = [];
  											{
  												resultItem1.data.map(function (resultItem2, k) {
  													if (resultItem1.inline == false) {
  														checkbox.push(React.createElement(
  															'div',
  															{ className: 'checkbox', key: "checkbox-" + resultItem2.text + k },
  															React.createElement(
  																'label',
  																null,
  																React.createElement('input', { type: 'checkbox', value: resultItem2.value, name: resultItem1.name, id: resultItem1.name }),
  																' ',
  																resultItem2.text
  															)
  														));
  													} else {
  														checkbox.push(React.createElement(
  															'label',
  															{ className: 'checkbox-inline', key: "checkbox-" + resultItem2.text + k },
  															React.createElement('input', { type: 'checkbox', value: resultItem2.value, name: resultItem1.name, id: resultItem1.name }),
  															' ',
  															resultItem2.text
  														));
  													}
  												});
  											};
  											objects.push(React.createElement(
  												'div',
  												{ className: 'form-group', key: "group-" + j },
  												React.createElement(
  													'label',
  													{ className: classNameHeader[j] },
  													resultItem1.text
  												),
  												React.createElement(
  													'div',
  													{ className: classNameBody[j] },
  													checkbox
  												)
  											));
  										} else if (resultItem1.type == 'radio') {
  											var radiogroup = [];
  											{
  												resultItem1.data.map(function (resultItem2, k) {
  													if (resultItem1.inline == false) {
  														radiogroup.push(React.createElement(
  															'div',
  															{ className: 'radio', key: "radio-" + resultItem2.text + k },
  															React.createElement(
  																'label',
  																null,
  																React.createElement('input', { type: 'radio', name: resultItem2.name, id: resultItem2.name, value: resultItem2.value }),
  																' ',
  																resultItem2.text
  															)
  														));
  													} else {
  														radiogroup.push(React.createElement(
  															'label',
  															{ className: 'radio-inline', key: "radio-" + resultItem2.text + k },
  															React.createElement('input', { type: 'radio', name: resultItem2.name, id: resultItem2.name, value: resultItem2.value }),
  															' ',
  															resultItem2.text
  														));
  													}
  												});
  											};
  
  											objects.push(React.createElement(
  												'div',
  												{ className: 'form-group', key: "group-" + j },
  												React.createElement(
  													'label',
  													{ className: classNameHeader[j] },
  													resultItem1.text
  												),
  												React.createElement(
  													'div',
  													{ className: classNameBody[j] },
  													radiogroup
  												)
  											));
  										} else if (resultItem1.type == 'select') {
  											var selectoption = [],
  											    option = resultItem1.data || [];
  											{
  												option.map(function (resultItem2, k) {
  													selectoption.push(React.createElement(
  														'option',
  														{ value: resultItem2.value },
  														resultItem2.text
  													));
  												});
  											};
  											if (resultItem1.multiple == true) {
  												objects.push(React.createElement(
  													'div',
  													{ className: 'form-group', key: "group-" + j },
  													React.createElement(
  														'label',
  														{ className: classNameHeader[j] },
  														resultItem1.text
  													),
  													React.createElement(
  														'div',
  														{ className: classNameBody[j] },
  														React.createElement(
  															'select',
  															{ required: required, multiple: true, className: 'form-control', name: resultItem1.name, id: resultItem1.name },
  															' ',
  															selectoption,
  															' '
  														)
  													)
  												));
  											} else {
  												objects.push(React.createElement(
  													'div',
  													{ className: 'form-group', key: "group-" + j },
  													React.createElement(
  														'label',
  														{ className: classNameHeader[j] },
  														resultItem1.text
  													),
  													React.createElement(
  														'div',
  														{ className: classNameBody[j] },
  														React.createElement(
  															'select',
  															{ required: required, className: 'form-control', name: resultItem1.name, id: resultItem1.name },
  															' ',
  															selectoption,
  															' '
  														)
  													)
  												));
  											}
  										} else if (resultItem1.type == 'button') {
  											var buttongroup = [];
  											{
  												resultItem1.data.map(function (resultItem2, k) {
  													var className = "btn " + resultItem2.cls;
  													if (typeof resultItem2.event == 'function') {
  														_this.onHandler = resultItem2.event;
  													}
  													buttongroup.push(React.createElement(
  														'button',
  														{ type: resultItem2.type, className: className, name: resultItem2.name, id: resultItem2.name, onClick: _this.onHandler },
  														' ',
  														resultItem2.text,
  														' '
  													));
  												});
  											};
  											objects.push(React.createElement(
  												'div',
  												{ className: 'form-group', key: "group-" + j },
  												React.createElement(
  													'label',
  													{ className: classNameHeader[j] },
  													resultItem1.text
  												),
  												React.createElement(
  													'div',
  													{ className: classNameBody[j] },
  													buttongroup
  												)
  											));
  										} else if (resultItem1.type == 'datetime') {
  											if (resultItem1.range == true) {
  												objects.push(React.createElement(
  													'div',
  													{ className: 'form-group', key: "group-" + j },
  													React.createElement(
  														'label',
  														{ className: classNameHeader[j] },
  														resultItem1.text
  													),
  													React.createElement(
  														'div',
  														{ required: required, className: "input-daterange " + classNameBody[j] },
  														React.createElement('input', { name: resultItem1.nameStart, id: resultItem1.nameStart, type: 'text', className: 'input-small datepicker' }),
  														React.createElement(
  															'span',
  															{ className: 'add-on' },
  															'to'
  														),
  														React.createElement('input', { name: resultItem1.nameEnd, id: resultItem1.nameEnd, type: 'text', className: 'input-small datepicker' })
  													)
  												));
  											} else {
  												objects.push(React.createElement(
  													'div',
  													{ className: 'form-group', key: "group-" + j },
  													React.createElement(
  														'label',
  														{ className: classNameHeader[j] },
  														resultItem1.text
  													),
  													React.createElement(
  														'div',
  														{ className: classNameBody[j] },
  														React.createElement('input', { required: required, name: resultItem1.name, id: resultItem1.name, className: 'datepicker' })
  													)
  												));
  											}
  										}
  									});
  								};
  								body.push(React.createElement(
  									'form',
  									{ role: 'form', className: 'form-horizontal', name: result.name, id: result.name },
  									objects
  								));
  								break;
  							case 'THUMBNAIL':
  								objects = [];
  								{
  									result.items.map(function (resultItem1, j) {
  										var src = typeof resultItem1.url == 'undefined' || resultItem1.url == '' ? "/placeholder-640x480.png" : resultItem1.url;
  										objects.push(React.createElement(
  											'div',
  											{ key: "thumbnail" + j, className: resultItem1.col },
  											React.createElement(
  												'div',
  												{ className: 'thumbnail' },
  												React.createElement('img', { src: src, alt: 'placeholder' }),
  												React.createElement(
  													'div',
  													{ className: 'caption' },
  													React.createElement(
  														'h3',
  														null,
  														resultItem1.caption
  													),
  													React.createElement(
  														'p',
  														null,
  														resultItem1.desc
  													)
  												)
  											)
  										));
  									});
  								};
  								if (result.panel == false) {
  									body.push(React.createElement(
  										'div',
  										{ className: 'row' },
  										objects
  									));
  								} else {
  									classHeader = "fa fa-fw " + result.iconHeader;
  									body.push(React.createElement(
  										'div',
  										{ key: result.id, className: cls },
  										React.createElement(
  											'div',
  											{ className: 'panel panel-default' },
  											React.createElement(
  												'div',
  												{ className: 'panel-heading' },
  												React.createElement('i', { className: classHeader }),
  												' ',
  												result.title
  											),
  											React.createElement(
  												'div',
  												{ className: 'panel-body' },
  												objects
  											)
  										)
  									));
  								}
  								break;
  						}
  
  						rows.push(React.createElement(
  							'div',
  							{ key: result.id, className: cls },
  							body
  						));
  					}
  				});
  			};
  			return React.createElement(
  				'div',
  				{ className: 'row' },
  				' ',
  				rows,
  				' '
  			);
  		},
  		render: function render() {
  			return this.loadHTML();
  		}
  	});
  
  	var Page = React.createClass({
  		displayName: 'Page',
  
  		render: function render() {
  			return React.createElement(
  				'div',
  				{ id: 'page-wrapper' },
  				React.createElement(PageHeading, null),
  				React.createElement(Content1, null),
  				React.createElement(Content2, null)
  			);
  		}
  	});
  
  	return { Header: Header, Body: Page };
  };
  
  Apps.Users = function (options) {
  	var dataField = options.dataField || [];
  	var Form = React.createClass({
  		displayName: 'Form',
  
  		componentWillUnmount: function componentWillUnmount() {
  			{
  				dataField.map(function (result, i) {
  					if (typeof result.componentWillUnmount != 'undefined') result.componentWillUnmount();
  				});
  			};
  		},
  		componentWillMount: function componentWillMount() {
  			{
  				dataField.map(function (result, i) {
  					if (typeof result.componentWillMount != 'undefined') result.componentWillMount();
  				});
  			};
  		},
  		componentDidMount: function componentDidMount() {
  			$('#wrapper').append('<div class="alert alert-dismissible fade in" role="alert" id="alert" style="position: absolute; top: 50px; right: 30px; display:none; width:220px;">' + '<div style="display:inline-flex">' + '<h4 id="alert-title"></h4> &nbsp;' + '<p id="alert-content"></p>' + '</div>' + '</div>');
  			{
  				dataField.map(function (result, i) {
  					if (typeof result.componentDidMount != 'undefined') result.componentDidMount();
  				});
  			};
  		},
  		componentWillReceiveProps: function componentWillReceiveProps(nextProps) {},
  		shouldComponentUpdate: function shouldComponentUpdate(nextProps, nextState) {
  			return true;
  		},
  		componentWillUpdate: function componentWillUpdate(nextProps, nextState) {},
  		loadHTML: function loadHTML() {
  			var rows = [],
  			    cls = "",
  			    objects = [],
  			    signin = [],
  			    buttonCollection = [],
  			    _this = this,
  			    style = [];
  
  			{
  				dataField.map(function (result, i) {
  					cls = result.cls;
  					{
  						result.items.map(function (resultItem1, j) {
  							resultItem1.id = "key-" + j;
  							style[i] = {};
  							style[i].color = resultItem1.color;
  							style[i].display = resultItem1.display;
  							var Class = resultItem1.cls || "";
  							if (typeof resultItem1.event == 'function') {
  								_this.onHandler = resultItem1.event;
  							}
  							switch (resultItem1.type.toUpperCase()) {
  								case 'LABEL':
  									objects.push(React.createElement(
  										'div',
  										{ className: 'form-group', key: resultItem1.id },
  										React.createElement(
  											'label',
  											{ ref: resultItem1.name, name: resultItem1.name, id: resultItem1.name, style: style[i] },
  											resultItem1.text
  										)
  									));
  									break;
  								case 'HIDDEN':
  									objects.push(React.createElement(
  										'div',
  										{ className: 'form-group', key: resultItem1.id },
  										React.createElement('input', { className: 'form-control', ref: resultItem1.name, name: resultItem1.name, id: resultItem1.name, type: 'hidden', value: resultItem1.value })
  									));
  									break;
  								case 'TEXT':
  									objects.push(React.createElement(
  										'div',
  										{ className: 'form-group', key: resultItem1.id },
  										React.createElement('input', { className: 'form-control', placeholder: resultItem1.text, ref: resultItem1.name, name: resultItem1.name, id: resultItem1.name, type: 'text', style: style[i] })
  									));
  									break;
  								case 'PASSWORD':
  									objects.push(React.createElement(
  										'div',
  										{ className: 'form-group', key: resultItem1.id },
  										React.createElement('input', { className: 'form-control', placeholder: resultItem1.text, ref: resultItem1.name, name: resultItem1.name, id: resultItem1.name, type: 'password', style: style[i] })
  									));
  									break;
  								case 'EMAIL':
  									objects.push(React.createElement(
  										'div',
  										{ className: 'form-group', key: resultItem1.id },
  										React.createElement('input', { className: 'form-control', placeholder: resultItem1.text, ref: resultItem1.name, name: resultItem1.name, id: resultItem1.name, type: 'email', style: style[i] })
  									));
  									break;
  								case 'CHECKBOX':
  									objects.push(React.createElement(
  										'div',
  										{ className: 'form-group', key: resultItem1.id },
  										React.createElement(
  											'label',
  											null,
  											React.createElement('input', { name: resultItem1.name, id: resultItem1.name, ref: resultItem1.name, value: resultItem1.value, type: 'checkbox', style: style[i] }),
  											' ',
  											resultItem1.text
  										)
  									));
  									break;
  								case 'BUTTON':
  									if (typeof resultItem1.event == 'function') {
  										_this.onHandler = resultItem1.event;
  									}
  
  									objects.push(React.createElement(
  										'div',
  										{ className: 'form-group', key: resultItem1.id, onClick: _this.onHandler },
  										React.createElement(
  											'a',
  											{ href: resultItem1.url, className: Class, id: resultItem1.name, name: resultItem1.name },
  											resultItem1.text
  										)
  									));
  									if (typeof resultItem1.additional != 'undefined') {
  										if (resultItem1.additional.length > 0) {
  											{
  												resultItem1.additional.map(function (resultItem2, k) {
  													var Class = "btn btn-block btn-social btn-" + resultItem2.toLowerCase();
  													var Icon = "fa fa-" + resultItem2.toLowerCase();
  													var SignAuth = resultItem2.charAt(0).toUpperCase() + resultItem2.slice(1).toLowerCase();
  													signin.push(React.createElement(
  														'a',
  														{ className: Class },
  														React.createElement('i', { className: Icon }),
  														' Sign in with ',
  														SignAuth
  													));
  												});
  											};
  
  											buttonCollection.push(React.createElement('hr', null));
  											buttonCollection.push(React.createElement(
  												'div',
  												null,
  												signin
  											));
  										}
  									}
  									break;
  								case 'SELECT':
  									var option = [];
  									if (typeof resultItem1.options != 'undefined') {
  										{
  											resultItem1.options.map(function (resultItem2, k) {
  												option.push(React.createElement(
  													'option',
  													{ value: resultItem2.value },
  													resultItem2.text
  												));
  											});
  										};
  									}
  									objects.push(React.createElement(
  										'div',
  										{ className: 'form-group', key: resultItem1.id },
  										React.createElement(
  											'select',
  											{ className: 'form-control', ref: resultItem1.name, name: resultItem1.name, id: resultItem1.name, style: style[i] },
  											React.createElement(
  												'option',
  												null,
  												typeof resultItem1.opt_text == 'undefined' ? Apps.optionSelectDefault : resultItem1.opt_text
  											),
  											option
  										)
  									));
  									break;
  								case 'RADIO':
  									var radiogroup = [];
  									{
  										resultItem1.data.map(function (resultItem2, k) {
  											if (resultItem1.inline == false) {
  												radiogroup.push(React.createElement(
  													'div',
  													{ className: 'radio', key: "radio-" + resultItem2.text + k },
  													React.createElement(
  														'label',
  														null,
  														React.createElement('input', { type: 'radio', ref: resultItem1.name, name: resultItem2.name, id: resultItem2.name, value: resultItem2.value }),
  														' ',
  														resultItem2.text
  													)
  												));
  											} else {
  												radiogroup.push(React.createElement(
  													'label',
  													{ className: 'radio-inline', key: "radio-" + resultItem2.text + k },
  													React.createElement('input', { type: 'radio', ref: resultItem1.name, name: resultItem2.name, id: resultItem2.name, value: resultItem2.value }),
  													' ',
  													resultItem2.text
  												));
  											}
  										});
  									};
  
  									objects.push(React.createElement(
  										'div',
  										{ className: 'form-group', key: "group-" + j },
  										React.createElement(
  											'label',
  											null,
  											resultItem1.text
  										),
  										React.createElement(
  											'div',
  											null,
  											radiogroup
  										)
  									));
  									break;
  							}
  						});
  					};
  					rows.push(React.createElement(
  						'div',
  						{ className: 'panel-heading', key: "pnl-heading" + i },
  						React.createElement(
  							'h3',
  							{ className: 'panel-title' },
  							result.title
  						)
  					));
  					rows.push(React.createElement(
  						'div',
  						{ className: 'panel-body', key: "pnl-body" + i },
  						React.createElement(
  							'form',
  							{ role: 'form', id: result.name, name: result.name },
  							React.createElement(
  								'fieldset',
  								null,
  								objects
  							)
  						),
  						buttonCollection
  					));
  				});
  			};
  
  			return React.createElement(
  				'div',
  				{ className: cls },
  				React.createElement(
  					'div',
  					{ className: 'login-panel panel panel-default' },
  					rows
  				)
  			);
  		},
  		render: function render() {
  			return this.loadHTML();
  		}
  	});
  
  	var Page = React.createClass({
  		displayName: 'Page',
  
  		render: function render() {
  			return React.createElement(
  				'div',
  				{ className: 'row' },
  				React.createElement(Form, null)
  			);
  		}
  	});
  
  	var EventHandler = React.createClass({
  		displayName: 'EventHandler',
  
  		clickHandler: function clickHandler() {
  			console.log(React.findDOMNode(this.refs.myTextInput).value);
  		},
  		render: function render() {
  			return React.createElement(
  				'div',
  				null,
  				React.createElement('input', { type: 'text', ref: 'myTextInput' }),
  				React.createElement(
  					'button',
  					{ id: 'btnClickMe', onClick: this.clickHandler },
  					'Click Me'
  				),
  				React.createElement(
  					'div',
  					{ onClick: this.clickHandler },
  					'Click Me'
  				)
  			);
  		}
  	});
  
  	return Page;
  };
  
  module.exports = Apps;
  
  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Apps.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  // http://rapiddg.com/blog/calling-rest-api-nodejs-script
  'use strict';
  
  var querystring = __webpack_require__(134);
  var http = __webpack_require__(130);
  var https = __webpack_require__(131);
  var reactCookie = __webpack_require__(6);
  
  // var host = 'www.thegamecrafter.com';
  var host = 'api.ceramic.dev:8080';
  // var host = 'ceramic-api.herokuapp.com'
  var username = 'admin';
  var password = 'admin';
  var apiKey = '*****';
  var sessionId = null;
  var deckId = '68DC5A20-EE4F-11E2-A00C-0858C0D5C2ED';
  
  var init = function init(host, username, password, apiKey, sessionId, deckId) {
  	this.host = host;
  	this.username = username;
  	this.password = password;
  	this.apiKey = apiKey;
  	this.sessionId = sessionId;
  	this.deckId = deckId;
  };
  
  var performRequest = function performRequest(endpoint, method, data, success, file) {
  	var dataString = JSON.stringify(data);
  	var headers = {};
  
  	headers.Authorization = reactCookie.load('auth');
  
  	if (method == 'GET') {
  		// endpoint += '?' + querystring.stringify(data);
  	} else {
  			if (file) headers["Content-Type"] = 'multipart/form-data';else headers["Content-Type"] = 'application/json';
  			headers["Content-Length"] = dataString.length;
  		}
  	var options = {
  		host: host,
  		path: endpoint,
  		method: method,
  		headers: headers,
  		withCredentials: false
  	};
  
  	var req = http.request(options, function (res) {
  		// res.setEncoding('utf8');
  
  		var responseString = '';
  
  		res.on('data', function (data) {
  			responseString += data;
  		});
  
  		res.on('end', function () {
  			var responseObject = {};
  
  			responseObject = JSON.parse(responseString);
  
  			success(responseObject);
  		});
  	});
  
  	req.write(dataString);
  	req.end();
  };
  module.exports.init = init;
  module.exports.performRequest = performRequest;
  
  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "RestCall.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  var cookie = __webpack_require__(124);
  
  var _rawCookie = {};
  var _res = undefined;
  
  function load(name, doNotParse) {
    var cookies = {};
  
    if (typeof document !== 'undefined') {
      cookies = cookie.parse(document.cookie);
    }
  
    var cookieVal = cookies && cookies[name] || _rawCookie[name];
  
    if (!doNotParse) {
      try {
        cookieVal = JSON.parse(cookieVal);
      } catch (e) {
        // Not serialized object
      }
    }
  
    return cookieVal;
  }
  
  function save(name, val, opt) {
    _rawCookie[name] = val;
  
    // allow you to work with cookies as objects.
    if (typeof val === 'object') {
      _rawCookie[name] = JSON.stringify(val);
    }
  
    // Cookies only work in the browser
    if (typeof document !== 'undefined') {
      document.cookie = cookie.serialize(name, _rawCookie[name], opt);
    }
  
    if (_res && _res.cookie) {
      _res.cookie(name, val, opt);
    }
  }
  
  function remove(name, path) {
    delete _rawCookie[name];
  
    if (typeof document !== 'undefined') {
      var removeCookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  
      if (path) {
        removeCookie += ' path=' + path;
      }
  
      document.cookie = removeCookie;
    }
  
    if (_res && _res.clearCookie) {
      var opt = path ? { path: path } : undefined;
      _res.clearCookie(name, opt);
    }
  }
  
  function setRawCookie(rawCookie) {
    _rawCookie = cookie.parse(rawCookie);
  }
  
  function plugToRequest(req, res) {
    if (req) {
      if (req.cookie) {
        _rawCookie = req.cookie;
      } else if (req.headers && req.headers.cookie) {
        setRawCookie(req.headers.cookie);
      }
    }
  
    _res = res;
  }
  
  var reactCookie = {
    load: load,
    save: save,
    remove: remove,
    setRawCookie: setRawCookie,
    plugToRequest: plugToRequest
  };
  
  if (typeof window !== 'undefined') {
    window['reactCookie'] = reactCookie;
  }
  
  module.exports = reactCookie;
  
  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "react-cookie.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 7 */
/***/ function(module, exports) {

  module.exports = require("fbjs/lib/ExecutionEnvironment");

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  var _fbjsLibInvariant = __webpack_require__(10);
  
  var _fbjsLibInvariant2 = _interopRequireDefault(_fbjsLibInvariant);
  
  var _coreLocation = __webpack_require__(115);
  
  var _coreLocation2 = _interopRequireDefault(_coreLocation);
  
  function handleClick(event) {
  	// If not left mouse click
  	if (event.button !== 0) {
  		return;
  	}
  
  	// If modified event
  	if (event.metaKey || event.altKey || event.ctrlKey || event.shiftKey) {
  		return;
  	}
  
  	var el = event.currentTarget;
  
  	(0, _fbjsLibInvariant2['default'])(el && el.nodeName === 'A', 'The target element must be a link.');
  
  	// Rebuild path
  	var path = el.pathname + el.search + (el.hash || '');
  
  	event.preventDefault();
  	_coreLocation2['default'].navigateTo(path);
  }
  
  exports['default'] = { handleClick: handleClick };
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Link.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 9 */
/***/ function(module, exports) {

  module.exports = require("express");

/***/ },
/* 10 */
/***/ function(module, exports) {

  module.exports = require("fbjs/lib/invariant");

/***/ },
/* 11 */
/***/ function(module, exports) {

  module.exports = require("fs");

/***/ },
/* 12 */
/***/ function(module, exports) {

  module.exports = require("path");

/***/ },
/* 13 */
/***/ function(module, exports) {

  module.exports = require("superagent");

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 38 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 39 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 40 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 42 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 43 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 44 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 45 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 48 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 50 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 51 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 52 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 53 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 54 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 55 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 56 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 57 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 58 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 59 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 60 */
/***/ function(module, exports, __webpack_require__) {

  exports = module.exports = __webpack_require__(2)();
  // imports
  
  
  // module
  exports.push([module.id, "", ""]);
  
  // exports


/***/ },
/* 61 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  /**
   * React Routing | http://www.kriasoft.com/react-routing
   * Copyright (c) Konstantin Tarkus <hello@tarkus.me> | The MIT License
   */
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
    value: true
  });
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var Match = function Match(route, path, keys, match) {
    _classCallCheck(this, Match);
  
    this.route = route;
    this.path = path;
    this.params = Object.create(null);
    for (var i = 1; i < match.length; i++) {
      this.params[keys[i - 1].name] = decodeParam(match[i]);
    }
  };
  
  function decodeParam(val) {
    if (!(typeof val === 'string' || val instanceof String)) {
      return val;
    }
  
    try {
      return decodeURIComponent(val);
    } catch (e) {
      var err = new TypeError('Failed to decode param \'' + val + '\'');
      err.status = 400;
      throw err;
    }
  }
  
  exports['default'] = Match;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Match.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 62 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  /**
   * React Routing | http://www.kriasoft.com/react-routing
   * Copyright (c) Konstantin Tarkus <hello@tarkus.me> | The MIT License
   */
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
    value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _pathToRegexp = __webpack_require__(120);
  
  var _pathToRegexp2 = _interopRequireDefault(_pathToRegexp);
  
  var _Match = __webpack_require__(61);
  
  var _Match2 = _interopRequireDefault(_Match);
  
  var Route = (function () {
    function Route(path, handlers) {
      _classCallCheck(this, Route);
  
      this.path = path;
      this.handlers = handlers;
      this.regExp = (0, _pathToRegexp2['default'])(path, this.keys = []);
    }
  
    _createClass(Route, [{
      key: 'match',
      value: function match(path) {
        var match = this.regExp.exec(path);
        return match ? new _Match2['default'](this, path, this.keys, match) : null;
      }
    }]);
  
    return Route;
  })();
  
  exports['default'] = Route;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Route.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 63 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  /**
   * React Routing | http://www.kriasoft.com/react-routing
   * Copyright (c) Konstantin Tarkus <hello@tarkus.me> | The MIT License
   */
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
    value: true
  });
  
  var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _Route = __webpack_require__(62);
  
  var _Route2 = _interopRequireDefault(_Route);
  
  var emptyFunction = function emptyFunction() {};
  
  var Router = (function () {
  
    /**
     * Creates a new instance of the `Router` class.
     */
  
    function Router(initialize) {
      _classCallCheck(this, Router);
  
      this.routes = [];
      this.events = Object.create(null);
  
      if (typeof initialize === 'function') {
        initialize(this.on.bind(this));
      }
    }
  
    /**
     * Adds a new route to the routing table or registers an event listener.
     *
     * @param {String} path A string in the Express format, an array of strings, or a regular expression.
     * @param {Function|Array} handlers Asynchronous route handler function(s).
     */
  
    _createClass(Router, [{
      key: 'on',
      value: function on(path) {
        for (var _len = arguments.length, handlers = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          handlers[_key - 1] = arguments[_key];
        }
  
        if (path === 'error') {
          this.events[path] = handlers[0];
        } else {
          this.routes.push(new _Route2['default'](path, handlers));
        }
      }
    }, {
      key: 'dispatch',
      value: function dispatch(state, cb) {
        var routes, handlers, value, result, done, next;
        return regeneratorRuntime.async(function dispatch$(context$2$0) {
          while (1) switch (context$2$0.prev = context$2$0.next) {
            case 0:
              next = function next() {
                var _handlers$next;
  
                var _value, _value2, match, handler;
  
                return regeneratorRuntime.async(function next$(context$3$0) {
                  while (1) switch (context$3$0.prev = context$3$0.next) {
                    case 0:
                      if (!((_handlers$next = handlers.next(), value = _handlers$next.value, done = _handlers$next.done, _handlers$next) && !done)) {
                        context$3$0.next = 16;
                        break;
                      }
  
                      _value = value;
                      _value2 = _slicedToArray(_value, 2);
                      match = _value2[0];
                      handler = _value2[1];
  
                      state.params = match.params;
  
                      if (!(handler.length > 1)) {
                        context$3$0.next = 12;
                        break;
                      }
  
                      context$3$0.next = 9;
                      return regeneratorRuntime.awrap(handler(state, next));
  
                    case 9:
                      context$3$0.t0 = context$3$0.sent;
                      context$3$0.next = 15;
                      break;
  
                    case 12:
                      context$3$0.next = 14;
                      return regeneratorRuntime.awrap(handler(state));
  
                    case 14:
                      context$3$0.t0 = context$3$0.sent;
  
                    case 15:
                      return context$3$0.abrupt('return', context$3$0.t0);
  
                    case 16:
                    case 'end':
                      return context$3$0.stop();
                  }
                }, null, this);
              };
  
              if (typeof state === 'string' || state instanceof String) {
                state = { path: state };
              }
              cb = cb || emptyFunction;
              routes = this.routes;
              handlers = regeneratorRuntime.mark(function callee$2$0() {
                var _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, route, match, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, handler;
  
                return regeneratorRuntime.wrap(function callee$2$0$(context$3$0) {
                  while (1) switch (context$3$0.prev = context$3$0.next) {
                    case 0:
                      _iteratorNormalCompletion = true;
                      _didIteratorError = false;
                      _iteratorError = undefined;
                      context$3$0.prev = 3;
                      _iterator = routes[Symbol.iterator]();
  
                    case 5:
                      if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                        context$3$0.next = 38;
                        break;
                      }
  
                      route = _step.value;
                      match = route.match(state.path);
  
                      if (!match) {
                        context$3$0.next = 35;
                        break;
                      }
  
                      _iteratorNormalCompletion2 = true;
                      _didIteratorError2 = false;
                      _iteratorError2 = undefined;
                      context$3$0.prev = 12;
                      _iterator2 = match.route.handlers[Symbol.iterator]();
  
                    case 14:
                      if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                        context$3$0.next = 21;
                        break;
                      }
  
                      handler = _step2.value;
                      context$3$0.next = 18;
                      return [match, handler];
  
                    case 18:
                      _iteratorNormalCompletion2 = true;
                      context$3$0.next = 14;
                      break;
  
                    case 21:
                      context$3$0.next = 27;
                      break;
  
                    case 23:
                      context$3$0.prev = 23;
                      context$3$0.t0 = context$3$0['catch'](12);
                      _didIteratorError2 = true;
                      _iteratorError2 = context$3$0.t0;
  
                    case 27:
                      context$3$0.prev = 27;
                      context$3$0.prev = 28;
  
                      if (!_iteratorNormalCompletion2 && _iterator2['return']) {
                        _iterator2['return']();
                      }
  
                    case 30:
                      context$3$0.prev = 30;
  
                      if (!_didIteratorError2) {
                        context$3$0.next = 33;
                        break;
                      }
  
                      throw _iteratorError2;
  
                    case 33:
                      return context$3$0.finish(30);
  
                    case 34:
                      return context$3$0.finish(27);
  
                    case 35:
                      _iteratorNormalCompletion = true;
                      context$3$0.next = 5;
                      break;
  
                    case 38:
                      context$3$0.next = 44;
                      break;
  
                    case 40:
                      context$3$0.prev = 40;
                      context$3$0.t1 = context$3$0['catch'](3);
                      _didIteratorError = true;
                      _iteratorError = context$3$0.t1;
  
                    case 44:
                      context$3$0.prev = 44;
                      context$3$0.prev = 45;
  
                      if (!_iteratorNormalCompletion && _iterator['return']) {
                        _iterator['return']();
                      }
  
                    case 47:
                      context$3$0.prev = 47;
  
                      if (!_didIteratorError) {
                        context$3$0.next = 50;
                        break;
                      }
  
                      throw _iteratorError;
  
                    case 50:
                      return context$3$0.finish(47);
  
                    case 51:
                      return context$3$0.finish(44);
  
                    case 52:
                    case 'end':
                      return context$3$0.stop();
                  }
                }, callee$2$0, this, [[3, 40, 44, 52], [12, 23, 27, 35], [28,, 30, 34], [45,, 47, 51]]);
              })();
              value = undefined, result = undefined, done = false;
  
            case 6:
              if (done) {
                context$2$0.next = 16;
                break;
              }
  
              context$2$0.next = 9;
              return regeneratorRuntime.awrap(next());
  
            case 9:
              result = context$2$0.sent;
  
              if (!result) {
                context$2$0.next = 14;
                break;
              }
  
              state.statusCode = 200;
              cb(state, result);
              return context$2$0.abrupt('return');
  
            case 14:
              context$2$0.next = 6;
              break;
  
            case 16:
              if (!this.events.error) {
                context$2$0.next = 32;
                break;
              }
  
              context$2$0.prev = 17;
  
              state.statusCode = 404;
              context$2$0.next = 21;
              return regeneratorRuntime.awrap(this.events.error(state, new Error('Cannot found a route matching \'' + state.path + '\'.')));
  
            case 21:
              result = context$2$0.sent;
  
              cb(state, result);
              context$2$0.next = 32;
              break;
  
            case 25:
              context$2$0.prev = 25;
              context$2$0.t0 = context$2$0['catch'](17);
  
              state.statusCode = 500;
              context$2$0.next = 30;
              return regeneratorRuntime.awrap(this.events.error(state, context$2$0.t0));
  
            case 30:
              result = context$2$0.sent;
  
              cb(state, result);
  
            case 32:
            case 'end':
              return context$2$0.stop();
          }
        }, null, this, [[17, 25]]);
      }
    }]);
  
    return Router;
  })();
  
  exports['default'] = Router;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Router.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 64 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _this = this;
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  var _path = __webpack_require__(12);
  
  var _express = __webpack_require__(9);
  
  var _jade = __webpack_require__(132);
  
  var _jade2 = _interopRequireDefault(_jade);
  
  var _frontMatter = __webpack_require__(129);
  
  var _frontMatter2 = _interopRequireDefault(_frontMatter);
  
  var _utilsFs = __webpack_require__(119);
  
  var _utilsFs2 = _interopRequireDefault(_utilsFs);
  
  // A folder with Jade/Markdown/HTML content pages
  var CONTENT_DIR = (0, _path.join)(__dirname, './content');
  
  // Extract 'front matter' metadata and generate HTML
  var parseJade = function parseJade(path, jadeContent) {
  	var content = (0, _frontMatter2['default'])(jadeContent);
  	var html = _jade2['default'].render(content.body, null, '  ');
  	var page = Object.assign({ path: path, content: html }, content.attributes);
  	return page;
  };
  
  var router = new _express.Router();
  
  router.get('/', function callee$0$0(req, res, next) {
  	var path, fileName, source, content;
  	return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
  		while (1) switch (context$1$0.prev = context$1$0.next) {
  			case 0:
  				context$1$0.prev = 0;
  				path = req.query.path;
  
  				if (!(!path || path === 'undefined')) {
  					context$1$0.next = 5;
  					break;
  				}
  
  				res.status(400).send({ error: 'The \'path\' query parameter cannot be empty.' });
  				return context$1$0.abrupt('return');
  
  			case 5:
  				fileName = (0, _path.join)(CONTENT_DIR, (path === '/' ? '/index' : path) + '.jade');
  				context$1$0.next = 8;
  				return regeneratorRuntime.awrap(_utilsFs2['default'].exists(fileName));
  
  			case 8:
  				if (context$1$0.sent) {
  					context$1$0.next = 10;
  					break;
  				}
  
  				fileName = (0, _path.join)(CONTENT_DIR, path + '/index.jade');
  
  			case 10:
  				context$1$0.next = 12;
  				return regeneratorRuntime.awrap(_utilsFs2['default'].exists(fileName));
  
  			case 12:
  				if (context$1$0.sent) {
  					context$1$0.next = 16;
  					break;
  				}
  
  				res.status(404).send({ error: 'The page \'' + path + '\' is not found.' });
  				context$1$0.next = 21;
  				break;
  
  			case 16:
  				context$1$0.next = 18;
  				return regeneratorRuntime.awrap(_utilsFs2['default'].readFile(fileName, { encoding: 'utf8' }));
  
  			case 18:
  				source = context$1$0.sent;
  				content = parseJade(path, source);
  
  				res.status(200).send(content);
  
  			case 21:
  				context$1$0.next = 26;
  				break;
  
  			case 23:
  				context$1$0.prev = 23;
  				context$1$0.t0 = context$1$0['catch'](0);
  
  				next(context$1$0.t0);
  
  			case 26:
  			case 'end':
  				return context$1$0.stop();
  		}
  	}, null, _this, [[0, 23]]);
  });
  
  exports['default'] = router;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "content.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 65 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _AppCss = __webpack_require__(14);
  
  var _AppCss2 = _interopRequireDefault(_AppCss);
  
  var _decoratorsWithContext = __webpack_require__(116);
  
  var _decoratorsWithContext2 = _interopRequireDefault(_decoratorsWithContext);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _Header = __webpack_require__(79);
  
  var _Header2 = _interopRequireDefault(_Header);
  
  var _Feedback = __webpack_require__(77);
  
  var _Feedback2 = _interopRequireDefault(_Feedback);
  
  var _Footer = __webpack_require__(78);
  
  var _Footer2 = _interopRequireDefault(_Footer);
  
  var App = (function () {
  	function App() {
  		_classCallCheck(this, _App);
  	}
  
  	_createClass(App, [{
  		key: 'render',
  		value: function render() {
  			return !this.props.error ? _react2['default'].createElement(
  				'div',
  				null,
  				_react2['default'].createElement(_Header2['default'], null),
  				this.props.children
  			) : this.props.children;
  		}
  	}], [{
  		key: 'propTypes',
  		value: {
  			children: _react.PropTypes.element.isRequired,
  			error: _react.PropTypes.object
  		},
  		enumerable: true
  	}]);
  
  	var _App = App;
  	App = (0, _decoratorsWithStyles2['default'])(_AppCss2['default'])(App) || App;
  	App = (0, _decoratorsWithContext2['default'])(App) || App;
  	return App;
  })();
  
  exports['default'] = App;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "App.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 66 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _CashInInputCss = __webpack_require__(15);
  
  var _CashInInputCss2 = _interopRequireDefault(_CashInInputCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Cash In',
  	additionalTitle: ' Input',
  	breadcrumbsIcon: 'fa-pencil',
  	subtitle: ['Payment', 'Cash In', 'Cash In Input']
  };
  
  var dataContent2 = [{
  	mode: 'Input',
  	name: 'formCashIn',
  	title: 'Add Cash In',
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		function setValue(CashInID, CashInDate, Description, Amount, Note) {
  			$("#cashInID").val(CashInID);
  			if (CashInDate != "") {
  				CashInDate = CashInDate.split("-");
  				$('#cashInDate.datepicker').datepicker('update', new Date(CashInDate[0], CashInDate[1] - 1, CashInDate[2].substr(0, 2)));
  			} else {
  				$('#cashInDate.datepicker').val('');
  			}
  			$('#description').val(Description);
  			$('#amount').val(Amount);
  			$('#note').val(Note);
  			$('#errorDisplay').text("");
  			$(':input[required], select[required]').trigger("change");
  		}
  		if (typeof QueryString.id != 'undefined') {
  			$.ajax({
  				url: _coreApps2['default'].host + "/v1/cash-in/" + QueryString.id,
  				type: 'GET',
  				dataType: 'json',
  				cache: false,
  				success: function success(data) {
  					if (data.status == "OK") {
  						var data = data.data;
  						setValue(data.cashInId, data.cashInDate, data.description, data.amount, data.note);
  					}
  				}
  			});
  		}
  
  		$("#btnSave").click(function (e) {
  			e.preventDefault();
  			var cashInId = $('#cashInID').val(),
  			    error = $("#errorDisplay"),
  			    boolVal = true,
  			    data = {
  				cashInDate: moment($('#cashInDate').val()).format("YYYY-MM-DD"),
  				description: $('#description').val(),
  				amount: $('#amount').val(),
  				note: $('#note').val()
  			};
  
  			$.each($(':input[required], select[required]'), function (value, index) {
  				if ($(this).val() == "") {
  					boolVal = false;
  					return;
  				}
  			});
  
  			if (!boolVal) return;
  
  			if (cashInId == "") {
  				_apiRestCall2['default'].performRequest('/v1/cash-in', 'POST', data, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							$('html, body').animate({
  								scrollTop: $('body').offset().top
  							}, 'slow');
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Save Cash In");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							setValue("", "", "", "", "");
  						}
  					}
  				});
  			} else {
  				_apiRestCall2['default'].performRequest('/v1/cash-in/' + cashInId, 'PUT', data, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							window.location.assign('/cash-in');
  						}
  					}
  				});
  			}
  		});
  	},
  	items: [{
  		value: '',
  		name: 'cashInID',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Amount',
  		name: 'amount',
  		type: 'number',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Description',
  		name: 'description',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Cash In Date',
  		name: 'cashInDate',
  		type: 'datetime',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Note',
  		name: 'note',
  		row: 5,
  		type: 'area',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'Back',
  			type: 'button',
  			name: 'btnBack',
  			cls: 'btn-default',
  			event: function event() {
  				window.location.assign("/cash-in");
  			}
  		}]
  	}]
  }];
  
  var CashInInput = (function () {
  	function CashInInput() {
  		_classCallCheck(this, _CashInInput);
  	}
  
  	_createClass(CashInInput, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _CashInInput = CashInInput;
  	CashInInput = (0, _decoratorsWithStyles2['default'])(_CashInInputCss2['default'])(CashInInput) || CashInInput;
  	return CashInInput;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "CashInInput.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 67 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _CashInCss = __webpack_require__(16);
  
  var _CashInCss2 = _interopRequireDefault(_CashInCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Cash In',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['Payment', 'Cash In'],
  	toolbar: [{
  		type: "button",
  		name: "btnAdd",
  		text: "Add",
  		cls: "btn-primary",
  		event: function event(e) {
  			window.location.assign("/cash-in-input");
  		}
  	}]
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'User',
  	name: 'table-cash-in',
  	panel: false,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-cash-in").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/cash-in",
  				"columns": [{ "title": "CashIn ID", "className": "cashInId", "data": "cashInId", "visible": false }, { "title": "cashIn Date", "className": "cashInDate", "data": "cashInDate",
  					"render": function render(data, type, full, meta) {
  						return moment(data).format(_coreApps2['default'].dateFormat);
  					}
  				}, { "title": "Description", "className": "description", "data": "description" }, { "title": "Amount", "className": "amount", "data": "amount", "type": "num-fmt",
  					"render": function render(data, type, full, meta) {
  						return FormatToCurrency(data);
  					}
  				}, { "title": "Note", "className": "note", "data": "note" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "cashInId",
  					"render": function render(data, type, full, meta) {
  						return "<a class='btn btn-default iEdit' href='cash-in-input?id=" + data + "'>Edit</a> <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-cash-in").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var cashInId = $(this).attr('data-attr');
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/cash-in/' + cashInId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								if (typeof data.error != 'undefined') {
  									$("#alert #alert-title").html("Error");
  									$("#alert #alert-content").html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  									$("#alert").addClass('alert-danger').show();
  									$("#alert").fadeOut(2000);
  								} else {
  									$("#alert #alert-title").html("Success");
  									$("#alert #alert-content").html("Delete Cash In");
  									$("#alert").addClass('alert-success').show();
  									$("#alert").fadeOut(2000);
  									datatable.destroy();
  									loadTable();
  								}
  							});
  						});
  					});
  				}
  			});
  		}
  
  		loadTable();
  	}
  }];
  
  var CashIn = (function () {
  	function CashIn() {
  		_classCallCheck(this, _CashIn);
  	}
  
  	_createClass(CashIn, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _CashIn = CashIn;
  	CashIn = (0, _decoratorsWithStyles2['default'])(_CashInCss2['default'])(CashIn) || CashIn;
  	return CashIn;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "CashIn.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 68 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _CashOutInputCss = __webpack_require__(17);
  
  var _CashOutInputCss2 = _interopRequireDefault(_CashOutInputCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Cash Out',
  	additionalTitle: ' Input',
  	breadcrumbsIcon: 'fa-pencil',
  	subtitle: ['Payment', 'Cash Out', 'Cash out Input']
  };
  
  var dataContent2 = [{
  	mode: 'input',
  	name: 'formCashOut',
  	title: 'Add Cash Out',
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		function setValue(CashOutID, CostCategoryID, CashOutDate, Description, Amount, Note) {
  			$('#cashOutID').val(CashOutID);
  			if (CostCategoryID == "") $('select#costCategory').prop('selectedIndex', 0);else $('select#costCategory').val(CostCategoryID);
  			if (CashOutDate != "") {
  				CashOutDate = CashOutDate.split("-");
  				$('#cashOutDate.datepicker').datepicker('update', new Date(CashOutDate[0], CashOutDate[1] - 1, CashOutDate[2].substr(0, 2)));
  			} else {
  				$('#cashOutDate.datepicker').val('');
  			}
  			$('#description').val(Description);
  			$('#amount').val(Amount);
  			$('#note').val(Note);
  			$('#errorDisplay').text("");
  			$(':input[required], select[required]').trigger("change");
  		}
  		$.ajax({
  			url: _coreApps2['default'].host + "/v1/cost-category",
  			type: 'GET',
  			dataType: 'json',
  			cache: false,
  			success: function success(data) {
  				$('select#costCategory').empty();
  				if (data.status == "OK") {
  					$.map(data.data || [], function (val, i) {
  						$('select#costCategory').append('<option value=' + val.costCategoryId + '>' + val.costCategoryName + '</option>');
  					});
  				}
  			},
  			async: false
  		});
  
  		if (typeof QueryString.id != 'undefined') {
  			$.ajax({
  				url: _coreApps2['default'].host + '/v1/cash-out/' + QueryString.id,
  				type: 'GET',
  				dataType: 'json',
  				cache: false,
  				success: function success(data) {
  					if (data.status == "OK") {
  						var data = data.data;
  						setValue(data.cashOutId, data.costCategory != null ? data.costCategory.costCategoryId : null, data.cashOutDate, data.description, data.amount, data.note);
  					}
  				}
  			});
  		}
  
  		$("#btnSave").click(function (e) {
  			e.preventDefault();
  			var cashOutId = $('#cashOutID').val(),
  			    error = $("#errorDisplay"),
  			    boolVal = true,
  			    data = {
  				costCategoryId: $("#costCategory").val(),
  				cashOutDate: moment($('#cashOutDate').val()).format("YYYY-MM-DD"),
  				description: $('#description').val(),
  				amount: $('#amount').val(),
  				note: $('#note').val()
  			};
  
  			$.each($(':input[required], select[required]'), function (value, index) {
  				if ($(this).val() == "") {
  					boolVal = false;
  					return;
  				}
  			});
  
  			if (!boolVal) return;
  
  			if (cashOutId == "") {
  				_apiRestCall2['default'].performRequest('/v1/cash-out', 'POST', data, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							$('html, body').animate({
  								scrollTop: $('body').offset().top
  							}, 'slow');
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Save Cash Out");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							setValue("", "", "", "", "", "");
  						}
  					}
  				});
  			} else {
  				_apiRestCall2['default'].performRequest('/v1/cash-out/' + cashOutId, 'PUT', data, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							window.location.assign('/cash-out');
  						}
  					}
  				});
  			}
  		});
  	},
  	items: [{
  		value: '',
  		name: 'cashOutID',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Amount',
  		name: 'amount',
  		type: 'number',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Cost Category',
  		type: 'select',
  		name: 'costCategory',
  		multiple: false,
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Description',
  		name: 'description',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Cash Out Date',
  		name: 'cashOutDate',
  		type: 'datetime',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Note',
  		name: 'note',
  		row: 5,
  		type: 'area',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'Back',
  			type: 'button',
  			name: 'btnBack',
  			cls: 'btn-default',
  			event: function event() {
  				window.location.assign("/cash-out");
  			}
  		}]
  	}]
  }];
  
  var CashOutInput = (function () {
  	function CashOutInput() {
  		_classCallCheck(this, _CashOutInput);
  	}
  
  	_createClass(CashOutInput, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _CashOutInput = CashOutInput;
  	CashOutInput = (0, _decoratorsWithStyles2['default'])(_CashOutInputCss2['default'])(CashOutInput) || CashOutInput;
  	return CashOutInput;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "CashOutInput.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 69 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _CashOutCss = __webpack_require__(18);
  
  var _CashOutCss2 = _interopRequireDefault(_CashOutCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Cash Out',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['Payment', 'Cash Out'],
  	toolbar: [{
  		type: "button",
  		name: "btnAdd",
  		text: "Add",
  		cls: "btn-primary",
  		event: function event(e) {
  			window.location.assign("/cash-out-input");
  		}
  	}]
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'User',
  	name: 'table-cash-out',
  	panel: false,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-cash-out").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/cash-out",
  				"columns": [{ "title": "Cash Out ID", "className": "cashOutId", "data": "cashOutId", "visible": false }, { "title": "Cost Category", "className": "costCategoryId", "data": "costCategory",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.costCategoryName : "";
  					}
  				}, { "title": "Cash Out Date", "className": "cashOutDate", "data": "cashOutDate",
  					"render": function render(data, type, full, meta) {
  						return moment(data).format(_coreApps2['default'].dateFormat);
  					}
  				}, { "title": "Description", "className": "description", "data": "description" }, { "title": "Amount", "className": "amount", "data": "amount", "type": "num-fmt",
  					"render": function render(data, type, full, meta) {
  						return FormatToCurrency(data);
  					}
  				}, { "title": "Note", "className": "note", "data": "note" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "cashOutId",
  					"render": function render(data, type, full, meta) {
  						return "<a class='btn btn-default iEdit' href='cash-out-input?id=" + data + "'>Edit</a> <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-cash-out").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var cashOutId = $(this).attr('data-attr');
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/cash-out/' + cashOutId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								if (typeof data.error != 'undefined') {
  									$("#alert #alert-title").html("Error");
  									$("#alert #alert-content").html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  									$("#alert").addClass('alert-danger').show();
  									$("#alert").fadeOut(2000);
  								} else {
  									$("#alert #alert-title").html("Success");
  									$("#alert #alert-content").html("Delete cash Out");
  									$("#alert").addClass('alert-success').show();
  									$("#alert").fadeOut(2000);
  									datatable.destroy();
  									loadTable();
  								}
  							});
  						});
  					});
  				}
  			});
  		}
  
  		loadTable();
  	}
  }];
  
  var CashOut = (function () {
  	function CashOut() {
  		_classCallCheck(this, _CashOut);
  	}
  
  	_createClass(CashOut, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _CashOut = CashOut;
  	CashOut = (0, _decoratorsWithStyles2['default'])(_CashOutCss2['default'])(CashOut) || CashOut;
  	return CashOut;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "CashOut.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 70 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _ContentPageCss = __webpack_require__(19);
  
  var _ContentPageCss2 = _interopRequireDefault(_ContentPageCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var ContentPage = (function () {
  	function ContentPage() {
  		_classCallCheck(this, _ContentPage);
  	}
  
  	_createClass(ContentPage, [{
  		key: 'render',
  		value: function render() {
  			this.context.onSetTitle(this.props.title);
  			return _react2['default'].createElement(
  				'div',
  				{ className: 'ContentPage' },
  				_react2['default'].createElement(
  					'div',
  					{ className: 'ContentPage-container' },
  					this.props.path === '/' ? null : _react2['default'].createElement(
  						'h1',
  						null,
  						this.props.title
  					),
  					_react2['default'].createElement('div', { dangerouslySetInnerHTML: { __html: this.props.content || '' } })
  				)
  			);
  		}
  	}], [{
  		key: 'propTypes',
  		value: {
  			path: _react.PropTypes.string.isRequired,
  			content: _react.PropTypes.string.isRequired,
  			title: _react.PropTypes.string
  		},
  		enumerable: true
  	}, {
  		key: 'contextTypes',
  		value: {
  			onSetTitle: _react.PropTypes.func.isRequired
  		},
  		enumerable: true
  	}]);
  
  	var _ContentPage = ContentPage;
  	ContentPage = (0, _decoratorsWithStyles2['default'])(_ContentPageCss2['default'])(ContentPage) || ContentPage;
  	return ContentPage;
  })();
  
  exports['default'] = ContentPage;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "ContentPage.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 71 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _CostCategoryCss = __webpack_require__(20);
  
  var _CostCategoryCss2 = _interopRequireDefault(_CostCategoryCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Cost Category',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['Payment', 'Cost Category']
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'cost-category',
  	name: 'table-cost-category',
  	panel: false,
  	loadAll: true,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	bodyAttr: '',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-cost-category").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/cost-category",
  				"columns": [{ "title": "Cost Category ID", "className": 'costCategoryId', "data": "costCategoryId", "visible": false }, { "title": "Cost Category Name", "className": 'costCategoryName', "data": "costCategoryName" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "costCategoryId",
  					"render": function render(data, type, full, meta) {
  						return "<a class='btn btn-default iEdit' data-attr=" + data + ">Edit</a> <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					event();
  					$("#table-cost-category").closest('.table-responsive').css('overflow', 'hidden');
  				}
  			});
  		}
  
  		function reset() {
  			$("#costCategoryID").val("");
  			$("#costCategoryName").val("");
  			$('#errorDisplay').text("");
  			$(':input[required], select[required]').trigger("change");
  		}
  
  		function event() {
  			$("#btnNew").off('click').click(function (e) {
  				reset();
  				$(':input[required], select[required]').trigger("change");
  			});
  			$("#btnSave").off('click').click(function (e) {
  				e.preventDefault();
  				var costCategoryIDParam = $('#costCategoryID').val(),
  				    error = $('#errorDisplay'),
  				    boolVal = true,
  				    data = { costCategoryName: $('#costCategoryName').val() };
  
  				$.each($(':input[required], select[required]'), function (value, index) {
  					if ($(this).val() == "") {
  						boolVal = false;
  						return;
  					}
  				});
  
  				if (!boolVal) return;
  
  				if (costCategoryIDParam == "") {
  					_apiRestCall2['default'].performRequest('/v1/cost-category', 'POST', data, function (data) {
  						if (typeof data.error != 'undefined') {
  							error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  							error.css('display', 'block');
  						} else {
  							if (data.status == "ERROR") {
  								error.html(data.message);
  								error.css('display', 'block');
  							} else {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Save Cost Category");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  								reset();
  							}
  						}
  					});
  				} else {
  					_apiRestCall2['default'].performRequest('/v1/cost-category/' + costCategoryIDParam, 'PUT', data, function (data) {
  						if (typeof data.error != 'undefined') {
  							error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  							error.css('display', 'block');
  						} else {
  							if (data.status == "ERROR") {
  								error.html(data.message);
  								error.css('display', 'block');
  							} else {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Update cost-category");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  								reset();
  							}
  						}
  					});
  				}
  			});
  
  			$(".iEdit").off('click').click(function (e) {
  				e.preventDefault();
  				var costCategoryIDEdit = $(this).attr('data-attr'),
  				    costCategoryNameEdit = $(this).closest('tr').find('td.costCategoryName').text();
  				$('html, body').animate({
  					scrollTop: $('form#formcost-category').offset().top
  				}, 'slow');
  				$('#costCategoryID').val(costCategoryIDEdit);
  				$('#costCategoryName').val(costCategoryNameEdit);
  				$(':input[required], select[required]').trigger("change");
  			});
  
  			$(".iDelete").off('click').click(function (e) {
  				e.preventDefault();
  				var costCategoryID = $(this).attr('data-attr'),
  				    costCategoryName = $(this).closest('tr').find('td.costCategoryName').text();
  				$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> cost-category ' + costCategoryName + '?');
  				$("#PopUpConfirm").modal("show");
  				$("#PopUpConfirm #btnYes").click(function (e) {
  					_apiRestCall2['default'].performRequest('/v1/cost-category/' + costCategoryID, 'DELETE', {}, function (data) {
  						$('html, body').animate({
  							scrollTop: $('body').offset().top
  						}, 'slow');
  						$("#PopUpConfirm").modal("hide");
  						if (typeof data.error != 'undefined') {
  							$("#alert #alert-title").html("Error");
  							$("#alert #alert-content").html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  							$("#alert").addClass('alert-danger').show();
  							$("#alert").fadeOut(2000);
  						} else {
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Delete cost-category");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							datatable.destroy();
  							loadTable();
  							reset();
  						}
  					});
  				});
  			});
  		}
  
  		loadTable();
  	}
  }, {
  	mode: 'Input',
  	name: 'formcost-category',
  	title: 'Add cost-category',
  	loadAll: true,
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	items: [{
  		value: '',
  		name: 'costCategoryID',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Cost Category Name',
  		name: 'costCategoryName',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'New',
  			type: 'reset',
  			name: 'btnNew',
  			cls: 'btn-default'
  		}]
  	}]
  }];
  
  var CostCategory = (function () {
  	function CostCategory() {
  		_classCallCheck(this, _CostCategory);
  	}
  
  	_createClass(CostCategory, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _CostCategory = CostCategory;
  	CostCategory = (0, _decoratorsWithStyles2['default'])(_CostCategoryCss2['default'])(CostCategory) || CostCategory;
  	return CostCategory;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "CostCategory.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 72 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _CustomerInputCss = __webpack_require__(21);
  
  var _CustomerInputCss2 = _interopRequireDefault(_CustomerInputCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Customer',
  	additionalTitle: ' Input',
  	breadcrumbsIcon: 'fa-pencil',
  	subtitle: ['User', 'Customer', 'Customer Input']
  };
  var dataContent2 = [{
  	mode: 'Input',
  	name: 'formCustomer',
  	title: 'Add Customer',
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		function setValue(CustomerID, CustomerName, CustomerGender, CustomerBirthdate, CustomerAddress, PhoneNumber) {
  			$('#customerID').val(CustomerID);
  			$('#customerName').val(CustomerName);
  			$('#customerGender[value=' + CustomerGender + ']').prop('checked', true);
  			if (CustomerBirthdate != "") {
  				CustomerBirthdate = CustomerBirthdate.split("-");
  				$("#customerBirthdate.datepicker").datepicker('update', new Date(CustomerBirthdate[0], CustomerBirthdate[1] - 1, CustomerBirthdate[2].substr(0, 2)));
  			} else {
  				$("#customerBirthdate.datepicker").val('');
  			}
  			$('#customerAddress').val(CustomerAddress);
  			$('#phoneNumber').val(PhoneNumber);
  			$('#errorDisplay').text("");
  			$(':input[required], select[required]').trigger("change");
  		}
  		if (typeof QueryString.id != 'undefined') {
  			$.ajax({
  				url: _coreApps2['default'].host + "/v1/customer/" + QueryString.id,
  				type: 'GET',
  				dataType: 'json',
  				cache: false,
  				success: function success(data) {
  					if (data.status == "OK") {
  						var data = data.data;
  						setValue(data.customerId, data.customerName, data.customerGender, data.customerBirthdate, data.customerAddress, data.phoneNumber);
  					}
  				}
  			});
  		}
  
  		$("#btnSave").click(function (e) {
  			e.preventDefault();
  			var customerId = $("#customerID").val(),
  			    error = $('#errorDisplay'),
  			    boolVal = true,
  			    data = {
  				customerName: $('#customerName').val(),
  				customerGender: $('#customerGender:checked').val(),
  				customerBirthdate: moment($('#customerBirthdate').val()).format("YYYY-MM-DD"),
  				customerAddress: $('#customerAddress').val(),
  				phoneNumber: $('#phoneNumber').val()
  			};
  			$.each($(':input[required], select[required]'), function (value, index) {
  				if ($(this).val() == "") {
  					boolVal = false;
  					return;
  				}
  			});
  
  			if (!boolVal) return;
  
  			if (customerId == "") {
  				_apiRestCall2['default'].performRequest('/v1/customer', 'POST', data, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							$('html, body').animate({
  								scrollTop: $('body').offset().top
  							}, 'slow');
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Save Customer");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							setValue("", "", "MALE", "", "", "");
  						}
  					}
  				});
  			} else {
  				_apiRestCall2['default'].performRequest('/v1/customer/' + customerId, 'PUT', data, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							window.location.assign("/customer");
  						}
  					}
  				});
  			}
  		});
  	},
  	items: [{
  		value: '',
  		name: 'customerID',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Customer Name',
  		name: 'customerName',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Gender',
  		type: 'radio',
  		cols: [2, 10],
  		data: [{
  			name: 'customerGender',
  			text: 'MALE',
  			value: 'MALE'
  		}, {
  			name: 'customerGender',
  			text: 'FEMALE',
  			value: 'FEMALE'
  		}]
  	}, {
  		text: 'Birth Date',
  		name: 'customerBirthdate',
  		type: 'datetime',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Address',
  		name: 'customerAddress',
  		rows: 5,
  		required: 'required',
  		type: 'area',
  		cols: [2, 10]
  	}, {
  		text: 'Phone Number',
  		name: 'phoneNumber',
  		type: 'text',
  		required: 'required',
  		placeholder: '(021) 800 800',
  		cols: [2, 10]
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'Back',
  			type: 'button',
  			name: 'btnBack',
  			cls: 'btn-default',
  			event: function event() {
  				window.location.assign("/customer");
  			}
  		}]
  	}]
  }];
  
  var Customer = (function () {
  	function Customer() {
  		_classCallCheck(this, _Customer);
  	}
  
  	_createClass(Customer, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _Customer = Customer;
  	Customer = (0, _decoratorsWithStyles2['default'])(_CustomerInputCss2['default'])(Customer) || Customer;
  	return Customer;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "CustomerInput.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 73 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _CustomerCss = __webpack_require__(22);
  
  var _CustomerCss2 = _interopRequireDefault(_CustomerCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Customer',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['User', 'Customer'],
  	toolbar: [{
  		type: "button",
  		name: "btnAdd",
  		text: "Add",
  		cls: "btn-primary",
  		event: function event(e) {
  			window.location.assign("/customer-input");
  		}
  	}]
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'User',
  	name: 'table-customer',
  	panel: false,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	bodyAttr: '',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-customer").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/customer",
  				"columns": [{ "title": "Customer ID", "className": "customerId", "data": "customerId", "visible": false }, { "title": "Customer Name", "className": "customerName", "data": "customerName" }, { "title": "Gender", "className": "customerGender", "data": "customerGender" }, { "title": "Birth Date", "className": "customerBirthdate", "data": "customerBirthdate",
  					"render": function render(data, type, full, meta) {
  						return moment(data).format(_coreApps2['default'].dateFormat);
  					}
  				}, { "title": "Address", "className": "customerAddress", "data": "customerAddress" }, { "title": "Phone Number", "className": "phoneNumber", "data": "phoneNumber" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "customerId",
  					"render": function render(data, type, full, meta) {
  						return "<a class='btn btn-default iEdit' href='customer-input?id=" + data + "'>Edit</a> <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-customer").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var customerId = $(this).attr('data-attr'),
  						    customerName = $(this).closest('tr').find('td.customerName').text();
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> Customer ' + customerName + '?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/customer/' + customerId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								if (typeof data.error != 'undefined') {
  									$("#alert #alert-title").html("Error");
  									$("#alert #alert-content").html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  									$("#alert").addClass('alert-danger').show();
  									$("#alert").fadeOut(2000);
  								} else {
  									$("#alert #alert-title").html("Success");
  									$("#alert #alert-content").html("Delete Customer");
  									$("#alert").addClass('alert-success').show();
  									$("#alert").fadeOut(2000);
  									datatable.destroy();
  									loadTable();
  								}
  							});
  						});
  					});
  				}
  			});
  		}
  
  		loadTable();
  	}
  }];
  
  var Customer = (function () {
  	function Customer() {
  		_classCallCheck(this, _Customer);
  	}
  
  	_createClass(Customer, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _Customer = Customer;
  	Customer = (0, _decoratorsWithStyles2['default'])(_CustomerCss2['default'])(Customer) || Customer;
  	return Customer;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Customer.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 74 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _DashboardCss = __webpack_require__(23);
  
  var _DashboardCss2 = _interopRequireDefault(_DashboardCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var dataPageHeading = {
  	title: 'Dashboard',
  	subtitle: ['Dashboard'],
  	additionalTitle: 'Statistics Overview',
  	breadcrumbsIcon: 'fa-dashboard'
  };
  var dataContent1 = [{
  	panel: 'panel-primary',
  	type: 'fa-comments',
  	text: '26',
  	desc: 'New Comments!',
  	url: '#'
  }, {
  	panel: 'panel-green',
  	type: 'fa-tasks',
  	text: '12',
  	desc: 'New Tasks!',
  	url: '#'
  }, {
  	panel: 'panel-yellow',
  	type: 'fa-shopping-cart',
  	text: '124',
  	desc: 'New Orders!',
  	url: '#'
  }, {
  	panel: 'panel-red',
  	type: 'fa-support',
  	text: '13',
  	desc: 'Support Tickets!',
  	url: '#'
  }];
  var dataContent2 = [{
  	type: 'AreaChart',
  	loadAll: true,
  	cls: 'col-lg-8',
  	componentDidMount: function componentDidMount() {
  		Morris.Area({
  			element: 'morris-area-chart',
  			data: [{
  				period: '2010 Q1',
  				iphone: 2666,
  				ipad: null,
  				itouch: 2647
  			}, {
  				period: '2010 Q2',
  				iphone: 2778,
  				ipad: 2294,
  				itouch: 2441
  			}, {
  				period: '2010 Q3',
  				iphone: 4912,
  				ipad: 1969,
  				itouch: 2501
  			}, {
  				period: '2010 Q4',
  				iphone: 3767,
  				ipad: 3597,
  				itouch: 5689
  			}, {
  				period: '2011 Q1',
  				iphone: 6810,
  				ipad: 1914,
  				itouch: 2293
  			}, {
  				period: '2011 Q2',
  				iphone: 5670,
  				ipad: 4293,
  				itouch: 1881
  			}, {
  				period: '2011 Q3',
  				iphone: 4820,
  				ipad: 3795,
  				itouch: 1588
  			}, {
  				period: '2011 Q4',
  				iphone: 15073,
  				ipad: 5967,
  				itouch: 5175
  			}, {
  				period: '2012 Q1',
  				iphone: 10687,
  				ipad: 4460,
  				itouch: 2028
  			}, {
  				period: '2012 Q2',
  				iphone: 8432,
  				ipad: 5713,
  				itouch: 1791
  			}],
  			xkey: 'period',
  			ykeys: ['iphone', 'ipad', 'itouch'],
  			labels: ['iPhone', 'iPad', 'iPod Touch'],
  			pointSize: 2,
  			hideHover: 'auto',
  			resize: true
  		});
  	},
  	menu: [{
  		text: 'Action',
  		url: '#',
  		separated: false
  	}, {
  		text: 'Another action',
  		url: '#',
  		separated: false
  	}, {
  		text: 'Something else here',
  		url: '#',
  		separated: false
  	}, {
  		text: 'Separated link',
  		url: '#',
  		separated: true
  	}]
  }, {
  	mode: 'Notification',
  	loadAll: true,
  	title: 'Notifications Panel',
  	cls: 'col-lg-4',
  	iconHeader: 'fa-bell',
  	items: [{
  		text: 'New Comment',
  		time: '4 minutes ago',
  		icon: 'fa-comment',
  		url: '#'
  	}, {
  		text: '3 New Followers',
  		time: '12 minutes ago',
  		icon: 'fa-twitter',
  		url: '#'
  	}, {
  		text: 'Message Sent',
  		time: '27 minutes ago',
  		icon: 'fa-envelope',
  		url: '#'
  	}, {
  		text: 'New Task',
  		time: '43 minutes ago',
  		icon: 'fa-tasks',
  		url: '#'
  	}, {
  		text: 'Server Rebooted',
  		time: '11:32 AM',
  		icon: 'fa-upload',
  		url: '#'
  	}, {
  		text: 'Server Crashed!',
  		time: '11:13 AM',
  		icon: 'fa-bolt',
  		url: '#'
  	}, {
  		text: 'Server Not Responding',
  		time: '10:57 AM',
  		icon: 'fa-warning',
  		url: '#'
  	}, {
  		text: 'New Order Placed',
  		time: '9:49 AM',
  		icon: 'fa-shopping-cart',
  		url: '#'
  	}]
  }, {
  	type: 'BarChart',
  	loadAll: true,
  	cls: 'col-lg-8',
  	table: [{
  		typeTable: 'table-responsive',
  		header: ['#', 'Date', 'Time', 'Amount'],
  		body: [['3326', '10/21/2013', '3:29 PM', '$321.33'], ['3325', '10/21/2013', '3:20 PM', '$234.34'], ['3324', '10/21/2013', '3:03 PM', '$724.17'], ['3323', '10/21/2013', '3:00 PM', '$23.71'], ['3322', '10/21/2013', '2:49 PM', '$8345.23'], ['3321', '10/21/2013', '2:23 PM', '$245.12'], ['3320', '10/21/2013', '2:15 PM', '$5663.54'], ['3319', '10/21/2013', '2:13 PM', '$943.45']]
  	}],
  	componentDidMount: function componentDidMount() {
  		Morris.Bar({
  			element: 'morris-bar-chart',
  			data: [{
  				y: '2006',
  				a: 100,
  				b: 90
  			}, {
  				y: '2007',
  				a: 75,
  				b: 65
  			}, {
  				y: '2008',
  				a: 50,
  				b: 40
  			}, {
  				y: '2009',
  				a: 75,
  				b: 65
  			}, {
  				y: '2010',
  				a: 50,
  				b: 40
  			}, {
  				y: '2011',
  				a: 75,
  				b: 65
  			}, {
  				y: '2012',
  				a: 100,
  				b: 90
  			}],
  			xkey: 'y',
  			ykeys: ['a', 'b'],
  			labels: ['Series A', 'Series B'],
  			hideHover: 'auto',
  			resize: true
  		});
  	},
  	menu: [{
  		text: 'Action',
  		url: '#',
  		separated: false
  	}, {
  		text: 'Another action',
  		url: '#',
  		separated: false
  	}, {
  		text: 'Something else here',
  		url: '#',
  		separated: false
  	}, {
  		text: 'Separated link',
  		url: '#',
  		separated: true
  	}]
  }, {
  	type: 'DonutChart',
  	loadAll: true,
  	cls: 'col-lg-4',
  	componentDidMount: function componentDidMount() {
  		Morris.Donut({
  			element: 'morris-donut-chart',
  			data: [{
  				label: "Download Sales",
  				value: 12
  			}, {
  				label: "In-Store Sales",
  				value: 30
  			}, {
  				label: "Mail-Order Sales",
  				value: 20
  			}],
  			resize: true
  		});
  	},
  	menu: [{
  		text: 'Action',
  		url: '#',
  		separated: false
  	}, {
  		text: 'Another action',
  		url: '#',
  		separated: false
  	}, {
  		text: 'Something else here',
  		url: '#',
  		separated: false
  	}, {
  		text: 'Separated link',
  		url: '#',
  		separated: true
  	}]
  }, {
  	mode: 'Timeline',
  	loadAll: true,
  	title: 'Responsive Timeline',
  	cls: 'col-lg-8',
  	iconHeader: 'fa-clock-o',
  	items: [{
  		header: 'Lorem ipsum dolor',
  		time: '11 hours ago via Twitter',
  		body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero laboriosam dolor perspiciatis omnis exercitationem. Beatae, officia pariatur? Est cum veniam excepturi. Maiores praesentium, porro voluptas suscipit facere rem dicta, debitis.',
  		icon: 'timeline-badge',
  		typeTimeline: 'timeline-badge',
  		typeAlert: '',
  		iconBadge: 'fa-check',
  		inverted: false // left timeline (Default false)
  	}, {
  		header: 'Lorem ipsum dolor',
  		time: '11 hours ago via Twitter',
  		body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem dolorem quibusdam, tenetur commodi provident cumque magni voluptatem libero, quis rerum. Fugiat esse debitis optio, tempore. Animi officiis alias, officia repellendus. @ Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium maiores odit qui est tempora eos, nostrum provident explicabo dignissimos debitis vel! Adipisci eius voluptates, ad aut recusandae minus eaque facere.',
  		icon: 'timeline-badge',
  		typeTimeline: 'timeline-badge',
  		typeAlert: 'warning',
  		iconBadge: 'fa-credit-card',
  		inverted: true // right timeline (Default false)
  	}, {
  		header: 'Lorem ipsum dolor',
  		time: '11 hours ago via Twitter',
  		body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus numquam facilis enim eaque, tenetur nam id qui vel velit similique nihil iure molestias aliquam, voluptatem totam quaerat, magni commodi quisquam.',
  		icon: 'timeline-badge',
  		typeTimeline: 'timeline-badge',
  		typeAlert: 'danger',
  		iconBadge: 'fa-bomb',
  		inverted: false
  	}, {
  		header: 'Lorem ipsum dolor',
  		time: '11 hours ago via Twitter',
  		body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates est quaerat asperiores sapiente, eligendi, nihil. Itaque quos, alias sapiente rerum quas odit! Aperiam officiis quidem delectus libero, omnis ut debitis!',
  		icon: 'timeline-panel',
  		typeTimeline: '',
  		typeAlert: '',
  		iconBadge: '',
  		inverted: true
  	}, {
  		header: 'Lorem ipsum dolor',
  		time: '11 hours ago via Twitter',
  		body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis minus modi quam ipsum alias at est molestiae excepturi delectus nesciunt, quibusdam debitis amet, beatae consequuntur impedit nulla qui! Laborum, atque.',
  		icon: 'timeline-panel',
  		typeTimeline: 'timeline-badge',
  		typeAlert: 'info',
  		iconBadge: 'fa-save',
  		inverted: false
  	}, {
  		header: 'Lorem ipsum dolor',
  		time: '11 hours ago via Twitter',
  		body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt obcaecati, quaerat tempore officia voluptas debitis consectetur culpa amet, accusamus dolorum fugiat, animi dicta aperiam, enim incidunt quisquam maxime neque eaque.',
  		icon: 'timeline-panel',
  		typeTimeline: 'timeline-badge',
  		typeAlert: 'success',
  		iconBadge: 'fa-graduation-cap',
  		inverted: true
  	}]
  }, {
  	mode: 'Table',
  	loadAll: true,
  	panel: true,
  	cls: 'col-lg-4',
  	title: 'Table',
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	header: ['#', 'Date', 'Time', 'Amount'],
  	body: [['3326', '10/21/2013', '3:29 PM', '$321.33'], ['3325', '10/21/2013', '3:20 PM', '$234.34'], ['3324', '10/21/2013', '3:03 PM', '$724.17'], ['3323', '10/21/2013', '3:00 PM', '$23.71'], ['3322', '10/21/2013', '2:49 PM', '$8345.23'], ['3321', '10/21/2013', '2:23 PM', '$245.12'], ['3320', '10/21/2013', '2:15 PM', '$5663.54'], ['3319', '10/21/2013', '2:13 PM', '$943.45']]
  }];
  
  var Dashboard = (function () {
  	function Dashboard() {
  		_classCallCheck(this, _Dashboard);
  	}
  
  	_createClass(Dashboard, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _Dashboard = Dashboard;
  	Dashboard = (0, _decoratorsWithStyles2['default'])(_DashboardCss2['default'])(Dashboard) || Dashboard;
  	return Dashboard;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent1: dataContent1,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Dashboard.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 75 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _DepartmentCss = __webpack_require__(24);
  
  var _DepartmentCss2 = _interopRequireDefault(_DepartmentCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Department',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['Access User', 'Department']
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'Department',
  	name: 'table-department',
  	panel: false,
  	loadAll: true,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	bodyAttr: '',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-department").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/department",
  				"columns": [{ "title": "Department ID", "className": 'departmentId', "data": "departmentId", "visible": false }, { "title": "Department Code", "className": 'departmentCode', "data": "departmentCode" }, { "title": "Department Name", "className": 'departmentName', "data": "departmentName" }, { "title": "Location", "className": 'location', "data": "location" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"render": function render(data, type, full, meta) {
  						return "<a class='btn btn-default iEdit' data-attr=" + full.departmentId + ">Edit</a> <a class='btn btn-danger iDelete' data-attr=" + full.departmentId + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					event();
  					$("#table-department").closest('.table-responsive').css('overflow', 'hidden');
  				}
  			});
  		}
  
  		function reset() {
  			$("#departmentID").val("");
  			$("#departmentCode").val("");
  			$("#departmentName").val("");
  			$("#location").val("");
  			$('#errorDisplay').text("");
  			$(':input[required], select[required]').trigger("change");
  		}
  
  		function event() {
  			$("#btnNew").off('click').click(function (e) {
  				reset();
  				$(':input[required], select[required]').trigger("change");
  			});
  
  			$("#btnSave").off('click').click(function (e) {
  				e.preventDefault();
  				var departmentId = $('#departmentID').val(),
  				    error = $('#errorDisplay'),
  				    boolVal = true,
  				    data = {
  					departmentCode: $('#departmentCode').val(),
  					departmentName: $('#departmentName').val(),
  					location: $('#location').val()
  				};
  
  				$.each($(':input[required], select[required]'), function (value, index) {
  					if ($(this).val() == "") {
  						boolVal = false;
  						return;
  					}
  				});
  
  				if (!boolVal) return;
  
  				if (departmentId == "") {
  					_apiRestCall2['default'].performRequest('/v1/department', 'POST', data, function (data) {
  						if (typeof data.error != 'undefined') {
  							error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  							error.css('display', 'block');
  						} else {
  							if (data.status == "ERROR") {
  								error.html(data.message);
  								error.css('display', 'block');
  							} else {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Save Department");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  								reset();
  							}
  						}
  					});
  				} else {
  					_apiRestCall2['default'].performRequest('/v1/department/' + departmentId, 'PUT', data, function (data) {
  						if (typeof data.error != 'undefined') {
  							error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  							error.css('display', 'block');
  						} else {
  							if (data.status == "ERROR") {
  								error.html(data.message);
  								error.css('display', 'block');
  							} else {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Update Department");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  								reset();
  							}
  						}
  					});
  				}
  			});
  
  			$(".iEdit").off('click').click(function (e) {
  				e.preventDefault();
  				var departmentIdEdit = $(this).attr('data-attr'),
  				    departmentCodeEdit = $(this).closest('tr').find('td.departmentCode').text(),
  				    departmentNameEdit = $(this).closest('tr').find('td.departmentName').text(),
  				    localtionEdit = $(this).closest('tr').find('td.location').text();
  
  				$('html, body').animate({
  					scrollTop: $('form#formDepartment').offset().top
  				}, 'slow');
  				$('#departmentID').val(departmentIdEdit);
  				$('#departmentCode').val(departmentCodeEdit);
  				$('#departmentName').val(departmentNameEdit);
  				$('#location').val(localtionEdit);
  				$(':input[required], select[required]').trigger("change");
  			});
  
  			$(".iDelete").off('click').click(function (e) {
  				e.preventDefault();
  				var departmentId = $(this).attr('data-attr'),
  				    departmentName = $(this).closest('tr').find('td.departmentName').text();
  				$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> Department ' + departmentName + '?');
  				$("#PopUpConfirm").modal("show");
  				$("#PopUpConfirm #btnYes").click(function (e) {
  					_apiRestCall2['default'].performRequest('/v1/department/' + departmentId, 'DELETE', {}, function (data) {
  						$('html, body').animate({
  							scrollTop: $('body').offset().top
  						}, 'slow');
  						$("#PopUpConfirm").modal("hide");
  						if (typeof data.error != 'undefined') {
  							$("#alert #alert-title").html("Error");
  							$("#alert #alert-content").html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  							$("#alert").addClass('alert-danger').show();
  							$("#alert").fadeOut(2000);
  						} else {
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Delete Department");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							datatable.destroy();
  							loadTable();
  							reset();
  						}
  					});
  				});
  			});
  		}
  
  		loadTable();
  	}
  }, {
  	mode: 'Input',
  	name: 'formDepartment',
  	title: 'Add Department',
  	loadAll: true,
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	items: [{
  		value: '',
  		name: 'departmentID',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Department Code',
  		name: 'departmentCode',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Department Name',
  		name: 'departmentName',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Location',
  		name: 'location',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'New',
  			type: 'reset',
  			name: 'btnNew',
  			cls: 'btn-default'
  		}]
  	}]
  }];
  
  var Department = (function () {
  	function Department() {
  		_classCallCheck(this, _Department);
  	}
  
  	_createClass(Department, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _Department = Department;
  	Department = (0, _decoratorsWithStyles2['default'])(_DepartmentCss2['default'])(Department) || Department;
  	return Department;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Department.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 76 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _ErrorPageCss = __webpack_require__(25);
  
  var _ErrorPageCss2 = _interopRequireDefault(_ErrorPageCss);
  
  var ErrorPage = (function () {
  	function ErrorPage() {
  		_classCallCheck(this, _ErrorPage);
  	}
  
  	_createClass(ErrorPage, [{
  		key: 'render',
  		value: function render() {
  			var title = 'Error';
  			this.context.onSetTitle(title);
  			return _react2['default'].createElement(
  				'div',
  				null,
  				_react2['default'].createElement(
  					'h1',
  					null,
  					title
  				),
  				_react2['default'].createElement(
  					'p',
  					null,
  					'Sorry, an critical error occurred on this page.'
  				)
  			);
  		}
  	}], [{
  		key: 'contextTypes',
  		value: {
  			onSetTitle: _react.PropTypes.func.isRequired,
  			onPageNotFound: _react.PropTypes.func.isRequired
  		},
  		enumerable: true
  	}]);
  
  	var _ErrorPage = ErrorPage;
  	ErrorPage = (0, _decoratorsWithStyles2['default'])(_ErrorPageCss2['default'])(ErrorPage) || ErrorPage;
  	return ErrorPage;
  })();
  
  exports['default'] = ErrorPage;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "ErrorPage.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 77 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _FeedbackCss = __webpack_require__(26);
  
  var _FeedbackCss2 = _interopRequireDefault(_FeedbackCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var Feedback = (function () {
  	function Feedback() {
  		_classCallCheck(this, _Feedback);
  	}
  
  	_createClass(Feedback, [{
  		key: 'render',
  		value: function render() {
  			return _react2['default'].createElement(
  				'div',
  				{ className: 'Feedback' },
  				_react2['default'].createElement(
  					'div',
  					{ className: 'Feedback-container' },
  					_react2['default'].createElement(
  						'a',
  						{ className: 'Feedback-link', href: 'https://gitter.im/kriasoft/react-starter-kit' },
  						'Ask a question'
  					),
  					_react2['default'].createElement(
  						'span',
  						{ className: 'Feedback-spacer' },
  						'|'
  					),
  					_react2['default'].createElement(
  						'a',
  						{ className: 'Feedback-link', href: 'https://github.com/kriasoft/react-starter-kit/issues/new' },
  						'Report an issue'
  					)
  				)
  			);
  		}
  	}]);
  
  	var _Feedback = Feedback;
  	Feedback = (0, _decoratorsWithStyles2['default'])(_FeedbackCss2['default'])(Feedback) || Feedback;
  	return Feedback;
  })();
  
  exports['default'] = Feedback;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Feedback.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 78 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _FooterCss = __webpack_require__(27);
  
  var _FooterCss2 = _interopRequireDefault(_FooterCss);
  
  var _decoratorsWithViewport = __webpack_require__(117);
  
  var _decoratorsWithViewport2 = _interopRequireDefault(_decoratorsWithViewport);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _utilsLink = __webpack_require__(8);
  
  var _utilsLink2 = _interopRequireDefault(_utilsLink);
  
  var Footer = (function () {
  	function Footer() {
  		_classCallCheck(this, _Footer);
  	}
  
  	_createClass(Footer, [{
  		key: 'render',
  		value: function render() {
  			// This is just an example how one can render CSS
  			var _props$viewport = this.props.viewport;
  			var width = _props$viewport.width;
  			var height = _props$viewport.height;
  
  			this.renderCss('.Footer-viewport:after {content:\' ' + width + 'x' + height + '\';}');
  
  			return _react2['default'].createElement(
  				'div',
  				{ className: 'Footer' },
  				_react2['default'].createElement(
  					'div',
  					{ className: 'Footer-container' },
  					_react2['default'].createElement(
  						'span',
  						{ className: 'Footer-text' },
  						'© Your Company'
  					),
  					_react2['default'].createElement(
  						'span',
  						{ className: 'Footer-spacer' },
  						'·'
  					),
  					_react2['default'].createElement(
  						'a',
  						{ className: 'Footer-link', href: '/', onClick: _utilsLink2['default'].handleClick },
  						'Home'
  					),
  					_react2['default'].createElement(
  						'span',
  						{ className: 'Footer-spacer' },
  						'·'
  					),
  					_react2['default'].createElement(
  						'a',
  						{ className: 'Footer-link', href: '/privacy', onClick: _utilsLink2['default'].handleClick },
  						'Privacy'
  					),
  					_react2['default'].createElement(
  						'span',
  						{ className: 'Footer-spacer' },
  						'·'
  					),
  					_react2['default'].createElement(
  						'a',
  						{ className: 'Footer-link', href: '/not-found', onClick: _utilsLink2['default'].handleClick },
  						'Not Found'
  					),
  					_react2['default'].createElement(
  						'span',
  						{ className: 'Footer-spacer' },
  						' | '
  					),
  					_react2['default'].createElement(
  						'span',
  						{ ref: 'viewport', className: 'Footer-viewport Footer-text Footer-text--muted' },
  						'Viewport:'
  					)
  				)
  			);
  		}
  	}], [{
  		key: 'propTypes',
  		value: {
  			viewport: _react.PropTypes.shape({
  				width: _react.PropTypes.number.isRequired,
  				height: _react.PropTypes.number.isRequired
  			}).isRequired
  		},
  		enumerable: true
  	}]);
  
  	var _Footer = Footer;
  	Footer = (0, _decoratorsWithStyles2['default'])(_FooterCss2['default'])(Footer) || Footer;
  	Footer = (0, _decoratorsWithViewport2['default'])(Footer) || Footer;
  	return Footer;
  })();
  
  exports['default'] = Footer;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Footer.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 79 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _HeaderCss = __webpack_require__(28);
  
  var _HeaderCss2 = _interopRequireDefault(_HeaderCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _utilsLink = __webpack_require__(8);
  
  var _utilsLink2 = _interopRequireDefault(_utilsLink);
  
  var _Navigation = __webpack_require__(81);
  
  var _Navigation2 = _interopRequireDefault(_Navigation);
  
  var _coreReactCookie = __webpack_require__(6);
  
  var _coreReactCookie2 = _interopRequireDefault(_coreReactCookie);
  
  var Header = (function () {
  	function Header() {
  		_classCallCheck(this, _Header);
  	}
  
  	_createClass(Header, [{
  		key: 'render',
  		value: function render() {
  			return _react2['default'].createElement(_Navigation2['default'], null);
  		}
  	}]);
  
  	var _Header = Header;
  	Header = (0, _decoratorsWithStyles2['default'])(_HeaderCss2['default'])(Header) || Header;
  	return Header;
  })();
  
  exports['default'] = Header;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Header.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 80 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _LoginPageCss = __webpack_require__(29);
  
  var _LoginPageCss2 = _interopRequireDefault(_LoginPageCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _coreReactCookie = __webpack_require__(6);
  
  var _coreReactCookie2 = _interopRequireDefault(_coreReactCookie);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  function login() {
  	var username = document.getElementById('username'),
  	    password = document.getElementById('password'),
  	    btnLogin = document.getElementById('btnLogin'),
  	    error = document.getElementById('errorDisplay');
  
  	if (username.value == "") {
  		error.innerHTML = "please input username!";
  		error.style.display = 'block';
  	} else if (password.value == "") {
  		error.innerHTML = "please input password!";
  		error.style.display = 'block';
  	} else if (username.value.length < 3) {
  		error.innerHTML = "username must be at least 3 characters!";
  		error.style.display = 'block';
  	} else if (password.value.length < 5) {
  		error.innerHTML = "password must be at least 5 characters!";
  		error.style.display = 'block';
  	} else {
  		error.innerHTML = "";
  		error.style.display = 'none';
  		btnLogin.innerHTML = "<img class='iloading' width='25px' src='/loading.gif'>";
  		_apiRestCall2['default'].performRequest('/v1/oauth/token', 'POST', {
  			username: username.value,
  			password: password.value,
  			grant_type: 'password'
  		}, function (data) {
  			btnLogin.innerHTML = "Login";
  			if (typeof data.error != 'undefined') {
  				error.innerHTML = data.error_description;
  				error.style.display = 'block';
  			} else {
  				if (data.status == "ERROR") {
  					error.innerHTML = data.message;
  					error.style.display = 'block';
  				} else {
  					var date = new Date();
  					date.setSeconds(date.getSeconds() + data.expires_in);
  					_coreReactCookie2['default'].save('username', username.value.toLowerCase(), { expires: date, maxAge: data.expires_in });
  					_coreReactCookie2['default'].save('auth', data.access_token, { expires: date, maxAge: data.expires_in });
  					window.location.assign("/");
  				}
  			}
  		});
  	}
  }
  
  var dataField = [{
  	title: 'Please Sign In',
  	name: 'formLogin',
  	cls: 'col-md-4 col-md-offset-4',
  	componentDidMount: function componentDidMount() {
  		document.onkeydown = function () {
  			if (window.event.keyCode == '13') {
  				login();
  			}
  		};
  	},
  	items: [{
  		text: 'Username',
  		name: 'username',
  		type: 'email'
  	}, {
  		text: 'Password',
  		name: 'password',
  		type: 'password'
  	}, {
  		text: 'Remember Me',
  		name: 'remember',
  		value: '0',
  		type: 'checkbox'
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		text: 'Login',
  		name: 'btnLogin',
  		url: '/',
  		event: function event(e) {
  			e.preventDefault();
  			login();
  		},
  		type: 'button',
  		cls: 'btn btn-lg btn-success btn-block'
  	}]
  }];
  
  var LoginPage = (function () {
  	function LoginPage() {
  		_classCallCheck(this, _LoginPage);
  	}
  
  	_createClass(LoginPage, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _LoginPage = LoginPage;
  	LoginPage = (0, _decoratorsWithStyles2['default'])(_LoginPageCss2['default'])(LoginPage) || LoginPage;
  	return LoginPage;
  })();
  
  var Content = _coreApps2['default'].Users({ dataField: dataField });
  
  exports['default'] = Content;
  module.exports = exports['default'];
  // additional: ['bitbucket', 'dropbox', 'facebook', 'flickr', 'github', 'google-plus', 'instagram', 'linkedin', 'pinterest', 'tumblr', 'twitter', 'vk']

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "LoginPage.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 81 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _classnames = __webpack_require__(123);
  
  var _classnames2 = _interopRequireDefault(_classnames);
  
  var _NavigationCss = __webpack_require__(30);
  
  var _NavigationCss2 = _interopRequireDefault(_NavigationCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _utilsLink = __webpack_require__(8);
  
  var _utilsLink2 = _interopRequireDefault(_utilsLink);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _coreReactCookie = __webpack_require__(6);
  
  var _coreReactCookie2 = _interopRequireDefault(_coreReactCookie);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  /*
  RestCall.performRequest('/v1/staff', 'GET', {}, function(data) {
  	if (data.status == "OK")
  		reactCookie.save("data", JSON.stringify(data.data));
  });
  Apps.props = reactCookie.load("data");
  console.log(Apps.props);
  */
  var dataMessages = [{
  	text: 'John Smith',
  	time: 'Yesterday at 4:32 PM',
  	desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...'
  }, {
  	text: 'John Smith 1',
  	time: 'Yesterday at 4:32 PM',
  	desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...'
  }, {
  	text: 'John Smith 2',
  	time: 'Yesterday at 4:32 PM',
  	desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...'
  }];
  var dataTask = [{
  	type: 'info',
  	percentage: '20%',
  	text: 'Task 1'
  }, {
  	type: 'success',
  	percentage: '40%',
  	text: 'Task 2'
  }, {
  	type: 'warning',
  	percentage: '60%',
  	text: 'Task 3'
  }, {
  	type: 'danger',
  	percentage: '80%',
  	text: 'Task 4'
  }];
  var dataNotif = [{
  	icon: "fa-comment",
  	text: "New Comment",
  	time: "4 minutes ago"
  }, {
  	icon: "fa-twitter",
  	text: "3 New Followers",
  	time: "12 minutes ago"
  }, {
  	icon: "fa-envelope",
  	text: "Message Sent",
  	time: "4 minutes ago"
  }, {
  	icon: "fa-tasks",
  	text: "New Task",
  	time: "4 minutes ago"
  }, {
  	icon: "fa-upload",
  	text: "Server Rebooted",
  	time: "4 minutes ago"
  }];
  var dataSideBar = [{
  	title: 'Dashboard',
  	url: '/',
  	icon: 'fa-dashboard'
  }, {
  	title: 'User',
  	url: '#',
  	icon: 'fa-user',
  	'class': '',
  	items: [{
  		text: 'Customer',
  		url: '/customer'
  	}, {
  		text: 'Staff',
  		url: '/staff'
  	}, {
  		text: 'Supplier',
  		url: '/supplier'
  	}]
  }, {
  	title: 'Product',
  	url: '#',
  	icon: 'fa-table',
  	'class': '',
  	items: [{
  		text: 'Product',
  		url: '/product'
  	}, {
  		text: 'Product Adjustment',
  		url: '/product-adjustment'
  	}, {
  		text: 'Product Category',
  		url: '/product-category'
  	}]
  }, {
  	title: 'Sales',
  	url: '#',
  	icon: 'fa-users',
  	'class': '',
  	items: [{
  		text: 'Sales Order',
  		url: '/sales-order'
  	}, {
  		text: 'Sales Invoice',
  		url: '/sales-invoice'
  	}, {
  		text: 'Sales Pricing',
  		url: '/sales-pricing'
  	}, {
  		text: 'Sales Return',
  		url: '/sales-return'
  	}]
  }, {
  	title: 'Access User',
  	url: '#',
  	icon: 'fa-user',
  	'class': '',
  	items: [{
  		text: 'Member',
  		url: '/user'
  	}, {
  		text: 'Department',
  		url: '/department'
  	}, {
  		text: 'Role',
  		url: '/role'
  	}, {
  		text: 'Staff Position',
  		url: '/staff-position'
  	}]
  }, {
  	title: 'Shipping Cart',
  	url: '/shipping-order',
  	icon: 'fa-shopping-cart',
  	'class': ''
  }, {
  	title: 'Payment',
  	url: '#',
  	icon: 'fa-folder-open',
  	'class': '',
  	items: [{
  		text: 'Payment',
  		url: '/payment'
  	}, {
  		text: 'Cash In',
  		url: '/cash-in'
  	}, {
  		text: 'Cash Out',
  		url: '/cash-out'
  	}, {
  		text: 'Cost Category',
  		url: '/cost-category'
  	}]
  }, {
  	title: 'Report',
  	url: '#',
  	icon: 'fa-envelope',
  	'class': '',
  	items: [{
  		text: 'Profit',
  		url: '/profit'
  	}, {
  		text: 'Transaction',
  		url: '/transaction'
  	}, {
  		text: 'Receivable',
  		url: '/receivable'
  	}]
  }];
  /*
  	{
  		id: "key-profile",
  		url: "#",
  		cls: "fa-user",
  		text: "Profile"
  	}, {
  		id: "key-settings",
  		url: "#",
  		cls: "fa-gear",
  		separated: true,
  		text: "Settings"
  	}, 
  */
  var dataAccount = [{
  	id: "key-logout",
  	url: "login",
  	cls: "fa-sign-out",
  	text: "Log Out",
  	event: function event(e) {
  		_coreReactCookie2['default'].remove('username');
  		_coreReactCookie2['default'].remove('auth');
  		_coreReactCookie2['default'].remove("data");
  	}
  }];
  
  var Navigation = (function () {
  	function Navigation() {
  		_classCallCheck(this, _Navigation);
  	}
  
  	_createClass(Navigation, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _Navigation = Navigation;
  	Navigation = (0, _decoratorsWithStyles2['default'])(_NavigationCss2['default'])(Navigation) || Navigation;
  	return Navigation;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataMessages: dataMessages,
  	dataTask: dataTask,
  	dataNotif: dataNotif,
  	dataAccount: dataAccount,
  	dataSideBar: dataSideBar
  }).Header;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Navigation.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 82 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _NotFoundPageCss = __webpack_require__(31);
  
  var _NotFoundPageCss2 = _interopRequireDefault(_NotFoundPageCss);
  
  var NotFoundPage = (function () {
  	function NotFoundPage() {
  		_classCallCheck(this, _NotFoundPage);
  	}
  
  	_createClass(NotFoundPage, [{
  		key: 'render',
  		value: function render() {
  			var title = 'Page Not Found';
  			this.context.onSetTitle(title);
  			this.context.onPageNotFound();
  			return _react2['default'].createElement(
  				'div',
  				null,
  				_react2['default'].createElement(
  					'h1',
  					null,
  					title
  				),
  				_react2['default'].createElement(
  					'p',
  					null,
  					'Sorry, but the page you were trying to view does not exist.'
  				)
  			);
  		}
  	}], [{
  		key: 'contextTypes',
  		value: {
  			onSetTitle: _react.PropTypes.func.isRequired,
  			onPageNotFound: _react.PropTypes.func.isRequired
  		},
  		enumerable: true
  	}]);
  
  	var _NotFoundPage = NotFoundPage;
  	NotFoundPage = (0, _decoratorsWithStyles2['default'])(_NotFoundPageCss2['default'])(NotFoundPage) || NotFoundPage;
  	return NotFoundPage;
  })();
  
  exports['default'] = NotFoundPage;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "NotFoundPage.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 83 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _PaymentInputCss = __webpack_require__(32);
  
  var _PaymentInputCss2 = _interopRequireDefault(_PaymentInputCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPayment = {};
  var dataPageHeading = {
  	title: 'Payment',
  	additionalTitle: ' Input',
  	breadcrumbsIcon: 'fa-pencil',
  	subtitle: ['Payment', 'Payment Input']
  };
  
  var dataContent2 = [{
  	mode: 'Input',
  	name: 'formPayment',
  	title: 'Add Payment',
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		function setValue(PaymentID, SalesInvoice, staffId, staffName, PaymentMethod, Amount, Note) {
  			$('#paymentID').val(PaymentID);
  			if (SalesInvoice == "") $('#salesInvoice').prop('selectedIndex', 0);else $('#salesInvoice').val(SalesInvoice);
  			$('#staff').val(staffName).attr('data-id', staffId);
  			if (PaymentMethod == "") $('#paymentMethod').prop('selectedIndex', 0);else $('#paymentMethod').val(PaymentMethod);
  			$('#amount').val(Amount);
  			$('#note').val(Note);
  			$('#errorDisplay').text("");
  			$(':input[required], select[required]').trigger("change");
  		}
  
  		_apiRestCall2['default'].performRequest('/v1/sales-invoice', 'GET', {}, function (data) {
  			if (data.status == "OK") {
  				var salesInvoice = data.data;
  				$.map(salesInvoice || [], function (val, i) {
  					$('#salesInvoice').append('<option value=' + val.salesInvoiceId + '>' + val.invoiceNo + '</option>');
  				});
  			}
  		});
  
  		if (typeof QueryString.id != 'undefined') {
  			$.ajax({
  				url: _coreApps2['default'].host + "/v1/payment/" + QueryString.id,
  				type: 'GET',
  				dataType: 'json',
  				cache: false,
  				success: function success(data) {
  					if (data.status == "OK") {
  						var data = data.data;
  						setValue(data.paymentId, data.salesInvoice.salesInvoiceId, data.staff.staffId, data.staff.staffName, data.paymentMethod, data.amount, data.note);
  
  						dataPayment.salesInvoiceId = data.salesInvoice.salesInvoiceId;
  						dataPayment.staffId = data.staff.staffId;
  						dataPayment.paymentMethod = data.paymentMethod;
  						dataPayment.amount = data.amount;
  						dataPayment.note = data.note;
  					}
  				}
  			});
  		}
  
  		// Autocomplete Staff
  		_apiRestCall2['default'].performRequest('/v1/staff', 'GET', {}, function (data) {
  			var autocomplete = [],
  			    onSelected = false;
  			if (data.status == "OK") {
  				for (var i in data.data) autocomplete.push({ id: data.data[i].staffId, value: data.data[i].staffName });
  			}
  			var bloodhound = new Bloodhound({
  				datumTokenizer: function datumTokenizer(datum) {
  					return Bloodhound.tokenizers.whitespace(datum.value);
  				},
  				queryTokenizer: Bloodhound.tokenizers.whitespace,
  				local: autocomplete
  			});
  
  			// Initialize the Bloodhound suggestion engine
  			bloodhound.initialize().done(function () {
  				$('#staff').typeahead({
  					highlight: true
  				}, {
  					templates: {
  						empty: ['<div class="tt-suggestion">', 'No Items Found', '</div>'].join('\n'),
  						suggestion: function suggestion(data) {
  							// return '<div>' + data.id + ' – ' + data.value + '</div>'
  							return '<div data-attr=' + data.id + '>' + data.value + '</div>';
  						}
  					},
  					displayKey: 'value',
  					source: bloodhound.ttAdapter()
  				}).on("typeahead:selected", function (e, datum) {
  					dataPayment.staffId = datum.id;
  					$(this).closest('span').siblings().remove();
  					onSelected = true;
  				}).on("change", function (e) {
  					if (!onSelected) dataPayment.staffId = $(".tt-suggestion:first").attr("data-attr");
  					onSelected = false;
  					if ($(this).val() != "") $(this).closest('span').siblings().remove();
  				}).on("keydown", function (e) {
  					if (e.keyCode == 9) {
  						// TABKEY
  						if (!onSelected) dataPayment.staffId = $(".tt-suggestion:first").attr("data-attr");
  						onSelected = false;
  						if ($(this).val() != "") $(this).closest('span').siblings().remove();
  					}
  				});
  			});
  		});
  
  		$("#btnSave").click(function (e) {
  			e.preventDefault();
  			var paymentId = $('#paymentID').val(),
  			    boolVal = true,
  			    error = $("#errorDisplay");
  			console.log(dataPayment);
  			$.each($(':input[required], select[required]'), function (value, index) {
  				if ($(this).val() == "") {
  					boolVal = false;
  					return;
  				}
  			});
  
  			if (!boolVal) return;
  
  			dataPayment.salesInvoiceId = $('#salesInvoice').val();
  			dataPayment.paymentMethod = $('#paymentMethod').val();
  			dataPayment.amount = $('#amount').val();
  			dataPayment.note = $('#note').val();
  
  			if (paymentId == "") {
  				_apiRestCall2['default'].performRequest('/v1/payment', 'POST', dataPayment, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							$('html, body').animate({
  								scrollTop: $('body').offset().top
  							}, 'slow');
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Save Payment");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							setValue("", "", "", "", "CASH", "", "");
  						}
  					}
  				});
  			} else {
  				_apiRestCall2['default'].performRequest('/v1/payment/' + paymentId, 'PUT', dataPayment, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							window.location.assign('/payment');
  						}
  					}
  				});
  			}
  		});
  	},
  	items: [{
  		value: '',
  		name: 'paymentID',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Invoice No',
  		name: 'salesInvoice',
  		type: 'select',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Staff Name',
  		name: 'staff',
  		type: 'autocomplete',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Payment Method',
  		type: 'select',
  		name: 'paymentMethod',
  		multiple: false,
  		cols: [2, 10],
  		required: 'required',
  		data: [{
  			text: 'CASH',
  			value: 'CASH'
  		}, {
  			text: 'CREDIT',
  			value: 'CREDIT'
  		}, {
  			text: 'TRANSFER',
  			value: 'TRANSFER'
  		}]
  	}, {
  		text: 'Amount',
  		name: 'amount',
  		type: 'number',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Note',
  		name: 'note',
  		row: 5,
  		type: 'area',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'Back',
  			type: 'button',
  			name: 'btnBack',
  			cls: 'btn-default',
  			event: function event() {
  				window.location.assign("/payment");
  			}
  		}]
  	}]
  }];
  
  var PaymentInput = (function () {
  	function PaymentInput() {
  		_classCallCheck(this, _PaymentInput);
  	}
  
  	_createClass(PaymentInput, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _PaymentInput = PaymentInput;
  	PaymentInput = (0, _decoratorsWithStyles2['default'])(_PaymentInputCss2['default'])(PaymentInput) || PaymentInput;
  	return PaymentInput;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "PaymentInput.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 84 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _PaymentCss = __webpack_require__(33);
  
  var _PaymentCss2 = _interopRequireDefault(_PaymentCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Payment',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['Payment'],
  	toolbar: [{
  		type: "button",
  		name: "btnAdd",
  		text: "Add",
  		cls: "btn-primary",
  		event: function event(e) {
  			window.location.assign("/payment-input");
  		}
  	}]
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'User',
  	name: 'table-payment',
  	panel: false,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-payment").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/payment",
  				"columns": [{ "title": "Payment ID", "className": "paymentId", "data": "paymentId", "visible": false }, { "title": "Staff", "className": "staffId", "data": "staff",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.staffName : "";
  					}
  				}, { "title": "Payment Date", "className": "paymentDate", "data": "paymentDate",
  					"render": function render(data, type, full, meta) {
  						return moment(data).format(_coreApps2['default'].dateFormat);
  					}
  				}, { "title": "Payment Method", "className": "paymentMethod", "data": "paymentMethod" }, { "title": "Amount", "className": "amount", "data": "amount", "type": "num-fmt",
  					"render": function render(data, type, full, meta) {
  						return FormatToCurrency(data);
  					}
  				}, { "title": "Note", "className": "note", "data": "note" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "paymentId",
  					"render": function render(data, type, full, meta) {
  						_coreApps2['default'].props[data] = full.salesInvoice;
  						return "<label class='viewDetail' data-attr=" + data + "><span class='glyphicon glyphicon-eye-open'></span> Detail</label> &nbsp;" + " <a class='btn btn-default iEdit' href='payment-input?id=" + data + "'>Edit</a>" + " <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-payment").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var paymentId = $(this).attr('data-attr');
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/payment/' + paymentId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								if (typeof data.error != 'undefined') {
  									$("#alert #alert-title").html("Error");
  									$("#alert #alert-content").html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  									$("#alert").addClass('alert-danger').show();
  									$("#alert").fadeOut(2000);
  								} else {
  									$("#alert #alert-title").html("Success");
  									$("#alert #alert-content").html("Delete Payment");
  									$("#alert").addClass('alert-success').show();
  									$("#alert").fadeOut(2000);
  									datatable.destroy();
  									loadTable();
  								}
  							});
  						});
  					});
  					$('label.viewDetail').css('cursor', 'pointer').click(function (e) {
  						var selected = $(this).attr('data-attr'),
  						    data = _coreApps2['default'].props[selected];
  						var row = "";
  						row += '<tr>' + '<td>' + data.salesInvoiceId + '</td>' + '<td>' + moment(data.invoiceDate).format(_coreApps2['default'].dateFormat) + '</td>' + '<td>' + data.status + '</td>' + '<td>' + (data.note == null ? " - " : data.note) + '</td>' + '</tr>';
  
  						$('.modal-title').html('Order Detail');
  						$('.modal-body').html('<div class="container-fluid">' + '<div class="row">' + '<table class="table">' + '<thead>' + '<tr>' + '<th>Invoice No</th>' + '<th>Product</th>' + '<th>Quantity</th>' + '<th>Note</th>' + '</tr>' + '</thead>' + '<tbody>' + row + '</tbody>' + '</table>' + '</div>' + '</div>');
  						$('.modal-footer').html("");
  						$('#popup').modal('show');
  					});
  				}
  			});
  		}
  
  		loadTable();
  	}
  }];
  
  var Payment = (function () {
  	function Payment() {
  		_classCallCheck(this, _Payment);
  	}
  
  	_createClass(Payment, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _Payment = Payment;
  	Payment = (0, _decoratorsWithStyles2['default'])(_PaymentCss2['default'])(Payment) || Payment;
  	return Payment;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Payment.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 85 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _ProductAdjustmentInputCss = __webpack_require__(34);
  
  var _ProductAdjustmentInputCss2 = _interopRequireDefault(_ProductAdjustmentInputCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataProductAdjustment = {};
  var dataPageHeading = {
  	title: 'Product Adjustment',
  	additionalTitle: ' Input',
  	subtitle: ['Product', 'Product Adjustment', 'Product Adjustment Input'],
  	breadcrumbsIcon: 'fa-pencil'
  };
  
  function setValue(ProductAdjustmentID, StaffId, StaffName, Reason, AdjustmentType) {
  	$('#productAdjustmentID').val(ProductAdjustmentID);
  	$('#staff').val(StaffName).attr('data-id', StaffId);
  	$('#reason').val(Reason);
  	$('#adjustmentType[value=' + AdjustmentType + ']').prop('checked', true);
  	$('#tableProductAdjustmentDetail tbody').remove();
  	$('#errorDisplay').text("");
  	$(':input[required], select[required]').trigger("change");
  }
  
  var dataContent1 = [];
  var dataContent2 = [{
  	mode: 'Input',
  	name: 'formProductAdjustmentInput',
  	title: 'Add Product Adjustment',
  	iconHeader: 'fa-plus',
  	loadAll: true,
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		if (typeof QueryString.id != 'undefined') {
  			$.ajax({
  				url: _coreApps2['default'].host + "/v1/product-adjustment/" + QueryString.id,
  				type: 'GET',
  				dataType: 'json',
  				cache: false,
  				success: function success(data) {
  					if (data.status == "OK") {
  						var data = data.data,
  						    details = data.details;
  						setValue(data.productAdjustmentId, data.staff.staffId, data.staff.staffName, data.reason, data.adjustmentType);
  
  						dataProductAdjustment.staffId = data.staff.staffId;
  						dataProductAdjustment.reason = data.reason;
  						dataProductAdjustment.adjustmentType = data.adjustmentType;
  
  						for (var i in details) _coreApps2['default'].props.push({ productId: details[i].productId, productName: details[i].product.productName, quantity: details[i].quantity });
  					}
  				},
  				async: false
  			});
  		}
  
  		_apiRestCall2['default'].performRequest('/v1/staff', 'GET', {}, function (data) {
  			var autocomplete = [],
  			    onSelected = false;
  			if (data.status == "OK") {
  				for (var i in data.data) autocomplete.push({ id: data.data[i].staffId, value: data.data[i].staffName });
  			}
  			var bloodhound = new Bloodhound({
  				datumTokenizer: function datumTokenizer(datum) {
  					return Bloodhound.tokenizers.whitespace(datum.value);
  				},
  				queryTokenizer: Bloodhound.tokenizers.whitespace,
  				local: autocomplete
  			});
  
  			// Initialize the Bloodhound suggestion engine
  			bloodhound.initialize().done(function () {
  				$('#staff').typeahead({
  					highlight: true
  				}, {
  					templates: {
  						empty: ['<div class="tt-suggestion">', 'No Items Found', '</div>'].join('\n'),
  						suggestion: function suggestion(data) {
  							// return '<div>' + data.id + ' – ' + data.value + '</div>'
  							return '<div data-attr=' + data.id + '>' + data.value + '</div>';
  						}
  					},
  					displayKey: 'value',
  					source: bloodhound.ttAdapter()
  				}).on("typeahead:selected", function (e, datum) {
  					dataProductAdjustment.staffId = datum.id;
  					$(this).closest('span').siblings().remove();
  					onSelected = true;
  				}).on("change", function (e) {
  					if (!onSelected) dataProductAdjustment.staffId = $(".tt-suggestion:first").attr("data-attr");
  					onSelected = false;
  					if ($(this).val() != "") $(this).closest('span').siblings().remove();
  				}).on("keydown", function (e) {
  					if (e.keyCode == 9) {
  						// TABKEY
  						if (!onSelected) dataProductAdjustment.staffId = $(".tt-suggestion:first").attr("data-attr");
  						onSelected = false;
  						if ($(this).val() != "") $(this).closest('span').siblings().remove();
  					}
  				});
  			});
  		});
  	},
  	items: [{
  		value: '',
  		name: 'productAdjustmentID',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Staff Name',
  		name: 'staff',
  		required: 'required',
  		type: 'autocomplete',
  		cols: [2, 10]
  	}, {
  		text: 'Reason',
  		name: 'reason',
  		rows: 5,
  		type: 'area',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Adjustment Type',
  		type: 'radio',
  		cols: [2, 10],
  		data: [{
  			name: 'adjustmentType',
  			text: 'IN',
  			value: 'IN'
  		}, {
  			name: 'adjustmentType',
  			text: 'OUT',
  			value: 'OUT'
  		}]
  	}]
  }, {
  	mode: 'Table',
  	name: 'tableProductAdjustmentDetail',
  	cls: 'col-lg-12',
  	loadAll: true,
  	typeTable: 'table-responsive',
  	componentDidMount: function componentDidMount() {
  		function loadTable() {
  			var rows = "",
  			    picture_default = "placeholder-640x480.png";
  			$("#tableProductAdjustmentDetail").html("<thead></thead><tbody></tbody><tfoot></tfoot>");
  			$("#tableProductAdjustmentDetail thead").html("<tr>" + "<th>Product</th>" + "<th>Quantity</th>" + "<th>Action</th>" + "</tr>");
  			if (_coreApps2['default'].props.length) {
  				for (var i in _coreApps2['default'].props) rows += "<tr id='row-" + i + "'>" + "<td id=" + _coreApps2['default'].props[i].productId + ">" + _coreApps2['default'].props[i].productName + "</td>" + "<td>" + _coreApps2['default'].props[i].quantity + "</td>" + "<td><a class='btn btn-danger iDeleteDetail' data-attr='" + i + "'>Delete</a></td>" + "</tr>";
  			}
  			$("#tableProductAdjustmentDetail tbody").html(rows);
  			$("#tableProductAdjustmentDetail tfoot").html("<tr><td colspan='3'><button class='btn btn-primary'>Browse</button></td></tr>");
  
  			$("a.iDeleteDetail").click(function (e) {
  				var index = $(this).attr('data-attr'),
  				    productIdParam = $(this).closest('td').siblings(':first').attr('id');
  				_coreApps2['default'].props = $.grep(_coreApps2['default'].props || [], function (value, index) {
  					return value.productId != productIdParam;
  				});
  				$("tr#row-" + index).remove();
  			});
  			$("#tableProductAdjustmentDetail tfoot button").click(function () {
  				$('.modal-title').html('Product Detail');
  				$('.modal-body').html('<table class="table table-hover" id="tblProduct"></table>');
  				var otable = $("#tblProduct").DataTable({
  					"retrieve": true,
  					"bAutoWidth": true,
  					"bProcessing": true,
  					"bServerSide": false,
  					"sAjaxSource": _coreApps2['default'].host + "/v1/product",
  					"columns": [{ "title": "Product ID", "className": "productId", "data": "productId", "visible": false }, { "title": "Product Name", "className": "productName", "data": "productName",
  						"render": function render(data, type, full, meta) {
  							return "<label id='" + full.productId + "'>" + data + "</label>";
  						}
  					}, { "title": "Category", "className": "productCategoryId", "data": "productCategory",
  						"render": function render(data, type, full, meta) {
  							return data != null ? data.productCategoryName : "";
  						}
  					}, { "title": "Initial Stock", "className": "initialStock", "data": "initialStock" }, { "title": "Currnet Stock", "className": "currentStock", "data": "currentStock" }, { "title": "Sell Price", "className": "sellPrice", "data": "sellPrice", "type": "num-fmt",
  						"render": function render(data, type, full, meta) {
  							return FormatToCurrency(data);
  						}
  					}, { "title": "Status", "className": "status", "data": "status" }, { "title": "Image", "className": "productPhoto", "data": "productPhoto",
  						"render": function render(data, type, full, meta) {
  							var link;
  							return "<img src=" + _coreApps2['default'].defaultDirectory + (data == null ? picture_default : data) + " width=100 height=100 />";
  						}
  					}],
  					"fnDrawCallback": function fnDrawCallback() {
  						$("#tblProduct tbody tr").off('click').click(function (e) {
  							e.preventDefault();
  							var productId = $(this).find('.productName label').attr('id'),
  							    productName = $(this).find('.productName label').text();
  							_coreApps2['default'].item = {};
  							_coreApps2['default'].item.productId = productId;
  							_coreApps2['default'].item.productName = productName;
  							if ($(this).hasClass('selected')) {
  								$(this).removeClass('selected');
  							} else {
  								$('#tblProduct tbody tr.selected').removeClass('selected');
  								$(this).addClass('selected');
  							}
  						});
  					}
  				});
  				$('#popup').modal('show');
  				$('.modal-body').css({ 'overflow-y': 'scroll', 'height': '500px' }).html($("#tblProduct"));
  				$('.modal-body').append('<form role="form" class="form-horizontal" id="frmProduct">' + '<div class="form-group">' + '<label class="control-label col-sm-2">Quantity</label>' + '<div class="col-sm-10">' + '<input class="form-control" type="number" name="quantityDetail" id="quantityDetail" placeholder="Quantity" value=0 />' + '</div>' + '</div>' + '</form>');
  				$("#btnSubmitPopup").off('click').click(function (e) {
  					var elQuantity = $("#frmProduct #quantityDetail")[1];
  					_coreApps2['default'].item.quantity = $(elQuantity).val();
  					var flag = true;
  					$.map(_coreApps2['default'].props || [], function (value, index) {
  						if (value.productId == _coreApps2['default'].item.productId) {
  							flag = false;
  							return;
  						}
  					});
  
  					if (typeof _coreApps2['default'].item.productId == 'undefined') return;
  
  					if (flag) _coreApps2['default'].props.push(_coreApps2['default'].item);else {
  						$('html, body').animate({
  							scrollTop: $('body').offset().top
  						}, 'slow');
  						$("#alert #alert-title").html("Error");
  						$("#alert #alert-content").html("Product Already Exists");
  						$("#alert").addClass('alert-danger').show();
  						$("#alert").fadeOut(2000);
  					}
  					$('#popup').modal('hide');
  					loadTable();
  				});
  			});
  		}
  		loadTable();
  	}
  }, {
  	mode: 'Input',
  	loadAll: true,
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		$("#btnSave").click(function (e) {
  			e.preventDefault();
  			var detail;
  			detail = $.map(_coreApps2['default'].props, function (value, index) {
  				var items = [];
  				items.push({ productId: value.productId, quantity: value.quantity });
  				return items;
  			});
  			var productAdjustmentId = $("#productAdjustmentID").val(),
  			    error = $('#errorDisplay'),
  			    boolVal = true;
  
  			$.each($(':input[required], select[required]'), function (value, index) {
  				if ($(this).val() == "") {
  					boolVal = false;
  					return;
  				}
  			});
  
  			if (!boolVal) return;
  
  			if (detail.length == 0) {
  				error.html("Product must have at least one item");
  				error.css('display', 'block');
  				return;
  			}
  
  			dataProductAdjustment.reason = $('#reason').val();
  			dataProductAdjustment.adjustmentType = $('#adjustmentType:checked').val();
  			dataProductAdjustment.details = JSON.stringify(detail);
  
  			if (productAdjustmentId == "") {
  				_apiRestCall2['default'].performRequest('/v1/product-adjustment', 'POST', dataProductAdjustment, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  							error.css('display', 'block');
  						} else {
  							$('html, body').animate({
  								scrollTop: $('body').offset().top
  							}, 'slow');
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Save Product Adjustment");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							setValue("", "", "", "IN");
  						}
  					}
  				});
  			} else {
  				_apiRestCall2['default'].performRequest('/v1/product-adjustment/' + productAdjustmentId, 'PUT', dataProductAdjustment, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							window.location.assign("/product-adjustment");
  						}
  					}
  				});
  			}
  		});
  	},
  	items: [{
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'Back',
  			type: 'button',
  			name: 'btnBack',
  			cls: 'btn-default',
  			event: function event() {
  				window.location.assign("/product-adjustment");
  			}
  		}]
  	}]
  }];
  
  var ProductAdjustmentInput = (function () {
  	function ProductAdjustmentInput() {
  		_classCallCheck(this, _ProductAdjustmentInput);
  	}
  
  	_createClass(ProductAdjustmentInput, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _ProductAdjustmentInput = ProductAdjustmentInput;
  	ProductAdjustmentInput = (0, _decoratorsWithStyles2['default'])(_ProductAdjustmentInputCss2['default'])(ProductAdjustmentInput) || ProductAdjustmentInput;
  	return ProductAdjustmentInput;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent1: dataContent1,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "ProductAdjustmentInput.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 86 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _ProductAdjustmentCss = __webpack_require__(35);
  
  var _ProductAdjustmentCss2 = _interopRequireDefault(_ProductAdjustmentCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Product Adjustment',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['Product', 'Product Adjustment'],
  	toolbar: [{
  		type: "button",
  		name: "btnAdd",
  		text: "Add",
  		cls: "btn-primary",
  		event: function event(e) {
  			window.location.assign("/product-adjustment-input");
  		}
  	}]
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'User',
  	name: 'table-product-adjustment',
  	panel: false,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-product-adjustment").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/product-adjustment",
  				"columns": [{ "title": "Product Adjustment ID", "className": "productAdjustmentId", "data": "productAdjustmentId", "visible": false }, { "title": "Staff", "className": "staffId", "data": "staff",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.staffName : "";
  					}
  				}, { "title": "Reason", "className": "reason", "data": "reason" }, { "title": "Adjustment Type", "className": "adjustmentType", "data": "adjustmentType" }, { "title": "Adjustment Date", "className": "adjustment_date", "data": "adjustment_date",
  					"render": function render(data, type, full, meta) {
  						return moment(data).format(_coreApps2['default'].dateFormat);
  					}
  				}, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "productAdjustmentId",
  					"render": function render(data, type, full, meta) {
  						_coreApps2['default'].props[data] = full.details;
  						return "<label class='viewDetail' data-attr=" + data + "><span class='glyphicon glyphicon-eye-open'></span> Detail</label> &nbsp;" + " <a class='btn btn-default iEdit' href='product-adjustment-input?id=" + data + "'>Edit</a>" + " <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-product-adjustment").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var productAdjustmentId = $(this).attr('data-attr');
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/product-adjustment/' + productAdjustmentId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								if (typeof data.error != 'undefined') {
  									$("#alert #alert-title").html("Error");
  									$("#alert #alert-content").html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  									$("#alert").addClass('alert-danger').show();
  									$("#alert").fadeOut(2000);
  								} else {
  									$("#alert #alert-title").html("Success");
  									$("#alert #alert-content").html("Delete Product Adjustment");
  									$("#alert").addClass('alert-success').show();
  									$("#alert").fadeOut(2000);
  									datatable.destroy();
  									loadTable();
  								}
  							});
  						});
  					});
  					$('label.viewDetail').css('cursor', 'pointer').click(function (e) {
  						var selected = $(this).attr('data-attr'),
  						    data = _coreApps2['default'].props[selected];
  						var row = "";
  						for (var i in data) {
  							row += '<tr>' + '<td>' + (parseInt(i) + 1) + '</td>' + '<td>' + data[i].product.productName + '</td>' + '<td>' + data[i].quantity + '</td>' + '</tr>';
  						}
  						$('.modal-title').html('Product Adjustment Detail');
  						$('.modal-body').html('<div class="container-fluid">' + '<div class="row">' + '<table class="table">' + '<thead>' + '<tr>' + '<th>No</th>' + '<th>Product</th>' + '<th>Product Adjustment Quantity</th>' + '</tr>' + '</thead>' + '<tbody>' + row + '</tbody>' + '</table>' + '</div>' + '</div>');
  						$('.modal-footer').html("");
  						$('#popup').modal('show');
  					});
  				}
  			});
  		}
  
  		loadTable();
  	}
  }];
  
  var ProductAdjustment = (function () {
  	function ProductAdjustment() {
  		_classCallCheck(this, _ProductAdjustment);
  	}
  
  	_createClass(ProductAdjustment, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _ProductAdjustment = ProductAdjustment;
  	ProductAdjustment = (0, _decoratorsWithStyles2['default'])(_ProductAdjustmentCss2['default'])(ProductAdjustment) || ProductAdjustment;
  	return ProductAdjustment;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "ProductAdjustment.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 87 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _ProductCategoryCss = __webpack_require__(36);
  
  var _ProductCategoryCss2 = _interopRequireDefault(_ProductCategoryCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Product Category',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['Product', 'Product Category']
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'User',
  	name: 'table-product-category',
  	panel: false,
  	loadAll: true,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	bodyAttr: '',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-product-category").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/product-category",
  				"columns": [{ "title": "Product Category ID", "className": "productCategoryId", "data": "productCategoryId", "visible": false }, { "title": "Product Category Name", "className": "productCategoryName", "data": "productCategoryName" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"render": function render(data, type, full, meta) {
  						return "<a class='btn btn-default iEdit' data-attr=" + full.productCategoryId + ">Edit</a> <a class='btn btn-danger iDelete' data-attr=" + full.productCategoryId + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					event();
  					$("#table-product-category").closest('.table-responsive').css('overflow', 'hidden');
  				}
  			});
  		}
  
  		function reset() {
  			$('#errorDisplay').html("");
  			$("#productCategoryId").val("");
  			$("#productCategoryName").val("");
  			$('#errorDisplay').text("");
  			$(':input[required], select[required]').trigger("change");
  		}
  
  		function event() {
  			$("#btnNew").off('click').click(function (e) {
  				reset();
  				$(':input[required], select[required]').trigger("change");
  			});
  			$("#btnSave").off('click').click(function (e) {
  				e.preventDefault();
  				var productCategoryIdParam = $('#productCategoryId').val(),
  				    error = $('#errorDisplay'),
  				    boolVal = true,
  				    data = { productCategoryName: $('#productCategoryName').val() };
  
  				$.each($(':input[required], select[required]'), function (value, index) {
  					if ($(this).val() == "") {
  						boolVal = false;
  						return;
  					}
  				});
  
  				if (!boolVal) return;
  
  				if (productCategoryIdParam == "") {
  					_apiRestCall2['default'].performRequest('/v1/product-category', 'POST', data, function (data) {
  						if (typeof data.error != 'undefined') {
  							error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  							error.css('display', 'block');
  						} else {
  							if (data.status == "ERROR") {
  								error.html(data.message);
  								error.css('display', 'block');
  							} else {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Save Product Category");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  								reset();
  							}
  						}
  					});
  				} else {
  					_apiRestCall2['default'].performRequest('/v1/product-category/' + productCategoryIdParam, 'PUT', data, function (data) {
  						if (typeof data.error != 'undefined') {
  							error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  							error.css('display', 'block');
  						} else {
  							if (data.status == "ERROR") {
  								error.html(data.message);
  								error.css('display', 'block');
  							} else {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Update Product Category");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  								reset();
  							}
  						}
  					});
  				}
  			});
  
  			$(".iEdit").off('click').click(function (e) {
  				e.preventDefault();
  				var productCategoryIdEdit = $(this).attr('data-attr'),
  				    productCategoryNameEdit = $(this).closest('tr').find('td.productCategoryName').text();
  				$('html, body').animate({
  					scrollTop: $('form#formProductCategory').offset().top
  				}, 'slow');
  				$('#productCategoryId').val(productCategoryIdEdit);
  				$('#productCategoryName').val(productCategoryNameEdit);
  				$(':input[required], select[required]').trigger("change");
  			});
  
  			$(".iDelete").off('click').click(function (e) {
  				e.preventDefault();
  				var productCategoryId = $(this).attr('data-attr'),
  				    productCategoryName = $(this).closest('tr').find('td.productCategoryName').text();
  				$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> Product Category ' + productCategoryName + '?');
  				$("#PopUpConfirm").modal("show");
  				$("#PopUpConfirm #btnYes").click(function (e) {
  					_apiRestCall2['default'].performRequest('/v1/product-category/' + productCategoryId, 'DELETE', {}, function (data) {
  						$('html, body').animate({
  							scrollTop: $('body').offset().top
  						}, 'slow');
  						$("#PopUpConfirm").modal("hide");
  						if (typeof data.error != 'undefined') {
  							$("#alert #alert-title").html("Error");
  							$("#alert #alert-content").html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  							$("#alert").addClass('alert-danger').show();
  							$("#alert").fadeOut(2000);
  						} else {
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Delete Product Category");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							datatable.destroy();
  							loadTable();
  							reset();
  						}
  					});
  				});
  			});
  		}
  
  		loadTable();
  	}
  }, {
  	mode: 'Input',
  	name: 'formProductCategory',
  	loadAll: true,
  	title: 'Add Product Category',
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	items: [{
  		value: '',
  		name: 'productCategoryId',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Category Name',
  		name: 'productCategoryName',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'New',
  			type: 'reset',
  			name: 'btnNew',
  			cls: 'btn-default'
  		}]
  	}]
  }];
  
  var ProductCategory = (function () {
  	function ProductCategory() {
  		_classCallCheck(this, _ProductCategory);
  	}
  
  	_createClass(ProductCategory, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _ProductCategory = ProductCategory;
  	ProductCategory = (0, _decoratorsWithStyles2['default'])(_ProductCategoryCss2['default'])(ProductCategory) || ProductCategory;
  	return ProductCategory;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "ProductCategory.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 88 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _ProductInputCss = __webpack_require__(37);
  
  var _ProductInputCss2 = _interopRequireDefault(_ProductInputCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Product',
  	additionalTitle: ' Input',
  	breadcrumbsIcon: 'fa-pencil',
  	subtitle: ['Product', 'Product Input']
  };
  
  var dataContent2 = [{
  	mode: 'Input',
  	name: 'formProduct',
  	title: 'Add Product',
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		function setValue(ProductID, ProductName, ProductCategoryID, InitialStock, CurrentStock, SellPrice, Status) {
  			$('#productID').val(ProductID);
  			$('#productName').val(ProductName);
  			if (ProductCategoryID == "") $('#category-product').prop('selectedIndex', 0);else $('#category-product').val(ProductCategoryID);
  			$('#initialStock').val(InitialStock);
  			$('#currentStock').val(CurrentStock);
  			$('#sellPrice').val(SellPrice);
  			if (Status == "") $('#status').prop('selectedIndex', 0);else $('#status').val(Status);
  			$('#errorDisplay').text("");
  			$(':input[required], select[required]').trigger("change");
  		}
  		$.ajax({
  			url: _coreApps2['default'].host + "/v1/product-category",
  			type: 'GET',
  			dataType: 'json',
  			cache: false,
  			success: function success(data) {
  				$('select#category-product').empty();
  				if (data.status == "OK") {
  					$.map(data.data || [], function (val, i) {
  						$('select#category-product').append('<option value=' + val.productCategoryId + '>' + val.productCategoryName + '</option>');
  					});
  				}
  			},
  			async: false
  		});
  
  		if (typeof QueryString.id != 'undefined') {
  			$.ajax({
  				url: _coreApps2['default'].host + '/v1/product/' + QueryString.id,
  				type: 'GET',
  				dataType: 'json',
  				cache: false,
  				success: function success(data) {
  					if (data.status == "OK") {
  						var data = data.data;
  						setValue(data.productId, data.productName, data.productCategory.productCategoryId, data.initialStock, data.currentStock, data.sellPrice, data.status);
  					}
  				}
  			});
  		}
  
  		$("#productPhoto").change(function (e) {
  			var files = e.target.files,
  			    attachmentFile = e.target.files,
  			    extension;
  			if (attachmentFile.length) {
  				extension = attachmentFile[0].name.substr(attachmentFile[0].name.length - 3).toLowerCase();
  				if (attachmentFile[0].size > 2000000) {
  					alert("File size cannot exceed 2 MB limit");
  					attachmentFile = null;
  				} else if (extension != 'jpg' && extension != 'png' && extension != 'gif') {
  					alert("File extension must be .jpg, .png, .gif");
  					attachmentFile = null;
  				}
  			}
  		});
  
  		$("#btnSave").click(function (e) {
  			e.preventDefault();
  			var upload = new FormData();
  			$.each($('#productPhoto')[0].files, function (i, file) {
  				upload.append('file-' + i, file);
  			});
  
  			var productId = $('#productID').val(),
  			    error = $("#errorDisplay"),
  			    boolVal = true,
  			    data = {
  				productName: $('#productName').val(),
  				productCategoryId: $('#category-product').val(),
  				stock: $('#initialStock').val(),
  				sellPrice: $('#sellPrice').val(),
  				status: $('#status').val(),
  				productPhoto: upload
  			};
  			$.each($(':input[required], select[required]'), function (value, index) {
  				if ($(this).val() == "") {
  					boolVal = false;
  					return;
  				}
  			});
  
  			if (!boolVal) return;
  
  			if (productId == "") {
  				_apiRestCall2['default'].performRequest('/v1/product', 'POST', data, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							$('html, body').animate({
  								scrollTop: $('body').offset().top
  							}, 'slow');
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Save Product");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							setValue("", "", "", "", "", "", "");
  						}
  					}
  				});
  			} else {
  				_apiRestCall2['default'].performRequest('/v1/product/' + productId, 'PUT', data, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							window.location.assign('/product');
  						}
  					}
  				});
  			}
  		});
  	},
  	items: [{
  		value: '',
  		name: 'productID',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Product Name',
  		name: 'productName',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Category',
  		type: 'select',
  		name: 'category-product',
  		multiple: false,
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Initial Stock',
  		name: 'initialStock',
  		type: 'number',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Current Stock',
  		name: 'currentStock',
  		type: 'number',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Sell Price',
  		name: 'sellPrice',
  		type: 'number',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Status',
  		name: 'status',
  		type: 'select',
  		multiple: false,
  		cols: [2, 10],
  		required: 'required',
  		data: [{
  			text: 'NEW',
  			value: 'NEW'
  		}, {
  			text: 'AVAILABLE',
  			value: 'AVAILABLE'
  		}, {
  			text: 'SOLD OUT',
  			value: 'SOLD OUT'
  		}]
  	}, {
  		text: 'Photo',
  		name: 'productPhoto',
  		type: 'file',
  		cols: [2, 10]
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'Back',
  			type: 'button',
  			name: 'btnBack',
  			cls: 'btn-default',
  			event: function event() {
  				window.location.assign("/product");
  			}
  		}]
  	}]
  }];
  
  var ProductInput = (function () {
  	function ProductInput() {
  		_classCallCheck(this, _ProductInput);
  	}
  
  	_createClass(ProductInput, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _ProductInput = ProductInput;
  	ProductInput = (0, _decoratorsWithStyles2['default'])(_ProductInputCss2['default'])(ProductInput) || ProductInput;
  	return ProductInput;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "ProductInput.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 89 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _ProductCss = __webpack_require__(38);
  
  var _ProductCss2 = _interopRequireDefault(_ProductCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Product',
  	additionalTitle: '',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['Product'],
  	toolbar: [{
  		type: "button",
  		name: "btnAdd",
  		text: "Add",
  		cls: "btn-primary",
  		event: function event(e) {
  			window.location.assign("/product-input");
  		}
  	}]
  };
  
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'Product',
  	name: 'table-product',
  	panel: false,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	bodyAttr: '',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			var picture_default = "placeholder-640x480.png";
  			datatable = $("#table-product").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/product",
  				"columns": [{ "title": "Product ID", "className": "productId", "data": "productId", "visible": false }, { "title": "Product Name", "className": "productName", "data": "productName" }, { "title": "Category", "className": "productCategoryId", "data": "productCategory",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.productCategoryName : "";
  					}
  				}, { "title": "Initial Stock", "className": "initialStock", "data": "initialStock" }, { "title": "Currnet Stock", "className": "currentStock", "data": "currentStock" }, { "title": "Sell Price", "className": "sellPrice", "data": "sellPrice", "type": "num-fmt",
  					"render": function render(data, type, full, meta) {
  						return FormatToCurrency(data);
  					}
  				}, { "title": "Status", "className": "status", "data": "status" }, { "title": "Image", "className": "productPhoto", "data": "productPhoto",
  					"render": function render(data, type, full, meta) {
  						var link;
  						return "<img src=" + _coreApps2['default'].defaultDirectory + (data == null ? picture_default : data) + " width=100 height=100 />";
  					}
  				}, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "productId",
  					"render": function render(data, type, full, meta) {
  						return "<a class='btn btn-default iEdit' href='product-input?id=" + data + "'>Edit</a> <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-product").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var productId = $(this).attr('data-attr'),
  						    productName = $(this).closest('tr').find('td.productName').text();
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> product ' + productName + '?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/product/' + productId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								if (typeof data.error != 'undefined') {
  									$("#alert #alert-title").html("Error");
  									$("#alert #alert-content").html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  									$("#alert").addClass('alert-danger').show();
  									$("#alert").fadeOut(2000);
  								} else {
  									$("#alert #alert-title").html("Success");
  									$("#alert #alert-content").html("Delete Product");
  									$("#alert").addClass('alert-success').show();
  									$("#alert").fadeOut(2000);
  									datatable.destroy();
  									loadTable();
  								}
  							});
  						});
  					});
  				}
  			});
  		}
  
  		loadTable();
  	}
  }];
  
  var Product = (function () {
  	function Product() {
  		_classCallCheck(this, _Product);
  	}
  
  	_createClass(Product, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _Product = Product;
  	Product = (0, _decoratorsWithStyles2['default'])(_ProductCss2['default'])(Product) || Product;
  	return Product;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Product.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 90 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _ProfitCss = __webpack_require__(39);
  
  var _ProfitCss2 = _interopRequireDefault(_ProfitCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Profit',
  	additionalTitle: '',
  	subtitle: ['Report', 'Profit'],
  	breadcrumbsIcon: 'fa-table',
  	toolbar: [{
  		type: "button",
  		name: "btnPrint",
  		text: "Print",
  		cls: "btn-success",
  		event: function event(e) {
  			e.preventDefault();
  		}
  	}]
  };
  var dataContent1 = [];
  var dataContent2 = [{
  	mode: 'Input',
  	loadAll: true,
  	name: 'formProfit',
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {},
  	items: [{
  		text: 'Interval Date',
  		type: 'datetime',
  		range: true,
  		nameStart: 'startDate',
  		endStart: 'endDate',
  		cols: [2, 10]
  	}, {
  		text: '',
  		type: 'button',
  		data: [{
  			type: 'submit',
  			cls: 'btn-primary',
  			name: 'btnLoadData',
  			text: 'Load'
  		}]
  	}]
  }, {
  	type: 'BarChart',
  	mode: 'Table',
  	title: 'Profit',
  	loadAll: true,
  	cls: 'col-lg-12',
  	table: [{
  		typeTable: 'table-responsive',
  		header: ['#', 'Date', 'Time', 'Amount'],
  		body: [['3326', '10/21/2013', '3:29 PM', '$321.33'], ['3325', '10/21/2013', '3:20 PM', '$234.34'], ['3324', '10/21/2013', '3:03 PM', '$724.17'], ['3323', '10/21/2013', '3:00 PM', '$23.71'], ['3322', '10/21/2013', '2:49 PM', '$8345.23'], ['3321', '10/21/2013', '2:23 PM', '$245.12'], ['3320', '10/21/2013', '2:15 PM', '$5663.54'], ['3319', '10/21/2013', '2:13 PM', '$943.45']]
  	}],
  	componentDidMount: function componentDidMount() {
  		Morris.Bar({
  			element: 'morris-bar-chart',
  			data: [{
  				y: '2006',
  				a: 100,
  				b: 90
  			}, {
  				y: '2007',
  				a: 75,
  				b: 65
  			}, {
  				y: '2008',
  				a: 50,
  				b: 40
  			}, {
  				y: '2009',
  				a: 75,
  				b: 65
  			}, {
  				y: '2010',
  				a: 50,
  				b: 40
  			}, {
  				y: '2011',
  				a: 75,
  				b: 65
  			}, {
  				y: '2012',
  				a: 100,
  				b: 90
  			}],
  			xkey: 'y',
  			ykeys: ['a', 'b'],
  			labels: ['Series A', 'Series B'],
  			hideHover: 'auto',
  			resize: true
  		});
  	},
  	menu: [{
  		text: 'Action',
  		url: '#',
  		separated: false
  	}, {
  		text: 'Another action',
  		url: '#',
  		separated: false
  	}, {
  		text: 'Something else here',
  		url: '#',
  		separated: false
  	}, {
  		text: 'Separated link',
  		url: '#',
  		separated: true
  	}]
  }];
  
  var Profit = (function () {
  	function Profit() {
  		_classCallCheck(this, _Profit);
  	}
  
  	_createClass(Profit, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _Profit = Profit;
  	Profit = (0, _decoratorsWithStyles2['default'])(_ProfitCss2['default'])(Profit) || Profit;
  	return Profit;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent1: dataContent1,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Profit.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 91 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _ReceivableCss = __webpack_require__(40);
  
  var _ReceivableCss2 = _interopRequireDefault(_ReceivableCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Receivable',
  	additionalTitle: '',
  	subtitle: ['Report', 'Receivable'],
  	breadcrumbsIcon: 'fa-table',
  	toolbar: [{
  		type: "button",
  		name: "btnPrint",
  		text: "Print",
  		cls: "btn-success",
  		event: function event(e) {
  			e.preventDefault();
  		}
  	}]
  };
  var dataContent1 = [];
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'Receivable',
  	name: 'table-receivable',
  	iconHeader: 'fa-shopping-cart',
  	typeTable: 'table-responsive',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-receivable").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/receivable",
  				"columns": [{ "title": "Receivable ID", "className": "salesOrderId", "data": "salesOrderId", "visible": false }, { "title": "Order No", "className": "orderNo", "data": "orderNo" }, { "title": "Staff", "className": "staffId", "data": "staff",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.staffName : "";
  					}
  				}, { "title": "Customer", "className": "customerId", "data": "customer",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.customerName : "";
  					}
  				}, { "title": "Order Date", "className": "orderDate", "data": "orderDate",
  					"render": function render(data, type, full, meta) {
  						return moment(data).format(_coreApps2['default'].dateFormat);
  					}
  				}, { "title": "Status", "className": "status", "data": "status" }, { "title": "Note", "className": "note", "data": "note" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "salesOrderId",
  					"render": function render(data, type, full, meta) {
  						_coreApps2['default'].props[data] = full.details;
  						return "<label class='viewDetail' data-attr=" + data + "><span class='glyphicon glyphicon-eye-open'></span> Detail</label> &nbsp;" + " <a class='btn btn-default iEdit' href='receivable-input?id=" + data + "'>Edit</a>" + " <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>" + " <a class='btn btn-info iPrint' data-attr=" + data + ">Print</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-receivable").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var salesOrderId = $(this).attr('data-attr');
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/receivable/' + salesOrderId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Delete Receivable");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  							});
  						});
  					});
  					$(".iPrint").click(function (e) {
  						var salesOrderId = $(this).attr('data-attr');
  						$.ajax({
  							url: _coreApps2['default'].host + '/v1/receivable/' + salesOrderId + '/print',
  							type: "GET"
  						}).done(function () {
  							console.log('File download a success!');
  						}).fail(function () {
  							console.log('File download failed!');
  						});
  					});
  					$('label.viewDetail').css('cursor', 'pointer').click(function (e) {
  						var selected = $(this).attr('data-attr'),
  						    data = _coreApps2['default'].props[selected];
  						var row = "";
  						for (var i in data) {
  							row += '<tr>' + '<td>' + (parseInt(i) + 1) + '</td>' + '<td>' + data[i].product.productName + '</td>' + '<td>' + data[i].quantity + '</td>' + '<td>' + FormatToCurrency(data[i].price) + '</td>' + '</tr>';
  						}
  						$('.modal-title').html('Order Detail');
  						$('.modal-body').html('<div class="container-fluid">' + '<div class="row">' + '<table class="table">' + '<thead>' + '<tr>' + '<th>No</th>' + '<th>Product</th>' + '<th>Quantity</th>' + '<th>Price</th>' + '</tr>' + '</thead>' + '<tbody>' + row + '</tbody>' + '</table>' + '</div>' + '</div>');
  						$('.modal-footer').html("");
  						$('#popup').modal('show');
  					});
  				}
  			});
  		}
  
  		// loadTable();
  	}
  }];
  
  var Receivable = (function () {
  	function Receivable() {
  		_classCallCheck(this, _Receivable);
  	}
  
  	_createClass(Receivable, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _Receivable = Receivable;
  	Receivable = (0, _decoratorsWithStyles2['default'])(_ReceivableCss2['default'])(Receivable) || Receivable;
  	return Receivable;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent1: dataContent1,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Receivable.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 92 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _RegisterPageCss = __webpack_require__(41);
  
  var _RegisterPageCss2 = _interopRequireDefault(_RegisterPageCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataField = [{
  	title: 'Sign Up',
  	cls: 'col-md-4 col-md-offset-4',
  	componentDidMount: function componentDidMount() {
  		QueryString = (function () {
  			// This function is anonymous, is executed immediately and
  			// the return value is assigned to QueryString!
  			var query_string = {};
  			var query = window.location.search.substring(1);
  			var vars = query.split("&");
  			for (var i = 0; i < vars.length; i++) {
  				var pair = vars[i].split("=");
  				// If first entry with this name
  				if (typeof query_string[pair[0]] === "undefined") {
  					query_string[pair[0]] = decodeURIComponent(pair[1]);
  					// If second entry with this name
  				} else if (typeof query_string[pair[0]] === "string") {
  						var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
  						query_string[pair[0]] = arr;
  						// If third or later entry with this name
  					} else {
  							query_string[pair[0]].push(decodeURIComponent(pair[1]));
  						}
  			}
  			return query_string;
  		})();
  
  		_apiRestCall2['default'].performRequest('/v1/role', 'GET', {}, function (data) {
  			if (data.status == "OK") {
  				var role = data.data;
  				$.map(role || [], function (val, i) {
  					$('#roleMenu').append('<option value=' + val.roleId + '>' + val.roleName + '</option>');
  				});
  			}
  		});
  		_apiRestCall2['default'].performRequest('/v1/staff', 'GET', {}, function (data) {
  			if (data.status == "OK") {
  				var staff = data.data;
  				$.map(staff || [], function (val, i) {
  					$('#staff').append('<option value=' + val.staffId + '>' + val.staffName + '</option>');
  				});
  			}
  		});
  
  		if (typeof QueryString.id != 'undefined') {
  			$.ajax({
  				url: _coreApps2['default'].host + "/v1/user/" + QueryString.id,
  				type: 'GET',
  				dataType: 'json',
  				cache: false,
  				beforeSend: function beforeSend(xhr) {
  					xhr.setRequestHeader("Authorization", reactCookie.load('auth'));
  				},
  				success: function success(data) {
  					if (data.status == "OK") {
  						var data = data.data;
  						console.log(data);
  						$('#userId').val(data.userId);
  						$('#username').val(data.username);
  						$('#email').val(data.email);
  						$('#roleMenu').val(data.role.roleId);
  						$('#staff').val(data.staff.staffId);
  						$('#verified').val(data.isVerified ? 1 : 0);
  					}
  				}
  			});
  		}
  	},
  	items: [{
  		name: 'userId',
  		type: 'hidden'
  	}, {
  		text: 'Username',
  		name: 'username',
  		type: 'text'
  	}, {
  		text: 'E-mail',
  		name: 'email',
  		type: 'email'
  	}, {
  		text: 'Password',
  		name: 'password',
  		type: 'password'
  	}, {
  		text: 'Confirm Password',
  		name: 'confirmPassword',
  		type: 'password'
  	}, {
  		name: 'roleMenu',
  		type: 'select',
  		opt_text: '-- SELECT ROLE MENU --',
  		options: []
  	}, {
  		name: 'staff',
  		type: 'select',
  		opt_text: '-- SELECT ROLE STAFF --',
  		options: []
  	}, {
  		name: 'verified',
  		type: 'select',
  		display: 'none',
  		opt_text: '-- SELECT Verified STAFF --',
  		options: [{
  			text: 'Yes',
  			value: 1
  		}, {
  			text: 'No',
  			value: 0
  		}]
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		text: 'Register',
  		url: '/',
  		event: function event(e) {
  			e.preventDefault();
  			var userId = document.getElementById('userId').value,
  			    data = {
  				username: document.getElementById('username').value,
  				email: document.getElementById('email').value,
  				password: document.getElementById('password').value,
  				roleId: document.getElementById('roleMenu').value,
  				staffId: document.getElementById('staff').value
  			};
  
  			if (data.password != document.getElementById('confirmPassword').value) {
  				alert('Password is Deference with Confirm!');
  			} else {
  				if (userId == "") {
  					_apiRestCall2['default'].performRequest('/v1/user', 'POST', data, function (data) {
  						if (data.status == "OK") {
  							if (data.data) {
  								alert(data.status);
  								window.location.assign('/');
  							}
  						}
  					});
  				} else {
  					_apiRestCall2['default'].performRequest('/v1/user/' + userId, 'PUT', data, function (data) {
  						if (data.status == "OK") {
  							if (data.data) {
  								alert(data.status);
  								window.location.assign('/');
  							}
  						}
  					});
  				}
  			}
  		},
  		type: 'button',
  		cls: 'btn btn-lg btn-success btn-block'
  	}]
  }];
  
  var RegisterPage = (function () {
  	function RegisterPage() {
  		_classCallCheck(this, _RegisterPage);
  	}
  
  	_createClass(RegisterPage, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _RegisterPage = RegisterPage;
  	RegisterPage = (0, _decoratorsWithStyles2['default'])(_RegisterPageCss2['default'])(RegisterPage) || RegisterPage;
  	return RegisterPage;
  })();
  
  var Content = _coreApps2['default'].Users({ dataField: dataField });
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "RegisterPage.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 93 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _RoleCss = __webpack_require__(42);
  
  var _RoleCss2 = _interopRequireDefault(_RoleCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Role',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['Access User', 'Role']
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'Role',
  	name: 'table-role',
  	panel: false,
  	loadAll: true,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	bodyAttr: '',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-role").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/role",
  				"columns": [{ "title": "Role ID", "className": 'roleId', "data": "roleId", "visible": false }, { "title": "Role Name", "className": 'roleName', "data": "roleName" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "roleId",
  					"render": function render(data, type, full, meta) {
  						return "<a class='btn btn-default iEdit' data-attr=" + data + ">Edit</a> <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					event();
  					$("#table-role").closest('.table-responsive').css('overflow', 'hidden');
  				}
  			});
  		}
  
  		function reset() {
  			$("#roleID").val("");
  			$("#roleName").val("");
  			$('#errorDisplay').text("");
  			$(':input[required], select[required]').trigger("change");
  		}
  
  		function event() {
  			$("#btnNew").off('click').click(function (e) {
  				reset();
  				$(':input[required], select[required]').trigger("change");
  			});
  			$("#btnSave").off('click').click(function (e) {
  				e.preventDefault();
  				var roleIdParam = $('#roleID').val(),
  				    error = $('#errorDisplay'),
  				    boolVal = true,
  				    data = { roleName: $('#roleName').val() };
  
  				$.each($(':input[required], select[required]'), function (value, index) {
  					if ($(this).val() == "") {
  						boolVal = false;
  						return;
  					}
  				});
  
  				if (!boolVal) return;
  
  				if (roleIdParam == "") {
  					_apiRestCall2['default'].performRequest('/v1/role', 'POST', data, function (data) {
  						if (typeof data.error != 'undefined') {
  							error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  							error.css('display', 'block');
  						} else {
  							if (data.status == "ERROR") {
  								error.html(data.message);
  								error.css('display', 'block');
  							} else {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Save Role");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  								reset();
  							}
  						}
  					});
  				} else {
  					_apiRestCall2['default'].performRequest('/v1/role/' + roleIdParam, 'PUT', data, function (data) {
  						if (typeof data.error != 'undefined') {
  							error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  							error.css('display', 'block');
  						} else {
  							if (data.status == "ERROR") {
  								error.html(data.message);
  								error.css('display', 'block');
  							} else {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Update Role");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  								reset();
  							}
  						}
  					});
  				}
  			});
  
  			$(".iEdit").off('click').click(function (e) {
  				e.preventDefault();
  				var roleIdEdit = $(this).attr('data-attr'),
  				    roleNameEdit = $(this).closest('tr').find('td.roleName').text();
  				$('html, body').animate({
  					scrollTop: $('form#formRole').offset().top
  				}, 'slow');
  				$('#roleID').val(roleIdEdit);
  				$('#roleName').val(roleNameEdit);
  				$(':input[required], select[required]').trigger("change");
  			});
  
  			$(".iDelete").off('click').click(function (e) {
  				e.preventDefault();
  				var roleId = $(this).attr('data-attr'),
  				    roleName = $(this).closest('tr').find('td.roleName').text();
  				$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> Role ' + roleName + '?');
  				$("#PopUpConfirm").modal("show");
  				$("#PopUpConfirm #btnYes").click(function (e) {
  					_apiRestCall2['default'].performRequest('/v1/role/' + roleId, 'DELETE', {}, function (data) {
  						$('html, body').animate({
  							scrollTop: $('body').offset().top
  						}, 'slow');
  						$("#PopUpConfirm").modal("hide");
  						$("#alert #alert-title").html("Success");
  						$("#alert #alert-content").html("Delete Role");
  						$("#alert").addClass('alert-success').show();
  						$("#alert").fadeOut(2000);
  						datatable.destroy();
  						loadTable();
  						reset();
  					});
  				});
  			});
  		}
  
  		loadTable();
  	}
  }, {
  	mode: 'Input',
  	name: 'formRole',
  	loadAll: true,
  	title: 'Add Role',
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	items: [{
  		value: '',
  		name: 'roleID',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Role Name',
  		name: 'roleName',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'New',
  			type: 'reset',
  			name: 'btnNew',
  			cls: 'btn-default'
  		}]
  	}]
  }];
  
  var Role = (function () {
  	function Role() {
  		_classCallCheck(this, _Role);
  	}
  
  	_createClass(Role, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _Role = Role;
  	Role = (0, _decoratorsWithStyles2['default'])(_RoleCss2['default'])(Role) || Role;
  	return Role;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Role.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 94 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _SalesInvoiceInputCss = __webpack_require__(43);
  
  var _SalesInvoiceInputCss2 = _interopRequireDefault(_SalesInvoiceInputCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataSalesInvoice = {};
  var dataPageHeading = {
  	title: 'Sales Invoice',
  	additionalTitle: ' Input',
  	subtitle: ['Sales', 'Sales Invoice', 'Sales Invoice Input'],
  	breadcrumbsIcon: 'fa-pencil'
  };
  
  function setValue(SalesInvoiceID, InvoiceNo, Shipping, StaffId, StaffName, InvoiceDate, Note) {
  	$('#salesInvoiceId').val(SalesInvoiceID);
  	$('#invoiceNo').val(InvoiceNo);
  	if (Shipping == "") $('#shipping').prop('selectedIndex', 0);else $('#shipping').val(Shipping);
  	$('#staff').val(StaffName).attr('data-id', StaffId);
  	if (InvoiceDate != "") {
  		InvoiceDate = InvoiceDate.split("-");
  		$("#invoiceDate.datepicker").datepicker('update', new Date(InvoiceDate[0], InvoiceDate[1] - 1, InvoiceDate[2].substr(0, 2)));
  	} else {
  		$("#invoiceDate.datepicker").val("");
  	}
  	$('#note').val(Note);
  	$('#errorDisplay').text("");
  	$(':input[required], select[required]').trigger("change");
  }
  
  var dataContent1 = [];
  var dataContent2 = [{
  	mode: 'Input',
  	name: 'formSalesInvoiceInput',
  	title: 'Add Sales Invoice',
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		_apiRestCall2['default'].performRequest('/v1/shipping-order', 'GET', {}, function (data) {
  			if (data.status == "OK") {
  				var shippingOrder = data.data;
  				$.map(shippingOrder || [], function (val, i) {
  					$('#shipping').append('<option value=' + val.shippingOrderId + '>' + val.shippingNo + '</option>');
  				});
  			}
  		});
  		if (typeof QueryString.id != 'undefined') {
  			$("#invoiceNo").closest(".form-group").show();
  			$.ajax({
  				url: _coreApps2['default'].host + "/v1/sales-invoice/" + QueryString.id,
  				type: 'GET',
  				dataType: 'json',
  				cache: false,
  				success: function success(data) {
  					if (data.status == "OK") {
  						var data = data.data;
  						setValue(data.salesInvoiceId, data.invoiceNo, data.shippingOrder.shippingOrderId, data.staff.staffId, data.staff.staffName, data.invoiceDate, data.note);
  
  						// dataSalesInvoice.invoiceNo = data.invoiceNo;
  						dataSalesInvoice.shippingOrderId = data.shippingOrder.shippingOrderId;
  						dataSalesInvoice.staffId = data.staff.staffId;
  						dataSalesInvoice.invoiceDate = moment(data.invoiceDate).format("YYYY-MM-DD");
  						dataSalesInvoice.note = data.note;
  					}
  				}
  			});
  		}
  
  		// Autocomplete Staff
  		_apiRestCall2['default'].performRequest('/v1/staff', 'GET', {}, function (data) {
  			var autocomplete = [],
  			    onSelected = false;
  			if (data.status == "OK") {
  				for (var i in data.data) autocomplete.push({ id: data.data[i].staffId, value: data.data[i].staffName });
  			}
  			var bloodhound = new Bloodhound({
  				datumTokenizer: function datumTokenizer(datum) {
  					return Bloodhound.tokenizers.whitespace(datum.value);
  				},
  				queryTokenizer: Bloodhound.tokenizers.whitespace,
  				local: autocomplete
  			});
  
  			// Initialize the Bloodhound suggestion engine
  			bloodhound.initialize().done(function () {
  				$('#staff').typeahead({
  					highlight: true
  				}, {
  					templates: {
  						empty: ['<div class="tt-suggestion">', 'No Items Found', '</div>'].join('\n'),
  						suggestion: function suggestion(data) {
  							// return '<div>' + data.id + ' – ' + data.value + '</div>'
  							return '<div data-attr=' + data.id + '>' + data.value + '</div>';
  						}
  					},
  					displayKey: 'value',
  					source: bloodhound.ttAdapter()
  				}).on("typeahead:selected", function (e, datum) {
  					dataSalesInvoice.staffId = datum.id;
  					$(this).closest('span').siblings().remove();
  					onSelected = true;
  				}).on("change", function (e) {
  					if (!onSelected) dataSalesInvoice.staffId = $(".tt-suggestion:first").attr("data-attr");
  					onSelected = false;
  					if ($(this).val() != "") $(this).closest('span').siblings().remove();
  				}).on("keydown", function (e) {
  					if (e.keyCode == 9) {
  						// TABKEY
  						if (!onSelected) dataSalesInvoice.staffId = $(".tt-suggestion:first").attr("data-attr");
  						onSelected = false;
  						if ($(this).val() != "") $(this).closest('span').siblings().remove();
  					}
  				});
  			});
  		});
  
  		$("#btnSave").click(function (e) {
  			e.preventDefault();
  			var salesInvoiceId = $('#salesInvoiceId').val(),
  			    boolVal = true,
  			    error = $('#errorDisplay');
  
  			// dataSalesInvoice.invoiceNo = $('#invoiceNo').val();
  			dataSalesInvoice.shippingOrderId = $('#shipping').val();
  			dataSalesInvoice.invoiceDate = moment($('#invoiceDate').val()).format("YYYY-MM-DD");
  			dataSalesInvoice.note = $('#note').val();
  
  			$.each($(':input[required], select[required]'), function (value, index) {
  				if ($(this).val() == "") {
  					boolVal = false;
  					return;
  				}
  			});
  
  			if (!boolVal) return;
  
  			if (salesInvoiceId == "") {
  				_apiRestCall2['default'].performRequest('/v1/sales-invoice', 'POST', dataSalesInvoice, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							$('html, body').animate({
  								scrollTop: $('body').offset().top
  							}, 'slow');
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Save Sales Invoice");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							setValue("", "", "", "", "", "", "");
  						}
  					}
  				});
  			} else {
  				_apiRestCall2['default'].performRequest('/v1/sales-invoice/' + salesInvoiceId, 'PUT', dataSalesInvoice, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							window.location.assign('/sales-invoice');
  						}
  					}
  				});
  			}
  		});
  	},
  	items: [{
  		value: '',
  		name: 'salesInvoiceId',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Invoice No',
  		name: 'invoiceNo',
  		disabled: 'disabled',
  		display: 'none',
  		type: 'text',
  
  		cols: [2, 10]
  	}, {
  		text: 'Shipping',
  		name: 'shipping',
  		type: 'select',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Staff',
  		name: 'staff',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Invoice Date',
  		name: 'invoiceDate',
  		type: 'datetime',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Note',
  		name: 'note',
  		rows: 5,
  		type: 'area',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			ts: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'Back',
  			type: 'button',
  			name: 'btnBack',
  			cls: 'btn-default',
  			event: function event() {
  				window.location.assign("/sales-invoice");
  			}
  		}]
  	}]
  }];
  
  var SalesInvoiceInput = (function () {
  	function SalesInvoiceInput() {
  		_classCallCheck(this, _SalesInvoiceInput);
  	}
  
  	_createClass(SalesInvoiceInput, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _SalesInvoiceInput = SalesInvoiceInput;
  	SalesInvoiceInput = (0, _decoratorsWithStyles2['default'])(_SalesInvoiceInputCss2['default'])(SalesInvoiceInput) || SalesInvoiceInput;
  	return SalesInvoiceInput;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent1: dataContent1,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "SalesInvoiceInput.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 95 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _SalesInvoiceCss = __webpack_require__(44);
  
  var _SalesInvoiceCss2 = _interopRequireDefault(_SalesInvoiceCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Sales Invoice',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['Sales', 'Sales Invoice'],
  	toolbar: [{
  		type: "button",
  		name: "btnAdd",
  		text: "Add",
  		cls: "btn-primary",
  		event: function event(e) {
  			window.location.assign("/sales-invoice-input");
  		}
  	}]
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'User',
  	name: 'table-sales-invoice',
  	panel: false,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-sales-invoice").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/sales-invoice",
  				"columns": [{ "title": "Sales Invoice ID", "className": "salesInvoiceId", "data": "salesInvoiceId", "visible": false }, { "title": "Invoice No", "className": "invoiceNo", "data": "invoiceNo" }, { "title": "Staff", "className": "staffId", "data": "staff",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.staffName : "";
  					}
  				}, { "title": "Invoice Date", "className": "invoiceDate", "data": "invoiceDate",
  					"render": function render(data, type, full, meta) {
  						return moment(data).format(_coreApps2['default'].dateFormat);
  					}
  				}, { "title": "Status", "className": "status", "data": "status" }, { "title": "Note", "className": "note", "data": "note" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "salesInvoiceId",
  					"render": function render(data, type, full, meta) {
  						_coreApps2['default'].props[data] = full.shippingOrder;
  						return "<label class='viewDetail' data-attr=" + data + "><span class='glyphicon glyphicon-eye-open'></span> Detail</label> &nbsp;" + " <a class='btn btn-default iEdit' href='sales-invoice-input?id=" + data + "'>Edit</a>" + " <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>" + " <a class='btn btn-info iPrint' data-attr=" + data + ">Print</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-sales-invoice").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var salesInvoiceId = $(this).attr('data-attr');
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/sales-invoice/' + salesInvoiceId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								if (typeof data.error != 'undefined') {
  									$("#alert #alert-title").html("Error");
  									$("#alert #alert-content").html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  									$("#alert").addClass('alert-danger').show();
  									$("#alert").fadeOut(2000);
  								} else {
  									$("#alert #alert-title").html("Success");
  									$("#alert #alert-content").html("Delete Sales Invoice");
  									$("#alert").addClass('alert-success').show();
  									$("#alert").fadeOut(2000);
  									datatable.destroy();
  									loadTable();
  								}
  							});
  						});
  					});
  					$(".iPrint").click(function (e) {
  						var salesInvoiceId = $(this).attr('data-attr');
  						window.location.assign('/sales-invoice/' + salesInvoiceId + '/print');
  					});
  					$('label.viewDetail').css('cursor', 'pointer').click(function (e) {
  						var selected = $(this).attr('data-attr'),
  						    data = _coreApps2['default'].props[selected];
  						var row = "";
  						row += '<tr>' + '<td>' + data.shippingOrderId + '</td>' + '<td>' + moment(data.shippingDate).format(_coreApps2['default'].dateFormat) + '</td>' + '<td>' + data.status + '</td>' + '</tr>';
  						$('.modal-title').html('Shipping Detail');
  						$('.modal-body').html('<div class="container-fluid">' + '<div class="row">' + '<table class="table">' + '<thead>' + '<tr>' + '<th>Shipping No</th>' + '<th>Shipping Date</th>' + '<th>Status</th>' + '</tr>' + '</thead>' + '<tbody>' + row + '</tbody>' + '</table>' + '</div>' + '</div>');
  						$('.modal-footer').html("");
  						$('#popup').modal('show');
  					});
  				}
  			});
  		}
  
  		loadTable();
  	}
  }];
  
  var SalesInvoice = (function () {
  	function SalesInvoice() {
  		_classCallCheck(this, _SalesInvoice);
  	}
  
  	_createClass(SalesInvoice, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _SalesInvoice = SalesInvoice;
  	SalesInvoice = (0, _decoratorsWithStyles2['default'])(_SalesInvoiceCss2['default'])(SalesInvoice) || SalesInvoice;
  	return SalesInvoice;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "SalesInvoice.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 96 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _SalesOrderInputCss = __webpack_require__(45);
  
  var _SalesOrderInputCss2 = _interopRequireDefault(_SalesOrderInputCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataSalesOrder = {};
  var dataPageHeading = {
  	title: 'Sales Order',
  	additionalTitle: ' Input',
  	subtitle: ['Sales', 'Sales Order', 'Sales Order Input'],
  	breadcrumbsIcon: 'fa-pencil'
  };
  
  function setValue(SalesOrderId, OrderNo, StaffId, StaffName, CustomerId, CustomerName, OrderDate, Note) {
  	$('#salesOrderID').val(SalesOrderId);
  	$('#orderNo').val(OrderNo);
  	$('#staff').val(StaffName).attr('data-id', StaffId);
  	$('#customer').val(CustomerName).attr('data-id', CustomerId);
  	if (OrderDate != "") {
  		OrderDate = OrderDate.split("-");
  		$("#orderDate.datepicker").datepicker('update', new Date(OrderDate[0], OrderDate[1] - 1, OrderDate[2].substr(0, 2)));
  	} else {
  		$("#orderDate.datepicker").val("");
  	}
  	$('#note').val(Note);
  	$('#tableSalesOrderDetail tbody').remove();
  	$('#errorDisplay').text("");
  	$(':input[required], select[required]').trigger("change");
  }
  
  var dataContent1 = [];
  var dataContent2 = [{
  	mode: 'Input',
  	name: 'formSalesOrderInput',
  	title: 'Add Sales Order',
  	loadAll: true,
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		if (typeof QueryString.id != 'undefined') {
  			$("#orderNo").closest(".form-group").show();
  			$.ajax({
  				url: _coreApps2['default'].host + "/v1/sales-order/" + QueryString.id,
  				type: 'GET',
  				dataType: 'json',
  				cache: false,
  				success: function success(data) {
  					if (data.status == "OK") {
  						$("#orderNo").prop("disabled", true);
  						var data = data.data,
  						    details = data.details;
  						setValue(data.salesOrderId, data.orderNo, data.staff.staffId, data.staff.staffName, data.customer.customerId, data.customer.customerName, data.orderDate, data.note);
  
  						// dataSalesOrder.orderNo = data.orderNo;
  						dataSalesOrder.staffId = data.staff.staffId;
  						dataSalesOrder.customerId = data.staff.customerId;
  						dataSalesOrder.orderDate = moment(data.orderDate).format("YYYY-MM-DD");
  						dataSalesOrder.note = data.note;
  
  						for (var i in details) {
  							_coreApps2['default'].props.push({ productId: details[i].productId, productName: details[i].product.productName, quantity: details[i].quantity, price: details[i].price });
  						}
  					}
  				},
  				async: false
  			});
  		}
  		// Autocomplete Staff
  		_apiRestCall2['default'].performRequest('/v1/staff', 'GET', {}, function (data) {
  			var autocomplete = [],
  			    onSelected = false;
  			if (data.status == "OK") {
  				for (var i in data.data) autocomplete.push({ id: data.data[i].staffId, value: data.data[i].staffName });
  			}
  			var bloodhound = new Bloodhound({
  				datumTokenizer: function datumTokenizer(datum) {
  					return Bloodhound.tokenizers.whitespace(datum.value);
  				},
  				queryTokenizer: Bloodhound.tokenizers.whitespace,
  				local: autocomplete
  			});
  
  			// Initialize the Bloodhound suggestion engine
  			bloodhound.initialize().done(function () {
  				$('#staff').typeahead({
  					highlight: true
  				}, {
  					templates: {
  						empty: ['<div class="tt-suggestion">', 'No Items Found', '</div>'].join('\n'),
  						suggestion: function suggestion(data) {
  							// return '<div>' + data.id + ' – ' + data.value + '</div>'
  							return '<div data-attr=' + data.id + '>' + data.value + '</div>';
  						}
  					},
  					displayKey: 'value',
  					source: bloodhound.ttAdapter()
  				}).on("typeahead:selected", function (e, datum) {
  					dataSalesOrder.staffId = datum.id;
  					$(this).closest('span').siblings().remove();
  					onSelected = true;
  				}).on("change", function (e) {
  					if (!onSelected) dataSalesOrder.staffId = $(".tt-suggestion:first").attr("data-attr");
  					onSelected = false;
  					if ($(this).val() != "") $(this).closest('span').siblings().remove();
  				}).on("keydown", function (e) {
  					if (e.keyCode == 9) {
  						// TABKEY
  						if (!onSelected) dataSalesOrder.staffId = $(".tt-suggestion:first").attr("data-attr");
  						onSelected = false;
  						if ($(this).val() != "") $(this).closest('span').siblings().remove();
  					}
  				});
  			});
  		});
  
  		_apiRestCall2['default'].performRequest('/v1/customer', 'GET', {}, function (data) {
  			var autocomplete = [],
  			    onSelected = false;
  			if (data.status == "OK") {
  				for (var i in data.data) autocomplete.push({ id: data.data[i].customerId, value: data.data[i].customerName });
  			}
  			var bloodhound = new Bloodhound({
  				datumTokenizer: function datumTokenizer(datum) {
  					return Bloodhound.tokenizers.whitespace(datum.value);
  				},
  				queryTokenizer: Bloodhound.tokenizers.whitespace,
  				local: autocomplete
  			});
  
  			// Initialize the Bloodhound suggestion engine
  			bloodhound.initialize().done(function () {
  				$('#customer').typeahead({
  					highlight: true
  				}, {
  					templates: {
  						empty: ['<div class="tt-suggestion">', 'No Items Found', '</div>'].join('\n'),
  						suggestion: function suggestion(data) {
  							// return '<div>' + data.id + ' – ' + data.value + '</div>'
  							return '<div>' + data.value + '</div>';
  						}
  					},
  					displayKey: 'value',
  					source: bloodhound.ttAdapter()
  				}).on("typeahead:selected", function (e, datum) {
  					dataSalesOrder.customerId = datum.id;
  					$(this).closest('span').siblings().remove();
  					onSelected = true;
  				}).on("change", function (e) {
  					if (!onSelected) dataSalesOrder.customerId = $(".tt-suggestion:first").attr("data-attr");
  					onSelected = false;
  					if ($(this).val() != "") $(this).closest('span').siblings().remove();
  				}).on("keydown", function (e) {
  					if (e.keyCode == 9) {
  						// TABKEY
  						if (!onSelected) dataSalesOrder.customerId = $(".tt-suggestion:first").attr("data-attr");
  						onSelected = false;
  						if ($(this).val() != "") $(this).closest('span').siblings().remove();
  					}
  				});
  			});
  		});
  	},
  	items: [{
  		value: '',
  		name: 'salesOrderID',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Order No',
  		name: 'orderNo',
  		type: 'text',
  		disabled: 'disabled',
  		display: 'none',
  		cols: [2, 10]
  	}, {
  		text: 'Staff Name',
  		name: 'staff',
  		type: 'autocomplete',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Customer Name',
  		name: 'customer',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Order Date',
  		name: 'orderDate',
  		type: 'datetime',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Note',
  		name: 'note',
  		rows: 5,
  		type: 'area',
  		required: 'required',
  		cols: [2, 10]
  	}]
  }, {
  	mode: 'Table',
  	name: 'tableSalesOrderDetail',
  	cls: 'col-lg-12',
  	loadAll: true,
  	typeTable: 'table-responsive',
  	componentDidMount: function componentDidMount() {
  		function loadTable() {
  			var rows = "",
  			    picture_default = "placeholder-640x480.png";
  			$("#tableSalesOrderDetail").html("<thead></thead><tbody></tbody><tfoot></tfoot>");
  			$("#tableSalesOrderDetail thead").html("<tr>" + "<th>Product</th>" + "<th>Quantity</th>" + "<th>Price</th>" + "<th>Action</th>" + "</tr>");
  			if (_coreApps2['default'].props.length) {
  				for (var i in _coreApps2['default'].props) rows += "<tr id='row-" + i + "'>" + "<td id=" + _coreApps2['default'].props[i].productId + ">" + _coreApps2['default'].props[i].productName + "</td>" + "<td>" + _coreApps2['default'].props[i].quantity + "</td>" + "<td>" + FormatToCurrency(_coreApps2['default'].props[i].price) + "</td>" + "<td><a class='btn btn-danger iDeleteDetail' data-attr='" + i + "'>Delete</a></td>" + "</tr>";
  			}
  			$("#tableSalesOrderDetail tbody").html(rows);
  			$("#tableSalesOrderDetail tfoot").html("<tr><td colspan='4'><button class='btn btn-primary'>Browse</button></td></tr>");
  
  			$("a.iDeleteDetail").click(function (e) {
  				var index = $(this).attr('data-attr'),
  				    productIdParam = $(this).closest('td').siblings(':first').attr('id');
  				_coreApps2['default'].props = $.grep(_coreApps2['default'].props || [], function (value, index) {
  					return value.productId != productIdParam;
  				});
  				$("tr#row-" + index).remove();
  			});
  
  			$("#tableSalesOrderDetail tfoot button").click(function () {
  				$('.modal-title').html('Product Detail');
  				$('.modal-body').html('<table class="table table-hover" id="tblProduct"></table>');
  				var otable = $("#tblProduct").DataTable({
  					"retrieve": true,
  					"bAutoWidth": true,
  					"bProcessing": true,
  					"bServerSide": false,
  					"sAjaxSource": _coreApps2['default'].host + "/v1/product",
  					"columns": [{ "title": "Product ID", "className": "productId", "data": "productId", "visible": false }, { "title": "Product Name", "className": "productName", "data": "productName",
  						"render": function render(data, type, full, meta) {
  							return "<label id='" + full.productId + "'>" + data + "</label>";
  						}
  					}, { "title": "Category", "className": "productCategoryId", "data": "productCategory",
  						"render": function render(data, type, full, meta) {
  							return data != null ? data.productCategoryName : "";
  						}
  					}, { "title": "Initial Stock", "className": "initialStock", "data": "initialStock" }, { "title": "Currnet Stock", "className": "currentStock", "data": "currentStock" }, { "title": "Sell Price", "className": "sellPrice", "data": "sellPrice", "type": "num-fmt",
  						"render": function render(data, type, full, meta) {
  							return FormatToCurrency(data);
  						}
  					}, { "title": "Status", "className": "status", "data": "status" }, { "title": "Image", "className": "productPhoto", "data": "productPhoto",
  						"render": function render(data, type, full, meta) {
  							var link;
  							return "<img src=" + _coreApps2['default'].defaultDirectory + (data == null ? picture_default : data) + " width=100 height=100 />";
  						}
  					}],
  					"fnDrawCallback": function fnDrawCallback() {
  						$("#tblProduct tbody tr").off('click').click(function (e) {
  							e.preventDefault();
  							var productId = $(this).find('.productName label').attr('id'),
  							    productName = $(this).find('.productName label').text();
  							_coreApps2['default'].item = {};
  							_coreApps2['default'].item.productId = productId;
  							_coreApps2['default'].item.productName = productName;
  							if ($(this).hasClass('selected')) {
  								$(this).removeClass('selected');
  							} else {
  								$('#tblProduct tbody tr.selected').removeClass('selected');
  								$(this).addClass('selected');
  							}
  						});
  					}
  				});
  				$('#popup').modal('show');
  				$('.modal-body').css({ 'overflow-y': 'scroll', 'height': '500px' }).html($("#tblProduct"));
  				$('.modal-body').append('<form role="form" class="form-horizontal" id="frmProduct">' + '<div class="form-group">' + '<label class="control-label col-sm-2">Quantity</label>' + '<div class="col-sm-10">' + '<input class="form-control" type="number" name="quantityDetail" id="quantityDetail" placeholder="Quantity" value=0 />' + '</div>' + '</div>' + '<div class="form-group">' + '<label class="control-label col-sm-2">Price</label>' + '<div class="col-sm-10">' + '<input class="form-control" type="number" name="priceDetail" id="priceDetail" placeholder="Price" value=0 />' + '</div>' + '</div>' + '</form>');
  				$("#btnSubmitPopup").off('click').click(function (e) {
  					var elQuantity = $("#frmProduct #quantityDetail")[1];
  					var elPrice = $("#frmProduct #priceDetail")[1];
  					_coreApps2['default'].item.quantity = $(elQuantity).val();
  					_coreApps2['default'].item.price = $(elPrice).val();
  					var flag = true;
  					$.map(_coreApps2['default'].props || [], function (value, index) {
  						if (value.productId == _coreApps2['default'].item.productId) {
  							flag = false;
  							return;
  						}
  					});
  
  					if (typeof _coreApps2['default'].item.productId == 'undefined') return;
  
  					if (flag) _coreApps2['default'].props.push(_coreApps2['default'].item);else {
  						$('html, body').animate({
  							scrollTop: $('body').offset().top
  						}, 'slow');
  						$("#alert #alert-title").html("Error");
  						$("#alert #alert-content").html("Product Already Exists");
  						$("#alert").addClass('alert-danger').show();
  						$("#alert").fadeOut(2000);
  					}
  					$('#popup').modal('hide');
  					loadTable();
  				});
  			});
  		}
  		loadTable();
  	}
  }, {
  	mode: 'Input',
  	loadAll: true,
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		$("#btnSave").click(function (e) {
  			e.preventDefault();
  			var detail;
  			detail = $.map(_coreApps2['default'].props, function (value, index) {
  				var items = [];
  				items.push({ productId: value.productId, quantity: value.quantity, price: value.price });
  				return items;
  			});
  			var salesOrderId = $("#salesOrderID").val(),
  			    boolVal = true,
  			    error = $("#errorDisplay");
  
  			$.each($(':input[required], select[required]'), function (value, index) {
  				if ($(this).val() == "") {
  					boolVal = false;
  					return;
  				}
  			});
  
  			if (!boolVal) return;
  
  			if (detail.length == 0) {
  				error.html("Product must have at least one item");
  				error.css('display', 'block');
  				return;
  			}
  
  			// dataSalesOrder.orderNo = $('#orderNo').val();
  			dataSalesOrder.orderDate = moment($('#orderDate').val()).format("YYYY-MM-DD");
  			dataSalesOrder.note = $('#note').val();
  			dataSalesOrder.details = JSON.stringify(detail);
  
  			if (salesOrderId == "") {
  				_apiRestCall2['default'].performRequest('/v1/sales-order', 'POST', dataSalesOrder, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							$('html, body').animate({
  								scrollTop: $('body').offset().top
  							}, 'slow');
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Save Sales Order");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							setValue("", "", "", "", "", "", "", "");
  						}
  					}
  				});
  			} else {
  				_apiRestCall2['default'].performRequest('/v1/sales-order/' + salesOrderId, 'PUT', dataSalesOrder, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							window.location.assign('/sales-order');
  						}
  					}
  				});
  			}
  		});
  	},
  	items: [{
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'Back',
  			type: 'button',
  			name: 'btnBack',
  			cls: 'btn-default',
  			event: function event() {
  				window.location.assign("/sales-order");
  			}
  		}]
  	}]
  }];
  
  var SalesOrderInput = (function () {
  	function SalesOrderInput() {
  		_classCallCheck(this, _SalesOrderInput);
  	}
  
  	_createClass(SalesOrderInput, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _SalesOrderInput = SalesOrderInput;
  	SalesOrderInput = (0, _decoratorsWithStyles2['default'])(_SalesOrderInputCss2['default'])(SalesOrderInput) || SalesOrderInput;
  	return SalesOrderInput;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent1: dataContent1,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "SalesOrderInput.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 97 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _SalesOrderCss = __webpack_require__(46);
  
  var _SalesOrderCss2 = _interopRequireDefault(_SalesOrderCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Sales Order',
  	additionalTitle: '',
  	subtitle: ['Sales', 'Sales Order'],
  	breadcrumbsIcon: 'fa-table',
  	toolbar: [{
  		type: "button",
  		name: "btnAdd",
  		text: "Add",
  		cls: "btn-primary",
  		event: function event(e) {
  			window.location.assign("/sales-order-input");
  		}
  	}]
  };
  var dataContent1 = [];
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'Sales Order',
  	name: 'table-sales-order',
  	iconHeader: 'fa-shopping-cart',
  	typeTable: 'table-responsive',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-sales-order").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/sales-order",
  				"columns": [{ "title": "Sales Order ID", "className": "salesOrderId", "data": "salesOrderId", "visible": false }, { "title": "Order No", "className": "orderNo", "data": "orderNo" }, { "title": "Staff", "className": "staffId", "data": "staff",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.staffName : "";
  					}
  				}, { "title": "Customer", "className": "customerId", "data": "customer",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.customerName : "";
  					}
  				}, { "title": "Order Date", "className": "orderDate", "data": "orderDate",
  					"render": function render(data, type, full, meta) {
  						return moment(data).format(_coreApps2['default'].dateFormat);
  					}
  				}, { "title": "Status", "className": "status", "data": "status" }, { "title": "Note", "className": "note", "data": "note" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "salesOrderId",
  					"render": function render(data, type, full, meta) {
  						_coreApps2['default'].props[data] = full.details;
  						return "<label class='viewDetail' data-attr=" + data + "><span class='glyphicon glyphicon-eye-open'></span> Detail</label> &nbsp;" + " <a class='btn btn-default iEdit' href='sales-order-input?id=" + data + "'>Edit</a>" + " <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>" + " <a class='btn btn-info iPrint' data-attr=" + data + ">Print</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-sales-order").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var salesOrderId = $(this).attr('data-attr');
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/sales-order/' + salesOrderId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Delete Sales Order");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  							});
  						});
  					});
  					$(".iPrint").click(function (e) {
  						var salesOrderId = $(this).attr('data-attr');
  						$.ajax({
  							url: _coreApps2['default'].host + '/v1/sales-order/' + salesOrderId + '/print',
  							type: "GET"
  						}).done(function () {
  							console.log('File download a success!');
  						}).fail(function () {
  							console.log('File download failed!');
  						});
  					});
  					$('label.viewDetail').css('cursor', 'pointer').click(function (e) {
  						var selected = $(this).attr('data-attr'),
  						    data = _coreApps2['default'].props[selected];
  						var row = "";
  						for (var i in data) {
  							row += '<tr>' + '<td>' + (parseInt(i) + 1) + '</td>' + '<td>' + data[i].product.productName + '</td>' + '<td>' + data[i].quantity + '</td>' + '<td>' + FormatToCurrency(data[i].price) + '</td>' + '</tr>';
  						}
  						$('.modal-title').html('Order Detail');
  						$('.modal-body').html('<div class="container-fluid">' + '<div class="row">' + '<table class="table">' + '<thead>' + '<tr>' + '<th>No</th>' + '<th>Product</th>' + '<th>Quantity</th>' + '<th>Price</th>' + '</tr>' + '</thead>' + '<tbody>' + row + '</tbody>' + '</table>' + '</div>' + '</div>');
  						$('.modal-footer').html("");
  						$('#popup').modal('show');
  					});
  				}
  			});
  		}
  
  		loadTable();
  	}
  }];
  
  var SalesOrder = (function () {
  	function SalesOrder() {
  		_classCallCheck(this, _SalesOrder);
  	}
  
  	_createClass(SalesOrder, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _SalesOrder = SalesOrder;
  	SalesOrder = (0, _decoratorsWithStyles2['default'])(_SalesOrderCss2['default'])(SalesOrder) || SalesOrder;
  	return SalesOrder;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent1: dataContent1,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "SalesOrder.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 98 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _SalesPricingInputCss = __webpack_require__(47);
  
  var _SalesPricingInputCss2 = _interopRequireDefault(_SalesPricingInputCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataSalesPricing = {};
  var dataPageHeading = {
  	title: 'Sales Pricing',
  	additionalTitle: ' Input',
  	subtitle: ['Sales', 'Sales Pricing', 'Sales Pricing Input'],
  	breadcrumbsIcon: 'fa-pencil'
  };
  
  function setValue(SalesPricingId, StaffId, StaffName) {
  	$('#SalesPricingID').val(SalesPricingId);
  	$('#staff').val(StaffName).attr('data-id', StaffId);
  	$('#tableSalesPricingDetail tbody').remove();
  	$('#errorDisplay').text("");
  	$(':input[required], select[required]').trigger("change");
  }
  
  var dataContent1 = [];
  var dataContent2 = [{
  	mode: 'Input',
  	name: 'formSalesPricingInput',
  	title: 'Add Sales Pricing',
  	loadAll: true,
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		if (typeof QueryString.id != 'undefined') {
  			$.ajax({
  				url: _coreApps2['default'].host + "/v1/sales-pricing/" + QueryString.id,
  				type: 'GET',
  				dataType: 'json',
  				cache: false,
  				success: function success(data) {
  					if (data.status == "OK") {
  						var data = data.data,
  						    details = data.details;
  						setValue(data.salesPricingId, data.staff.staffId, data.staff.staffName);
  
  						dataSalesPricing.staffId = data.staff.staffId;
  
  						for (var i in details) _coreApps2['default'].props.push({ productId: details[i].productId, productName: details[i].product.productName, price: details[i].price });
  					}
  				},
  				async: false
  			});
  		}
  
  		// Autocomplete Staff
  		_apiRestCall2['default'].performRequest('/v1/staff', 'GET', {}, function (data) {
  			var autocomplete = [],
  			    onSelected = false;
  			if (data.status == "OK") {
  				for (var i in data.data) autocomplete.push({ id: data.data[i].staffId, value: data.data[i].staffName });
  			}
  			var bloodhound = new Bloodhound({
  				datumTokenizer: function datumTokenizer(datum) {
  					return Bloodhound.tokenizers.whitespace(datum.value);
  				},
  				queryTokenizer: Bloodhound.tokenizers.whitespace,
  				local: autocomplete
  			});
  
  			// Initialize the Bloodhound suggestion engine
  			bloodhound.initialize().done(function () {
  				$('#staff').typeahead({
  					highlight: true
  				}, {
  					templates: {
  						empty: ['<div class="tt-suggestion">', 'No Items Found', '</div>'].join('\n'),
  						suggestion: function suggestion(data) {
  							// return '<div>' + data.id + ' – ' + data.value + '</div>'
  							return '<div data-attr=' + data.id + '>' + data.value + '</div>';
  						}
  					},
  					displayKey: 'value',
  					source: bloodhound.ttAdapter()
  				}).on("typeahead:selected", function (e, datum) {
  					dataSalesPricing.staffId = datum.id;
  					$(this).closest('span').siblings().remove();
  					onSelected = true;
  				}).on("change", function (e) {
  					if (!onSelected) dataSalesPricing.staffId = $(".tt-suggestion:first").attr("data-attr");
  					onSelected = false;
  					if ($(this).val() != "") $(this).closest('span').siblings().remove();
  				}).on("keydown", function (e) {
  					if (e.keyCode == 9) {
  						// TABKEY
  						if (!onSelected) dataSalesPricing.staffId = $(".tt-suggestion:first").attr("data-attr");
  						onSelected = false;
  						if ($(this).val() != "") $(this).closest('span').siblings().remove();
  					}
  				});
  			});
  		});
  	},
  	items: [{
  		value: '',
  		name: 'SalesPricingID',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Staff Name',
  		name: 'staff',
  		required: 'required',
  		type: 'autocomplete',
  		cols: [2, 10]
  	}]
  }, {
  	mode: 'Table',
  	name: 'tableSalesPricingDetail',
  	cls: 'col-lg-12',
  	loadAll: true,
  	typeTable: 'table-responsive',
  	componentDidMount: function componentDidMount() {
  		function loadTable() {
  			var rows = "",
  			    picture_default = "placeholder-640x480.png";
  			$("#tableSalesPricingDetail").html("<thead></thead><tbody></tbody><tfoot></tfoot>");
  			$("#tableSalesPricingDetail thead").html("<tr>" + "<th>Product</th>" + "<th>Price</th>" + "<th>Action</th>" + "</tr>");
  			if (_coreApps2['default'].props.length) {
  				for (var i in _coreApps2['default'].props) rows += "<tr id='row-" + i + "'>" + "<td id=" + _coreApps2['default'].props[i].productId + ">" + _coreApps2['default'].props[i].productName + "</td>" + "<td>" + FormatToCurrency(_coreApps2['default'].props[i].price) + "</td>" + "<td><a class='btn btn-danger iDeleteDetail' data-attr='" + i + "'>Delete</a></td>" + "</tr>";
  			}
  			$("#tableSalesPricingDetail tbody").html(rows);
  			$("#tableSalesPricingDetail tfoot").html("<tr><td colspan='3'><button class='btn btn-primary'>Browse</button></td></tr>");
  
  			$("a.iDeleteDetail").click(function (e) {
  				var index = $(this).attr('data-attr'),
  				    productIdParam = $(this).closest('td').siblings(':first').attr('id');
  				_coreApps2['default'].props = $.grep(_coreApps2['default'].props || [], function (value, index) {
  					return value.productId != productIdParam;
  				});
  				$("tr#row-" + index).remove();
  			});
  			$("#tableSalesPricingDetail tfoot button").click(function () {
  				$('.modal-title').html('Product Detail');
  				$('.modal-body').html('<table class="table table-hover" id="tblProduct"></table>');
  				var otable = $("#tblProduct").DataTable({
  					"retrieve": true,
  					"bAutoWidth": true,
  					"bProcessing": true,
  					"bServerSide": false,
  					"sAjaxSource": _coreApps2['default'].host + "/v1/product",
  					"columns": [{ "title": "Product ID", "className": "productId", "data": "productId", "visible": false }, { "title": "Product Name", "className": "productName", "data": "productName",
  						"render": function render(data, type, full, meta) {
  							return "<label id='" + full.productId + "'>" + data + "</label>";
  						}
  					}, { "title": "Category", "className": "productCategoryId", "data": "productCategory",
  						"render": function render(data, type, full, meta) {
  							return data != null ? data.productCategoryName : "";
  						}
  					}, { "title": "Initial Stock", "className": "initialStock", "data": "initialStock" }, { "title": "Currnet Stock", "className": "currentStock", "data": "currentStock" }, { "title": "Sell Price", "className": "sellPrice", "data": "sellPrice", "type": "num-fmt",
  						"render": function render(data, type, full, meta) {
  							return FormatToCurrency(data);
  						}
  					}, { "title": "Status", "className": "status", "data": "status" }, { "title": "Image", "className": "productPhoto", "data": "productPhoto",
  						"render": function render(data, type, full, meta) {
  							var link;
  							return "<img src=" + _coreApps2['default'].defaultDirectory + (data == null ? picture_default : data) + " width=100 height=100 />";
  						}
  					}],
  					"fnDrawCallback": function fnDrawCallback() {
  						$("#tblProduct tbody tr").off('click').click(function (e) {
  							e.preventDefault();
  							var productId = $(this).find('.productName label').attr('id'),
  							    productName = $(this).find('.productName label').text();
  							_coreApps2['default'].item = {};
  							_coreApps2['default'].item.productId = productId;
  							_coreApps2['default'].item.productName = productName;
  							if ($(this).hasClass('selected')) {
  								$(this).removeClass('selected');
  							} else {
  								$('#tblProduct tbody tr.selected').removeClass('selected');
  								$(this).addClass('selected');
  							}
  						});
  					}
  				});
  				$('#popup').modal('show');
  				$('.modal-body').css({ 'overflow-y': 'scroll', 'height': '500px' }).html($("#tblProduct"));
  				$('.modal-body').append('<form role="form" class="form-horizontal" id="frmProduct">' + '<div class="form-group">' + '<label class="control-label col-sm-2">Price</label>' + '<div class="col-sm-10">' + '<input class="form-control" type="number" name="priceDetail" id="priceDetail" placeholder="Price" value=0 />' + '</div>' + '</div>' + '</form>');
  				$("#btnSubmitPopup").off('click').click(function (e) {
  					var elPrice = $("#frmProduct #priceDetail")[1];
  					_coreApps2['default'].item.price = $(elPrice).val();
  					var flag = true;
  					$.map(_coreApps2['default'].props || [], function (value, index) {
  						if (value.productId == _coreApps2['default'].item.productId) {
  							flag = false;
  							return;
  						}
  					});
  
  					if (typeof _coreApps2['default'].item.productId == 'undefined') return;
  
  					if (flag) _coreApps2['default'].props.push(_coreApps2['default'].item);else {
  						$('html, body').animate({
  							scrollTop: $('body').offset().top
  						}, 'slow');
  						$("#alert #alert-title").html("Error");
  						$("#alert #alert-content").html("Product Already Exists");
  						$("#alert").addClass('alert-danger').show();
  						$("#alert").fadeOut(2000);
  					}
  					$('#popup').modal('hide');
  					loadTable();
  				});
  			});
  		}
  		loadTable();
  	}
  }, {
  	mode: 'Input',
  	loadAll: true,
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		$("#btnSave").click(function (e) {
  			e.preventDefault();
  			var detail;
  			detail = $.map(_coreApps2['default'].props, function (value, index) {
  				var items = [];
  				items.push({ productId: value.productId, price: value.price });
  				return items;
  			});
  			var SalesPricingId = $("#SalesPricingID").val(),
  			    boolVal = true,
  			    error = $("#errorDisplay");
  
  			$.each($(':input[required], select[required]'), function (value, index) {
  				if ($(this).val() == "") {
  					boolVal = false;
  					return;
  				}
  			});
  
  			if (!boolVal) return;
  
  			if (detail.length == 0) {
  				error.html("Product must have at least one item");
  				error.css('display', 'block');
  				return;
  			}
  
  			dataSalesPricing.details = JSON.stringify(detail);
  
  			if (SalesPricingId == "") {
  				_apiRestCall2['default'].performRequest('/v1/sales-pricing', 'POST', dataSalesPricing, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							$('html, body').animate({
  								scrollTop: $('body').offset().top
  							}, 'slow');
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Save Sales Pricing");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							setValue("", "", "");
  						}
  					}
  				});
  			} else {
  				_apiRestCall2['default'].performRequest('/v1/sales-pricing/' + SalesPricingId, 'PUT', dataSalesPricing, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							window.location.assign('/sales-pricing');
  						}
  					}
  				});
  			}
  		});
  	},
  	items: [{
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'Back',
  			type: 'button',
  			name: 'btnBack',
  			cls: 'btn-default',
  			event: function event() {
  				window.location.assign("/sales-pricing");
  			}
  		}]
  	}]
  }];
  
  var SalesPricingInput = (function () {
  	function SalesPricingInput() {
  		_classCallCheck(this, _SalesPricingInput);
  	}
  
  	_createClass(SalesPricingInput, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _SalesPricingInput = SalesPricingInput;
  	SalesPricingInput = (0, _decoratorsWithStyles2['default'])(_SalesPricingInputCss2['default'])(SalesPricingInput) || SalesPricingInput;
  	return SalesPricingInput;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent1: dataContent1,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "SalesPricingInput.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 99 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _SalesPricingCss = __webpack_require__(48);
  
  var _SalesPricingCss2 = _interopRequireDefault(_SalesPricingCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Sales Pricing',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['Sales', 'Sales Pricing'],
  	toolbar: [{
  		type: "button",
  		name: "btnAdd",
  		text: "Add",
  		cls: "btn-primary",
  		event: function event(e) {
  			window.location.assign("/sales-pricing-input");
  		}
  	}]
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'User',
  	name: 'table-sales-pricing',
  	panel: false,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	bodyAttr: '',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-sales-pricing").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/sales-pricing",
  				"columns": [{ "title": "Sales Pricing ID", "className": "salesPricingId", "data": "salesPricingId", "visible": false }, { "title": "Staff", "className": "staffId", "data": "staff",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.staffName : "";
  					}
  				}, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "salesPricingId",
  					"render": function render(data, type, full, meta) {
  						_coreApps2['default'].props[data] = full.details;
  						return "<label class='viewDetail' data-attr=" + data + "><span class='glyphicon glyphicon-eye-open'></span> Detail</label> &nbsp;" + "<a class='btn btn-default iEdit' href='sales-pricing-input?id=" + data + "'>Edit</a>" + " <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-sales-pricing").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var salesPricingId = $(this).attr('data-attr');
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/sales-pricing/' + salesPricingId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Delete Sales Invoice");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  							});
  						});
  					});
  					$('label.viewDetail').css('cursor', 'pointer').click(function (e) {
  						var selected = $(this).attr('data-attr'),
  						    data = _coreApps2['default'].props[selected];
  						var row = "";
  						for (var i in data) {
  							row += '<tr>' + '<td>' + (parseInt(i) + 1) + '</td>' + '<td>' + data[i].product.productName + '</td>' + '<td>' + FormatToCurrency(data[i].price) + '</td>' + '</tr>';
  						}
  						$('.modal-title').html('Pricing Detail');
  						$('.modal-body').html('<div class="container-fluid">' + '<div class="row">' + '<table class="table">' + '<thead>' + '<tr>' + '<th>No</th>' + '<th>Product</th>' + '<th>Price</th>' + '</tr>' + '</thead>' + '<tbody>' + row + '</tbody>' + '</table>' + '</div>' + '</div>');
  						$('.modal-footer').html("");
  						$('#popup').modal('show');
  					});
  				}
  			});
  		}
  
  		loadTable();
  	}
  }];
  
  var SalesPricing = (function () {
  	function SalesPricing() {
  		_classCallCheck(this, _SalesPricing);
  	}
  
  	_createClass(SalesPricing, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _SalesPricing = SalesPricing;
  	SalesPricing = (0, _decoratorsWithStyles2['default'])(_SalesPricingCss2['default'])(SalesPricing) || SalesPricing;
  	return SalesPricing;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "SalesPricing.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 100 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _SalesReturnInputCss = __webpack_require__(49);
  
  var _SalesReturnInputCss2 = _interopRequireDefault(_SalesReturnInputCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataSalesReturn = {};
  var dataPageHeading = {
  	title: 'Sales Return',
  	additionalTitle: ' Input',
  	subtitle: ['Sales', 'Sales Return', 'Sales Return Input'],
  	breadcrumbsIcon: 'fa-pencil'
  };
  var dataContent1 = [];
  var dataContent2 = [{
  	mode: 'Input',
  	name: 'formSalesReturnInput',
  	title: 'Add Sales Return',
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		function setValue(SalesReturnID, StaffId, StaffName, SalesInvoice, ReturnDate, Note) {
  			$('#salesReturnId').val(SalesReturnID);
  			$('#staff').val(StaffName).attr('data-id', StaffId);
  			if (SalesInvoice == "") $('#salesInvoice').prop('selectedIndex', 0);else $('#salesInvoice').val(SalesInvoice);
  			if (ReturnDate != "") {
  				ReturnDate = ReturnDate.split("-");
  				$("#returnDate.datepicker").datepicker('update', new Date(ReturnDate[0], ReturnDate[1] - 1, ReturnDate[2].substr(0, 2)));
  			} else {
  				$("#returnDate.datepicker").val("");
  			}
  			$('#note').val(Note);
  			$('#errorDisplay').text("");
  			$(':input[required], select[required]').trigger("change");
  		}
  
  		_apiRestCall2['default'].performRequest('/v1/sales-invoice', 'GET', {}, function (data) {
  			if (data.status == "OK") {
  				var salesInvoiceOption = data.data;
  				$.map(salesInvoiceOption || [], function (val, i) {
  					$('#salesInvoice').append('<option value=' + val.salesInvoiceId + '>' + val.invoiceNo + '</option>');
  				});
  			}
  		});
  
  		// Autocomplete Staff
  		_apiRestCall2['default'].performRequest('/v1/staff', 'GET', {}, function (data) {
  			var autocomplete = [],
  			    onSelected = false;
  			if (data.status == "OK") {
  				for (var i in data.data) autocomplete.push({ id: data.data[i].staffId, value: data.data[i].staffName });
  			}
  			var bloodhound = new Bloodhound({
  				datumTokenizer: function datumTokenizer(datum) {
  					return Bloodhound.tokenizers.whitespace(datum.value);
  				},
  				queryTokenizer: Bloodhound.tokenizers.whitespace,
  				local: autocomplete
  			});
  
  			// Initialize the Bloodhound suggestion engine
  			bloodhound.initialize().done(function () {
  				$('#staff').typeahead({
  					highlight: true
  				}, {
  					templates: {
  						empty: ['<div class="tt-suggestion">', 'No Items Found', '</div>'].join('\n'),
  						suggestion: function suggestion(data) {
  							// return '<div>' + data.id + ' – ' + data.value + '</div>'
  							return '<div data-attr=' + data.id + '>' + data.value + '</div>';
  						}
  					},
  					displayKey: 'value',
  					source: bloodhound.ttAdapter()
  				}).on("typeahead:selected", function (e, datum) {
  					dataSalesReturn.staffId = datum.id;
  					$(this).closest('span').siblings().remove();
  					onSelected = true;
  				}).on("change", function (e) {
  					if (!onSelected) dataSalesReturn.staffId = $(".tt-suggestion:first").attr("data-attr");
  					onSelected = false;
  					if ($(this).val() != "") $(this).closest('span').siblings().remove();
  				}).on("keydown", function (e) {
  					if (e.keyCode == 9) {
  						// TABKEY
  						if (!onSelected) dataSalesReturn.staffId = $(".tt-suggestion:first").attr("data-attr");
  						onSelected = false;
  						if ($(this).val() != "") $(this).closest('span').siblings().remove();
  					}
  				});
  			});
  		});
  		if (typeof QueryString.id != 'undefined') {
  			$.ajax({
  				url: _coreApps2['default'].host + "/v1/sales-return/" + QueryString.id,
  				type: 'GET',
  				dataType: 'json',
  				cache: false,
  				success: function success(data) {
  					if (data.status == "OK") {
  						var data = data.data;
  						setValue(data.salesReturnId, data.staff.staffId, data.staff.staffName, data.salesInvoiceId, data.returnDate, data.note);
  
  						dataSalesReturn.staffId = data.staff.staffId;
  						dataSalesReturn.salesInvoiceId = data.salesInvoiceId;
  						dataSalesReturn.returnDate = data.returnDate;
  						dataSalesReturn.note = data.note;
  					}
  				}
  			});
  		}
  
  		$("#btnSave").click(function (e) {
  			e.preventDefault();
  			var salesReturnId = $('#salesReturnId').val(),
  			    boolVal = true,
  			    error = $('#errorDisplay');
  
  			$.each($(':input[required], select[required]'), function (value, index) {
  				if ($(this).val() == "") {
  					boolVal = false;
  					return;
  				}
  			});
  
  			if (!boolVal) return;
  
  			dataSalesReturn.salesInvoiceId = $('#salesInvoice').val();
  			dataSalesReturn.returnDate = moment($('#returnDate').val()).format("YYYY-MM-DD");
  			dataSalesReturn.note = $('#note').val();
  
  			if (salesReturnId == "") {
  				_apiRestCall2['default'].performRequest('/v1/sales-return', 'POST', dataSalesReturn, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							$('html, body').animate({
  								scrollTop: $('body').offset().top
  							}, 'slow');
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Save Sales Return");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							setValue("", "", "", "", "", "");
  						}
  					}
  				});
  			} else {
  				_apiRestCall2['default'].performRequest('/v1/sales-return/' + salesReturnId, 'PUT', dataSalesReturn, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							window.location.assign('/sales-return');
  						}
  					}
  				});
  			}
  		});
  	},
  	items: [{
  		value: '',
  		name: 'salesReturnId',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Sales Invoice',
  		name: 'salesInvoice',
  		type: 'select',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Staff Name',
  		name: 'staff',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Return Date',
  		name: 'returnDate',
  		type: 'datetime',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Note',
  		name: 'note',
  		rows: 5,
  		type: 'area',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			ts: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'Back',
  			type: 'button',
  			name: 'btnBack',
  			cls: 'btn-default',
  			event: function event() {
  				window.location.assign("/sales-return");
  			}
  		}]
  	}]
  }];
  
  var SalesReturnInput = (function () {
  	function SalesReturnInput() {
  		_classCallCheck(this, _SalesReturnInput);
  	}
  
  	_createClass(SalesReturnInput, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _SalesReturnInput = SalesReturnInput;
  	SalesReturnInput = (0, _decoratorsWithStyles2['default'])(_SalesReturnInputCss2['default'])(SalesReturnInput) || SalesReturnInput;
  	return SalesReturnInput;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent1: dataContent1,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "SalesReturnInput.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 101 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _SalesReturnCss = __webpack_require__(50);
  
  var _SalesReturnCss2 = _interopRequireDefault(_SalesReturnCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Sales Return',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['Sales', 'Sales Return'],
  	toolbar: [{
  		type: "button",
  		name: "btnAdd",
  		text: "Add",
  		cls: "btn-primary",
  		event: function event(e) {
  			window.location.assign("/sales-return-input");
  		}
  	}]
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'User',
  	name: 'table-sales-return',
  	panel: false,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-sales-return").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/sales-return",
  				"columns": [{ "title": "Sales Return ID", "className": "salesReturnId", "data": "salesReturnId", "visible": false }, { "title": "Return Date", "className": "returnDate", "data": "returnDate",
  					"render": function render(data, type, full, meta) {
  						return moment(data).format(_coreApps2['default'].dateFormat);
  					}
  				}, { "title": "Status", "className": "status", "data": "status" }, { "title": "Note", "className": "note", "data": "note" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "salesReturnId",
  					"render": function render(data, type, full, meta) {
  						return "<a class='btn btn-default iEdit' href='sales-return-input?id=" + data + "'>Edit</a>" + " <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-sales-return").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var salesReturnId = $(this).attr('data-attr');
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/sales-return/' + salesReturnId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Delete Sales Return");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  							});
  						});
  					});
  				}
  			});
  		}
  
  		loadTable();
  	}
  }];
  
  var SalesReturn = (function () {
  	function SalesReturn() {
  		_classCallCheck(this, _SalesReturn);
  	}
  
  	_createClass(SalesReturn, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _SalesReturn = SalesReturn;
  	SalesReturn = (0, _decoratorsWithStyles2['default'])(_SalesReturnCss2['default'])(SalesReturn) || SalesReturn;
  	return SalesReturn;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "SalesReturn.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 102 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _ShippingOrderInputCss = __webpack_require__(51);
  
  var _ShippingOrderInputCss2 = _interopRequireDefault(_ShippingOrderInputCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataShippingOrder = {};
  var dataPageHeading = {
  	title: 'Shipping Order',
  	additionalTitle: ' Input',
  	subtitle: ['Shipping Order', 'Shipping Order Input'],
  	breadcrumbsIcon: 'fa-pencil'
  };
  
  function setValue(ShippingOrderID, ShippingNo, SalesOrderID, StaffId, StaffName, ShippingDate, Note) {
  	$('#shippingOrderID').val(ShippingOrderID);
  	$('#shippingNo').val(ShippingNo);
  	$('#salesOrder').val(SalesOrderID);
  	$('#staff').val(StaffName).attr('data-id', StaffId);
  	if (ShippingDate != "") {
  		ShippingDate = ShippingDate.split("-");
  		$("#shippingDate.datepicker").datepicker('update', new Date(ShippingDate[0], ShippingDate[1] - 1, ShippingDate[2].substr(0, 2)));
  	} else {
  		$("#shippingDate.datepicker").val("");
  	}
  	$('#note').val(Note);
  	$('#tableShippingOrderDetail tbody').remove();
  	$('#errorDisplay').text("");
  	$(':input[required], select[required]').trigger("change");
  }
  
  var dataContent1 = [];
  var dataContent2 = [{
  	mode: 'Input',
  	name: 'formShippingOrderInput',
  	title: 'Add Shipping Order',
  	iconHeader: 'fa-plus',
  	loadAll: true,
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		_apiRestCall2['default'].performRequest('/v1/sales-order', 'GET', {}, function (data) {
  			if (data.status == "OK") {
  				var salesOrder = data.data;
  				$.map(salesOrder || [], function (val, i) {
  					$('#salesOrder').append('<option value=' + val.salesOrderId + '>' + val.orderNo + '</option>');
  				});
  			}
  		});
  		if (typeof QueryString.id != 'undefined') {
  			$("#shippingNo").closest(".form-group").show();
  			$.ajax({
  				url: _coreApps2['default'].host + "/v1/shipping-order/" + QueryString.id,
  				type: 'GET',
  				dataType: 'json',
  				cache: false,
  				success: function success(data) {
  					if (data.status == "OK") {
  						var data = data.data,
  						    details = data.details;
  						setValue(data.shippingOrderId, data.shippingNo, data.salesOrderId, data.staff.staffId, data.staff.staffName, data.shippingDate, data.note);
  
  						dataShippingOrder.salesOrderId = data.salesOrderId;
  						// dataShippingOrder.shippingNo = data.shippingNo;
  						dataShippingOrder.staffId = data.staff.staffId;
  						dataShippingOrder.shippingDate = moment(data.shippingDate).format("YYYY-MM-DD");
  						dataShippingOrder.note = data.note;
  
  						for (var i in details) _coreApps2['default'].props.push({ productId: details[i].productId, productName: details[i].product.productName, quantity: details[i].quantity });
  					}
  				},
  				async: false
  			});
  		}
  
  		// Autocomplete Staff
  		_apiRestCall2['default'].performRequest('/v1/staff', 'GET', {}, function (data) {
  			var autocomplete = [],
  			    onSelected = false;
  			if (data.status == "OK") {
  				for (var i in data.data) autocomplete.push({ id: data.data[i].staffId, value: data.data[i].staffName });
  			}
  			var bloodhound = new Bloodhound({
  				datumTokenizer: function datumTokenizer(datum) {
  					return Bloodhound.tokenizers.whitespace(datum.value);
  				},
  				queryTokenizer: Bloodhound.tokenizers.whitespace,
  				local: autocomplete
  			});
  
  			// Initialize the Bloodhound suggestion engine
  			bloodhound.initialize().done(function () {
  				$('#staff').typeahead({
  					highlight: true
  				}, {
  					templates: {
  						empty: ['<div class="tt-suggestion">', 'No Items Found', '</div>'].join('\n'),
  						suggestion: function suggestion(data) {
  							// return '<div>' + data.id + ' – ' + data.value + '</div>'
  							return '<div data-attr=' + data.id + '>' + data.value + '</div>';
  						}
  					},
  					displayKey: 'value',
  					source: bloodhound.ttAdapter()
  				}).on("typeahead:selected", function (e, datum) {
  					dataShippingOrder.staffId = datum.id;
  					$(this).closest('span').siblings().remove();
  					onSelected = true;
  				}).on("change", function (e) {
  					if (!onSelected) dataShippingOrder.staffId = $(".tt-suggestion:first").attr("data-attr");
  					onSelected = false;
  					if ($(this).val() != "") $(this).closest('span').siblings().remove();
  				}).on("keydown", function (e) {
  					if (e.keyCode == 9) {
  						// TABKEY
  						if (!onSelected) dataShippingOrder.staffId = $(".tt-suggestion:first").attr("data-attr");
  						onSelected = false;
  						if ($(this).val() != "") $(this).closest('span').siblings().remove();
  					}
  				});
  			});
  		});
  	},
  	items: [{
  		value: '',
  		name: 'shippingOrderID',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Shipping No',
  		name: 'shippingNo',
  		type: 'text',
  		disabled: 'disabled',
  		display: 'none',
  		cols: [2, 10]
  	}, {
  		text: 'Order No',
  		name: 'salesOrder',
  		type: 'select',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Staff',
  		name: 'staff',
  		type: 'autocomplete',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Shipping Date',
  		name: 'shippingDate',
  		type: 'datetime',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Note',
  		name: 'note',
  		rows: 5,
  		type: 'area',
  		required: 'required',
  		cols: [2, 10]
  	}]
  }, {
  	mode: 'Table',
  	name: 'tableShippingOrderDetail',
  	cls: 'col-lg-12',
  	loadAll: true,
  	typeTable: 'table-responsive',
  	componentDidMount: function componentDidMount() {
  		function loadTable() {
  			var rows = "",
  			    picture_default = "placeholder-640x480.png";
  			$("#tableShippingOrderDetail").html("<thead></thead><tbody></tbody><tfoot></tfoot>");
  			$("#tableShippingOrderDetail thead").html("<tr>" + "<th>Product</th>" + "<th>Quantity</th>" + "<th>Action</th>" + "</tr>");
  			if (_coreApps2['default'].props.length) {
  				for (var i in _coreApps2['default'].props) rows += "<tr id='row-" + i + "'>" + "<td id=" + _coreApps2['default'].props[i].productId + ">" + _coreApps2['default'].props[i].productName + "</td>" + "<td>" + _coreApps2['default'].props[i].quantity + "</td>" + "<td><a class='btn btn-danger iDeleteDetail' data-attr='" + i + "'>Delete</a></td>" + "</tr>";
  			}
  			$("#tableShippingOrderDetail tbody").html(rows);
  			$("#tableShippingOrderDetail tfoot").html("<tr><td colspan='3'><button class='btn btn-primary'>Browse</button></td></tr>");
  
  			$("a.iDeleteDetail").click(function (e) {
  				var index = $(this).attr('data-attr'),
  				    productIdParam = $(this).closest('td').siblings(':first').attr('id');
  				_coreApps2['default'].props = $.grep(_coreApps2['default'].props || [], function (value, index) {
  					return value.productId != productIdParam;
  				});
  				$("tr#row-" + index).remove();
  			});
  			$("#tableShippingOrderDetail tfoot button").click(function () {
  				$('.modal-title').html('Product Detail');
  				$('.modal-body').html('<table class="table table-hover" id="tblProduct"></table>');
  				var otable = $("#tblProduct").DataTable({
  					"retrieve": true,
  					"bAutoWidth": true,
  					"bProcessing": true,
  					"bServerSide": false,
  					"sAjaxSource": _coreApps2['default'].host + "/v1/product",
  					"columns": [{ "title": "Product ID", "className": "productId", "data": "productId", "visible": false }, { "title": "Product Name", "className": "productName", "data": "productName",
  						"render": function render(data, type, full, meta) {
  							return "<label id='" + full.productId + "'>" + data + "</label>";
  						}
  					}, { "title": "Category", "className": "productCategoryId", "data": "productCategory",
  						"render": function render(data, type, full, meta) {
  							return data != null ? data.productCategoryName : "";
  						}
  					}, { "title": "Initial Stock", "className": "initialStock", "data": "initialStock" }, { "title": "Currnet Stock", "className": "currentStock", "data": "currentStock" }, { "title": "Sell Price", "className": "sellPrice", "data": "sellPrice", "type": "num-fmt",
  						"render": function render(data, type, full, meta) {
  							return FormatToCurrency(data);
  						}
  					}, { "title": "Status", "className": "status", "data": "status" }, { "title": "Image", "className": "productPhoto", "data": "productPhoto",
  						"render": function render(data, type, full, meta) {
  							var link;
  							return "<img src=" + _coreApps2['default'].defaultDirectory + (data == null ? picture_default : data) + " width=100 height=100 />";
  						}
  					}],
  					"fnDrawCallback": function fnDrawCallback() {
  						$("#tblProduct tbody tr").off('click').click(function (e) {
  							e.preventDefault();
  							var productId = $(this).find('.productName label').attr('id'),
  							    productName = $(this).find('.productName label').text();
  							_coreApps2['default'].item = {};
  							_coreApps2['default'].item.productId = productId;
  							_coreApps2['default'].item.productName = productName;
  							if ($(this).hasClass('selected')) {
  								$(this).removeClass('selected');
  							} else {
  								$('#tblProduct tbody tr.selected').removeClass('selected');
  								$(this).addClass('selected');
  							}
  						});
  					}
  				});
  				$('#popup').modal('show');
  				$('.modal-body').css({ 'overflow-y': 'scroll', 'height': '500px' }).html($("#tblProduct"));
  				$('.modal-body').append('<form role="form" class="form-horizontal" id="frmProduct">' + '<div class="form-group">' + '<label class="control-label col-sm-2">Quantity</label>' + '<div class="col-sm-10">' + '<input class="form-control" type="number" name="quantityDetail" id="quantityDetail" placeholder="Quantity" value=0 />' + '</div>' + '</form>');
  				$("#btnSubmitPopup").off('click').click(function (e) {
  					var elQuantity = $("#frmProduct #quantityDetail")[1];
  					_coreApps2['default'].item.quantity = $(elQuantity).val();
  					var flag = true;
  					$.map(_coreApps2['default'].props || [], function (value, index) {
  						if (value.productId == _coreApps2['default'].item.productId) {
  							flag = false;
  							return;
  						}
  					});
  
  					if (typeof _coreApps2['default'].item.productId == 'undefined') return;
  
  					if (flag) _coreApps2['default'].props.push(_coreApps2['default'].item);else {
  						$('html, body').animate({
  							scrollTop: $('body').offset().top
  						}, 'slow');
  						$("#alert #alert-title").html("Error");
  						$("#alert #alert-content").html("Product Already Exists");
  						$("#alert").addClass('alert-danger').show();
  						$("#alert").fadeOut(2000);
  					}
  					$('#popup').modal('hide');
  					loadTable();
  				});
  			});
  		}
  		loadTable();
  	}
  }, {
  	mode: 'Input',
  	loadAll: true,
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		$("#btnSave").click(function (e) {
  			e.preventDefault();
  			var detail;
  			detail = $.map(_coreApps2['default'].props, function (value, index) {
  				var items = [];
  				items.push({ productId: value.productId, quantity: value.quantity });
  				return items;
  			});
  			var shippingOrderId = $("#shippingOrderID").val(),
  			    boolVal = true,
  			    error = $("#errorDisplay");
  
  			$.each($(':input[required], select[required]'), function (value, index) {
  				if ($(this).val() == "") {
  					boolVal = false;
  					return;
  				}
  			});
  
  			if (!boolVal) return;
  
  			if (detail.length == 0) {
  				error.html("Product must have at least one item");
  				error.css('display', 'block');
  				return;
  			}
  
  			dataShippingOrder.salesOrderId = $('#salesOrder').val();
  			// dataShippingOrder.shippingNo = $('#shippingNo').val();
  			dataShippingOrder.shippingDate = moment($('#shippingDate').val()).format("YYYY-MM-DD");
  			dataShippingOrder.status = $('#status').val();
  			dataShippingOrder.note = $('#note').val();
  			dataShippingOrder.details = JSON.stringify(detail);
  
  			if (shippingOrderId == "") {
  				_apiRestCall2['default'].performRequest('/v1/shipping-order', 'POST', dataShippingOrder, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							$('html, body').animate({
  								scrollTop: $('body').offset().top
  							}, 'slow');
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Save shipping Order");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							setValue("", "", "", "", "", "", "");
  						}
  					}
  				});
  			} else {
  				_apiRestCall2['default'].performRequest('/v1/shipping-order/' + shippingOrderId, 'PUT', dataShippingOrder, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							window.location.assign('/shipping-order');
  						}
  					}
  				});
  			}
  		});
  	},
  	items: [{
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'Back',
  			type: 'button',
  			name: 'btnBack',
  			cls: 'btn-default',
  			event: function event() {
  				window.location.assign("/shipping-order");
  			}
  		}]
  	}]
  }];
  
  var ShippingOrderInput = (function () {
  	function ShippingOrderInput() {
  		_classCallCheck(this, _ShippingOrderInput);
  	}
  
  	_createClass(ShippingOrderInput, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _ShippingOrderInput = ShippingOrderInput;
  	ShippingOrderInput = (0, _decoratorsWithStyles2['default'])(_ShippingOrderInputCss2['default'])(ShippingOrderInput) || ShippingOrderInput;
  	return ShippingOrderInput;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent1: dataContent1,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "ShippingOrderInput.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 103 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _ShippingOrderCss = __webpack_require__(52);
  
  var _ShippingOrderCss2 = _interopRequireDefault(_ShippingOrderCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Shipping Order',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['Shipping Order'],
  	toolbar: [{
  		type: "button",
  		name: "btnAdd",
  		text: "Add",
  		cls: "btn-primary",
  		event: function event(e) {
  			window.location.assign("/shipping-order-input");
  		}
  	}]
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'shipping-order',
  	name: 'table-shipping-order',
  	panel: false,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-shipping-order").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/shipping-order",
  				"columns": [{ "title": "Shipping Order ID", "className": "shippingOrderId", "data": "shippingOrderId", "visible": false }, { "title": "Shipping Order No", "className": "shippingNo", "data": "shippingNo" }, { "title": "Staff", "className": "staffId", "data": "staff",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.staffName : "";
  					}
  				}, { "title": "Shipping Date", "className": "shippingDate", "data": "shippingDate",
  					"render": function render(data, type, full, meta) {
  						return moment(data).format(_coreApps2['default'].dateFormat);
  					}
  				}, { "title": "Status", "className": "status", "data": "status" }, { "title": "Note", "className": "note", "data": "note" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "shippingOrderId",
  					"render": function render(data, type, full, meta) {
  						_coreApps2['default'].props[data] = full.details;
  						return "<label class='viewDetail' data-attr=" + data + "><span class='glyphicon glyphicon-eye-open'></span> Detail</label> &nbsp;" + " <a class='btn btn-default iEdit' href='shipping-order-input?id=" + data + "'>Edit</a>" + " <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>" + " <a class='btn btn-info iPrint' data-attr=" + data + ">Print</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-shipping-order").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var shippingOrderId = $(this).attr('data-attr');
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/shipping-order/' + shippingOrderId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Delete Shipping Order");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  							});
  						});
  					});
  					$(".iPrint").click(function (e) {
  						var shippingOrderId = $(this).attr('data-attr');
  						window.location.assign('/shipping-order/' + shippingOrderId + '/print');
  					});
  					$('label.viewDetail').css('cursor', 'pointer').click(function (e) {
  						var selected = $(this).attr('data-attr'),
  						    data = _coreApps2['default'].props[selected];
  						var row = "";
  						for (var i in data) {
  							row += '<tr>' + '<td>' + (parseInt(i) + 1) + '</td>' + '<td>' + data[i].product.productName + '</td>' + '<td>' + data[i].quantity + '</td>' + '</tr>';
  						}
  						$('.modal-title').html('Order Detail');
  						$('.modal-body').html('<div class="container-fluid">' + '<div class="row">' + '<table class="table">' + '<thead>' + '<tr>' + '<th>No</th>' + '<th>Product</th>' + '<th>Quantity</th>' + '</tr>' + '</thead>' + '<tbody>' + row + '</tbody>' + '</table>' + '</div>' + '</div>');
  						$('.modal-footer').html("");
  						$('#popup').modal('show');
  					});
  				}
  			});
  		}
  
  		loadTable();
  	}
  }];
  
  var ShippingOrder = (function () {
  	function ShippingOrder() {
  		_classCallCheck(this, _ShippingOrder);
  	}
  
  	_createClass(ShippingOrder, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _ShippingOrder = ShippingOrder;
  	ShippingOrder = (0, _decoratorsWithStyles2['default'])(_ShippingOrderCss2['default'])(ShippingOrder) || ShippingOrder;
  	return ShippingOrder;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "ShippingOrder.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 104 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _StaffInputCss = __webpack_require__(53);
  
  var _StaffInputCss2 = _interopRequireDefault(_StaffInputCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Staff',
  	additionalTitle: ' Input',
  	breadcrumbsIcon: 'fa-pencil',
  	subtitle: ['User', 'Staff', 'Staff Input']
  };
  var dataContent2 = [{
  	mode: 'Input',
  	name: 'formStaff',
  	title: 'Add Staff',
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		function setValue(StaffID, DepartmentId, staffPositionId, StaffName, StaffGender, StaffBirthdate, StaffAddress, PhoneNumber) {
  			$('#staffID').val(StaffID);
  			if (DepartmentId == "") $('#department').prop('selectedIndex', 0);else $('#department').val(DepartmentId);
  			if (staffPositionId == "") $('#staffPosition').prop('selectedIndex', 0);else $('#staffPosition').val(staffPositionId);
  			$('#staffName').val(StaffName);
  			$('#staffGender[value=' + StaffGender + ']').prop('checked', true);
  			if (StaffBirthdate != "") {
  				StaffBirthdate = StaffBirthdate.split("-");
  				$("#staffBirthdate.datepicker").datepicker('update', new Date(StaffBirthdate[0], StaffBirthdate[1] - 1, StaffBirthdate[2].substr(0, 2)));
  			} else {
  				$("#staffBirthdate.datepicker").val("");
  			}
  			$('#staffAddress').val(StaffAddress);
  			$('#phoneNumber').val(PhoneNumber);
  			$('#errorDisplay').text("");
  			$(':input[required], select[required]').trigger("change");
  		}
  
  		_apiRestCall2['default'].performRequest('/v1/department', 'GET', {}, function (data) {
  			if (data.status == "OK") {
  				var role = data.data;
  				$.map(role || [], function (val, i) {
  					$('#department').append('<option value=' + val.departmentId + '>' + val.departmentName + '</option>');
  				});
  			}
  		});
  		_apiRestCall2['default'].performRequest('/v1/staff-position', 'GET', {}, function (data) {
  			if (data.status == "OK") {
  				var staff = data.data;
  				$.map(staff || [], function (val, i) {
  					$('#staffPosition').append('<option value=' + val.staffPositionId + '>' + val.staffPositionName + '</option>');
  				});
  			}
  		});
  
  		if (typeof QueryString.id != 'undefined') {
  			$.ajax({
  				url: _coreApps2['default'].host + "/v1/staff/" + QueryString.id,
  				type: 'GET',
  				dataType: 'json',
  				cache: false,
  				success: function success(data) {
  					if (data.status == "OK") {
  						var data = data.data;
  						setValue(data.staffId, data.department.departmentId, data.staffPosition.staffPositionId, data.staffName, data.staffGender, data.staffBirthdate, data.staffAddress, data.phoneNumber);
  					}
  				}
  			});
  		}
  
  		$("#btnSave").click(function (e) {
  			e.preventDefault();
  			var staffId = $("#staffID").val(),
  			    error = $("#errorDisplay"),
  			    boolVal = true,
  			    data = {
  				staffName: $('#staffName').val(),
  				departmentId: $("#department").val(),
  				staffPositionId: $("#staffPosition").val(),
  				staffGender: $('#staffGender:checked').val(),
  				staffBirthdate: moment($('#staffBirthdate').val()).format("YYYY-MM-DD"),
  				staffAddress: $('#staffAddress').val(),
  				phoneNumber: $('#phoneNumber').val()
  			};
  			$.each($(':input[required], select[required]'), function (value, index) {
  				if ($(this).val() == "") {
  					boolVal = false;
  					return;
  				}
  			});
  
  			if (!boolVal) return;
  
  			if (staffId == "") {
  				_apiRestCall2['default'].performRequest('/v1/staff', 'POST', data, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							$('html, body').animate({
  								scrollTop: $('body').offset().top
  							}, 'slow');
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Save Staff");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							setValue("", "", "", "", "MALE", "", "", "");
  						}
  					}
  				});
  			} else {
  				_apiRestCall2['default'].performRequest('/v1/staff/' + staffId, 'PUT', data, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							window.location.assign('/staff');
  						}
  					}
  				});
  			}
  		});
  	},
  	items: [{
  		value: '',
  		name: 'staffID',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Staff Name',
  		name: 'staffName',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Department',
  		name: 'department',
  		type: 'select',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Position',
  		name: 'staffPosition',
  		type: 'select',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Gender',
  		type: 'radio',
  		cols: [2, 10],
  		data: [{
  			name: 'staffGender',
  			text: 'MALE',
  			value: 'MALE'
  		}, {
  			name: 'staffGender',
  			text: 'FEMALE',
  			value: 'FEMALE'
  		}]
  	}, {
  		text: 'Birth Date',
  		name: 'staffBirthdate',
  		type: 'datetime',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Address',
  		name: 'staffAddress',
  		rows: 5,
  		type: 'area',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Phone Number',
  		name: 'phoneNumber',
  		type: 'text',
  		required: 'required',
  		placeholder: '(021) 800 800',
  		cols: [2, 10]
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'Back',
  			type: 'button',
  			name: 'btnBack',
  			cls: 'btn-default',
  			event: function event() {
  				window.location.assign("/staff");
  			}
  		}]
  	}]
  }];
  
  var StaffInput = (function () {
  	function StaffInput() {
  		_classCallCheck(this, _StaffInput);
  	}
  
  	_createClass(StaffInput, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _StaffInput = StaffInput;
  	StaffInput = (0, _decoratorsWithStyles2['default'])(_StaffInputCss2['default'])(StaffInput) || StaffInput;
  	return StaffInput;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "StaffInput.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 105 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _StaffPositionCss = __webpack_require__(54);
  
  var _StaffPositionCss2 = _interopRequireDefault(_StaffPositionCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Staff Position',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['Access User', 'Staff Position']
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'staff-position',
  	name: 'table-staff-position',
  	panel: false,
  	loadAll: true,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	bodyAttr: '',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-staff-position").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/staff-position",
  				"columns": [{ "title": "Staff Position ID", "className": 'staffPositionId', "data": "staffPositionId", "visible": false }, { "title": "Staff Position Name", "className": 'staffPositionName', "data": "staffPositionName" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "staffPositionId",
  					"render": function render(data, type, full, meta) {
  						return "<a class='btn btn-default iEdit' data-attr=" + data + ">Edit</a> <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					event();
  					$("#table-staff-position").closest('.table-responsive').css('overflow', 'hidden');
  				}
  			});
  		}
  
  		function reset() {
  			$("#staffPositionId").val("");
  			$("#staffPositionName").val("");
  			$('#errorDisplay').text("");
  			$(':input[required], select[required]').trigger("change");
  		}
  
  		function event() {
  			$("#btnNew").off('click').click(function (e) {
  				reset();
  				$(':input[required], select[required]').trigger("change");
  			});
  
  			$("#btnSave").off('click').click(function (e) {
  				e.preventDefault();
  
  				var staffPositionId = $('#staffPositionId').val(),
  				    error = $('#errorDisplay'),
  				    boolVal = true,
  				    data = { staffPositionName: $('#staffPositionName').val() };
  
  				$.each($(':input[required], select[required]'), function (value, index) {
  					if ($(this).val() == "") {
  						boolVal = false;
  						return;
  					}
  				});
  
  				if (!boolVal) return;
  
  				if (staffPositionId == "") {
  					_apiRestCall2['default'].performRequest('/v1/staff-position', 'POST', data, function (data) {
  						if (typeof data.error != 'undefined') {
  							error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  							error.css('display', 'block');
  						} else {
  							if (data.status == "ERROR") {
  								error.html(data.message);
  								error.css('display', 'block');
  							} else {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Save Staff Position");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  								reset();
  							}
  						}
  					});
  				} else {
  					_apiRestCall2['default'].performRequest('/v1/staff-position/' + staffPositionId, 'PUT', data, function (data) {
  						if (typeof data.error != 'undefined') {
  							error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  							error.css('display', 'block');
  						} else {
  							if (data.status == "ERROR") {
  								error.html(data.message);
  								error.css('display', 'block');
  							} else {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Update Staff Position");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  								reset();
  							}
  						}
  					});
  				}
  			});
  
  			$(".iEdit").off('click').click(function (e) {
  				e.preventDefault();
  				var staffPositionIdEdit = $(this).attr('data-attr'),
  				    staffPositionNameEdit = $(this).closest('tr').find('td.staffPositionName').text();
  
  				$('html, body').animate({
  					scrollTop: $('form#formStaffPosition').offset().top
  				}, 'slow');
  				$('#staffPositionId').val(staffPositionIdEdit);
  				$('#staffPositionName').val(staffPositionNameEdit);
  				$(':input[required], select[required]').trigger("change");
  			});
  
  			$(".iDelete").off('click').click(function (e) {
  				e.preventDefault();
  				var staffPositionId = $(this).attr('data-attr'),
  				    staffPositionNameDelete = $(this).closest('tr').find('td.staffPositionName').text();
  				$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> Staff Position ' + staffPositionNameDelete + '?');
  				$("#PopUpConfirm").modal("show");
  				$("#PopUpConfirm #btnYes").click(function (e) {
  					_apiRestCall2['default'].performRequest('/v1/staff-position/' + staffPositionId, 'DELETE', {}, function (data) {
  						$('html, body').animate({
  							scrollTop: $('body').offset().top
  						}, 'slow');
  						$("#PopUpConfirm").modal("hide");
  						$("#alert #alert-title").html("Success");
  						$("#alert #alert-content").html("Delete Staff Position");
  						$("#alert").addClass('alert-success').show();
  						$("#alert").fadeOut(2000);
  						datatable.destroy();
  						loadTable();
  						reset();
  					});
  				});
  			});
  		}
  
  		loadTable();
  	}
  }, {
  	mode: 'Input',
  	name: 'formStaffPosition',
  	title: 'Add Staff Position',
  	loadAll: true,
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	items: [{
  		value: '',
  		name: 'staffPositionId',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Staff Position Name',
  		name: 'staffPositionName',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'New',
  			type: 'reset',
  			name: 'btnNew',
  			cls: 'btn-default'
  		}]
  	}]
  }];
  
  var StaffPosition = (function () {
  	function StaffPosition() {
  		_classCallCheck(this, _StaffPosition);
  	}
  
  	_createClass(StaffPosition, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _StaffPosition = StaffPosition;
  	StaffPosition = (0, _decoratorsWithStyles2['default'])(_StaffPositionCss2['default'])(StaffPosition) || StaffPosition;
  	return StaffPosition;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "StaffPosition.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 106 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _StaffCss = __webpack_require__(55);
  
  var _StaffCss2 = _interopRequireDefault(_StaffCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Staff',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['User', 'Staff'],
  	toolbar: [{
  		type: "button",
  		name: "btnAdd",
  		text: "Add",
  		cls: "btn-primary",
  		event: function event(e) {
  			window.location.assign("/staff-input");
  		}
  	}]
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'User',
  	name: 'table-staff',
  	panel: false,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	bodyAttr: '',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-staff").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/staff",
  				"columns": [{ "title": "Staff ID", "className": "staffId", "data": "staffId", "visible": false }, { "title": "Department", "className": "departmentId", "data": "department",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.departmentName : "";
  					}
  				}, { "title": "Position", "className": "staffPositionId", "data": "staffPosition",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.staffPositionName : "";
  					}
  				}, { "title": "Staff Name", "className": "staffName", "data": "staffName" }, { "title": "Gender", "className": "staffGender", "data": "staffGender" }, { "title": "Birth Date", "className": "staffBirthdate", "data": "staffBirthdate",
  					"render": function render(data, type, full, meta) {
  						return moment(data).format(_coreApps2['default'].dateFormat);
  					}
  				}, { "title": "Address", "className": "staffAddress", "data": "staffAddress" }, { "title": "Phone Number", "className": "phoneNumber", "data": "phoneNumber" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "staffId",
  					"render": function render(data, type, full, meta) {
  						return "<a class='btn btn-default iEdit' href='staff-input?id=" + data + "'>Edit</a> <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-staff").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var staffId = $(this).attr('data-attr'),
  						    staffName = $(this).closest('tr').find('td.staffName').text();
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> staff ' + staffName + '?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/staff/' + staffId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Delete Staff");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  							});
  						});
  					});
  				}
  			});
  		}
  
  		loadTable();
  	}
  }];
  
  var Staff = (function () {
  	function Staff() {
  		_classCallCheck(this, _Staff);
  	}
  
  	_createClass(Staff, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _Staff = Staff;
  	Staff = (0, _decoratorsWithStyles2['default'])(_StaffCss2['default'])(Staff) || Staff;
  	return Staff;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Staff.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 107 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _SupplierInputCss = __webpack_require__(56);
  
  var _SupplierInputCss2 = _interopRequireDefault(_SupplierInputCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Supplier',
  	additionalTitle: ' Input',
  	breadcrumbsIcon: 'fa-pencil',
  	subtitle: ['User', 'Supplier', 'Supplier Input']
  };
  var dataContent2 = [{
  	mode: 'Input',
  	name: 'formSupplier',
  	title: 'Add Supplier',
  	iconHeader: 'fa-plus',
  	cls: 'col-lg-12',
  	componentDidMount: function componentDidMount() {
  		function setValue(SupplierID, SupplierName, SupplierGender, SupplierAddress, PhoneNumber) {
  			$('#supplierID').val(SupplierID);
  			$('#supplierName').val(SupplierName);
  			$('#supplierGender[value=' + SupplierGender + ']').prop('checked', true);
  			$('#supplierAddress').val(SupplierAddress);
  			$('#phoneNumber').val(PhoneNumber);
  			$('#errorDisplay').text("");
  			$(':input[required], select[required]').trigger("change");
  		}
  		if (typeof QueryString.id != 'undefined') {
  			$.ajax({
  				url: _coreApps2['default'].host + "/v1/supplier/" + QueryString.id,
  				type: 'GET',
  				dataType: 'json',
  				cache: false,
  				success: function success(data) {
  					if (data.status == "OK") {
  						var data = data.data;
  						setValue(data.supplierId, data.supplierName, data.supplierGender, data.supplierAddress, data.phoneNumber);
  					}
  				}
  			});
  		}
  
  		$("#btnSave").off('click').click(function (e) {
  			e.preventDefault();
  			var supplierId = $("#supplierID").val(),
  			    error = $("#errorDisplay"),
  			    boolVal = true,
  			    data = {
  				supplierName: $('#supplierName').val(),
  				supplierGender: $('#supplierGender:checked').val(),
  				supplierAddress: $('#supplierAddress').val(),
  				phoneNumber: $('#phoneNumber').val()
  			};
  
  			$.each($(':input[required], select[required]'), function (value, index) {
  				if ($(this).val() == "") {
  					boolVal = false;
  					return;
  				}
  			});
  
  			if (!boolVal) return;
  
  			if (supplierId == "") {
  				_apiRestCall2['default'].performRequest('/v1/supplier', 'POST', data, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							$('html, body').animate({
  								scrollTop: $('body').offset().top
  							}, 'slow');
  							$("#alert #alert-title").html("Success");
  							$("#alert #alert-content").html("Save Supplier");
  							$("#alert").addClass('alert-success').show();
  							$("#alert").fadeOut(2000);
  							setValue("", "", "MALE", "", "");
  						}
  					}
  				});
  			} else {
  				_apiRestCall2['default'].performRequest('/v1/supplier/' + supplierId, 'PUT', data, function (data) {
  					if (typeof data.error != 'undefined') {
  						error.html(typeof data.error_description == 'undefined' ? data.message : data.error_description);
  						error.css('display', 'block');
  					} else {
  						if (data.status == "ERROR") {
  							error.html(data.message);
  							error.css('display', 'block');
  						} else {
  							window.location.assign('/supplier');
  						}
  					}
  				});
  			}
  		});
  	},
  	items: [{
  		value: '',
  		name: 'supplierID',
  		type: 'hidden',
  		cols: [2, 10]
  	}, {
  		text: 'Supplier Name',
  		name: 'supplierName',
  		type: 'text',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Gender',
  		type: 'radio',
  		cols: [2, 10],
  		data: [{
  			name: 'supplierGender',
  			text: 'MALE',
  			value: 'MALE'
  		}, {
  			name: 'supplierGender',
  			text: 'FEMALE',
  			value: 'FEMALE'
  		}]
  	}, {
  		text: 'Address',
  		name: 'supplierAddress',
  		rows: 5,
  		type: 'area',
  		required: 'required',
  		cols: [2, 10]
  	}, {
  		text: 'Phone Number',
  		name: 'phoneNumber',
  		type: 'text',
  		required: 'required',
  		placeholder: '(021) 800 800',
  		cols: [2, 10]
  	}, {
  		text: '',
  		name: 'errorDisplay',
  		type: 'label',
  		color: 'red',
  		display: 'none'
  	}, {
  		type: 'button',
  		data: [{
  			text: 'Save',
  			type: 'submit',
  			name: 'btnSave',
  			cls: 'btn-primary'
  		}, {
  			text: 'Back',
  			type: 'button',
  			name: 'btnBack',
  			cls: 'btn-default',
  			event: function event() {
  				window.location.assign("/supplier");
  			}
  		}]
  	}]
  }];
  
  var Supplier = (function () {
  	function Supplier() {
  		_classCallCheck(this, _Supplier);
  	}
  
  	_createClass(Supplier, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _Supplier = Supplier;
  	Supplier = (0, _decoratorsWithStyles2['default'])(_SupplierInputCss2['default'])(Supplier) || Supplier;
  	return Supplier;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "SupplierInput.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 108 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _SupplierCss = __webpack_require__(57);
  
  var _SupplierCss2 = _interopRequireDefault(_SupplierCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Supplier',
  	breadcrumbsIcon: 'fa-table',
  	subtitle: ['User', 'Supplier'],
  	toolbar: [{
  		type: "button",
  		name: "btnAdd",
  		text: "Add",
  		cls: "btn-primary",
  		event: function event(e) {
  			window.location.assign("/supplier-input");
  		}
  	}]
  };
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'User',
  	name: 'table-supplier',
  	panel: false,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	bodyAttr: '',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-supplier").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/supplier",
  				"columns": [{ "title": "Supplier ID", "className": "supplierId", "data": "supplierId", "visible": false }, { "title": "Supplier Name", "className": "supplierName", "data": "supplierName" }, { "title": "Gender", "className": "supplierGender", "data": "supplierGender" }, { "title": "Address", "className": "supplierAddress", "data": "supplierAddress" }, { "title": "Phone Number", "className": "phoneNumber", "data": "phoneNumber" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "supplierId",
  					"render": function render(data, type, full, meta) {
  						return "<a class='btn btn-default iEdit' href='supplier-input?id=" + data + "'>Edit</a> <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-supplier").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var supplierId = $(this).attr('data-attr'),
  						    supplierName = $(this).closest('tr').find('td.supplierName').text();
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> supplier ' + supplierName + '?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/supplier/' + supplierId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Delete Supplier");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  							});
  						});
  					});
  				}
  			});
  		}
  
  		loadTable();
  	}
  }];
  
  var Supplier = (function () {
  	function Supplier() {
  		_classCallCheck(this, _Supplier);
  	}
  
  	_createClass(Supplier, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _Supplier = Supplier;
  	Supplier = (0, _decoratorsWithStyles2['default'])(_SupplierCss2['default'])(Supplier) || Supplier;
  	return Supplier;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Supplier.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 109 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _ThumbnailCss = __webpack_require__(58);
  
  var _ThumbnailCss2 = _interopRequireDefault(_ThumbnailCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var dataPageHeading = {
  	title: 'Thumbnail',
  	additionalTitle: '',
  	breadcrumbsIcon: 'fa-th-list'
  };
  var dataContent1 = [];
  var dataContent2 = [{
  	mode: 'Thumbnail',
  	cls: 'col-lg-12',
  	panel: false,
  	items: [{
  		url: '',
  		col: 'col-sm-6 col-md-4',
  		caption: 'Thumbnail label',
  		desc: 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.'
  	}, {
  		url: '',
  		col: 'col-sm-6 col-md-4',
  		caption: 'Thumbnail label',
  		desc: 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.'
  	}, {
  		url: '',
  		col: 'col-sm-6 col-md-4',
  		caption: 'Thumbnail label',
  		desc: 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.'
  	}]
  }];
  
  var Thumbnail = (function () {
  	function Thumbnail() {
  		_classCallCheck(this, _Thumbnail);
  	}
  
  	_createClass(Thumbnail, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _Thumbnail = Thumbnail;
  	Thumbnail = (0, _decoratorsWithStyles2['default'])(_ThumbnailCss2['default'])(Thumbnail) || Thumbnail;
  	return Thumbnail;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent1: dataContent1,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Thumbnail.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 110 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _TransactionCss = __webpack_require__(59);
  
  var _TransactionCss2 = _interopRequireDefault(_TransactionCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Transaction',
  	additionalTitle: '',
  	subtitle: ['Report', 'Transaction'],
  	breadcrumbsIcon: 'fa-table',
  	toolbar: [{
  		type: "button",
  		name: "btnPrint",
  		text: "Print",
  		cls: "btn-success",
  		event: function event(e) {
  			e.preventDefault();
  		}
  	}]
  };
  var dataContent1 = [];
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'Transaction',
  	name: 'table-transaction',
  	iconHeader: 'fa-shopping-cart',
  	typeTable: 'table-responsive',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-transaction").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/transaction",
  				"columns": [{ "title": "Transaction ID", "className": "salesOrderId", "data": "salesOrderId", "visible": false }, { "title": "Order No", "className": "orderNo", "data": "orderNo" }, { "title": "Staff", "className": "staffId", "data": "staff",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.staffName : "";
  					}
  				}, { "title": "Customer", "className": "customerId", "data": "customer",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.customerName : "";
  					}
  				}, { "title": "Order Date", "className": "orderDate", "data": "orderDate",
  					"render": function render(data, type, full, meta) {
  						return moment(data).format(_coreApps2['default'].dateFormat);
  					}
  				}, { "title": "Status", "className": "status", "data": "status" }, { "title": "Note", "className": "note", "data": "note" }, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "salesOrderId",
  					"render": function render(data, type, full, meta) {
  						_coreApps2['default'].props[data] = full.details;
  						return "<label class='viewDetail' data-attr=" + data + "><span class='glyphicon glyphicon-eye-open'></span> Detail</label> &nbsp;" + " <a class='btn btn-default iEdit' href='transaction-input?id=" + data + "'>Edit</a>" + " <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>" + " <a class='btn btn-info iPrint' data-attr=" + data + ">Print</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-transaction").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var salesOrderId = $(this).attr('data-attr');
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/transaction/' + salesOrderId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Delete Transaction");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  							});
  						});
  					});
  					$(".iPrint").click(function (e) {
  						var salesOrderId = $(this).attr('data-attr');
  						$.ajax({
  							url: _coreApps2['default'].host + '/v1/transaction/' + salesOrderId + '/print',
  							type: "GET"
  						}).done(function () {
  							console.log('File download a success!');
  						}).fail(function () {
  							console.log('File download failed!');
  						});
  					});
  					$('label.viewDetail').css('cursor', 'pointer').click(function (e) {
  						var selected = $(this).attr('data-attr'),
  						    data = _coreApps2['default'].props[selected];
  						var row = "";
  						for (var i in data) {
  							row += '<tr>' + '<td>' + (parseInt(i) + 1) + '</td>' + '<td>' + data[i].product.productName + '</td>' + '<td>' + data[i].quantity + '</td>' + '<td>' + FormatToCurrency(data[i].price) + '</td>' + '</tr>';
  						}
  						$('.modal-title').html('Order Detail');
  						$('.modal-body').html('<div class="container-fluid">' + '<div class="row">' + '<table class="table">' + '<thead>' + '<tr>' + '<th>No</th>' + '<th>Product</th>' + '<th>Quantity</th>' + '<th>Price</th>' + '</tr>' + '</thead>' + '<tbody>' + row + '</tbody>' + '</table>' + '</div>' + '</div>');
  						$('.modal-footer').html("");
  						$('#popup').modal('show');
  					});
  				}
  			});
  		}
  
  		// loadTable();
  	}
  }];
  
  var Transaction = (function () {
  	function Transaction() {
  		_classCallCheck(this, _Transaction);
  	}
  
  	_createClass(Transaction, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _Transaction = Transaction;
  	Transaction = (0, _decoratorsWithStyles2['default'])(_TransactionCss2['default'])(Transaction) || Transaction;
  	return Transaction;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent1: dataContent1,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "Transaction.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 111 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _UserCss = __webpack_require__(60);
  
  var _UserCss2 = _interopRequireDefault(_UserCss);
  
  var _decoratorsWithStyles = __webpack_require__(3);
  
  var _decoratorsWithStyles2 = _interopRequireDefault(_decoratorsWithStyles);
  
  var _coreApps = __webpack_require__(4);
  
  var _coreApps2 = _interopRequireDefault(_coreApps);
  
  var _apiRestCall = __webpack_require__(5);
  
  var _apiRestCall2 = _interopRequireDefault(_apiRestCall);
  
  var dataPageHeading = {
  	title: 'Member',
  	subtitle: ['User', 'Member'],
  	additionalTitle: '',
  	breadcrumbsIcon: 'fa-table',
  	toolbar: [{
  		type: "button",
  		name: "btnAdd",
  		text: "Add",
  		cls: "btn-primary",
  		event: function event(e) {
  			window.location.assign("/register");
  		}
  	}]
  };
  var dataContent1 = [];
  var dataContent2 = [{
  	mode: 'Table',
  	cls: 'col-lg-12',
  	title: 'User',
  	name: 'table-user',
  	panel: false,
  	iconHeader: 'fa-table',
  	typeTable: 'table-responsive',
  	componentDidMount: function componentDidMount() {
  		var datatable;
  		function loadTable() {
  			datatable = $("#table-user").DataTable({
  				"retrieve": true,
  				"bAutoWidth": true,
  				"bProcessing": true,
  				"bServerSide": false,
  				"sAjaxSource": _coreApps2['default'].host + "/v1/user",
  				"columns": [{ "title": "User ID", "className": "userId", "data": "userId", "visible": false }, { "title": "Username", "className": "username", "data": "username" }, { "title": "role", "className": "roleId", "data": "role",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.roleName : "";
  					}
  				}, { "title": "Staff", "className": "staffId", "data": "staff",
  					"render": function render(data, type, full, meta) {
  						return data != null ? data.staffName : "";
  					}
  				}, { "title": "Email", "className": "email", "data": "email" }, { "title": "Verified", "className": "isVerified", "data": "isVerified",
  					"render": function render(data, type, full, meta) {
  						return data ? 'Yes' : 'No';
  					}
  				}, {
  					"title": "Action",
  					"className": 'iAction',
  					"sortable": false,
  					"data": "userId",
  					"render": function render(data, type, full, meta) {
  						return "<a class='btn btn-default iEdit' href='register?id=" + data + "'>Edit</a> <a class='btn btn-danger iDelete' data-attr=" + data + ">Delete</a></td>";
  					}
  				}],
  				"fnDrawCallback": function fnDrawCallback() {
  					$("#table-user").closest('.table-responsive').css('overflow', 'hidden');
  					$(".iDelete").click(function (e) {
  						e.preventDefault();
  						var userId = $(this).attr('data-attr');
  						$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
  						$("#PopUpConfirm").modal("show");
  						$("#PopUpConfirm #btnYes").click(function (e) {
  							_apiRestCall2['default'].performRequest('/v1/user/' + userId, 'DELETE', {}, function (data) {
  								$('html, body').animate({
  									scrollTop: $('body').offset().top
  								}, 'slow');
  								$("#PopUpConfirm").modal("hide");
  								$("#alert #alert-title").html("Success");
  								$("#alert #alert-content").html("Delete User");
  								$("#alert").addClass('alert-success').show();
  								$("#alert").fadeOut(2000);
  								datatable.destroy();
  								loadTable();
  							});
  						});
  					});
  				}
  			});
  		}
  
  		loadTable();
  	}
  }];
  
  var User = (function () {
  	function User() {
  		_classCallCheck(this, _User);
  	}
  
  	_createClass(User, [{
  		key: 'render',
  		value: function render() {
  			return null;
  		}
  	}]);
  
  	var _User = User;
  	User = (0, _decoratorsWithStyles2['default'])(_UserCss2['default'])(User) || User;
  	return User;
  })();
  
  var Content = _coreApps2['default'].Widget({
  	dataPageHeading: dataPageHeading,
  	dataContent1: dataContent1,
  	dataContent2: dataContent2
  }).Body;
  
  exports['default'] = Content;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "User.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 112 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  var _fbjsLibKeyMirror = __webpack_require__(127);
  
  var _fbjsLibKeyMirror2 = _interopRequireDefault(_fbjsLibKeyMirror);
  
  exports['default'] = (0, _fbjsLibKeyMirror2['default'])({
  	CHANGE_LOCATION: null
  });
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "ActionTypes.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 113 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
    value: true
  });
  
  var _flux = __webpack_require__(128);
  
  var dispatcher = new _flux.Dispatcher();
  
  exports['default'] = dispatcher;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "dispatcher.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 114 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  var _superagent = __webpack_require__(13);
  
  var _superagent2 = _interopRequireDefault(_superagent);
  
  var _fbjsLibExecutionEnvironment = __webpack_require__(7);
  
  var _fbjsLibExecutionEnvironment2 = _interopRequireDefault(_fbjsLibExecutionEnvironment);
  
  var getUrl = function getUrl(path) {
  	return path.startsWith('http') ? path : _fbjsLibExecutionEnvironment2['default'].canUseDOM ? path : process.env.WEBSITE_HOSTNAME ? 'http://' + process.env.WEBSITE_HOSTNAME + path : 'http://127.0.0.1:' + global.server.get('port') + path;
  };
  
  var http = {
  	get: function get(path) {
  		return new Promise(function (resolve, reject) {
  			_superagent2['default'].get(getUrl(path)).accept('application/json').end(function (err, res) {
  				if (err) {
  					if (err.status === 404) {
  						resolve(null);
  					} else {
  						reject(err);
  					}
  				} else {
  					resolve(res.body);
  				}
  			});
  		});
  	}
  };
  
  exports['default'] = http;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "http.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 115 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  var _fbjsLibExecutionEnvironment = __webpack_require__(7);
  
  var _coreDispatcher = __webpack_require__(113);
  
  var _coreDispatcher2 = _interopRequireDefault(_coreDispatcher);
  
  var _constantsActionTypes = __webpack_require__(112);
  
  var _constantsActionTypes2 = _interopRequireDefault(_constantsActionTypes);
  
  var location = {
  	navigateTo: function navigateTo(path, options) {
  		if (_fbjsLibExecutionEnvironment.canUseDOM) {
  			if (options && options.replace) {
  				window.history.replaceState({}, document.title, path);
  			} else {
  				window.history.pushState({}, document.title, path);
  			}
  		}
  
  		_coreDispatcher2['default'].dispatch({
  			type: _constantsActionTypes2['default'].CHANGE_LOCATION,
  			path: path
  		});
  	}
  };
  
  exports['default'] = location;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "location.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 116 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _fbjsLibEmptyFunction = __webpack_require__(126);
  
  var _fbjsLibEmptyFunction2 = _interopRequireDefault(_fbjsLibEmptyFunction);
  
  function withContext(ComposedComponent) {
  	return (function () {
  		function WithContext() {
  			_classCallCheck(this, WithContext);
  		}
  
  		_createClass(WithContext, [{
  			key: 'getChildContext',
  			value: function getChildContext() {
  				var context = this.props.context;
  				return {
  					onInsertCss: context.onInsertCss || _fbjsLibEmptyFunction2['default'],
  					onSetTitle: context.onSetTitle || _fbjsLibEmptyFunction2['default'],
  					onSetMeta: context.onSetMeta || _fbjsLibEmptyFunction2['default'],
  					onPageNotFound: context.onPageNotFound || _fbjsLibEmptyFunction2['default']
  				};
  			}
  		}, {
  			key: 'render',
  			value: function render() {
  				var _props = this.props;
  				var context = _props.context;
  
  				var other = _objectWithoutProperties(_props, ['context']);
  
  				return _react2['default'].createElement(ComposedComponent, other);
  			}
  		}], [{
  			key: 'propTypes',
  			value: {
  				context: _react.PropTypes.shape({
  					onInsertCss: _react.PropTypes.func,
  					onSetTitle: _react.PropTypes.func,
  					onSetMeta: _react.PropTypes.func,
  					onPageNotFound: _react.PropTypes.func
  				})
  			},
  			enumerable: true
  		}, {
  			key: 'childContextTypes',
  			value: {
  				onInsertCss: _react.PropTypes.func.isRequired,
  				onSetTitle: _react.PropTypes.func.isRequired,
  				onSetMeta: _react.PropTypes.func.isRequired,
  				onPageNotFound: _react.PropTypes.func.isRequired
  			},
  			enumerable: true
  		}]);
  
  		return WithContext;
  	})();
  }
  
  exports['default'] = withContext;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "withContext.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 117 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
  
  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
  
  var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
  
  function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _eventemitter3 = __webpack_require__(125);
  
  var _eventemitter32 = _interopRequireDefault(_eventemitter3);
  
  var _fbjsLibExecutionEnvironment = __webpack_require__(7);
  
  var EE = undefined;
  var viewport = { width: 1366, height: 768 };
  var RESIZE_EVENT = 'resize';
  
  function handleWindowResize() {
  	if (viewport.width !== window.innerWidth || viewport.height !== window.innerHeight) {
  		viewport = { width: window.innerWidth, height: window.innerHeight };
  		EE.emit(RESIZE_EVENT, viewport);
  	}
  }
  
  function withViewport(ComposedComponent) {
  	return (function (_Component) {
  		_inherits(WithViewport, _Component);
  
  		function WithViewport() {
  			_classCallCheck(this, WithViewport);
  
  			_get(Object.getPrototypeOf(WithViewport.prototype), 'constructor', this).call(this);
  
  			this.state = {
  				viewport: _fbjsLibExecutionEnvironment.canUseDOM ? { width: window.innerWidth, height: window.innerHeight } : viewport
  			};
  		}
  
  		_createClass(WithViewport, [{
  			key: 'componentDidMount',
  			value: function componentDidMount() {
  				if (!EE) {
  					EE = new _eventemitter32['default']();
  					window.addEventListener('resize', handleWindowResize);
  					window.addEventListener('orientationchange', handleWindowResize);
  				}
  				EE.on(RESIZE_EVENT, this.handleResize, this);
  			}
  		}, {
  			key: 'componentWillUnmount',
  			value: function componentWillUnmount() {
  				EE.removeListener(RESIZE_EVENT, this.handleResize, this);
  				if (!EE.listeners(RESIZE_EVENT, true)) {
  					window.removeEventListener('resize', handleWindowResize);
  					window.removeEventListener('orientationchange', handleWindowResize);
  					EE = null;
  				}
  			}
  		}, {
  			key: 'render',
  			value: function render() {
  				return _react2['default'].createElement(ComposedComponent, _extends({}, this.props, { viewport: this.state.viewport }));
  			}
  		}, {
  			key: 'handleResize',
  			value: function handleResize(value) {
  				this.setState({ viewport: value });
  			}
  		}]);
  
  		return WithViewport;
  	})(_react.Component);
  }
  exports['default'] = withViewport;
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "withViewport.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 118 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  var _this = this;
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  var _react = __webpack_require__(1);
  
  var _react2 = _interopRequireDefault(_react);
  
  var _reactRoutingSrcRouter = __webpack_require__(63);
  
  var _reactRoutingSrcRouter2 = _interopRequireDefault(_reactRoutingSrcRouter);
  
  var _coreHttp = __webpack_require__(114);
  
  var _coreHttp2 = _interopRequireDefault(_coreHttp);
  
  /* Component */
  
  var _componentsApp = __webpack_require__(65);
  
  var _componentsApp2 = _interopRequireDefault(_componentsApp);
  
  var _componentsContentPage = __webpack_require__(70);
  
  var _componentsContentPage2 = _interopRequireDefault(_componentsContentPage);
  
  var _componentsNotFoundPage = __webpack_require__(82);
  
  var _componentsNotFoundPage2 = _interopRequireDefault(_componentsNotFoundPage);
  
  var _componentsErrorPage = __webpack_require__(76);
  
  var _componentsErrorPage2 = _interopRequireDefault(_componentsErrorPage);
  
  /* Page */
  
  var _componentsLoginPage = __webpack_require__(80);
  
  var _componentsLoginPage2 = _interopRequireDefault(_componentsLoginPage);
  
  var _componentsDashboard = __webpack_require__(74);
  
  var _componentsDashboard2 = _interopRequireDefault(_componentsDashboard);
  
  var _componentsCustomer = __webpack_require__(73);
  
  var _componentsCustomer2 = _interopRequireDefault(_componentsCustomer);
  
  var _componentsCustomerInput = __webpack_require__(72);
  
  var _componentsCustomerInput2 = _interopRequireDefault(_componentsCustomerInput);
  
  var _componentsStaff = __webpack_require__(106);
  
  var _componentsStaff2 = _interopRequireDefault(_componentsStaff);
  
  var _componentsStaffInput = __webpack_require__(104);
  
  var _componentsStaffInput2 = _interopRequireDefault(_componentsStaffInput);
  
  var _componentsSupplier = __webpack_require__(108);
  
  var _componentsSupplier2 = _interopRequireDefault(_componentsSupplier);
  
  var _componentsSupplierInput = __webpack_require__(107);
  
  var _componentsSupplierInput2 = _interopRequireDefault(_componentsSupplierInput);
  
  var _componentsDepartment = __webpack_require__(75);
  
  var _componentsDepartment2 = _interopRequireDefault(_componentsDepartment);
  
  var _componentsProduct = __webpack_require__(89);
  
  var _componentsProduct2 = _interopRequireDefault(_componentsProduct);
  
  var _componentsProductInput = __webpack_require__(88);
  
  var _componentsProductInput2 = _interopRequireDefault(_componentsProductInput);
  
  var _componentsProductAdjustment = __webpack_require__(86);
  
  var _componentsProductAdjustment2 = _interopRequireDefault(_componentsProductAdjustment);
  
  var _componentsProductAdjustmentInput = __webpack_require__(85);
  
  var _componentsProductAdjustmentInput2 = _interopRequireDefault(_componentsProductAdjustmentInput);
  
  var _componentsProductCategory = __webpack_require__(87);
  
  var _componentsProductCategory2 = _interopRequireDefault(_componentsProductCategory);
  
  var _componentsSalesInvoice = __webpack_require__(95);
  
  var _componentsSalesInvoice2 = _interopRequireDefault(_componentsSalesInvoice);
  
  var _componentsSalesInvoiceInput = __webpack_require__(94);
  
  var _componentsSalesInvoiceInput2 = _interopRequireDefault(_componentsSalesInvoiceInput);
  
  var _componentsSalesOrder = __webpack_require__(97);
  
  var _componentsSalesOrder2 = _interopRequireDefault(_componentsSalesOrder);
  
  var _componentsSalesOrderInput = __webpack_require__(96);
  
  var _componentsSalesOrderInput2 = _interopRequireDefault(_componentsSalesOrderInput);
  
  var _componentsSalesPricing = __webpack_require__(99);
  
  var _componentsSalesPricing2 = _interopRequireDefault(_componentsSalesPricing);
  
  var _componentsSalesPricingInput = __webpack_require__(98);
  
  var _componentsSalesPricingInput2 = _interopRequireDefault(_componentsSalesPricingInput);
  
  var _componentsSalesReturn = __webpack_require__(101);
  
  var _componentsSalesReturn2 = _interopRequireDefault(_componentsSalesReturn);
  
  var _componentsSalesReturnInput = __webpack_require__(100);
  
  var _componentsSalesReturnInput2 = _interopRequireDefault(_componentsSalesReturnInput);
  
  var _componentsShippingOrder = __webpack_require__(103);
  
  var _componentsShippingOrder2 = _interopRequireDefault(_componentsShippingOrder);
  
  var _componentsShippingOrderInput = __webpack_require__(102);
  
  var _componentsShippingOrderInput2 = _interopRequireDefault(_componentsShippingOrderInput);
  
  var _componentsPayment = __webpack_require__(84);
  
  var _componentsPayment2 = _interopRequireDefault(_componentsPayment);
  
  var _componentsPaymentInput = __webpack_require__(83);
  
  var _componentsPaymentInput2 = _interopRequireDefault(_componentsPaymentInput);
  
  var _componentsCashIn = __webpack_require__(67);
  
  var _componentsCashIn2 = _interopRequireDefault(_componentsCashIn);
  
  var _componentsCashInInput = __webpack_require__(66);
  
  var _componentsCashInInput2 = _interopRequireDefault(_componentsCashInInput);
  
  var _componentsCashOut = __webpack_require__(69);
  
  var _componentsCashOut2 = _interopRequireDefault(_componentsCashOut);
  
  var _componentsCashOutInput = __webpack_require__(68);
  
  var _componentsCashOutInput2 = _interopRequireDefault(_componentsCashOutInput);
  
  var _componentsCostCategory = __webpack_require__(71);
  
  var _componentsCostCategory2 = _interopRequireDefault(_componentsCostCategory);
  
  var _componentsProfit = __webpack_require__(90);
  
  var _componentsProfit2 = _interopRequireDefault(_componentsProfit);
  
  var _componentsTransaction = __webpack_require__(110);
  
  var _componentsTransaction2 = _interopRequireDefault(_componentsTransaction);
  
  var _componentsReceivable = __webpack_require__(91);
  
  var _componentsReceivable2 = _interopRequireDefault(_componentsReceivable);
  
  var _componentsUser = __webpack_require__(111);
  
  var _componentsUser2 = _interopRequireDefault(_componentsUser);
  
  var _componentsRole = __webpack_require__(93);
  
  var _componentsRole2 = _interopRequireDefault(_componentsRole);
  
  var _componentsStaffPosition = __webpack_require__(105);
  
  var _componentsStaffPosition2 = _interopRequireDefault(_componentsStaffPosition);
  
  var _componentsThumbnail = __webpack_require__(109);
  
  var _componentsThumbnail2 = _interopRequireDefault(_componentsThumbnail);
  
  var _componentsRegisterPage = __webpack_require__(92);
  
  var _componentsRegisterPage2 = _interopRequireDefault(_componentsRegisterPage);
  
  var reactCookie = __webpack_require__(6);
  
  var router = new _reactRoutingSrcRouter2['default'](function (on) {
  	on('*', function callee$1$0(state, next) {
  		var component;
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					context$2$0.next = 2;
  					return regeneratorRuntime.awrap(next());
  
  				case 2:
  					component = context$2$0.sent;
  
  					if (!(state.path == '/login')) {
  						context$2$0.next = 11;
  						break;
  					}
  
  					if (!(typeof reactCookie.load('auth') != 'undefined')) {
  						context$2$0.next = 8;
  						break;
  					}
  
  					return context$2$0.abrupt('return', component && _react2['default'].createElement(
  						_componentsApp2['default'],
  						{ context: state.context },
  						_react2['default'].createElement(_componentsDashboard2['default'], null)
  					));
  
  				case 8:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsLoginPage2['default'], null));
  
  				case 9:
  					context$2$0.next = 20;
  					break;
  
  				case 11:
  					if (!(state.path == '/register')) {
  						context$2$0.next = 15;
  						break;
  					}
  
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsRegisterPage2['default'], null));
  
  				case 15:
  					if (!(typeof reactCookie.load('auth') == 'undefined')) {
  						context$2$0.next = 19;
  						break;
  					}
  
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsLoginPage2['default'], null));
  
  				case 19:
  					return context$2$0.abrupt('return', component && _react2['default'].createElement(
  						_componentsApp2['default'],
  						{ context: state.context },
  						component
  					));
  
  				case 20:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/login', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsLoginPage2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/register', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsRegisterPage2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsDashboard2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/staff', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsStaff2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/staff-input', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsStaffInput2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/customer', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsCustomer2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/customer-input', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsCustomerInput2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/supplier', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsSupplier2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/supplier-input', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsSupplierInput2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/product', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsProduct2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/product-input', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsProductInput2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/sales-invoice', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsSalesInvoice2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/sales-invoice-input', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsSalesInvoiceInput2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/sales-order', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsSalesOrder2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/sales-order-input', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsSalesOrderInput2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/sales-pricing', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsSalesPricing2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/sales-pricing-input', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsSalesPricingInput2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/sales-return', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsSalesReturn2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/sales-return-input', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsSalesReturnInput2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/product-adjustment', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsProductAdjustment2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/product-adjustment-input', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsProductAdjustmentInput2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/product-category', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsProductCategory2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/user', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsUser2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/department', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsDepartment2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/role', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsRole2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/staff-position', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsStaffPosition2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/shipping-order', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsShippingOrder2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/shipping-order-input', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsShippingOrderInput2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/payment', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsPayment2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/payment-input', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsPaymentInput2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/cash-in', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsCashIn2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/cash-in-input', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsCashInInput2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/cash-out', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsCashOut2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/cash-out-input', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsCashOutInput2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/cost-category', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsCostCategory2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/profit', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsProfit2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/transaction', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsTransaction2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  	on('/receivable', function callee$1$0() {
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					return context$2$0.abrupt('return', _react2['default'].createElement(_componentsReceivable2['default'], null));
  
  				case 1:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  
  	on('*', function callee$1$0(state) {
  		var content;
  		return regeneratorRuntime.async(function callee$1$0$(context$2$0) {
  			while (1) switch (context$2$0.prev = context$2$0.next) {
  				case 0:
  					context$2$0.next = 2;
  					return regeneratorRuntime.awrap(_coreHttp2['default'].get('/api/content'));
  
  				case 2:
  					content = context$2$0.sent;
  					return context$2$0.abrupt('return', content && _react2['default'].createElement(_componentsContentPage2['default'], content));
  
  				case 4:
  				case 'end':
  					return context$2$0.stop();
  			}
  		}, null, _this);
  	});
  
  	on('error', function (state, error) {
  		return state.statusCode === 404 ? _react2['default'].createElement(
  			_componentsApp2['default'],
  			{ context: state.context, error: error },
  			_react2['default'].createElement(_componentsNotFoundPage2['default'], null)
  		) : _react2['default'].createElement(
  			_componentsApp2['default'],
  			{ context: state.context, error: error },
  			_react2['default'].createElement(_componentsErrorPage2['default'], null)
  		);
  	});
  });
  
  exports['default'] = router;
  module.exports = exports['default'];

  // const content = await http.get(`/api/content?path=${state.path}`);

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "router.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 119 */
/***/ function(module, exports, __webpack_require__) {

  /* REACT HOT LOADER */ if (false) { (function () { var ReactHotAPI = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\node_modules\\react-hot-api\\modules\\index.js"), RootInstanceProvider = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\RootInstanceProvider.js"), ReactMount = require("react/lib/ReactMount"), React = require("react"); module.makeHot = module.hot.data ? module.hot.data.makeHot : ReactHotAPI(function () { return RootInstanceProvider.getRootInstances(ReactMount); }, React); })(); } (function () {
  
  'use strict';
  
  Object.defineProperty(exports, '__esModule', {
  	value: true
  });
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
  
  var _fs = __webpack_require__(11);
  
  var _fs2 = _interopRequireDefault(_fs);
  
  var exists = function exists(filename) {
  	return new Promise(function (resolve) {
  		_fs2['default'].exists(filename, resolve);
  	});
  };
  
  var readFile = function readFile(filename) {
  	return new Promise(function (resolve, reject) {
  		_fs2['default'].readFile(filename, 'utf8', function (err, data) {
  			if (err) {
  				reject(err);
  			} else {
  				resolve(data);
  			}
  		});
  	});
  };
  
  exports['default'] = { exists: exists, readFile: readFile };
  module.exports = exports['default'];

  /* REACT HOT LOADER */ }).call(this); if (false) { (function () { module.hot.dispose(function (data) { data.makeHot = module.makeHot; }); if (module.exports && module.makeHot) { var makeExportsHot = require("d:\\Project\\furniture-project\\node_modules\\react-hot-loader\\makeExportsHot.js"), foundReactClasses = false; if (makeExportsHot(module, require("react"))) { foundReactClasses = true; } var shouldAcceptModule = true && foundReactClasses; if (shouldAcceptModule) { module.hot.accept(function (err) { if (err) { console.error("Cannot not apply hot update to " + "fs.js" + ": " + err.message); } }); } } })(); }

/***/ },
/* 120 */
/***/ function(module, exports, __webpack_require__) {

  var isarray = __webpack_require__(121)
  
  /**
   * Expose `pathToRegexp`.
   */
  module.exports = pathToRegexp
  module.exports.parse = parse
  module.exports.compile = compile
  module.exports.tokensToFunction = tokensToFunction
  module.exports.tokensToRegExp = tokensToRegExp
  
  /**
   * The main path matching regexp utility.
   *
   * @type {RegExp}
   */
  var PATH_REGEXP = new RegExp([
    // Match escaped characters that would otherwise appear in future matches.
    // This allows the user to escape special characters that won't transform.
    '(\\\\.)',
    // Match Express-style parameters and un-named parameters with a prefix
    // and optional suffixes. Matches appear as:
    //
    // "/:test(\\d+)?" => ["/", "test", "\d+", undefined, "?", undefined]
    // "/route(\\d+)"  => [undefined, undefined, undefined, "\d+", undefined, undefined]
    // "/*"            => ["/", undefined, undefined, undefined, undefined, "*"]
    '([\\/.])?(?:(?:\\:(\\w+)(?:\\(((?:\\\\.|[^()])+)\\))?|\\(((?:\\\\.|[^()])+)\\))([+*?])?|(\\*))'
  ].join('|'), 'g')
  
  /**
   * Parse a string for the raw tokens.
   *
   * @param  {String} str
   * @return {Array}
   */
  function parse (str) {
    var tokens = []
    var key = 0
    var index = 0
    var path = ''
    var res
  
    while ((res = PATH_REGEXP.exec(str)) != null) {
      var m = res[0]
      var escaped = res[1]
      var offset = res.index
      path += str.slice(index, offset)
      index = offset + m.length
  
      // Ignore already escaped sequences.
      if (escaped) {
        path += escaped[1]
        continue
      }
  
      // Push the current path onto the tokens.
      if (path) {
        tokens.push(path)
        path = ''
      }
  
      var prefix = res[2]
      var name = res[3]
      var capture = res[4]
      var group = res[5]
      var suffix = res[6]
      var asterisk = res[7]
  
      var repeat = suffix === '+' || suffix === '*'
      var optional = suffix === '?' || suffix === '*'
      var delimiter = prefix || '/'
      var pattern = capture || group || (asterisk ? '.*' : '[^' + delimiter + ']+?')
  
      tokens.push({
        name: name || key++,
        prefix: prefix || '',
        delimiter: delimiter,
        optional: optional,
        repeat: repeat,
        pattern: escapeGroup(pattern)
      })
    }
  
    // Match any characters still remaining.
    if (index < str.length) {
      path += str.substr(index)
    }
  
    // If the path exists, push it onto the end.
    if (path) {
      tokens.push(path)
    }
  
    return tokens
  }
  
  /**
   * Compile a string to a template function for the path.
   *
   * @param  {String}   str
   * @return {Function}
   */
  function compile (str) {
    return tokensToFunction(parse(str))
  }
  
  /**
   * Expose a method for transforming tokens into the path function.
   */
  function tokensToFunction (tokens) {
    // Compile all the tokens into regexps.
    var matches = new Array(tokens.length)
  
    // Compile all the patterns before compilation.
    for (var i = 0; i < tokens.length; i++) {
      if (typeof tokens[i] === 'object') {
        matches[i] = new RegExp('^' + tokens[i].pattern + '$')
      }
    }
  
    return function (obj) {
      var path = ''
      var data = obj || {}
  
      for (var i = 0; i < tokens.length; i++) {
        var token = tokens[i]
  
        if (typeof token === 'string') {
          path += token
  
          continue
        }
  
        var value = data[token.name]
        var segment
  
        if (value == null) {
          if (token.optional) {
            continue
          } else {
            throw new TypeError('Expected "' + token.name + '" to be defined')
          }
        }
  
        if (isarray(value)) {
          if (!token.repeat) {
            throw new TypeError('Expected "' + token.name + '" to not repeat, but received "' + value + '"')
          }
  
          if (value.length === 0) {
            if (token.optional) {
              continue
            } else {
              throw new TypeError('Expected "' + token.name + '" to not be empty')
            }
          }
  
          for (var j = 0; j < value.length; j++) {
            segment = encodeURIComponent(value[j])
  
            if (!matches[i].test(segment)) {
              throw new TypeError('Expected all "' + token.name + '" to match "' + token.pattern + '", but received "' + segment + '"')
            }
  
            path += (j === 0 ? token.prefix : token.delimiter) + segment
          }
  
          continue
        }
  
        segment = encodeURIComponent(value)
  
        if (!matches[i].test(segment)) {
          throw new TypeError('Expected "' + token.name + '" to match "' + token.pattern + '", but received "' + segment + '"')
        }
  
        path += token.prefix + segment
      }
  
      return path
    }
  }
  
  /**
   * Escape a regular expression string.
   *
   * @param  {String} str
   * @return {String}
   */
  function escapeString (str) {
    return str.replace(/([.+*?=^!:${}()[\]|\/])/g, '\\$1')
  }
  
  /**
   * Escape the capturing group by escaping special characters and meaning.
   *
   * @param  {String} group
   * @return {String}
   */
  function escapeGroup (group) {
    return group.replace(/([=!:$\/()])/g, '\\$1')
  }
  
  /**
   * Attach the keys as a property of the regexp.
   *
   * @param  {RegExp} re
   * @param  {Array}  keys
   * @return {RegExp}
   */
  function attachKeys (re, keys) {
    re.keys = keys
    return re
  }
  
  /**
   * Get the flags for a regexp from the options.
   *
   * @param  {Object} options
   * @return {String}
   */
  function flags (options) {
    return options.sensitive ? '' : 'i'
  }
  
  /**
   * Pull out keys from a regexp.
   *
   * @param  {RegExp} path
   * @param  {Array}  keys
   * @return {RegExp}
   */
  function regexpToRegexp (path, keys) {
    // Use a negative lookahead to match only capturing groups.
    var groups = path.source.match(/\((?!\?)/g)
  
    if (groups) {
      for (var i = 0; i < groups.length; i++) {
        keys.push({
          name: i,
          prefix: null,
          delimiter: null,
          optional: false,
          repeat: false,
          pattern: null
        })
      }
    }
  
    return attachKeys(path, keys)
  }
  
  /**
   * Transform an array into a regexp.
   *
   * @param  {Array}  path
   * @param  {Array}  keys
   * @param  {Object} options
   * @return {RegExp}
   */
  function arrayToRegexp (path, keys, options) {
    var parts = []
  
    for (var i = 0; i < path.length; i++) {
      parts.push(pathToRegexp(path[i], keys, options).source)
    }
  
    var regexp = new RegExp('(?:' + parts.join('|') + ')', flags(options))
  
    return attachKeys(regexp, keys)
  }
  
  /**
   * Create a path regexp from string input.
   *
   * @param  {String} path
   * @param  {Array}  keys
   * @param  {Object} options
   * @return {RegExp}
   */
  function stringToRegexp (path, keys, options) {
    var tokens = parse(path)
    var re = tokensToRegExp(tokens, options)
  
    // Attach keys back to the regexp.
    for (var i = 0; i < tokens.length; i++) {
      if (typeof tokens[i] !== 'string') {
        keys.push(tokens[i])
      }
    }
  
    return attachKeys(re, keys)
  }
  
  /**
   * Expose a function for taking tokens and returning a RegExp.
   *
   * @param  {Array}  tokens
   * @param  {Array}  keys
   * @param  {Object} options
   * @return {RegExp}
   */
  function tokensToRegExp (tokens, options) {
    options = options || {}
  
    var strict = options.strict
    var end = options.end !== false
    var route = ''
    var lastToken = tokens[tokens.length - 1]
    var endsWithSlash = typeof lastToken === 'string' && /\/$/.test(lastToken)
  
    // Iterate over the tokens and create our regexp string.
    for (var i = 0; i < tokens.length; i++) {
      var token = tokens[i]
  
      if (typeof token === 'string') {
        route += escapeString(token)
      } else {
        var prefix = escapeString(token.prefix)
        var capture = token.pattern
  
        if (token.repeat) {
          capture += '(?:' + prefix + capture + ')*'
        }
  
        if (token.optional) {
          if (prefix) {
            capture = '(?:' + prefix + '(' + capture + '))?'
          } else {
            capture = '(' + capture + ')?'
          }
        } else {
          capture = prefix + '(' + capture + ')'
        }
  
        route += capture
      }
    }
  
    // In non-strict mode we allow a slash at the end of match. If the path to
    // match already ends with a slash, we remove it for consistency. The slash
    // is valid at the end of a path match, not in the middle. This is important
    // in non-ending mode, where "/test/" shouldn't match "/test//route".
    if (!strict) {
      route = (endsWithSlash ? route.slice(0, -2) : route) + '(?:\\/(?=$))?'
    }
  
    if (end) {
      route += '$'
    } else {
      // In non-ending mode, we need the capturing groups to match as much as
      // possible by using a positive lookahead to the end or next path segment.
      route += strict && endsWithSlash ? '' : '(?=\\/|$)'
    }
  
    return new RegExp('^' + route, flags(options))
  }
  
  /**
   * Normalize the given path string, returning a regular expression.
   *
   * An empty array can be passed in for the keys, which will hold the
   * placeholder key descriptions. For example, using `/user/:id`, `keys` will
   * contain `[{ name: 'id', delimiter: '/', optional: false, repeat: false }]`.
   *
   * @param  {(String|RegExp|Array)} path
   * @param  {Array}                 [keys]
   * @param  {Object}                [options]
   * @return {RegExp}
   */
  function pathToRegexp (path, keys, options) {
    keys = keys || []
  
    if (!isarray(keys)) {
      options = keys
      keys = []
    } else if (!options) {
      options = {}
    }
  
    if (path instanceof RegExp) {
      return regexpToRegexp(path, keys, options)
    }
  
    if (isarray(path)) {
      return arrayToRegexp(path, keys, options)
    }
  
    return stringToRegexp(path, keys, options)
  }


/***/ },
/* 121 */
/***/ function(module, exports) {

  module.exports = Array.isArray || function (arr) {
    return Object.prototype.toString.call(arr) == '[object Array]';
  };


/***/ },
/* 122 */
/***/ function(module, exports) {

  module.exports = require("babel/polyfill");

/***/ },
/* 123 */
/***/ function(module, exports) {

  module.exports = require("classnames");

/***/ },
/* 124 */
/***/ function(module, exports) {

  module.exports = require("cookie");

/***/ },
/* 125 */
/***/ function(module, exports) {

  module.exports = require("eventemitter3");

/***/ },
/* 126 */
/***/ function(module, exports) {

  module.exports = require("fbjs/lib/emptyFunction");

/***/ },
/* 127 */
/***/ function(module, exports) {

  module.exports = require("fbjs/lib/keyMirror");

/***/ },
/* 128 */
/***/ function(module, exports) {

  module.exports = require("flux");

/***/ },
/* 129 */
/***/ function(module, exports) {

  module.exports = require("front-matter");

/***/ },
/* 130 */
/***/ function(module, exports) {

  module.exports = require("http");

/***/ },
/* 131 */
/***/ function(module, exports) {

  module.exports = require("https");

/***/ },
/* 132 */
/***/ function(module, exports) {

  module.exports = require("jade");

/***/ },
/* 133 */
/***/ function(module, exports) {

  module.exports = require("lodash");

/***/ },
/* 134 */
/***/ function(module, exports) {

  module.exports = require("querystring");

/***/ },
/* 135 */
/***/ function(module, exports) {

  module.exports = require("react-dom/server");

/***/ }
/******/ ]);
//# sourceMappingURL=server.js.map