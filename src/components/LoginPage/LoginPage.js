import React, { PropTypes } from 'react';
import styles from './LoginPage.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import reactCookie from '../../core/react-cookie';
import RestCall from '../../api/RestCall';

function login() {
	var username = document.getElementById('username'),
		password = document.getElementById('password'),
		btnLogin = document.getElementById('btnLogin'),
		error = document.getElementById('errorDisplay');

	if (username.value == "") {
		error.innerHTML = "please input username!";
		error.style.display = 'block';
	}
	else if (password.value == "") {
		error.innerHTML = "please input password!";
		error.style.display = 'block';
	}
	else if (username.value.length < 3) {
		error.innerHTML = "username must be at least 3 characters!";
		error.style.display = 'block';	
	}
	else if (password.value.length < 5) {
		error.innerHTML = "password must be at least 5 characters!";
		error.style.display = 'block';	
	}
	else {
		error.innerHTML = "";
		error.style.display = 'none';
		btnLogin.innerHTML = "<img class='iloading' width='25px' src='/loading.gif'>";
		RestCall.performRequest('/v1/oauth/token', 'POST', {
			username: username.value,
			password: password.value,
			grant_type: 'password'
		}, function(data) {
			btnLogin.innerHTML = "Login";
			if (typeof data.error != 'undefined') {
				error.innerHTML = data.error_description;
				error.style.display = 'block';
			}
			else {
				if (data.status == "ERROR") {
					error.innerHTML = data.message;
					error.style.display = 'block';
				}
				else {
					var date = new Date();
					date.setSeconds(date.getSeconds() + data.expires_in);
					reactCookie.save('username', username.value.toLowerCase(), {expires: date, maxAge: data.expires_in});
					reactCookie.save('auth', data.access_token, {expires: date, maxAge: data.expires_in});
					window.location.assign("/");
				}
			}
		});
	}
}

var dataField = [{
	title: 'Please Sign In',
	name: 'formLogin',
	cls: 'col-md-4 col-md-offset-4',
	componentDidMount: function () {
		document.onkeydown=function(){
			if(window.event.keyCode=='13'){
				login();
			}
		}
	},
	items:[{
		text: 'Username',
		name: 'username',
		type: 'email',
	}, {
		text: 'Password',
		name: 'password',
		type: 'password',
	}, {
		text: 'Remember Me',
		name: 'remember',
		value: '0',
		type: 'checkbox',
	}, {
		text: '',
		name: 'errorDisplay',
		type: 'label',
		color: 'red',
		display: 'none'
	}, {
		text: 'Login',
		name: 'btnLogin',
		url: '/',
		event: function(e) {
			e.preventDefault();
			login();
		},
		type: 'button',
		cls: 'btn btn-lg btn-success btn-block',
		// additional: ['bitbucket', 'dropbox', 'facebook', 'flickr', 'github', 'google-plus', 'instagram', 'linkedin', 'pinterest', 'tumblr', 'twitter', 'vk']
	}]
}];

@withStyles(styles)

class LoginPage {
	render() {
		return (null);
	}
}

var Content = Apps.Users({ dataField: dataField });

export default Content;