import React, { PropTypes } from 'react';
import styles from './ShippingOrder.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Shipping Order',
		breadcrumbsIcon: 'fa-table',
		subtitle: ['Shipping Order'],
		toolbar: [{
			type: "button",
			name: "btnAdd",
			text: "Add",
			cls: "btn-primary",
			event: function(e) {
				window.location.assign("/shipping-order-input");
			}
		}]
	};
var dataContent2 = 
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'shipping-order',
		name: 'table-shipping-order',
		panel: false,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-shipping-order").DataTable({
    				"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/shipping-order",
					"columns": [
						{ "title": "Shipping Order ID", "className": "shippingOrderId", "data": "shippingOrderId", "visible": false },
						{ "title": "Shipping Order No", "className": "shippingNo", "data": "shippingNo" },
						{ "title": "Staff", "className": "staffId", "data": "staff",
							"render": function ( data, type, full, meta ) { 
								return (data != null) ? data.staffName : "";
							}
						},
						{ "title": "Shipping Date", "className": "shippingDate", "data": "shippingDate",
							"render": function ( data, type, full, meta ) { 
								return moment(data).format(Apps.dateFormat)
							}
						},
						{ "title": "Status", "className": "status", "data": "status" },
						{ "title": "Note", "className": "note", "data": "note" },
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"data": "shippingOrderId",
							"render": function ( data, type, full, meta ) { 
								Apps.props[data] = full.details;
								return "<label class='viewDetail' data-attr="+data+"><span class='glyphicon glyphicon-eye-open'></span> Detail</label> &nbsp;"+
								" <a class='btn btn-default iEdit' href='shipping-order-input?id="+data+"'>Edit</a>" + 
								" <a class='btn btn-danger iDelete' data-attr="+data+">Delete</a></td>" +
								" <a class='btn btn-info iPrint' data-attr="+data+">Print</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						$("#table-shipping-order").closest('.table-responsive').css('overflow', 'hidden');
						$(".iDelete").click(function(e) {
							e.preventDefault();
							var shippingOrderId = $(this).attr('data-attr');
							$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
							$("#PopUpConfirm").modal("show");
							$("#PopUpConfirm #btnYes").click(function(e) {
								RestCall.performRequest('/v1/shipping-order/'+shippingOrderId, 'DELETE', {}, function(data) {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#PopUpConfirm").modal("hide");
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Delete Shipping Order");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
								});
							});
						});
						$(".iPrint").click(function(e) {
							var shippingOrderId = $(this).attr('data-attr');
							window.location.assign('/shipping-order/'+shippingOrderId+'/print');
						})
						$('label.viewDetail').css('cursor', 'pointer').click(function(e) {
							var selected = $(this).attr('data-attr'),
								data = Apps.props[selected];
							var row = "";
							for (var i in data) {
								row += '<tr>' +
									'<td>'+(parseInt(i)+1)+'</td>' +
									'<td>'+data[i].product.productName+'</td>' +
									'<td>'+data[i].quantity+'</td>' +
								'</tr>';
							}
							$('.modal-title').html('Order Detail');
							$('.modal-body').html('<div class="container-fluid">' +
								'<div class="row">' +
									'<table class="table">' +
										'<thead>' +
											'<tr>' +
												'<th>No</th>' + 
												'<th>Product</th>' + 
												'<th>Quantity</th>' +
											'</tr>' +
										'</thead>' +
										'<tbody>' +
											row +
										'</tbody>' +
									'</table>' +
								'</div>' +
							'</div>');
							$('.modal-footer').html("")
							$('#popup').modal('show');
						});
					}
				});
			}

			loadTable();
		}
	}];

@withStyles(styles)

class ShippingOrder {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;