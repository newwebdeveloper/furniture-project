import React, { PropTypes } from 'react';
import styles from './CustomerInput.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Customer',
		additionalTitle: ' Input',
		breadcrumbsIcon: 'fa-pencil',
		subtitle: ['User', 'Customer', 'Customer Input']
	};
var dataContent2 = 
	[{
		mode: 'Input',
		name: 'formCustomer',
		title: 'Add Customer',
		iconHeader: 'fa-plus',
		cls: 'col-lg-12',
		componentDidMount: function () {
			function setValue (CustomerID, CustomerName, CustomerGender, CustomerBirthdate, CustomerAddress, PhoneNumber) {
				$('#customerID').val(CustomerID)
				$('#customerName').val(CustomerName)
				$('#customerGender[value='+CustomerGender+']').prop('checked', true);
				if (CustomerBirthdate != "") {
					CustomerBirthdate = CustomerBirthdate.split("-")
					$("#customerBirthdate.datepicker").datepicker('update', new Date(CustomerBirthdate[0], CustomerBirthdate[1] - 1, CustomerBirthdate[2].substr(0, 2)));
				}
				else {
					$("#customerBirthdate.datepicker").val('');
				}
				$('#customerAddress').val(CustomerAddress)
				$('#phoneNumber').val(PhoneNumber)
				$('#errorDisplay').text("")
				$(':input[required], select[required]').trigger("change");
			}
			if (typeof QueryString.id != 'undefined') {
				$.ajax({
					url: Apps.host + "/v1/customer/" + QueryString.id,
					type: 'GET',
					dataType: 'json',
					cache: false,
					success: function(data) {
						if (data.status == "OK") {
							var data = data.data;
							setValue(data.customerId, data.customerName, data.customerGender, data.customerBirthdate, data.customerAddress, data.phoneNumber);
						}
					}
				});
			}

			$("#btnSave").click(function(e) {
				e.preventDefault();
				var customerId = $("#customerID").val(),
					error = $('#errorDisplay'),
					boolVal = true,
					data = {
						customerName: $('#customerName').val(),
						customerGender: $('#customerGender:checked').val(),
						customerBirthdate: moment($('#customerBirthdate').val()).format("YYYY-MM-DD"),
						customerAddress: $('#customerAddress').val(),
						phoneNumber: $('#phoneNumber').val()
					}
				$.each($(':input[required], select[required]'), function(value, index) {
					if ($(this).val() == "") {
						boolVal = false;
						return;
					}
				});

				if (!boolVal)
					return;

				if (customerId == "") {
					RestCall.performRequest('/v1/customer', 'POST', data, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								$('html, body').animate({
									scrollTop: $('body').offset().top
								}, 'slow');
								$("#alert #alert-title").html("Success");
								$("#alert #alert-content").html("Save Customer");
								$("#alert").addClass('alert-success').show();
								$("#alert").fadeOut(2000);
								setValue("", "", "MALE", "", "", "");
							}
						}
					});
				}
				else {
					RestCall.performRequest('/v1/customer/'+customerId, 'PUT', data, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								window.location.assign("/customer");
							}
						}
					});
				}
			});
		},
		items:[{
			value: '',
			name: 'customerID',
			type: 'hidden',
			cols: [ 2, 10 ]
		}, {
			text: 'Customer Name',
			name: 'customerName',
			type: 'text',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Gender',
			type: 'radio',
			cols: [ 2, 10 ],
			data: [{
				name: 'customerGender',
				text: 'MALE',
				value: 'MALE'
			}, {
				name: 'customerGender',
				text: 'FEMALE',
				value: 'FEMALE'
			}]
		}, {
			text: 'Birth Date',
			name: 'customerBirthdate',
			type: 'datetime',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Address',
			name: 'customerAddress',
			rows: 5,
			required: 'required',
			type: 'area',
			cols: [ 2, 10 ]
		}, {
			text: 'Phone Number',
			name: 'phoneNumber',
			type: 'text',
			required: 'required',
			placeholder: '(021) 800 800',
			cols: [ 2, 10 ]
		}, {
			text: '',
			name: 'errorDisplay',
			type: 'label',
			color: 'red',
			display: 'none'
		}, {
			type: 'button',
			data: [{
				text: 'Save',
				type: 'submit',
				name: 'btnSave',
				cls: 'btn-primary'
			}, {
				text: 'Back',
				type: 'button',
				name: 'btnBack',
				cls: 'btn-default',
				event: function() {
					window.location.assign("/customer");
				}
			}]
		}]
	}];

@withStyles(styles)

class Customer {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;