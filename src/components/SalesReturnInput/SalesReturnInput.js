import React, { PropTypes } from 'react';
import styles from './SalesReturnInput.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataSalesReturn = {};
var dataPageHeading =
	{
		title: 'Sales Return',
		additionalTitle: ' Input',
		subtitle: ['Sales', 'Sales Return', 'Sales Return Input'],
		breadcrumbsIcon: 'fa-pencil'
	};
var dataContent1 = [];
var dataContent2 =
	[{
		mode: 'Input',
		name: 'formSalesReturnInput',
		title: 'Add Sales Return',
		iconHeader: 'fa-plus',
		cls: 'col-lg-12',
		componentDidMount: function () {
			function setValue (SalesReturnID, StaffId, StaffName, SalesInvoice, ReturnDate, Note) {
				$('#salesReturnId').val(SalesReturnID)
				$('#staff').val(StaffName).attr('data-id', StaffId)
				if (SalesInvoice == "")
					$('#salesInvoice').prop('selectedIndex', 0)
				else
					$('#salesInvoice').val(SalesInvoice)
				if (ReturnDate != "") {
					ReturnDate = ReturnDate.split("-");
					$("#returnDate.datepicker").datepicker('update', new Date(ReturnDate[0], ReturnDate[1] - 1, ReturnDate[2].substr(0, 2)));
				}
				else {
					$("#returnDate.datepicker").val("");
				}
				$('#note').val(Note)
				$('#errorDisplay').text("")
				$(':input[required], select[required]').trigger("change");
			}

			RestCall.performRequest('/v1/sales-invoice', 'GET', {}, function(data) {
				if (data.status == "OK") {
					var salesInvoiceOption = data.data;
					$.map(salesInvoiceOption || [], function(val, i) {
						$('#salesInvoice').append('<option value='+val.salesInvoiceId+'>'+val.invoiceNo+'</option>');
					});	
				}
			});
			
			// Autocomplete Staff
			RestCall.performRequest('/v1/staff', 'GET', {}, function(data) {
				var autocomplete = [], onSelected = false;
				if (data.status == "OK") {
					for (var i in data.data)
						autocomplete.push({ id: data.data[i].staffId, value: data.data[i].staffName });
				}
				var bloodhound = new Bloodhound({
					datumTokenizer: function (datum) {
						return Bloodhound.tokenizers.whitespace(datum.value);
					},
					queryTokenizer: Bloodhound.tokenizers.whitespace,
					local: autocomplete
				});
				
				// Initialize the Bloodhound suggestion engine
				bloodhound.initialize().done(function() {
					$('#staff').typeahead({
						highlight: true
					}, {
						templates: {
							empty: [
								'<div class="tt-suggestion">',
								'No Items Found',
								'</div>'
							].join('\n'),
							suggestion: function (data){
								// return '<div>' + data.id + ' – ' + data.value + '</div>'
								return '<div data-attr='+data.id+'>' + data.value + '</div>'
							}
						},
						displayKey: 'value',
						source: bloodhound.ttAdapter()
					}).on("typeahead:selected", function (e, datum) {
						dataSalesReturn.staffId = datum.id;
						$(this).closest('span').siblings().remove();
						onSelected = true;
					}).on("change", function(e) {
						if (!onSelected) 
							dataSalesReturn.staffId = $(".tt-suggestion:first").attr("data-attr")
						onSelected = false;
						if ($(this).val() != "")
							$(this).closest('span').siblings().remove();
					}).on("keydown", function(e) {
						if (e.keyCode == 9) { // TABKEY
							if (!onSelected) 
								dataSalesReturn.staffId = $(".tt-suggestion:first").attr("data-attr")
							onSelected = false;
							if ($(this).val() != "")
								$(this).closest('span').siblings().remove();
						}
					});
				});
			});
			if (typeof QueryString.id != 'undefined') {
				$.ajax({
					url: Apps.host + "/v1/sales-return/" + QueryString.id,
					type: 'GET',
					dataType: 'json',
					cache: false,
					success: function(data) {
						if (data.status == "OK") {
							var data = data.data;
							setValue(data.salesReturnId, data.staff.staffId, data.staff.staffName, data.salesInvoiceId, data.returnDate, data.note);

							dataSalesReturn.staffId = data.staff.staffId;
							dataSalesReturn.salesInvoiceId = data.salesInvoiceId;
							dataSalesReturn.returnDate = data.returnDate;
							dataSalesReturn.note = data.note;
						}
					}
				});
			}

			$("#btnSave").click(function(e) {
				e.preventDefault();
				var salesReturnId = $('#salesReturnId').val(),
					boolVal = true,
					error = $('#errorDisplay');

				$.each($(':input[required], select[required]'), function(value, index) {
					if ($(this).val() == "") {
						boolVal = false;
						return;
					}
				});

				if (!boolVal)
					return;

				dataSalesReturn.salesInvoiceId = $('#salesInvoice').val()
				dataSalesReturn.returnDate = moment($('#returnDate').val()).format("YYYY-MM-DD")
				dataSalesReturn.note = $('#note').val()
					
				if (salesReturnId == "") {
					RestCall.performRequest('/v1/sales-return', 'POST', dataSalesReturn, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								$('html, body').animate({
									scrollTop: $('body').offset().top
								}, 'slow');
								$("#alert #alert-title").html("Success");
								$("#alert #alert-content").html("Save Sales Return");
								$("#alert").addClass('alert-success').show();
								$("#alert").fadeOut(2000);
								setValue("", "", "", "", "", "");
							}
						}
					});
				}
				else {
					RestCall.performRequest('/v1/sales-return/'+salesReturnId, 'PUT', dataSalesReturn, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								window.location.assign('/sales-return');
							}
						}
					});
				}
			});
		},
		items:[{
			value: '',
			name: 'salesReturnId',
			type: 'hidden',
			cols: [ 2, 10 ]
		}, {
			text: 'Sales Invoice',
			name: 'salesInvoice',
			type: 'select',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Staff Name',
			name: 'staff',
			type: 'text',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Return Date',
			name: 'returnDate',
			type: 'datetime',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Note',
			name: 'note',
			rows: 5,
			type: 'area',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: '',
			name: 'errorDisplay',
			type: 'label',
			color: 'red',
			display: 'none'
		}, {
			type: 'button',
			data: [{
				text: 'Save',
				ts: 'submit',
				name: 'btnSave',
				cls: 'btn-primary'
			}, {
				text: 'Back',
				type: 'button',
				name: 'btnBack',
				cls: 'btn-default',
				event: function() {
					window.location.assign("/sales-return");
				}
			}]
		}]
	}];

@withStyles(styles)

class SalesReturnInput {
	render() {
		return (null)
	}
}

var Content = Apps.Widget({
	dataPageHeading: dataPageHeading,
	dataContent1: dataContent1,
	dataContent2: dataContent2
}).Body;

export default Content;
