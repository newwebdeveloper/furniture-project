import React, { PropTypes } from 'react';
import styles from './User.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Member',
		subtitle: ['User', 'Member'],
		additionalTitle: '',
		breadcrumbsIcon: 'fa-table',
		toolbar: [{
			type: "button",
			name: "btnAdd",
			text: "Add",
			cls: "btn-primary",
			event: function(e) {
				window.location.assign("/register");
			}
		}]
	};
var dataContent1 = [];
var dataContent2 = 
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'User',
		name: 'table-user',
		panel: false,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-user").DataTable({
					"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/user",
					"columns": [
						{ "title": "User ID", "className": "userId", "data": "userId", "visible": false },
						{ "title": "Username", "className": "username", "data": "username" },
						{ "title": "role", "className": "roleId", "data": "role",
							"render": function ( data, type, full, meta ) { 
								return (data != null) ? data.roleName : "";
							}
						},
						{ "title": "Staff", "className": "staffId", "data": "staff",
							"render": function ( data, type, full, meta ) { 
								return (data != null) ? data.staffName : "";
							}
						},
						{ "title": "Email", "className": "email", "data": "email" },
						{ "title": "Verified", "className": "isVerified", "data": "isVerified",
							"render": function ( data, type, full, meta ) { 
								return ((data) ? 'Yes' : 'No');
							} 
						},
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"data": "userId",
							"render": function ( data, type, full, meta ) { 
								return "<a class='btn btn-default iEdit' href='register?id="+data+"'>Edit</a> <a class='btn btn-danger iDelete' data-attr="+data+">Delete</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						$("#table-user").closest('.table-responsive').css('overflow', 'hidden');
						$(".iDelete").click(function(e) {
							e.preventDefault();
							var userId = $(this).attr('data-attr');
							$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
							$("#PopUpConfirm").modal("show");
							$("#PopUpConfirm #btnYes").click(function(e) {
								RestCall.performRequest('/v1/user/'+userId, 'DELETE', {}, function(data) {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#PopUpConfirm").modal("hide");
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Delete User");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
								});
							});
						});
					}
				});
			}

			loadTable();
		}
	}];

@withStyles(styles)

class User {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading, 
	dataContent1: dataContent1, 
	dataContent2: dataContent2 
}).Body;

export default Content;