import React, { PropTypes } from 'react';
import styles from './StaffPosition.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Staff Position',
		breadcrumbsIcon: 'fa-table',
		subtitle: ['Access User', 'Staff Position']
	};
var dataContent2 = 
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'staff-position',
		name: 'table-staff-position',
		panel: false,
		loadAll: true,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		bodyAttr: '',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-staff-position").DataTable({
    				"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/staff-position",
					"columns": [
						{ "title": "Staff Position ID", "className": 'staffPositionId', "data": "staffPositionId", "visible": false },
						{ "title": "Staff Position Name", "className": 'staffPositionName', "data": "staffPositionName" },
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"data": "staffPositionId",
							"render": function ( data, type, full, meta ) { 
								return "<a class='btn btn-default iEdit' data-attr="+data+">Edit</a> <a class='btn btn-danger iDelete' data-attr="+data+">Delete</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						event();
						$("#table-staff-position").closest('.table-responsive').css('overflow', 'hidden');
					}
				});
			}

			function reset() {
				$("#staffPositionId").val("");
				$("#staffPositionName").val("");
				$('#errorDisplay').text("")
				$(':input[required], select[required]').trigger("change");
			}

			function event () {
				$("#btnNew").off('click').click(function(e) {
					reset();
					$(':input[required], select[required]').trigger("change");
				});

				$("#btnSave").off('click').click(function(e) {
					e.preventDefault();

					var staffPositionId = $('#staffPositionId').val(),
						error = $('#errorDisplay'),
						boolVal = true,
						data = { staffPositionName: $('#staffPositionName').val() };

					$.each($(':input[required], select[required]'), function(value, index) {
						if ($(this).val() == "") {
							boolVal = false;
							return;
						}
					});

					if (!boolVal)
						return;

					if (staffPositionId == "") {
						RestCall.performRequest('/v1/staff-position', 'POST', data, function(data) {
							if (typeof data.error != 'undefined') {
								error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
								error.css('display', 'block');
							}
							else {
								if (data.status == "ERROR") {
									error.html(data.message);
									error.css('display', 'block');
								}
								else {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Save Staff Position");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
									reset();
								}
							}
						});
					}
					else {
						RestCall.performRequest('/v1/staff-position/'+staffPositionId, 'PUT', data, function(data) {
							if (typeof data.error != 'undefined') {
								error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
								error.css('display', 'block');
							}
							else {
								if (data.status == "ERROR") {
									error.html(data.message);
									error.css('display', 'block');
								}
								else {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Update Staff Position");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
									reset();
								}
							}
						});
					}
				})
				
				$(".iEdit").off('click').click(function(e) {
					e.preventDefault();
					var staffPositionIdEdit = $(this).attr('data-attr'),
						staffPositionNameEdit = $(this).closest('tr').find('td.staffPositionName').text();

					$('html, body').animate({
						scrollTop: $('form#formStaffPosition').offset().top
					}, 'slow');
					$('#staffPositionId').val(staffPositionIdEdit);
					$('#staffPositionName').val(staffPositionNameEdit);
					$(':input[required], select[required]').trigger("change");
				});

				$(".iDelete").off('click').click(function(e) {
					e.preventDefault();
					var staffPositionId = $(this).attr('data-attr'),
						staffPositionNameDelete = $(this).closest('tr').find('td.staffPositionName').text();
					$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> Staff Position '+staffPositionNameDelete+'?');
					$("#PopUpConfirm").modal("show");
					$("#PopUpConfirm #btnYes").click(function(e) {
						RestCall.performRequest('/v1/staff-position/'+staffPositionId, 'DELETE', {}, function(data) {
							$('html, body').animate({
								scrollTop: $('body').offset().top
							}, 'slow');
							$("#PopUpConfirm").modal("hide");
							$("#alert #alert-title").html("Success");
							$("#alert #alert-content").html("Delete Staff Position");
							$("#alert").addClass('alert-success').show();
							$("#alert").fadeOut(2000);
							datatable.destroy();
							loadTable();
							reset();
						});
					});
				});
			}

			loadTable();
		}
	},
	{
		mode: 'Input',
		name: 'formStaffPosition',
		title: 'Add Staff Position',
		loadAll: true,
		iconHeader: 'fa-plus',
		cls: 'col-lg-12',
		items:[{
			value: '',
			name: 'staffPositionId',
			type: 'hidden',
			cols: [ 2, 10 ]
		}, {
			text: 'Staff Position Name',
			name: 'staffPositionName',
			type: 'text',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: '',
			name: 'errorDisplay',
			type: 'label',
			color: 'red',
			display: 'none'
		}, {
			type: 'button',
			data: [{
				text: 'Save',
				type: 'submit',
				name: 'btnSave',
				cls: 'btn-primary'
			}, {
				text: 'New',
				type: 'reset',
				name: 'btnNew',
				cls: 'btn-default'
			}]
		}]
	}];

@withStyles(styles)

class StaffPosition {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;