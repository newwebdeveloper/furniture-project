import React from 'react';
import styles from './Header.css';
import withStyles from '../../decorators/withStyles';
import Link from '../../utils/Link';
import Navigation from '../Navigation';
import reactCookie from '../../core/react-cookie';

@withStyles(styles)
class Header {
	render() {
		return (<Navigation />);
	}
}

export default Header;