import React, { PropTypes } from 'react';
import styles from './SalesReturn.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Sales Return',
		breadcrumbsIcon: 'fa-table',
		subtitle: ['Sales', 'Sales Return'],
		toolbar: [{
			type: "button",
			name: "btnAdd",
			text: "Add",
			cls: "btn-primary",
			event: function(e) {
				window.location.assign("/sales-return-input");
			}
		}]
	};
var dataContent2 = 
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'User',
		name: 'table-sales-return',
		panel: false,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-sales-return").DataTable({
    				"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/sales-return",
					"columns": [
						{ "title": "Sales Return ID", "className": "salesReturnId", "data": "salesReturnId", "visible": false },
						{ "title": "Return Date", "className": "returnDate", "data": "returnDate",
							"render": function ( data, type, full, meta ) { 
								return moment(data).format(Apps.dateFormat)
							}
						},
						{ "title": "Status", "className": "status", "data": "status" },
						{ "title": "Note", "className": "note", "data": "note" },
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"data": "salesReturnId",
							"render": function ( data, type, full, meta ) { 
								return "<a class='btn btn-default iEdit' href='sales-return-input?id="+data+"'>Edit</a>" +
								" <a class='btn btn-danger iDelete' data-attr="+data+">Delete</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						$("#table-sales-return").closest('.table-responsive').css('overflow', 'hidden');
						$(".iDelete").click(function(e) {
							e.preventDefault();
							var salesReturnId = $(this).attr('data-attr');
							$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
							$("#PopUpConfirm").modal("show");
							$("#PopUpConfirm #btnYes").click(function(e) {
								RestCall.performRequest('/v1/sales-return/'+salesReturnId, 'DELETE', {}, function(data) {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#PopUpConfirm").modal("hide");
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Delete Sales Return");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
								});
							});
						});
					}
				});
			}

			loadTable();
		}
	}];

@withStyles(styles)

class SalesReturn {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;