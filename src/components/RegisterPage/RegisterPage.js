import React, { PropTypes } from 'react';
import styles from './RegisterPage.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';
var dataField = [{
	title: 'Sign Up',
	cls: 'col-md-4 col-md-offset-4',
	componentDidMount: function () {
		QueryString = function () {
			// This function is anonymous, is executed immediately and 
			// the return value is assigned to QueryString!
			var query_string = {};
			var query = window.location.search.substring(1);
			var vars = query.split("&");
			for (var i=0;i<vars.length;i++) {
				var pair = vars[i].split("=");
					// If first entry with this name
				if (typeof query_string[pair[0]] === "undefined") {
					query_string[pair[0]] = decodeURIComponent(pair[1]);
					// If second entry with this name
				} else if (typeof query_string[pair[0]] === "string") {
					var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
					query_string[pair[0]] = arr;
					// If third or later entry with this name
				} else {
					query_string[pair[0]].push(decodeURIComponent(pair[1]));
				}
			}
			return query_string;
		}();

		RestCall.performRequest('/v1/role', 'GET', {}, function(data) {
			if (data.status == "OK") {
				var role = data.data;
				$.map(role || [], function(val, i) {
					$('#roleMenu').append('<option value='+val.roleId+'>'+val.roleName+'</option>');
				});	
			}
		});
		RestCall.performRequest('/v1/staff', 'GET', {}, function(data) {
			if (data.status == "OK") {
				var staff = data.data;
				$.map(staff || [], function(val, i) {
					$('#staff').append('<option value='+val.staffId+'>'+val.staffName+'</option>');
				});	
			}
		});

		if (typeof QueryString.id != 'undefined') {
			$.ajax({
				url: Apps.host + "/v1/user/" + QueryString.id,
				type: 'GET',
				dataType: 'json',
				cache: false,
 				beforeSend: function (xhr){
 					xhr.setRequestHeader("Authorization", reactCookie.load('auth'));
 				},
				success: function(data) {
					if (data.status == "OK") {
						var data = data.data;
						console.log(data);
						$('#userId').val(data.userId)
						$('#username').val(data.username)
						$('#email').val(data.email)
						$('#roleMenu').val(data.role.roleId)
						$('#staff').val(data.staff.staffId)
						$('#verified').val((data.isVerified) ? 1 : 0)
					}
				}
			});
		}
	},
	items:[{
		name: 'userId',
		type: 'hidden',
	}, {
		text: 'Username',
		name: 'username',
		type: 'text',
	}, {
		text: 'E-mail',
		name: 'email',
		type: 'email',
	}, {
		text: 'Password',
		name: 'password',
		type: 'password',
	}, {
		text: 'Confirm Password',
		name: 'confirmPassword',
		type: 'password',
	}, {
		name: 'roleMenu',
		type: 'select',
		opt_text: '-- SELECT ROLE MENU --',
		options: []
	}, {
		name: 'staff',
		type: 'select',
		opt_text: '-- SELECT ROLE STAFF --',
		options: []
	}, {
		name: 'verified',
		type: 'select',
		display: 'none',
		opt_text: '-- SELECT Verified STAFF --',
		options: [{
			text: 'Yes',
			value: 1
		}, {
			text: 'No',
			value: 0
		}]
	}, {
		text: '',
		name: 'errorDisplay',
		type: 'label',
		color: 'red',
		display: 'none'
	}, {
		text: 'Register',
		url: '/',
		event: function(e) {
			e.preventDefault();
			var userId = document.getElementById('userId').value,
				data = {
					username: document.getElementById('username').value,
					email: document.getElementById('email').value,
					password: document.getElementById('password').value,
					roleId: document.getElementById('roleMenu').value,
					staffId: document.getElementById('staff').value
				}

			if (data.password != document.getElementById('confirmPassword').value) {
				alert('Password is Deference with Confirm!');
			}
			else {
				if (userId == "") {
					RestCall.performRequest('/v1/user', 'POST', data, function(data) {
						if (data.status == "OK") {
							if (data.data) {
								alert(data.status);
								window.location.assign('/');
							}
						}
					});
				}
				else {
					RestCall.performRequest('/v1/user/'+userId, 'PUT', data, function(data) {
						if (data.status == "OK") {
							if (data.data) {
								alert(data.status);
								window.location.assign('/');
							}
						}
					});	
				}
			}
		},
		type: 'button',
		cls: 'btn btn-lg btn-success btn-block'
	}]
}];
@withStyles(styles)

class RegisterPage {
	render() {
		return (null);
	}
}

var Content = Apps.Users({ dataField: dataField });

export default Content;