import React, { PropTypes } from 'react';
import styles from './Staff.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Staff',
		breadcrumbsIcon: 'fa-table',
		subtitle: ['User', 'Staff'],
		toolbar: [{
			type: "button",
			name: "btnAdd",
			text: "Add",
			cls: "btn-primary",
			event: function(e) {
				window.location.assign("/staff-input");
			}
		}]
	};
var dataContent2 = 
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'User',
		name: 'table-staff',
		panel: false,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		bodyAttr: '',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-staff").DataTable({
    				"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/staff",
					"columns": [
						{ "title": "Staff ID", "className": "staffId", "data": "staffId", "visible": false },
						{ "title": "Department", "className": "departmentId", "data": "department",
							"render": function ( data, type, full, meta ) { 
								return (data != null) ? data.departmentName : "";
							}
						},
						{ "title": "Position", "className": "staffPositionId", "data": "staffPosition",
							"render": function ( data, type, full, meta ) { 
								return (data != null) ? data.staffPositionName : "";
							}
						},
						{ "title": "Staff Name", "className": "staffName", "data": "staffName" },
						{ "title": "Gender", "className": "staffGender", "data": "staffGender" },
						{ "title": "Birth Date", "className": "staffBirthdate", "data": "staffBirthdate",
							"render": function ( data, type, full, meta ) { 
								return moment(data).format(Apps.dateFormat)
							}
						},
						{ "title": "Address", "className": "staffAddress", "data": "staffAddress" },
						{ "title": "Phone Number", "className": "phoneNumber", "data": "phoneNumber" },
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"data": "staffId",
							"render": function ( data, type, full, meta ) { 
								return "<a class='btn btn-default iEdit' href='staff-input?id="+data+"'>Edit</a> <a class='btn btn-danger iDelete' data-attr="+data+">Delete</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						$("#table-staff").closest('.table-responsive').css('overflow', 'hidden');
						$(".iDelete").click(function(e) {
							e.preventDefault();
							var staffId = $(this).attr('data-attr'),
								staffName = $(this).closest('tr').find('td.staffName').text();
							$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> staff '+staffName+'?');
							$("#PopUpConfirm").modal("show");
							$("#PopUpConfirm #btnYes").click(function(e) {
								RestCall.performRequest('/v1/staff/'+staffId, 'DELETE', {}, function(data) {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#PopUpConfirm").modal("hide");
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Delete Staff");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
								});
							});
						});
					}
				});
			}

			loadTable();
		}
	}];

@withStyles(styles)

class Staff {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;