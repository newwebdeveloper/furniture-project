import React, { PropTypes } from 'react';
import styles from './ProductInput.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Product',
		additionalTitle: ' Input',
		breadcrumbsIcon: 'fa-pencil',
		subtitle: ['Product', 'Product Input']
	};

var dataContent2 = 
[
	{
		mode: 'Input',
		name: 'formProduct',
		title: 'Add Product',
		iconHeader: 'fa-plus',
		cls: 'col-lg-12',
		componentDidMount: function () {
			function setValue (ProductID, ProductName, ProductCategoryID, InitialStock, CurrentStock, SellPrice, Status) {
				$('#productID').val(ProductID)
				$('#productName').val(ProductName)
				if (ProductCategoryID == "")
					$('#category-product').prop('selectedIndex',0)
				else
					$('#category-product').val(ProductCategoryID)
				$('#initialStock').val(InitialStock)
				$('#currentStock').val(CurrentStock)
				$('#sellPrice').val(SellPrice)
				if (Status == "")
					$('#status').prop('selectedIndex',0)
				else
					$('#status').val(Status)
				$('#errorDisplay').text("")
				$(':input[required], select[required]').trigger("change");
			}
			$.ajax({
				url: Apps.host + "/v1/product-category",
				type: 'GET',
				dataType: 'json',
				cache: false,
				success: function(data) {
					$('select#category-product').empty();
					if (data.status == "OK") {
						$.map(data.data || [], function(val, i) {
							$('select#category-product').append('<option value='+val.productCategoryId+'>'+val.productCategoryName+'</option>');
						});
					}
				},
				async: false
			});

			if (typeof QueryString.id != 'undefined') {
				$.ajax({
					url: Apps.host + '/v1/product/'+QueryString.id,
					type: 'GET',
					dataType: 'json',
					cache: false,
					success: function(data) {
						if (data.status == "OK") {
							var data = data.data;
							setValue(data.productId, data.productName, data.productCategory.productCategoryId, data.initialStock, data.currentStock, data.sellPrice, data.status);
						}
					}
				});
			}

			$("#productPhoto").change(function(e) {
				var files = e.target.files,
					attachmentFile = e.target.files,
					extension;
				if(attachmentFile.length) {
					extension = attachmentFile[0].name.substr(attachmentFile[0].name.length-3).toLowerCase();
					if(attachmentFile[0].size > 2000000)
					{
						alert("File size cannot exceed 2 MB limit");
						attachmentFile = null;
					}
					else if(extension != 'jpg' && extension != 'png' && extension != 'gif') {
						alert("File extension must be .jpg, .png, .gif");
						attachmentFile = null;
					}
				}
			})

			$("#btnSave").click(function(e) {
				e.preventDefault();
				var upload = new FormData();
				$.each($('#productPhoto')[0].files, function(i, file) {
					upload.append('file-'+i, file);
				});

				var productId = $('#productID').val(),
					error = $("#errorDisplay"),
					boolVal = true,
					data = {
						productName: $('#productName').val(),
						productCategoryId: $('#category-product').val(),
						stock: $('#initialStock').val(),
						sellPrice: $('#sellPrice').val(),
						status: $('#status').val(),
						productPhoto: upload
					};
				$.each($(':input[required], select[required]'), function(value, index) {
					if ($(this).val() == "") {
						boolVal = false;
						return;
					}
				});

				if (!boolVal)
					return;

				if (productId == "") {
					RestCall.performRequest('/v1/product', 'POST', data, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								$('html, body').animate({
									scrollTop: $('body').offset().top
								}, 'slow');
								$("#alert #alert-title").html("Success");
								$("#alert #alert-content").html("Save Product");
								$("#alert").addClass('alert-success').show();
								$("#alert").fadeOut(2000);
								setValue("", "", "", "", "", "", "");
							}
						}
					});
				}
				else {
					RestCall.performRequest('/v1/product/'+productId, 'PUT', data, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								window.location.assign('/product');
							}
						}
					});	
				}
			});
		},
		items:[{
			value: '',
			name: 'productID',
			type: 'hidden',
			cols: [ 2, 10 ]
		}, {
			text: 'Product Name',
			name: 'productName',
			type: 'text',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Category',
			type: 'select',
			name: 'category-product',
			multiple: false,
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Initial Stock',
			name: 'initialStock',
			type: 'number',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Current Stock',
			name: 'currentStock',
			type: 'number',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Sell Price',
			name: 'sellPrice',
			type: 'number',
			required: 'required',
			cols: [ 2, 10 ]
		}, {			
			text: 'Status',
			name: 'status',
			type: 'select',
			multiple: false,
			cols: [ 2, 10 ],
			required: 'required',
			data: [{
				text: 'NEW',
				value: 'NEW'
			}, {
				text: 'AVAILABLE',
				value: 'AVAILABLE'
			}, {
				text: 'SOLD OUT',
				value: 'SOLD OUT'
			}]
		}, {
			text: 'Photo',
			name: 'productPhoto',
			type: 'file',
			cols: [ 2, 10 ]
		}, {
			text: '',
			name: 'errorDisplay',
			type: 'label',
			color: 'red',
			display: 'none'
		}, {
			type: 'button',
			data: [{
				text: 'Save',
				type: 'submit',
				name: 'btnSave',
				cls: 'btn-primary'
			}, {
				text: 'Back',
				type: 'button',
				name: 'btnBack',
				cls: 'btn-default',
				event: function() {
					window.location.assign("/product");
				}
			}]
		}]
	},
];

@withStyles(styles)

class ProductInput {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;