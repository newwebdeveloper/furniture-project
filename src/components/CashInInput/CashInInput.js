import React, { PropTypes } from 'react';
import styles from './CashInInput.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Cash In',
		additionalTitle: ' Input',
		breadcrumbsIcon: 'fa-pencil',
		subtitle: ['Payment', 'Cash In', 'Cash In Input']
	};

var dataContent2 = 
[
	{
		mode: 'Input',
		name: 'formCashIn',
		title: 'Add Cash In',
		iconHeader: 'fa-plus',
		cls: 'col-lg-12',
		componentDidMount: function () {
			function setValue (CashInID, Description, Amount, Note) {
				$("#cashInID").val(CashInID);
				$('#description').val(Description)
				$('#amount').val(Amount)
				$('#note').val(Note)
				$('#errorDisplay').text("")
				$(':input[required], select[required]').trigger("change");
			}
			if (typeof QueryString.id != 'undefined') {
				$.ajax({
					url: Apps.host + "/v1/cash-in/" + QueryString.id,
					type: 'GET',
					dataType: 'json',
					cache: false,
					success: function(data) {
						if (data.status == "OK") {
							var data = data.data;
							setValue(data.cashInId, data.description, data.amount, data.note);
						}
					}
				});
			}

			$("#btnSave").click(function(e) {
				e.preventDefault();
				var cashInId = $('#cashInID').val(),
					error = $("#errorDisplay"),
					boolVal = true,
					data = {
						description: $('#description').val(),
						amount: $('#amount').val(),
						note: $('#note').val()
					};

				$.each($(':input[required], select[required]'), function(value, index) {
					if ($(this).val() == "") {
						boolVal = false;
						return;
					}
				});

				if (!boolVal)
					return;

				if (cashInId == "") {
					RestCall.performRequest('/v1/cash-in', 'POST', data, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								$('html, body').animate({
									scrollTop: $('body').offset().top
								}, 'slow');
								$("#alert #alert-title").html("Success");
								$("#alert #alert-content").html("Save Cash In");
								$("#alert").addClass('alert-success').show();
								$("#alert").fadeOut(2000);
								setValue("", "", "", "", "");
							}
						}
					});
				}
				else {
					RestCall.performRequest('/v1/cash-in/'+cashInId, 'PUT', data, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								window.location.assign('/cash-in');
							}
						}
					});	
				}
			});
		},
		items:[{
			value: '',
			name: 'cashInID',
			type: 'hidden',
			cols: [ 2, 10 ]
		}, {
			text: 'Amount',
			name: 'amount',
			type: 'number',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Description',
			name: 'description',
			type: 'text',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Note',
			name: 'note',
			row: 5,
			type: 'area',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: '',
			name: 'errorDisplay',
			type: 'label',
			color: 'red',
			display: 'none'
		}, {
			type: 'button',
			data: [{
				text: 'Save',
				type: 'submit',
				name: 'btnSave',
				cls: 'btn-primary'
			}, {
				text: 'Back',
				type: 'button',
				name: 'btnBack',
				cls: 'btn-default',
				event: function() {
					window.location.assign("/cash-in");
				}
			}]
		}]
	},
];

@withStyles(styles)

class CashInInput {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;