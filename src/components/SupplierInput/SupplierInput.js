import React, { PropTypes } from 'react';
import styles from './SupplierInput.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Supplier',
		additionalTitle: ' Input',
		breadcrumbsIcon: 'fa-pencil',
		subtitle: ['User', 'Supplier', 'Supplier Input']
	};
var dataContent2 = 
	[{
		mode: 'Input',
		name: 'formSupplier',
		title: 'Add Supplier',
		iconHeader: 'fa-plus',
		cls: 'col-lg-12',
		componentDidMount: function () {
			function setValue (SupplierID, SupplierName, SupplierGender, SupplierAddress, PhoneNumber) {
				$('#supplierID').val(SupplierID)
				$('#supplierName').val(SupplierName)
				$('#supplierGender[value='+SupplierGender+']').prop('checked', true);
				$('#supplierAddress').val(SupplierAddress)
				$('#phoneNumber').val(PhoneNumber)
				$('#errorDisplay').text("")
				$(':input[required], select[required]').trigger("change");
			}
			if (typeof QueryString.id != 'undefined') {
				$.ajax({
					url: Apps.host + "/v1/supplier/" + QueryString.id,
					type: 'GET',
					dataType: 'json',
					cache: false,
					success: function(data) {
						if (data.status == "OK") {
							var data = data.data;
							setValue(data.supplierId, data.supplierName, data.supplierGender, data.supplierAddress, data.phoneNumber);
						}
					}
				});
			}

			$("#btnSave").off('click').click(function(e) {
				e.preventDefault();
				var supplierId = $("#supplierID").val(),
					error = $("#errorDisplay"),
					boolVal = true,
					data = {
						supplierName: $('#supplierName').val(),
						supplierGender: $('#supplierGender:checked').val(),
						supplierAddress: $('#supplierAddress').val(),
						phoneNumber: $('#phoneNumber').val()
					}
					
				$.each($(':input[required], select[required]'), function(value, index) {
					if ($(this).val() == "") {
						boolVal = false;
						return;
					}
				});

				if (!boolVal)
					return;

				if (supplierId == "") {
					RestCall.performRequest('/v1/supplier', 'POST', data, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								$('html, body').animate({
									scrollTop: $('body').offset().top
								}, 'slow');
								$("#alert #alert-title").html("Success");
								$("#alert #alert-content").html("Save Supplier");
								$("#alert").addClass('alert-success').show();
								$("#alert").fadeOut(2000);
								setValue("", "", "MALE", "", "");
							}
						}
					});
				}
				else {
					RestCall.performRequest('/v1/supplier/'+supplierId, 'PUT', data, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								window.location.assign('/supplier');
							}
						}
					});
				}
			});
		},
		items:[{
			value: '',
			name: 'supplierID',
			type: 'hidden',
			cols: [ 2, 10 ]
		}, {
			text: 'Supplier Name',
			name: 'supplierName',
			type: 'text',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Gender',
			type: 'radio',
			cols: [ 2, 10 ],
			data: [{
				name: 'supplierGender',
				text: 'MALE',
				value: 'MALE'
			}, {
				name: 'supplierGender',
				text: 'FEMALE',
				value: 'FEMALE'
			}]
		}, {
			text: 'Address',
			name: 'supplierAddress',
			rows: 5,
			type: 'area',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Phone Number',
			name: 'phoneNumber',
			type: 'text',
			required: 'required',
			placeholder: '(021) 800 800',
			cols: [ 2, 10 ]
		}, {
			text: '',
			name: 'errorDisplay',
			type: 'label',
			color: 'red',
			display: 'none'
		}, {
			type: 'button',
			data: [{
				text: 'Save',
				type: 'submit',
				name: 'btnSave',
				cls: 'btn-primary'
			}, {
				text: 'Back',
				type: 'button',
				name: 'btnBack',
				cls: 'btn-default',
				event: function() {
					window.location.assign("/supplier");
				}
			}]
		}]
	}];

@withStyles(styles)

class Supplier {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;