import React, { PropTypes } from 'react';
import styles from './SalesPricing.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Sales Pricing',
		breadcrumbsIcon: 'fa-table',
		subtitle: ['Sales', 'Sales Pricing'],
		toolbar: [{
			type: "button",
			name: "btnAdd",
			text: "Add",
			cls: "btn-primary",
			event: function(e) {
				window.location.assign("/sales-pricing-input");
			}
		}]
	};
var dataContent2 = 
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'User',
		name: 'table-sales-pricing',
		panel: false,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		bodyAttr: '',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-sales-pricing").DataTable({
    				"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/sales-pricing",
					"columns": [
						{ "title": "Sales Pricing ID", "className": "salesPricingId", "data": "salesPricingId", "visible": false },
						{ "title": "Staff", "className": "staffId", "data": "staff",
							"render": function ( data, type, full, meta ) { 
								return (data != null) ? data.staffName : "";
							}
						},
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"data": "salesPricingId",
							"render": function ( data, type, full, meta ) {
								Apps.props[data] = full.details; 
								return "<label class='viewDetail' data-attr="+data+"><span class='glyphicon glyphicon-eye-open'></span> Detail</label> &nbsp;" +
								"<a class='btn btn-default iEdit' href='sales-pricing-input?id="+data+"'>Edit</a>" +
								" <a class='btn btn-danger iDelete' data-attr="+data+">Delete</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						$("#table-sales-pricing").closest('.table-responsive').css('overflow', 'hidden');
						$(".iDelete").click(function(e) {
							e.preventDefault();
							var salesPricingId = $(this).attr('data-attr');
							$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
							$("#PopUpConfirm").modal("show");
							$("#PopUpConfirm #btnYes").click(function(e) {
								RestCall.performRequest('/v1/sales-pricing/'+salesPricingId, 'DELETE', {}, function(data) {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#PopUpConfirm").modal("hide");
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Delete Sales Invoice");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
								});
							});
						});
						$('label.viewDetail').css('cursor', 'pointer').click(function(e) {
							var selected = $(this).attr('data-attr'),
								data = Apps.props[selected];
							var row = "";
							for (var i in data) {
								row += '<tr>' +
									'<td>'+(parseInt(i)+1)+'</td>' +
									'<td>'+data[i].product.productName+'</td>' +
									'<td>'+FormatToCurrency(data[i].price)+'</td>' +
								'</tr>';
							}
							$('.modal-title').html('Pricing Detail');
							$('.modal-body').html('<div class="container-fluid">' +
								'<div class="row">' +
									'<table class="table">' +
										'<thead>' +
											'<tr>' +
												'<th>No</th>' + 
												'<th>Product</th>' + 
												'<th>Price</th>' + 
											'</tr>' +
										'</thead>' +
										'<tbody>' +
											row +
										'</tbody>' +
									'</table>' +
								'</div>' +
							'</div>');
							$('.modal-footer').html("")
							$('#popup').modal('show');
						});
					}
				});
			}

			loadTable();
		}
	}];

@withStyles(styles)

class SalesPricing {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;