import React, { PropTypes } from 'react';
import styles from './Customer.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Customer',
		breadcrumbsIcon: 'fa-table',
		subtitle: ['User', 'Customer'],
		toolbar: [{
			type: "button",
			name: "btnAdd",
			text: "Add",
			cls: "btn-primary",
			event: function(e) {
				window.location.assign("/customer-input");
			}
		}]
	};
var dataContent2 = 
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'User',
		name: 'table-customer',
		panel: false,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		bodyAttr: '',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-customer").DataTable({
					"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/customer",
					"columns": [
						{ "title": "Customer ID", "className": "customerId", "data": "customerId", "visible": false },
						{ "title": "Customer Name", "className": "customerName", "data": "customerName" },
						{ "title": "Gender", "className": "customerGender", "data": "customerGender" },
						{ "title": "Birth Date", "className": "customerBirthdate", "data": "customerBirthdate",
							"render": function ( data, type, full, meta ) { 
								return moment(data).format(Apps.dateFormat)
							}
						},
						{ "title": "Address", "className": "customerAddress", "data": "customerAddress" },
						{ "title": "Phone Number", "className": "phoneNumber", "data": "phoneNumber" },
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"data": "customerId",
							"render": function ( data, type, full, meta ) { 
								return "<a class='btn btn-default iEdit' href='customer-input?id="+data+"'>Edit</a> <a class='btn btn-danger iDelete' data-attr="+data+">Delete</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						$("#table-customer").closest('.table-responsive').css('overflow', 'hidden');
						$(".iDelete").click(function(e) {
							e.preventDefault();
							var customerId = $(this).attr('data-attr'),
								customerName = $(this).closest('tr').find('td.customerName').text();
							$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> Customer '+customerName+'?');
							$("#PopUpConfirm").modal("show");
							$("#PopUpConfirm #btnYes").click(function(e) {
								RestCall.performRequest('/v1/customer/'+customerId, 'DELETE', {}, function(data) {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#PopUpConfirm").modal("hide");
									if (typeof data.error != 'undefined') {
										$("#alert #alert-title").html("Error");
										$("#alert #alert-content").html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
										$("#alert").addClass('alert-danger').show();
										$("#alert").fadeOut(2000);
									}
									else {
										$("#alert #alert-title").html("Success");
										$("#alert #alert-content").html("Delete Customer");
										$("#alert").addClass('alert-success').show();
										$("#alert").fadeOut(2000);
										datatable.destroy();
										loadTable();
									}
								});
							});
						});
					}
				});
			}

			loadTable();
		}
	}];

@withStyles(styles)

class Customer {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;