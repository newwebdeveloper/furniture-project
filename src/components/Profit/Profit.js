import React, { PropTypes } from 'react';
import styles from './Profit.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading =
	{
		title: 'Profit',
		additionalTitle: '',
		subtitle: ['Report', 'Profit'],
		breadcrumbsIcon: 'fa-table',
		toolbar: [{
			type: "button",
			name: "btnPrint",
			text: "Print",
			cls: "btn-success",
			event: function(e) {
				e.preventDefault();
			}
		}]
	};
var dataContent1 = [];
var dataContent2 =
	[{
		mode: 'Input',
		loadAll: true,
		name: 'formProfit',
		cls: 'col-lg-12',
		componentDidMount: function () {
			$("#btnLoadData").click(function(e) {
				e.preventDefault();
				if ($("#startDate").val() == "" || $("#endDate").val() == "")
					return;

				var startDate = moment($("#startDate").val()).format("YYYY-MM-DD"),
					endDate = moment($("#endDate").val()).format("YYYY-MM-DD");
				
				$.ajax({
					url: Apps.host + "/v1/profit-loss-report/" + startDate + "." + endDate,
					type: 'GET',
					dataType: 'json',
					cache: false,
					success: function(data) {
						if (data.status == "OK") {
							var data = data.data;
							console.log(data);
						}
					}
				});
			});
		},
		items:[{
			text: 'Interval Date',
			type: 'datetime',
			range: true,
			nameStart: 'startDate',
			nameEnd: 'endDate',
			cols: [ 2, 10 ]
		}, {
			text: '',
			type: 'button',
			data: [{
				type: 'submit',
				cls: 'btn-primary',
				name: 'btnLoadData',
				text: 'Load'
			}]
		}]
	}, {
		type: 'BarChart',
		mode: 'Table',
		title: 'Profit',
		loadAll: true,
		cls: 'col-lg-12',
		table: [{
			typeTable: 'table-responsive',
			header: ['#', 'Date', 'Time', 'Amount'],
			body: [
				['3326', '10/21/2013', '3:29 PM', '$321.33'],
				['3325', '10/21/2013', '3:20 PM', '$234.34'],
				['3324', '10/21/2013', '3:03 PM', '$724.17'],
				['3323', '10/21/2013', '3:00 PM', '$23.71'],
				['3322', '10/21/2013', '2:49 PM', '$8345.23'],
				['3321', '10/21/2013', '2:23 PM', '$245.12'],
				['3320', '10/21/2013', '2:15 PM', '$5663.54'],
				['3319', '10/21/2013', '2:13 PM', '$943.45'],
			]	
		}],
		componentDidMount: function() {
			Morris.Bar({
				element: 'morris-bar-chart',
				data: [{
					y: '2006',
					a: 100,
					b: 90
				}, {
					y: '2007',
					a: 75,
					b: 65
				}, {
					y: '2008',
					a: 50,
					b: 40
				}, {
					y: '2009',
					a: 75,
					b: 65
				}, {
					y: '2010',
					a: 50,
					b: 40
				}, {
					y: '2011',
					a: 75,
					b: 65
				}, {
					y: '2012',
					a: 100,
					b: 90
				}],
				xkey: 'y',
				ykeys: ['a', 'b'],
				labels: ['Series A', 'Series B'],
				hideHover: 'auto',
				resize: true
			});
		},
		menu: [{
			text: 'Action',
			url: '#',
			separated: false
		}, {
			text: 'Another action',
			url: '#',
			separated: false
		}, {
			text: 'Something else here',
			url: '#',
			separated: false
		}, {
			text: 'Separated link',
			url: '#',
			separated: true
		}]
	}];

@withStyles(styles)

class Profit {
	render() {
		return (null)
	}
}

var Content = Apps.Widget({
	dataPageHeading: dataPageHeading,
	dataContent1: dataContent1,
	dataContent2: dataContent2
}).Body;

export default Content;
