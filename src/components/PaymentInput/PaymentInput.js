import React, { PropTypes } from 'react';
import styles from './PaymentInput.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPayment = {};
var dataPageHeading = 
	{
		title: 'Payment',
		additionalTitle: ' Input',
		breadcrumbsIcon: 'fa-pencil',
		subtitle: ['Payment', 'Payment Input']
	};

var dataContent2 = 
[
	{
		mode: 'Input',
		name: 'formPayment',
		title: 'Add Payment',
		iconHeader: 'fa-plus',
		cls: 'col-lg-12',
		componentDidMount: function () {
			function setValue (PaymentID, SalesInvoice, staffId, staffName, PaymentMethod, Amount, Note) {
				$('#paymentID').val(PaymentID)
				if (SalesInvoice == "")
					$('#salesInvoice').prop('selectedIndex', 0)
				else
					$('#salesInvoice').val(SalesInvoice)
				$('#staff').val(staffName).attr('data-id', staffId)
				if (PaymentMethod == "")
					$('#paymentMethod').prop('selectedIndex', 0)
				else
					$('#paymentMethod').val(PaymentMethod)
				$('#amount').val(Amount)
				$('#note').val(Note)
				$('#errorDisplay').text("")
				$(':input[required], select[required]').trigger("change");
			}

			RestCall.performRequest('/v1/sales-invoice', 'GET', {}, function(data) {
				if (data.status == "OK") {
					var salesInvoice = data.data;
					$.map(salesInvoice || [], function(val, i) {
						$('#salesInvoice').append('<option value='+val.salesInvoiceId+'>'+val.invoiceNo+'</option>');
					});	
				}
			});

			if (typeof QueryString.id != 'undefined') {
				$.ajax({
					url: Apps.host + "/v1/payment/" + QueryString.id,
					type: 'GET',
					dataType: 'json',
					cache: false,
					success: function(data) {
						if (data.status == "OK") {
							var data = data.data;
							setValue(data.paymentId, data.salesInvoice.salesInvoiceId, data.staff.staffId, data.staff.staffName, data.paymentMethod, data.amount, data.note)

							dataPayment.salesInvoiceId = data.salesInvoice.salesInvoiceId;
							dataPayment.staffId = data.staff.staffId;
							dataPayment.paymentMethod = data.paymentMethod;
							dataPayment.amount = data.amount;
							dataPayment.note = data.note;
						}
					}
				});
			}

			// Autocomplete Staff
			RestCall.performRequest('/v1/staff', 'GET', {}, function(data) {
				var autocomplete = [], onSelected = false;
				if (data.status == "OK") {
					for (var i in data.data)
						autocomplete.push({ id: data.data[i].staffId, value: data.data[i].staffName });
				}
				var bloodhound = new Bloodhound({
					datumTokenizer: function (datum) {
						return Bloodhound.tokenizers.whitespace(datum.value);
					},
					queryTokenizer: Bloodhound.tokenizers.whitespace,
					local: autocomplete
				});
				
				// Initialize the Bloodhound suggestion engine
				bloodhound.initialize().done(function() {
					$('#staff').typeahead({
						highlight: true
					}, {
						templates: {
							empty: [
								'<div class="tt-suggestion">',
								'No Items Found',
								'</div>'
							].join('\n'),
							suggestion: function (data){
								// return '<div>' + data.id + ' – ' + data.value + '</div>'
								return '<div data-attr='+data.id+'>' + data.value + '</div>'
							}
						},
						displayKey: 'value',
						source: bloodhound.ttAdapter()
					}).on("typeahead:selected", function (e, datum) {
						dataPayment.staffId = datum.id;
						$(this).closest('span').siblings().remove();
						onSelected = true;
					}).on("change", function(e) {
						if (!onSelected) 
							dataPayment.staffId = $(".tt-suggestion:first").attr("data-attr")
						onSelected = false;
						if ($(this).val() != "")
							$(this).closest('span').siblings().remove();
					}).on("keydown", function(e) {
						if (e.keyCode == 9) { // TABKEY
							if (!onSelected) 
								dataPayment.staffId = $(".tt-suggestion:first").attr("data-attr")
							onSelected = false;
							if ($(this).val() != "")
								$(this).closest('span').siblings().remove();
						}
					})
				});
			});

			$("#btnSave").click(function(e) {
				e.preventDefault();
				var paymentId = $('#paymentID').val(),
					boolVal = true,
					error = $("#errorDisplay");
				console.log(dataPayment);
				$.each($(':input[required], select[required]'), function(value, index) {
					if ($(this).val() == "") {
						boolVal = false;
						return;
					}
				});

				if (!boolVal)
					return;

				dataPayment.salesInvoiceId = $('#salesInvoice').val();
				dataPayment.paymentMethod = $('#paymentMethod').val();
				dataPayment.amount = $('#amount').val();
				dataPayment.note = $('#note').val();

				if (paymentId == "") {
					RestCall.performRequest('/v1/payment', 'POST', dataPayment, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								$('html, body').animate({
									scrollTop: $('body').offset().top
								}, 'slow');
								$("#alert #alert-title").html("Success");
								$("#alert #alert-content").html("Save Payment");
								$("#alert").addClass('alert-success').show();
								$("#alert").fadeOut(2000);
								setValue("", "", "", "", "CASH", "", "")
							}
						}
					});
				}
				else {
					RestCall.performRequest('/v1/payment/'+paymentId, 'PUT', dataPayment, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								window.location.assign('/payment');
							}
						}
					});	
				}
			});
		},
		items:[{
			value: '',
			name: 'paymentID',
			type: 'hidden',
			cols: [ 2, 10 ]
		}, {
			text: 'Invoice No',
			name: 'salesInvoice',
			type: 'select',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Staff Name',
			name: 'staff',
			type: 'autocomplete',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Payment Method',
			type: 'select',
			name: 'paymentMethod',
			multiple: false,
			cols: [ 2, 10 ],
			required: 'required',
			data: [{
				text: 'CASH',
				value: 'CASH'
			}, {
				text: 'CREDIT',
				value: 'CREDIT'
			}, {
				text: 'TRANSFER',
				value: 'TRANSFER'
			}]
		}, {
			text: 'Amount',
			name: 'amount',
			type: 'number',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Note',
			name: 'note',
			row: 5,
			type: 'area',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: '',
			name: 'errorDisplay',
			type: 'label',
			color: 'red',
			display: 'none'
		}, {
			type: 'button',
			data: [{
				text: 'Save',
				type: 'submit',
				name: 'btnSave',
				cls: 'btn-primary'
			}, {
				text: 'Back',
				type: 'button',
				name: 'btnBack',
				cls: 'btn-default',
				event: function() {
					window.location.assign("/payment");
				}
			}]
		}]
	},
];

@withStyles(styles)

class PaymentInput {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;