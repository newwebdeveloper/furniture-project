import React, { PropTypes } from 'react';
import styles from './Role.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Role',
		breadcrumbsIcon: 'fa-table',
		subtitle: ['Access User', 'Role']
	};
var dataContent2 = 
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'Role',
		name: 'table-role',
		panel: false,
		loadAll: true,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		bodyAttr: '',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-role").DataTable({
    				"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/role",
					"columns": [
						{ "title": "Role ID", "className": 'roleId', "data": "roleId", "visible": false },
						{ "title": "Role Name", "className": 'roleName', "data": "roleName" },
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"data": "roleId",
							"render": function ( data, type, full, meta ) { 
								return "<a class='btn btn-default iEdit' data-attr="+data+">Edit</a> <a class='btn btn-danger iDelete' data-attr="+data+">Delete</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						event();
						$("#table-role").closest('.table-responsive').css('overflow', 'hidden');
					}
				});
			}

			function reset() {
				$("#roleID").val("");
				$("#roleName").val("");
				$('#errorDisplay').text("")
				$(':input[required], select[required]').trigger("change");
			}

			function event() {
				$("#btnNew").off('click').click(function(e) {
					reset();
					$(':input[required], select[required]').trigger("change");
				});
				$("#btnSave").off('click').click(function(e) {
					e.preventDefault();
					var roleIdParam = $('#roleID').val(),
						error = $('#errorDisplay'),
						boolVal = true,
						data = { roleName: $('#roleName').val() }

					$.each($(':input[required], select[required]'), function(value, index) {
						if ($(this).val() == "") {
							boolVal = false;
							return;
						}
					});

					if (!boolVal)
						return;
					
					if (roleIdParam == "") {
						RestCall.performRequest('/v1/role', 'POST', data, function(data) {
							if (typeof data.error != 'undefined') {
								error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
								error.css('display', 'block');
							}
							else {
								if (data.status == "ERROR") {
									error.html(data.message);
									error.css('display', 'block');
								}
								else {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Save Role");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
									reset();
								}
							}
						});
					}
					else {
						RestCall.performRequest('/v1/role/'+roleIdParam, 'PUT', data, function(data) {
							if (typeof data.error != 'undefined') {
								error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
								error.css('display', 'block');
							}
							else {
								if (data.status == "ERROR") {
									error.html(data.message);
									error.css('display', 'block');
								}
								else {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Update Role");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
									reset();
								}
							}
						});
					}
				});
				
				$(".iEdit").off('click').click(function(e) {
					e.preventDefault();
					var roleIdEdit = $(this).attr('data-attr'),
						roleNameEdit = $(this).closest('tr').find('td.roleName').text();
					$('html, body').animate({
						scrollTop: $('form#formRole').offset().top
					}, 'slow');
					$('#roleID').val(roleIdEdit);
					$('#roleName').val(roleNameEdit);
					$(':input[required], select[required]').trigger("change");
				});

				$(".iDelete").off('click').click(function(e) {
					e.preventDefault();
					var roleId = $(this).attr('data-attr'),
						roleName = $(this).closest('tr').find('td.roleName').text();
					$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> Role '+roleName+'?');
					$("#PopUpConfirm").modal("show");
					$("#PopUpConfirm #btnYes").click(function(e) {
						RestCall.performRequest('/v1/role/'+roleId, 'DELETE', {}, function(data) {
							$('html, body').animate({
								scrollTop: $('body').offset().top
							}, 'slow');
							$("#PopUpConfirm").modal("hide");
							$("#alert #alert-title").html("Success");
							$("#alert #alert-content").html("Delete Role");
							$("#alert").addClass('alert-success').show();
							$("#alert").fadeOut(2000);
							datatable.destroy();
							loadTable();
							reset();
						});
					});
				});
			}

			loadTable();
		}
	},
	{
		mode: 'Input',
		name: 'formRole',
		loadAll: true,
		title: 'Add Role',
		iconHeader: 'fa-plus',
		cls: 'col-lg-12',
		items:[{
			value: '',
			name: 'roleID',
			type: 'hidden',
			cols: [ 2, 10 ]
		}, {
			text: 'Role Name',
			name: 'roleName',
			type: 'text',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: '',
			name: 'errorDisplay',
			type: 'label',
			color: 'red',
			display: 'none'
		}, {
			type: 'button',
			data: [{
				text: 'Save',
				type: 'submit',
				name: 'btnSave',
				cls: 'btn-primary'
			}, {
				text: 'New',
				type: 'reset',
				name: 'btnNew',
				cls: 'btn-default'
			}]
		}]
	}];

@withStyles(styles)

class Role {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;