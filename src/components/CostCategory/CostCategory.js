import React, { PropTypes } from 'react';
import styles from './CostCategory.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Cost Category',
		breadcrumbsIcon: 'fa-table',
		subtitle: ['Payment', 'Cost Category']
	};
var dataContent2 = 
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'cost-category',
		name: 'table-cost-category',
		panel: false,
		loadAll: true,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		bodyAttr: '',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-cost-category").DataTable({
    				"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/cost-category",
					"columns": [
						{ "title": "Cost Category ID", "className": 'costCategoryId', "data": "costCategoryId", "visible": false },
						{ "title": "Cost Category Name", "className": 'costCategoryName', "data": "costCategoryName" },
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"data": "costCategoryId",
							"render": function ( data, type, full, meta ) { 
								return "<a class='btn btn-default iEdit' data-attr="+data+">Edit</a> <a class='btn btn-danger iDelete' data-attr="+data+">Delete</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						event();
						$("#table-cost-category").closest('.table-responsive').css('overflow', 'hidden');
					}
				});
			}

			function reset() {
				$("#costCategoryID").val("");
				$("#costCategoryName").val("");
				$('#errorDisplay').text("")
				$(':input[required], select[required]').trigger("change");
			}

			function event() {
				$("#btnNew").off('click').click(function(e) {
					reset();
					$(':input[required], select[required]').trigger("change");
				});
				$("#btnSave").off('click').click(function(e) {
					e.preventDefault();
					var costCategoryIDParam = $('#costCategoryID').val(),
						error = $('#errorDisplay'),
						boolVal = true,
						data = { costCategoryName: $('#costCategoryName').val() }

					$.each($(':input[required], select[required]'), function(value, index) {
						if ($(this).val() == "") {
							boolVal = false;
							return;
						}
					});

					if (!boolVal)
						return;
					
					if (costCategoryIDParam == "") {
						RestCall.performRequest('/v1/cost-category', 'POST', data, function(data) {
							if (typeof data.error != 'undefined') {
								error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
								error.css('display', 'block');
							}
							else {
								if (data.status == "ERROR") {
									error.html(data.message);
									error.css('display', 'block');
								}
								else {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Save Cost Category");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
									reset();
								}
							}
						});
					}
					else {
						RestCall.performRequest('/v1/cost-category/'+costCategoryIDParam, 'PUT', data, function(data) {
							if (typeof data.error != 'undefined') {
								error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
								error.css('display', 'block');
							}
							else {
								if (data.status == "ERROR") {
									error.html(data.message);
									error.css('display', 'block');
								}
								else {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Update cost-category");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
									reset();
								}
							}
						});
					}
				});
				
				$(".iEdit").off('click').click(function(e) {
					e.preventDefault();
					var costCategoryIDEdit = $(this).attr('data-attr'),
						costCategoryNameEdit = $(this).closest('tr').find('td.costCategoryName').text();
					$('html, body').animate({
						scrollTop: $('form#formcost-category').offset().top
					}, 'slow');
					$('#costCategoryID').val(costCategoryIDEdit);
					$('#costCategoryName').val(costCategoryNameEdit);
					$(':input[required], select[required]').trigger("change");
				});

				$(".iDelete").off('click').click(function(e) {
					e.preventDefault();
					var costCategoryID = $(this).attr('data-attr'),
						costCategoryName = $(this).closest('tr').find('td.costCategoryName').text();
					$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> cost-category '+costCategoryName+'?');
					$("#PopUpConfirm").modal("show");
					$("#PopUpConfirm #btnYes").click(function(e) {
						RestCall.performRequest('/v1/cost-category/'+costCategoryID, 'DELETE', {}, function(data) {
							$('html, body').animate({
								scrollTop: $('body').offset().top
							}, 'slow');
							$("#PopUpConfirm").modal("hide");
							if (typeof data.error != 'undefined') {
								$("#alert #alert-title").html("Error");
								$("#alert #alert-content").html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
								$("#alert").addClass('alert-danger').show();
								$("#alert").fadeOut(2000);
							}
							else {
								$("#alert #alert-title").html("Success");
								$("#alert #alert-content").html("Delete cost-category");
								$("#alert").addClass('alert-success').show();
								$("#alert").fadeOut(2000);
								datatable.destroy();
								loadTable();
								reset();
							}
						});
					});
				});
			}

			loadTable();
		}
	},
	{
		mode: 'Input',
		name: 'formcost-category',
		title: 'Add cost-category',
		loadAll: true,
		iconHeader: 'fa-plus',
		cls: 'col-lg-12',
		items:[{
			value: '',
			name: 'costCategoryID',
			type: 'hidden',
			cols: [ 2, 10 ]
		}, {
			text: 'Cost Category Name',
			name: 'costCategoryName',
			type: 'text',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: '',
			name: 'errorDisplay',
			type: 'label',
			color: 'red',
			display: 'none'
		}, {
			type: 'button',
			data: [{
				text: 'Save',
				type: 'submit',
				name: 'btnSave',
				cls: 'btn-primary'
			}, {
				text: 'New',
				type: 'reset',
				name: 'btnNew',
				cls: 'btn-default'
			}]
		}]
	}];

@withStyles(styles)

class CostCategory {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;