import React, { PropTypes } from 'react';
import styles from './StaffInput.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Staff',
		additionalTitle: ' Input',
		breadcrumbsIcon: 'fa-pencil',
		subtitle: ['User', 'Staff', 'Staff Input']
	};
var dataContent2 = 
	[{
		mode: 'Input',
		name: 'formStaff',
		title: 'Add Staff',
		iconHeader: 'fa-plus',
		cls: 'col-lg-12',
		componentDidMount: function () {
			function setValue(StaffID, DepartmentId, staffPositionId, StaffName, StaffGender, StaffBirthdate, StaffAddress, PhoneNumber) {
				$('#staffID').val(StaffID)
				if (DepartmentId == "")
					$('#department').prop('selectedIndex',0)
				else
					$('#department').val(DepartmentId)
				if (staffPositionId == "")
					$('#staffPosition').prop('selectedIndex',0)
				else
					$('#staffPosition').val(staffPositionId)
				$('#staffName').val(StaffName)
				$('#staffGender[value='+StaffGender+']').prop('checked', true);
				if (StaffBirthdate != "") {
					StaffBirthdate = StaffBirthdate.split("-");
					$("#staffBirthdate.datepicker").datepicker('update', new Date(StaffBirthdate[0], StaffBirthdate[1] - 1, StaffBirthdate[2].substr(0, 2)));
				}
				else {
					$("#staffBirthdate.datepicker").val("");
				}
				$('#staffAddress').val(StaffAddress)
				$('#phoneNumber').val(PhoneNumber)
				$('#errorDisplay').text("")
				$(':input[required], select[required]').trigger("change");
			}

			RestCall.performRequest('/v1/department', 'GET', {}, function(data) {
				if (data.status == "OK") {
					var role = data.data;
					$.map(role || [], function(val, i) {
						$('#department').append('<option value='+val.departmentId+'>'+val.departmentName+'</option>');
					});	
				}
			});
			RestCall.performRequest('/v1/staff-position', 'GET', {}, function(data) {
				if (data.status == "OK") {
					var staff = data.data;
					$.map(staff || [], function(val, i) {
						$('#staffPosition').append('<option value='+val.staffPositionId+'>'+val.staffPositionName+'</option>');
					});	
				}
			});

			if (typeof QueryString.id != 'undefined') {
				$.ajax({
					url: Apps.host + "/v1/staff/" +QueryString.id,
					type: 'GET',
					dataType: 'json',
					cache: false,
					success: function(data) {
						if (data.status == "OK") {
							var data = data.data;
							setValue(data.staffId, data.department.departmentId, data.staffPosition.staffPositionId, data.staffName, data.staffGender, data.staffBirthdate, data.staffAddress, data.phoneNumber);
						}
					}
				});
			}

			$("#btnSave").click(function(e) {
				e.preventDefault();
				var staffId = $("#staffID").val(),
					error = $("#errorDisplay"),
					boolVal = true,
					data = {
						staffName: $('#staffName').val(),
						departmentId: $("#department").val(),
						staffPositionId: $("#staffPosition").val(),
						staffGender: $('#staffGender:checked').val(),
						staffBirthdate: moment($('#staffBirthdate').val()).format("YYYY-MM-DD"),
						staffAddress: $('#staffAddress').val(),
						phoneNumber: $('#phoneNumber').val()
					};
				$.each($(':input[required], select[required]'), function(value, index) {
					if ($(this).val() == "") {
						boolVal = false;
						return;
					}
				});

				if (!boolVal)
					return;

				if (staffId == "") {
					RestCall.performRequest('/v1/staff', 'POST', data, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								$('html, body').animate({
									scrollTop: $('body').offset().top
								}, 'slow');
								$("#alert #alert-title").html("Success");
								$("#alert #alert-content").html("Save Staff");
								$("#alert").addClass('alert-success').show();
								$("#alert").fadeOut(2000);
								setValue("", "", "", "", "MALE", "", "", "");
							}
						}
					});
				}
				else {
					RestCall.performRequest('/v1/staff/'+staffId, 'PUT', data, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								window.location.assign('/staff');
							}
						}
					});
				}
			});
		},
		items:[{
			value: '',
			name: 'staffID',
			type: 'hidden',
			cols: [ 2, 10 ]
		}, {
			text: 'Staff Name',
			name: 'staffName',
			type: 'text',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Department',
			name: 'department',
			type: 'select',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Position',
			name: 'staffPosition',
			type: 'select',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Gender',
			type: 'radio',
			cols: [ 2, 10 ],
			data: [{
				name: 'staffGender',
				text: 'MALE',
				value: 'MALE'
			}, {
				name: 'staffGender',
				text: 'FEMALE',
				value: 'FEMALE'
			}]
		}, {
			text: 'Birth Date',
			name: 'staffBirthdate',
			type: 'datetime',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Address',
			name: 'staffAddress',
			rows: 5,
			type: 'area',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Phone Number',
			name: 'phoneNumber',
			type: 'text',
			required: 'required',
			placeholder: '(021) 800 800',
			cols: [ 2, 10 ]
		}, {
			text: '',
			name: 'errorDisplay',
			type: 'label',
			color: 'red',
			display: 'none'
		}, {
			type: 'button',
			data: [{
				text: 'Save',
				type: 'submit',
				name: 'btnSave',
				cls: 'btn-primary'
			}, {
				text: 'Back',
				type: 'button',
				name: 'btnBack',
				cls: 'btn-default',
				event: function() {
					window.location.assign("/staff");
				}
			}]
		}]
	}];

@withStyles(styles)

class StaffInput {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;