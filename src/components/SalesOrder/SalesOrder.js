import React, { PropTypes } from 'react';
import styles from './SalesOrder.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading =
	{
		title: 'Sales Order',
		additionalTitle: '',
		subtitle: ['Sales', 'Sales Order'],
		breadcrumbsIcon: 'fa-table',
		toolbar: [{
			type: "button",
			name: "btnAdd",
			text: "Add",
			cls: "btn-primary",
			event: function(e) {
				window.location.assign("/sales-order-input");
			}
		}]
	};
var dataContent1 = [];
var dataContent2 =
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'Sales Order',
		name: 'table-sales-order',
		iconHeader: 'fa-shopping-cart',
		typeTable: 'table-responsive',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-sales-order").DataTable({
    				"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/sales-order",
					"columns": [
						{ "title": "Sales Order ID", "className": "salesOrderId", "data": "salesOrderId", "visible": false },
						{ "title": "Order No", "className": "orderNo", "data": "orderNo" },
						{ "title": "Staff", "className": "staffId", "data": "staff",
							"render": function ( data, type, full, meta ) { 
								return (data != null) ? data.staffName : "";
							}
						},
						{ "title": "Customer", "className": "customerId", "data": "customer",
							"render": function ( data, type, full, meta ) { 
								return (data != null) ? data.customerName : "";
							}
						},
						{ "title": "Order Date", "className": "orderDate", "data": "orderDate",
							"render": function ( data, type, full, meta ) { 
								return moment(data).format(Apps.dateFormat)
							}
						},
						{ "title": "Status", "className": "status", "data": "status" },
						{ "title": "Note", "className": "note", "data": "note" },
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"data": "salesOrderId",
							"render": function ( data, type, full, meta ) {
								Apps.props[data] = full.details;
								return "<label class='viewDetail' data-attr="+data+"><span class='glyphicon glyphicon-eye-open'></span> Detail</label> &nbsp;"+
								" <a class='btn btn-default iEdit' href='sales-order-input?id="+data+"'>Edit</a>"+
								" <a class='btn btn-danger iDelete' data-attr="+data+">Delete</a></td>" +
								" <a class='btn btn-info iPrint' data-attr="+data+">Print</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						$("#table-sales-order").closest('.table-responsive').css('overflow', 'hidden');
						$(".iDelete").click(function(e) {
							e.preventDefault();
							var salesOrderId = $(this).attr('data-attr');
							$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
							$("#PopUpConfirm").modal("show");
							$("#PopUpConfirm #btnYes").click(function(e) {
								RestCall.performRequest('/v1/sales-order/'+salesOrderId, 'DELETE', {}, function(data) {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#PopUpConfirm").modal("hide");
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Delete Sales Order");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
								});
							});
						});
						$(".iPrint").click(function(e) {
							var salesOrderId = $(this).attr('data-attr');
							$.ajax({
								url: Apps.host + '/v1/sales-order/'+salesOrderId+'/print',
								type: "GET"
							}).done(function () { console.log('File download a success!'); })
							.fail(function () { console.log('File download failed!'); });
						})
						$('label.viewDetail').css('cursor', 'pointer').click(function(e) {
							var selected = $(this).attr('data-attr'),
								data = Apps.props[selected];
							var row = "";
							for (var i in data) {
								row += '<tr>' +
									'<td>'+(parseInt(i)+1)+'</td>' +
									'<td>'+data[i].product.productName+'</td>' +
									'<td>'+data[i].quantity+'</td>' +
									'<td>'+FormatToCurrency(data[i].price)+'</td>' +
								'</tr>';
							}
							$('.modal-title').html('Order Detail');
							$('.modal-body').html('<div class="container-fluid">' +
								'<div class="row">' +
									'<table class="table">' +
										'<thead>' +
											'<tr>' +
												'<th>No</th>' + 
												'<th>Product</th>' + 
												'<th>Quantity</th>' + 
												'<th>Price</th>' + 
											'</tr>' +
										'</thead>' +
										'<tbody>' +
											row +
										'</tbody>' +
									'</table>' +
								'</div>' +
							'</div>');
							$('.modal-footer').html("")
							$('#popup').modal('show');
						});
					}
				});
			}

			loadTable();
		}
	}];

@withStyles(styles)

class SalesOrder {
	render() {
		return (null)
	}
}

var Content = Apps.Widget({
	dataPageHeading: dataPageHeading,
	dataContent1: dataContent1,
	dataContent2: dataContent2
}).Body;

export default Content;
