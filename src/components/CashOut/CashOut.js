import React, { PropTypes } from 'react';
import styles from './CashOut.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Cash Out',
		breadcrumbsIcon: 'fa-table',
		subtitle: ['Payment', 'Cash Out'],
		toolbar: [{
			type: "button",
			name: "btnAdd",
			text: "Add",
			cls: "btn-primary",
			event: function(e) {
				window.location.assign("/cash-out-input");
			}
		}]
	};
var dataContent2 = 
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'User',
		name: 'table-cash-out',
		panel: false,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-cash-out").DataTable({
					"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/cash-out",
					"columns": [
						{ "title": "Cash Out ID", "className": "cashOutId", "data": "cashOutId", "visible": false },
						{ "title": "Cost Category", "className": "costCategoryId", "data": "costCategory",
							"render": function ( data, type, full, meta ) { 
								return (data != null) ? data.costCategoryName : "";
							}
						},
						{ "title": "Cash Out Date", "className": "cashOutDate", "data": "cashOutDate",
							"render": function ( data, type, full, meta ) { 
								return moment(data).format(Apps.dateFormat)
							}
						},
						{ "title": "Description", "className": "description", "data": "description" },
						{ "title": "Amount", "className": "amount", "data": "amount", "type": "num-fmt",
							"render": function ( data, type, full, meta ) { 
								return FormatToCurrency(data);
							}
						},
						{ "title": "Note", "className": "note", "data": "note" },
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"data": "cashOutId",
							"render": function ( data, type, full, meta ) { 
								return "<a class='btn btn-default iEdit' href='cash-out-input?id="+data+"'>Edit</a> <a class='btn btn-danger iDelete' data-attr="+data+">Delete</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						$("#table-cash-out").closest('.table-responsive').css('overflow', 'hidden');
						$(".iDelete").click(function(e) {
							e.preventDefault();
							var cashOutId = $(this).attr('data-attr');
							$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
							$("#PopUpConfirm").modal("show");
							$("#PopUpConfirm #btnYes").click(function(e) {
								RestCall.performRequest('/v1/cash-out/'+cashOutId, 'DELETE', {}, function(data) {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#PopUpConfirm").modal("hide");
									if (typeof data.error != 'undefined') {
										$("#alert #alert-title").html("Error");
										$("#alert #alert-content").html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
										$("#alert").addClass('alert-danger').show();
										$("#alert").fadeOut(2000);
									}
									else {
										$("#alert #alert-title").html("Success");
										$("#alert #alert-content").html("Delete cash Out");
										$("#alert").addClass('alert-success').show();
										$("#alert").fadeOut(2000);
										datatable.destroy();
										loadTable();
									}
								});
							});
						});
					}
				});
			}

			loadTable();
		}
	}];

@withStyles(styles)

class CashOut {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;