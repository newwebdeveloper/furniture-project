import React, { PropTypes } from 'react';
import styles from './CashOutInput.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Cash Out',
		additionalTitle: ' Input',
		breadcrumbsIcon: 'fa-pencil',
		subtitle: ['Payment', 'Cash Out', 'Cash out Input']
	};

var dataContent2 = 
[
	{
		mode: 'input',
		name: 'formCashOut',
		title: 'Add Cash Out',
		iconHeader: 'fa-plus',
		cls: 'col-lg-12',
		componentDidMount: function () {
			function setValue(CashOutID, CostCategoryID, Description, Amount, Note) {
				$('#cashOutID').val(CashOutID)
				if (CostCategoryID == "")
					$('select#costCategory').prop('selectedIndex', 0)
				else
					$('select#costCategory').val(CostCategoryID)
				$('#description').val(Description)
				$('#amount').val(Amount)
				$('#note').val(Note)
				$('#errorDisplay').text("")
				$(':input[required], select[required]').trigger("change");
			}
			$.ajax({
				url: Apps.host + "/v1/cost-category",
				type: 'GET',
				dataType: 'json',
				cache: false,
				success: function(data) {
					$('select#costCategory').empty();
					if (data.status == "OK") {
						$.map(data.data || [], function(val, i) {
							$('select#costCategory').append('<option value='+val.costCategoryId+'>'+val.costCategoryName+'</option>');
						});
					}
				},
				async: false
			});

			if (typeof QueryString.id != 'undefined') {
				$.ajax({
					url: Apps.host + '/v1/cash-out/'+QueryString.id,
					type: 'GET',
					dataType: 'json',
					cache: false,
					success: function(data) {
						if (data.status == "OK") {
							var data = data.data;
							setValue(data.cashOutId, (data.costCategory != null) ? data.costCategory.costCategoryId : null, data.description, data.amount, data.note);
						}
					}
				});
			}

			$("#btnSave").click(function(e) {
				e.preventDefault();
				var cashOutId = $('#cashOutID').val(),
					error = $("#errorDisplay"),
					boolVal = true,
					data = {
						costCategoryId: $("#costCategory").val(),
						description: $('#description').val(),
						amount: $('#amount').val(),
						note: $('#note').val()
					};

				$.each($(':input[required], select[required]'), function(value, index) {
					if ($(this).val() == "") {
						boolVal = false;
						return;
					}
				});

				if (!boolVal)
					return;

				if (cashOutId == "") {
					RestCall.performRequest('/v1/cash-out', 'POST', data, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								$('html, body').animate({
									scrollTop: $('body').offset().top
								}, 'slow');
								$("#alert #alert-title").html("Success");
								$("#alert #alert-content").html("Save Cash Out");
								$("#alert").addClass('alert-success').show();
								$("#alert").fadeOut(2000);
								setValue("", "", "", "", "", "");
							}
						}
					});
				}
				else {
					RestCall.performRequest('/v1/cash-out/'+cashOutId, 'PUT', data, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								window.location.assign('/cash-out');
							}
						}
					});	
				}
			});
		},
		items:[{
			value: '',
			name: 'cashOutID',
			type: 'hidden',
			cols: [ 2, 10 ]
		}, {
			text: 'Amount',
			name: 'amount',
			type: 'number',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Cost Category',
			type: 'select',
			name: 'costCategory',
			multiple: false,
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Description',
			name: 'description',
			type: 'text',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Note',
			name: 'note',
			row: 5,
			type: 'area',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: '',
			name: 'errorDisplay',
			type: 'label',
			color: 'red',
			display: 'none'
		}, {
			type: 'button',
			data: [{
				text: 'Save',
				type: 'submit',
				name: 'btnSave',
				cls: 'btn-primary'
			}, {
				text: 'Back',
				type: 'button',
				name: 'btnBack',
				cls: 'btn-default',
				event: function() {
					window.location.assign("/cash-out");
				}
			}]
		}]
	},
];

@withStyles(styles)

class CashOutInput {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;