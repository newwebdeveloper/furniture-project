import React, { PropTypes } from 'react';
import styles from './Supplier.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Supplier',
		breadcrumbsIcon: 'fa-table',
		subtitle: ['User', 'Supplier'],
		toolbar: [{
			type: "button",
			name: "btnAdd",
			text: "Add",
			cls: "btn-primary",
			event: function(e) {
				window.location.assign("/supplier-input");
			}
		}]
	};
var dataContent2 = 
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'User',
		name: 'table-supplier',
		panel: false,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		bodyAttr: '',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-supplier").DataTable({
    				"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/supplier",
					"columns": [
						{ "title": "Supplier ID", "className": "supplierId", "data": "supplierId", "visible": false },
						{ "title": "Supplier Name", "className": "supplierName", "data": "supplierName" },
						{ "title": "Gender", "className": "supplierGender", "data": "supplierGender" },
						{ "title": "Address", "className": "supplierAddress", "data": "supplierAddress" },
						{ "title": "Phone Number", "className": "phoneNumber", "data": "phoneNumber" },
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"data": "supplierId",
							"render": function ( data, type, full, meta ) { 
								return "<a class='btn btn-default iEdit' href='supplier-input?id="+data+"'>Edit</a> <a class='btn btn-danger iDelete' data-attr="+data+">Delete</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						$("#table-supplier").closest('.table-responsive').css('overflow', 'hidden');
						$(".iDelete").click(function(e) {
							e.preventDefault();
							var supplierId = $(this).attr('data-attr'),
								supplierName = $(this).closest('tr').find('td.supplierName').text();
							$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> supplier '+supplierName+'?');
							$("#PopUpConfirm").modal("show");
							$("#PopUpConfirm #btnYes").click(function(e) {
								RestCall.performRequest('/v1/supplier/'+supplierId, 'DELETE', {}, function(data) {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#PopUpConfirm").modal("hide");
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Delete Supplier");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
								});
							});
						});
					}
				});
			}

			loadTable();
		},
	}];


@withStyles(styles)

class Supplier {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;