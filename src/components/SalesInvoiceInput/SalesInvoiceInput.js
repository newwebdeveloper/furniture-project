import React, { PropTypes } from 'react';
import styles from './SalesInvoiceInput.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataSalesInvoice = {};
var dataPageHeading =
	{
		title: 'Sales Invoice',
		additionalTitle: ' Input',
		subtitle: ['Sales', 'Sales Invoice', 'Sales Invoice Input'],
		breadcrumbsIcon: 'fa-pencil'
	};

function setValue (SalesInvoiceID, InvoiceNo, Shipping, StaffId, StaffName, InvoiceDate, Note) {
	$('#salesInvoiceId').val(SalesInvoiceID)
	$('#invoiceNo').val(InvoiceNo)
	if (Shipping == "")
		$('#shipping').prop('selectedIndex', 0)
	else	
		$('#shipping').val(Shipping)
	$('#staff').val(StaffName).attr('data-id', StaffId)
	if (InvoiceDate != "") {
		InvoiceDate = InvoiceDate.split("-")
		$("#invoiceDate.datepicker").datepicker('update', new Date(InvoiceDate[0], InvoiceDate[1] - 1, InvoiceDate[2].substr(0, 2)));
	}
	else {
		$("#invoiceDate.datepicker").val("");
	}
	$('#note').val(Note)
	$('#errorDisplay').text("")
	$(':input[required], select[required]').trigger("change");
}

var dataContent1 = [];
var dataContent2 =
	[{
		mode: 'Input',
		name: 'formSalesInvoiceInput',
		title: 'Add Sales Invoice',
		iconHeader: 'fa-plus',
		cls: 'col-lg-12',
		componentDidMount: function () {
			RestCall.performRequest('/v1/shipping-order', 'GET', {}, function(data) {
				if (data.status == "OK") {
					var shippingOrder = data.data;
					$.map(shippingOrder || [], function(val, i) {
						$('#shipping').append('<option value='+val.shippingOrderId+'>'+val.shippingNo+'</option>');
					});	
				}
			});
			if (typeof QueryString.id != 'undefined') {
				$("#invoiceNo").closest(".form-group").show();
				$.ajax({
					url: Apps.host + "/v1/sales-invoice/" + QueryString.id,
					type: 'GET',
					dataType: 'json',
					cache: false,
					success: function(data) {
						if (data.status == "OK") {
							var data = data.data;
							setValue(data.salesInvoiceId, data.invoiceNo, data.shippingOrder.shippingOrderId, data.staff.staffId, data.staff.staffName, data.invoiceDate, data.note);

							// dataSalesInvoice.invoiceNo = data.invoiceNo;
							dataSalesInvoice.shippingOrderId = data.shippingOrder.shippingOrderId;
							dataSalesInvoice.staffId = data.staff.staffId;
							dataSalesInvoice.invoiceDate = moment(data.invoiceDate).format("YYYY-MM-DD");
							dataSalesInvoice.note = data.note
						}
					}
				});
			}

			// Autocomplete Staff
			RestCall.performRequest('/v1/staff', 'GET', {}, function(data) {
				var autocomplete = [], onSelected = false;
				if (data.status == "OK") {
					for (var i in data.data)
						autocomplete.push({ id: data.data[i].staffId, value: data.data[i].staffName });
				}
				var bloodhound = new Bloodhound({
					datumTokenizer: function (datum) {
						return Bloodhound.tokenizers.whitespace(datum.value);
					},
					queryTokenizer: Bloodhound.tokenizers.whitespace,
					local: autocomplete
				});
				
				// Initialize the Bloodhound suggestion engine
				bloodhound.initialize().done(function() {
					$('#staff').typeahead({
						highlight: true
					}, {
						templates: {
							empty: [
								'<div class="tt-suggestion">',
								'No Items Found',
								'</div>'
							].join('\n'),
							suggestion: function (data){
								// return '<div>' + data.id + ' – ' + data.value + '</div>'
								return '<div data-attr='+data.id+'>' + data.value + '</div>'
							}
						},
						displayKey: 'value',
						source: bloodhound.ttAdapter()
					}).on("typeahead:selected", function (e, datum) {
						dataSalesInvoice.staffId = datum.id;
						$(this).closest('span').siblings().remove();
						onSelected = true;
					}).on("change", function(e) {
						if (!onSelected) 
							dataSalesInvoice.staffId = $(".tt-suggestion:first").attr("data-attr")
						onSelected = false;
						if ($(this).val() != "")
							$(this).closest('span').siblings().remove();
					}).on("keydown", function(e) {
						if (e.keyCode == 9) { // TABKEY
							if (!onSelected) 
								dataSalesInvoice.staffId = $(".tt-suggestion:first").attr("data-attr")
							onSelected = false;
							if ($(this).val() != "")
								$(this).closest('span').siblings().remove();
						}
					});
				});
			});

			$("#btnSave").click(function(e) {
				e.preventDefault();
				var salesInvoiceId = $('#salesInvoiceId').val(),
					boolVal = true,
					error = $('#errorDisplay');

				// dataSalesInvoice.invoiceNo = $('#invoiceNo').val();
				dataSalesInvoice.shippingOrderId = $('#shipping').val();
				dataSalesInvoice.invoiceDate = moment($('#invoiceDate').val()).format("YYYY-MM-DD");
				dataSalesInvoice.note = $('#note').val();

				$.each($(':input[required], select[required]'), function(value, index) {
					if ($(this).val() == "") {
						boolVal = false;
						return;
					}
				});

				if (!boolVal)
					return;

				if (salesInvoiceId == "") {
					RestCall.performRequest('/v1/sales-invoice', 'POST', dataSalesInvoice, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								$('html, body').animate({
									scrollTop: $('body').offset().top
								}, 'slow');
								$("#alert #alert-title").html("Success");
								$("#alert #alert-content").html("Save Sales Invoice");
								$("#alert").addClass('alert-success').show();
								$("#alert").fadeOut(2000);
								setValue("", "", "", "", "", "", "");
							}
						}
					});
				}
				else {
					RestCall.performRequest('/v1/sales-invoice/'+salesInvoiceId, 'PUT', dataSalesInvoice, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								window.location.assign('/sales-invoice');
							}
						}
					});
				}
			});
		},
		items:[{
			value: '',
			name: 'salesInvoiceId',
			type: 'hidden',
			cols: [ 2, 10 ]
		}, {
			text: 'Invoice No',
			name: 'invoiceNo',
			disabled: 'disabled',
			display: 'none',
			type: 'text',

			cols: [ 2, 10 ]
		}, {
			text: 'Shipping',
			name: 'shipping',
			type: 'select',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Staff',
			name: 'staff',
			type: 'text',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Invoice Date',
			name: 'invoiceDate',
			type: 'datetime',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Note',
			name: 'note',
			rows: 5,
			type: 'area',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: '',
			name: 'errorDisplay',
			type: 'label',
			color: 'red',
			display: 'none'
		}, {
			type: 'button',
			data: [{
				text: 'Save',
				ts: 'submit',
				name: 'btnSave',
				cls: 'btn-primary',
			}, {
				text: 'Back',
				type: 'button',
				name: 'btnBack',
				cls: 'btn-default',
				event: function() {
					window.location.assign("/sales-invoice");
				}
			}]
		}]
	}];

@withStyles(styles)

class SalesInvoiceInput {
	render() {
		return (null)
	}
}

var Content = Apps.Widget({
	dataPageHeading: dataPageHeading,
	dataContent1: dataContent1,
	dataContent2: dataContent2
}).Body;

export default Content;
