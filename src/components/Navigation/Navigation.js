import React, { PropTypes } from 'react';
import classNames from 'classnames';
import styles from './Navigation.css';
import withStyles from '../../decorators/withStyles';
import Link from '../../utils/Link';
import Apps from '../../core/Apps';
import reactCookie from '../../core/react-cookie';
import RestCall from '../../api/RestCall';
/*
RestCall.performRequest('/v1/staff', 'GET', {}, function(data) {
	if (data.status == "OK")
		reactCookie.save("data", JSON.stringify(data.data));
});
Apps.props = reactCookie.load("data");
console.log(Apps.props);
*/
var dataMessages = 
	[{
		text: 'John Smith',
		time: 'Yesterday at 4:32 PM',
		desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...'
	},
	{
		text: 'John Smith 1',
		time: 'Yesterday at 4:32 PM',
		desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...'
	},
	{
		text: 'John Smith 2',
		time: 'Yesterday at 4:32 PM',
		desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...'
	}];
var dataTask = 
	[{
		type: 'info',
		percentage: '20%',
		text: 'Task 1'
	}, {
		type: 'success',
		percentage: '40%',
		text: 'Task 2'
	}, {
		type: 'warning',
		percentage: '60%',
		text: 'Task 3'
	}, {
		type: 'danger',
		percentage: '80%',
		text: 'Task 4'
	}];
var dataNotif = 
	[{
		icon: "fa-comment",
		text: "New Comment",
		time: "4 minutes ago"
	}, {
		icon: "fa-twitter",
		text: "3 New Followers",
		time: "12 minutes ago"
	}, {
		icon: "fa-envelope",
		text: "Message Sent",
		time: "4 minutes ago"
	}, {
		icon: "fa-tasks",
		text: "New Task",
		time: "4 minutes ago"
	}, {
		icon: "fa-upload",
		text: "Server Rebooted",
		time: "4 minutes ago"
	}];
var dataSideBar = 
	[{
		title: 'Dashboard',
		url: '/',
		icon: 'fa-dashboard'
	}, {
		title: 'User',
		url: '#',
		icon: 'fa-user',
		class: '',
		items: [{
			text: 'Customer',
			url: '/customer',
		}, {
			text: 'Staff',
			url: '/staff',
		}, {
			text: 'Supplier',
			url: '/supplier',
		}]
	}, {
		title: 'Product',
		url: '#',
		icon: 'fa-table',
		class: '',
		items: [{
			text: 'Product',
			url: '/product',
		}, {
			text: 'Product Adjustment',
			url: '/product-adjustment',
		}, {
			text: 'Product Category',
			url: '/product-category',
		}]
	}, {
		title: 'Sales',
		url: '#',
		icon: 'fa-users',
		class: '',
		items: [{
			text: 'Sales Order',
			url: '/sales-order',
		}, {
			text: 'Sales Invoice',
			url: '/sales-invoice',
		}, {
			text: 'Sales Pricing',
			url: '/sales-pricing',
		}, {
			text: 'Sales Return',
			url: '/sales-return',
		}]
	}, {
		title: 'Access User',
		url: '#',
		icon: 'fa-user',
		class: '',
		items: [{
			text: 'Member',
			url: '/user',
		}, {
			text: 'Department',
			url: '/department',
		}, {
			text: 'Role',
			url: '/role',
		}, {
			text: 'Staff Position',
			url: '/staff-position',
		}]
	}, {
		title: 'Shipping Cart',
		url: '/shipping-order',
		icon: 'fa-shopping-cart',
		class: '',
	}, {
		title: 'Payment',
		url: '#',
		icon: 'fa-folder-open',
		class: '',
		items: [{
			text: 'Payment',
			url: '/payment',
		}, {
			text: 'Cash In',
			url: '/cash-in',
		}, {
			text: 'Cash Out',
			url: '/cash-out',
		}, {
			text: 'Cost Category',
			url: '/cost-category',
		}]
	}, {
		title: 'Report',
		url: '#',
		icon: 'fa-envelope',
		class: '',
		items: [{
			text: 'Profit',
			url: '/profit',
		}, {
			text: 'Transaction',
			url: '/transaction',
		}, {
			text: 'Receivable',
			url: '/receivable',
		}]
	}];
/*
	{
		id: "key-profile",
		url: "#",
		cls: "fa-user",
		text: "Profile"
	}, {
		id: "key-settings",
		url: "#",
		cls: "fa-gear",
		separated: true,
		text: "Settings"
	}, 
*/
var dataAccount = 
	[{
		id: "key-logout",
		url: "login",
		cls: "fa-sign-out",
		text: "Log Out",
		event: function(e) {
			reactCookie.remove('username');
			reactCookie.remove('auth');
			reactCookie.remove("data");
		}
	}];
@withStyles(styles)

class Navigation {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataMessages: dataMessages, 
	dataTask: dataTask, 
	dataNotif: dataNotif,
	dataAccount: dataAccount,
	dataSideBar: dataSideBar
}).Header;

export default Content;