import React, { PropTypes } from 'react';
import styles from './Department.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Department',
		breadcrumbsIcon: 'fa-table',
		subtitle: ['Access User', 'Department']
	};
var dataContent2 = 
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'Department',
		name: 'table-department',
		panel: false,
		loadAll: true,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		bodyAttr: '',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-department").DataTable({
    				"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/department",
					"columns": [
						{ "title": "Department ID", "className": 'departmentId', "data": "departmentId", "visible": false },
						{ "title": "Department Code", "className": 'departmentCode', "data": "departmentCode" },
						{ "title": "Department Name", "className": 'departmentName', "data": "departmentName" },
						{ "title": "Location", "className": 'location', "data": "location" },
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"render": function ( data, type, full, meta ) { 
								return "<a class='btn btn-default iEdit' data-attr="+full.departmentId+">Edit</a> <a class='btn btn-danger iDelete' data-attr="+full.departmentId+">Delete</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						event();
						$("#table-department").closest('.table-responsive').css('overflow', 'hidden');
					}
				});
			}

			function reset() {
				$("#departmentID").val("");
				$("#departmentCode").val("");
				$("#departmentName").val("");
				$("#location").val("");
				$('#errorDisplay').text("")
				$(':input[required], select[required]').trigger("change");
			}

			function event () {
				$("#btnNew").off('click').click(function(e) {
					reset();
					$(':input[required], select[required]').trigger("change");
				});

				$("#btnSave").off('click').click(function(e) {
					e.preventDefault();
					var departmentId = $('#departmentID').val(),
						error = $('#errorDisplay'),
						boolVal = true,
						data = { 
							departmentCode: $('#departmentCode').val(),
							departmentName: $('#departmentName').val(),
							location: $('#location').val()
						};

					$.each($(':input[required], select[required]'), function(value, index) {
						if ($(this).val() == "") {
							boolVal = false;
							return;
						}
					});

					if (!boolVal)
						return;

					if (departmentId == "") {
						RestCall.performRequest('/v1/department', 'POST', data, function(data) {
							if (typeof data.error != 'undefined') {
								error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
								error.css('display', 'block');
							}
							else {
								if (data.status == "ERROR") {
									error.html(data.message);
									error.css('display', 'block');
								}
								else {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Save Department");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
									reset();
								}
							}
						});
					}
					else {
						RestCall.performRequest('/v1/department/'+departmentId, 'PUT', data, function(data) {
							if (typeof data.error != 'undefined') {
								error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
								error.css('display', 'block');
							}
							else {
								if (data.status == "ERROR") {
									error.html(data.message);
									error.css('display', 'block');
								}
								else {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Update Department");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
									reset();
								}
							}
						});
					}
				})
				
				$(".iEdit").off('click').click(function(e) {
					e.preventDefault();
					var departmentIdEdit = $(this).attr('data-attr'),
						departmentCodeEdit = $(this).closest('tr').find('td.departmentCode').text(),
						departmentNameEdit = $(this).closest('tr').find('td.departmentName').text(),
						localtionEdit = $(this).closest('tr').find('td.location').text();

					$('html, body').animate({
						scrollTop: $('form#formDepartment').offset().top
					}, 'slow');
					$('#departmentID').val(departmentIdEdit);
					$('#departmentCode').val(departmentCodeEdit);
					$('#departmentName').val(departmentNameEdit);
					$('#location').val(localtionEdit);
					$(':input[required], select[required]').trigger("change");
				});

				$(".iDelete").off('click').click(function(e) {
					e.preventDefault();
					var departmentId = $(this).attr('data-attr'),
						departmentName = $(this).closest('tr').find('td.departmentName').text();
					$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> Department '+departmentName+'?');
					$("#PopUpConfirm").modal("show");
					$("#PopUpConfirm #btnYes").click(function(e) {
						RestCall.performRequest('/v1/department/'+departmentId, 'DELETE', {}, function(data) {
							$('html, body').animate({
								scrollTop: $('body').offset().top
							}, 'slow');
							$("#PopUpConfirm").modal("hide");
							if (typeof data.error != 'undefined') {
								$("#alert #alert-title").html("Error");
								$("#alert #alert-content").html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
								$("#alert").addClass('alert-danger').show();
								$("#alert").fadeOut(2000);
							}
							else {
								$("#alert #alert-title").html("Success");
								$("#alert #alert-content").html("Delete Department");
								$("#alert").addClass('alert-success').show();
								$("#alert").fadeOut(2000);
								datatable.destroy();
								loadTable();
								reset();
							}
						});
					});
				});
			}
			
			loadTable();
		}
	},
	{
		mode: 'Input',
		name: 'formDepartment',
		title: 'Add Department',
		loadAll: true,
		iconHeader: 'fa-plus',
		cls: 'col-lg-12',
		items:[{
			value: '',
			name: 'departmentID',
			type: 'hidden',
			cols: [ 2, 10 ]
		}, {
			text: 'Department Code',
			name: 'departmentCode',
			type: 'text',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Department Name',
			name: 'departmentName',
			type: 'text',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: 'Location',
			name: 'location',
			type: 'text',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: '',
			name: 'errorDisplay',
			type: 'label',
			color: 'red',
			display: 'none'
		}, {
			type: 'button',
			data: [{
				text: 'Save',
				type: 'submit',
				name: 'btnSave',
				cls: 'btn-primary',
			}, {
				text: 'New',
				type: 'reset',
				name: 'btnNew',
				cls: 'btn-default'
			}]
		}]
	}];


@withStyles(styles)

class Department {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;