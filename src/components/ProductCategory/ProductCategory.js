import React, { PropTypes } from 'react';
import styles from './ProductCategory.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Product Category',
		breadcrumbsIcon: 'fa-table',
		subtitle: ['Product', 'Product Category']
	};
var dataContent2 = 
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'User',
		name: 'table-product-category',
		panel: false,
		loadAll: true,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		bodyAttr: '',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-product-category").DataTable({
    				"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/product-category",
					"columns": [
						{ "title": "Product Category ID", "className": "productCategoryId", "data": "productCategoryId", "visible": false },
						{ "title": "Product Category Name", "className": "productCategoryName", "data": "productCategoryName" },
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"render": function ( data, type, full, meta ) { 
								return "<a class='btn btn-default iEdit' data-attr="+full.productCategoryId+">Edit</a> <a class='btn btn-danger iDelete' data-attr="+full.productCategoryId+">Delete</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						event();
						$("#table-product-category").closest('.table-responsive').css('overflow', 'hidden');
					}
				});
			}

			function reset() {
				$('#errorDisplay').html("");
				$("#productCategoryId").val("");
				$("#productCategoryName").val("");
				$('#errorDisplay').text("")
				$(':input[required], select[required]').trigger("change");
			}

			function event() {
				$("#btnNew").off('click').click(function(e) {
					reset();
					$(':input[required], select[required]').trigger("change");
				});
				$("#btnSave").off('click').click(function(e) {
					e.preventDefault();
					var productCategoryIdParam = $('#productCategoryId').val(),
						error = $('#errorDisplay'),
						boolVal = true,
						data = { productCategoryName: $('#productCategoryName').val() }

					$.each($(':input[required], select[required]'), function(value, index) {
						if ($(this).val() == "") {
							boolVal = false;
							return;
						}
					});

					if (!boolVal)
						return;

					if (productCategoryIdParam == "") {
						RestCall.performRequest('/v1/product-category', 'POST', data, function(data) {
							if (typeof data.error != 'undefined') {
								error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
								error.css('display', 'block');
							}
							else {
								if (data.status == "ERROR") {
									error.html(data.message);
									error.css('display', 'block');
								}
								else {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Save Product Category");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
									reset();
								}
							}
						});
					}
					else {
						RestCall.performRequest('/v1/product-category/'+productCategoryIdParam, 'PUT', data, function(data) {
							if (typeof data.error != 'undefined') {
								error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
								error.css('display', 'block');
							}
							else {
								if (data.status == "ERROR") {
									error.html(data.message);
									error.css('display', 'block');
								}
								else {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#alert #alert-title").html("Success");
									$("#alert #alert-content").html("Update Product Category");
									$("#alert").addClass('alert-success').show();
									$("#alert").fadeOut(2000);
									datatable.destroy();
									loadTable();
									reset();
								}
							}
						});
					}
				});
				
				$(".iEdit").off('click').click(function(e) {
					e.preventDefault();
					var productCategoryIdEdit = $(this).attr('data-attr'),
						productCategoryNameEdit = $(this).closest('tr').find('td.productCategoryName').text();
					$('html, body').animate({
						scrollTop: $('form#formProductCategory').offset().top
					}, 'slow');
					$('#productCategoryId').val(productCategoryIdEdit);
					$('#productCategoryName').val(productCategoryNameEdit);
					$(':input[required], select[required]').trigger("change");
				});

				$(".iDelete").off('click').click(function(e) {
					e.preventDefault();
					var productCategoryId = $(this).attr('data-attr'),
						productCategoryName = $(this).closest('tr').find('td.productCategoryName').text();
					$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> Product Category '+productCategoryName+'?');
					$("#PopUpConfirm").modal("show");
					$("#PopUpConfirm #btnYes").click(function(e) {
						RestCall.performRequest('/v1/product-category/'+productCategoryId, 'DELETE', {}, function(data) {
							$('html, body').animate({
								scrollTop: $('body').offset().top
							}, 'slow');
							$("#PopUpConfirm").modal("hide");
							if (typeof data.error != 'undefined') {
								$("#alert #alert-title").html("Error");
								$("#alert #alert-content").html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
								$("#alert").addClass('alert-danger').show();
								$("#alert").fadeOut(2000);
							}
							else {
								$("#alert #alert-title").html("Success");
								$("#alert #alert-content").html("Delete Product Category");
								$("#alert").addClass('alert-success').show();
								$("#alert").fadeOut(2000);
								datatable.destroy();
								loadTable();
								reset();
							}
						});
					});
				});
			}

			loadTable();
		}
	},
	{
		mode: 'Input',
		name: 'formProductCategory',
		loadAll: true,
		title: 'Add Product Category',
		iconHeader: 'fa-plus',
		cls: 'col-lg-12',
		items:[{
			value: '',
			name: 'productCategoryId',
			type: 'hidden',
			cols: [ 2, 10 ]
		}, {
			text: 'Category Name',
			name: 'productCategoryName',
			type: 'text',
			required: 'required',
			cols: [ 2, 10 ]
		}, {
			text: '',
			name: 'errorDisplay',
			type: 'label',
			color: 'red',
			display: 'none'
		}, {
			type: 'button',
			data: [{
				text: 'Save',
				type: 'submit',
				name: 'btnSave',
				cls: 'btn-primary'
			}, {
				text: 'New',
				type: 'reset',
				name: 'btnNew',
				cls: 'btn-default'
			}]
		}]
	}];

@withStyles(styles)

class ProductCategory {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;