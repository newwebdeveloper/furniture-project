import React, { PropTypes } from 'react';
import styles from './Thumbnail.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';

var dataPageHeading = 
	{
		title: 'Thumbnail',
		additionalTitle: '',
		breadcrumbsIcon: 'fa-th-list' 
	};
var dataContent1 = [];
var dataContent2 = 
	[{
		mode: 'Thumbnail',
		cls: 'col-lg-12',
		panel: false,
		items: [{
			url: '',
			col: 'col-sm-6 col-md-4',
			caption: 'Thumbnail label',
			desc: 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.'
		}, {
			url: '',
			col: 'col-sm-6 col-md-4',
			caption: 'Thumbnail label',
			desc: 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.'
		}, {
			url: '',
			col: 'col-sm-6 col-md-4',
			caption: 'Thumbnail label',
			desc: 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.'
		}]
	}];

@withStyles(styles)

class Thumbnail {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading, 
	dataContent1: dataContent1, 
	dataContent2: dataContent2 
}).Body;

export default Content;