import React, { PropTypes } from 'react';
import styles from './Product.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Product',
		additionalTitle: '',
		breadcrumbsIcon: 'fa-table',
		subtitle: ['Product'],
		toolbar: [{
			type: "button",
			name: "btnAdd",
			text: "Add",
			cls: "btn-primary",
			event: function(e) {
				window.location.assign("/product-input");
			}
		}]
	};

var dataContent2 = [
	{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'Product',
		name: 'table-product',
		panel: false,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		bodyAttr: '',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				var picture_default = "placeholder-640x480.png";
				datatable = $("#table-product").DataTable({
    				"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/product",
					"columns": [
						{ "title": "Product ID", "className": "productId", "data": "productId", "visible": false },
						{ "title": "Product Name", "className": "productName", "data": "productName" },
						{ "title": "Category", "className": "productCategoryId", "data": "productCategory",
							"render": function ( data, type, full, meta ) { 
								return (data != null) ? data.productCategoryName : "";
							}
						},
						{ "title": "Initial Stock", "className": "initialStock", "data": "initialStock" },
						{ "title": "Currnet Stock", "className": "currentStock", "data": "currentStock" },
						{ "title": "Sell Price", "className": "sellPrice", "data": "sellPrice", "type": "num-fmt",
							"render": function ( data, type, full, meta ) { 
								return FormatToCurrency(data);
							}
						},
						{ "title": "Status", "className": "status", "data": "status" },
						{ "title": "Image", "className": "productPhoto", "data": "productPhoto",
							"render": function ( data, type, full, meta ) { 
								var link;
								return "<img src=" + Apps.defaultDirectory +((data == null) ? picture_default : data)+" width=100 height=100 />";
							}
						},
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"data": "productId",
							"render": function ( data, type, full, meta ) { 
								return "<a class='btn btn-default iEdit' href='product-input?id="+data+"'>Edit</a> <a class='btn btn-danger iDelete' data-attr="+data+">Delete</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						$("#table-product").closest('.table-responsive').css('overflow', 'hidden');
						$(".iDelete").click(function(e) {
							e.preventDefault();
							var productId = $(this).attr('data-attr'),
								productName = $(this).closest('tr').find('td.productName').text();
							$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> product '+productName+'?');
							$("#PopUpConfirm").modal("show");
							$("#PopUpConfirm #btnYes").click(function(e) {
								RestCall.performRequest('/v1/product/'+productId, 'DELETE', {}, function(data) {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#PopUpConfirm").modal("hide");
									if (typeof data.error != 'undefined') {
										$("#alert #alert-title").html("Error");
										$("#alert #alert-content").html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
										$("#alert").addClass('alert-danger').show();
										$("#alert").fadeOut(2000);
									}
									else {
										$("#alert #alert-title").html("Success");
										$("#alert #alert-content").html("Delete Product");
										$("#alert").addClass('alert-success').show();
										$("#alert").fadeOut(2000);
										datatable.destroy();
										loadTable();
									}
								});
							});
						});
					}
				});
			}

			loadTable();
		}
	},
];

@withStyles(styles)

class Product {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;