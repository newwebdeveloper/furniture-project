import React, { PropTypes } from 'react';
import styles from './ProductAdjustment.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Product Adjustment',
		breadcrumbsIcon: 'fa-table',
		subtitle: ['Product', 'Product Adjustment'],
		toolbar: [{
			type: "button",
			name: "btnAdd",
			text: "Add",
			cls: "btn-primary",
			event: function(e) {
				window.location.assign("/product-adjustment-input");
			}
		}]
	};
var dataContent2 = 
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'User',
		name: 'table-product-adjustment',
		panel: false,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-product-adjustment").DataTable({
    				"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/product-adjustment",
					"columns": [
						{ "title": "Product Adjustment ID", "className": "productAdjustmentId", "data": "productAdjustmentId", "visible": false },
						{ "title": "Staff", "className": "staffId", "data": "staff",
							"render": function ( data, type, full, meta ) { 
								return (data != null) ? data.staffName : "";
							}
						},
						{ "title": "Reason", "className": "reason", "data": "reason" },
						{ "title": "Adjustment Type", "className": "adjustmentType", "data": "adjustmentType" },
						{ "title": "Adjustment Date", "className": "adjustment_date", "data": "adjustment_date",
							"render": function ( data, type, full, meta ) { 
								return moment(data).format(Apps.dateFormat)
							}
						},
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"data": "productAdjustmentId",
							"render": function ( data, type, full, meta ) { 
								Apps.props[data] = full.details; 
								return "<label class='viewDetail' data-attr="+data+"><span class='glyphicon glyphicon-eye-open'></span> Detail</label> &nbsp;" +
								" <a class='btn btn-default iEdit' href='product-adjustment-input?id="+data+"'>Edit</a>" +
								" <a class='btn btn-danger iDelete' data-attr="+data+">Delete</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						$("#table-product-adjustment").closest('.table-responsive').css('overflow', 'hidden');
						$(".iDelete").click(function(e) {
							e.preventDefault();
							var productAdjustmentId = $(this).attr('data-attr');
							$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
							$("#PopUpConfirm").modal("show");
							$("#PopUpConfirm #btnYes").click(function(e) {
								RestCall.performRequest('/v1/product-adjustment/'+productAdjustmentId, 'DELETE', {}, function(data) {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#PopUpConfirm").modal("hide");
									if (typeof data.error != 'undefined') {
										$("#alert #alert-title").html("Error");
										$("#alert #alert-content").html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
										$("#alert").addClass('alert-danger').show();
										$("#alert").fadeOut(2000);
									}
									else {
										$("#alert #alert-title").html("Success");
										$("#alert #alert-content").html("Delete Product Adjustment");
										$("#alert").addClass('alert-success').show();
										$("#alert").fadeOut(2000);
										datatable.destroy();
										loadTable();
									}
								});
							});
						});
						$('label.viewDetail').css('cursor', 'pointer').click(function(e) {
							var selected = $(this).attr('data-attr'),
								data = Apps.props[selected];
							var row = "";
							for (var i in data) {
								row += '<tr>' +
									'<td>'+(parseInt(i)+1)+'</td>' +
									'<td>'+data[i].product.productName+'</td>' +
									'<td>'+data[i].quantity+'</td>' +
								'</tr>';
							}
							$('.modal-title').html('Product Adjustment Detail');
							$('.modal-body').html('<div class="container-fluid">' +
								'<div class="row">' +
									'<table class="table">' +
										'<thead>' +
											'<tr>' +
												'<th>No</th>' + 
												'<th>Product</th>' + 
												'<th>Product Adjustment Quantity</th>' + 
											'</tr>' +
										'</thead>' +
										'<tbody>' +
											row +
										'</tbody>' +
									'</table>' +
								'</div>' +
							'</div>');
							$('.modal-footer').html("")
							$('#popup').modal('show');
						});
					}
				});
			}

			loadTable();
		}
	}];

@withStyles(styles)

class ProductAdjustment {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;