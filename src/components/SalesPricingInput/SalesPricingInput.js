import React, { PropTypes } from 'react';
import styles from './SalesPricingInput.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataSalesPricing = {};
var dataPageHeading =
	{
		title: 'Sales Pricing',
		additionalTitle: ' Input',
		subtitle: ['Sales', 'Sales Pricing', 'Sales Pricing Input'],
		breadcrumbsIcon: 'fa-pencil'
	};

function setValue (SalesPricingId, StaffId, StaffName) {
	$('#SalesPricingID').val(SalesPricingId)
	$('#staff').val(StaffName).attr('data-id', StaffId)
	$('#tableSalesPricingDetail tbody').remove();
	$('#errorDisplay').text("")
	$(':input[required], select[required]').trigger("change");
}

var dataContent1 = [];
var dataContent2 =
	[{
		mode: 'Input',
		name: 'formSalesPricingInput',
		title: 'Add Sales Pricing',
		loadAll: true,
		iconHeader: 'fa-plus',
		cls: 'col-lg-12',
		componentDidMount: function () {
			if (typeof QueryString.id != 'undefined') {
				$.ajax({
					url: Apps.host + "/v1/sales-pricing/" + QueryString.id,
					type: 'GET',
					dataType: 'json',
					cache: false,
					success: function(data) {
						if (data.status == "OK") {
							var data = data.data,
								details = data.details;
							setValue(data.salesPricingId, data.staff.staffId, data.staff.staffName);

							dataSalesPricing.staffId = data.staff.staffId;

							for (var i in details)
								Apps.props.push({ productId: details[i].productId, productName: details[i].product.productName, price: details[i].price });
						}
					},
					async: false
				});
			}

			// Autocomplete Staff
			RestCall.performRequest('/v1/staff', 'GET', {}, function(data) {
				var autocomplete = [], onSelected = false;
				if (data.status == "OK") {
					for (var i in data.data)
						autocomplete.push({ id: data.data[i].staffId, value: data.data[i].staffName });
				}
				var bloodhound = new Bloodhound({
					datumTokenizer: function (datum) {
						return Bloodhound.tokenizers.whitespace(datum.value);
					},
					queryTokenizer: Bloodhound.tokenizers.whitespace,
					local: autocomplete
				});
				
				// Initialize the Bloodhound suggestion engine
				bloodhound.initialize().done(function() {
					$('#staff').typeahead({
						highlight: true
					}, {
						templates: {
							empty: [
								'<div class="tt-suggestion">',
								'No Items Found',
								'</div>'
							].join('\n'),
							suggestion: function (data){
								// return '<div>' + data.id + ' – ' + data.value + '</div>'
								return '<div data-attr='+data.id+'>' + data.value + '</div>'
							}
						},
						displayKey: 'value',
						source: bloodhound.ttAdapter()
					}).on("typeahead:selected", function (e, datum) {
						dataSalesPricing.staffId = datum.id;
						$(this).closest('span').siblings().remove();
						onSelected = true;
					}).on("change", function(e) {
						if (!onSelected) 
							dataSalesPricing.staffId = $(".tt-suggestion:first").attr("data-attr")
						onSelected = false;
						if ($(this).val() != "")
							$(this).closest('span').siblings().remove();
					}).on("keydown", function(e) {
						if (e.keyCode == 9) { // TABKEY
							if (!onSelected) 
								dataSalesPricing.staffId = $(".tt-suggestion:first").attr("data-attr")
							onSelected = false;
							if ($(this).val() != "")
								$(this).closest('span').siblings().remove();
						}
					});
				});
			});
		},
		items:[{
			value: '',
			name: 'SalesPricingID',
			type: 'hidden',
			cols: [ 2, 10 ]
		}, {
			text: 'Staff Name',
			name: 'staff',
			required: 'required',
			type: 'autocomplete',
			cols: [ 2, 10 ]
		}]
	}, {
		mode: 'Table',
		name: 'tableSalesPricingDetail',
		cls: 'col-lg-12',
		loadAll: true,
		typeTable: 'table-responsive',
		componentDidMount: function () {
			function loadTable() {
				var rows = "", picture_default = "placeholder-640x480.png";
				$("#tableSalesPricingDetail").html("<thead></thead><tbody></tbody><tfoot></tfoot>");
				$("#tableSalesPricingDetail thead").html(
					"<tr>" +
						"<th>Product</th>" +
						"<th>Price</th>" +
						"<th>Action</th>" +
					"</tr>");
				if (Apps.props.length) {
					for (var i in Apps.props)
					rows += "<tr id='row-"+i+"'>" +
						"<td id="+Apps.props[i].productId+">"+Apps.props[i].productName+"</td>" +
						"<td>"+FormatToCurrency(Apps.props[i].price)+"</td>" +
						"<td><a class='btn btn-danger iDeleteDetail' data-attr='"+i+"'>Delete</a></td>" +
					"</tr>";
				}
				$("#tableSalesPricingDetail tbody").html(rows);
				$("#tableSalesPricingDetail tfoot").html("<tr><td colspan='3'><button class='btn btn-primary'>Browse</button></td></tr>");

				$("a.iDeleteDetail").click(function(e) {
					var index = $(this).attr('data-attr'),
						productIdParam = $(this).closest('td').siblings(':first').attr('id');
					Apps.props = $.grep(Apps.props || [], function(value, index) {
						return value.productId != productIdParam;
					});
					$("tr#row-"+index).remove();
				});
				$("#tableSalesPricingDetail tfoot button").click(function() {
					$('.modal-title').html('Product Detail');
					$('.modal-body').html('<table class="table table-hover" id="tblProduct"></table>');
					var otable = $("#tblProduct").DataTable({
						"retrieve": true,
						"bAutoWidth": true,
						"bProcessing": true,
						"bServerSide": false,
						"sAjaxSource": Apps.host + "/v1/product",
						"columns": [
							{ "title": "Product ID", "className": "productId", "data": "productId", "visible": false },
							{ "title": "Product Name", "className": "productName", "data": "productName",
								"render": function ( data, type, full, meta ) { 
									return "<label id='"+full.productId+"'>"+data+"</label>";
								}
							},
							{ "title": "Category", "className": "productCategoryId", "data": "productCategory",
								"render": function ( data, type, full, meta ) { 
									return (data != null) ? data.productCategoryName : "";
								}
							},
							{ "title": "Initial Stock", "className": "initialStock", "data": "initialStock" },
							{ "title": "Currnet Stock", "className": "currentStock", "data": "currentStock" },
							{ "title": "Sell Price", "className": "sellPrice", "data": "sellPrice", "type": "num-fmt",
								"render": function ( data, type, full, meta ) { 
									return FormatToCurrency(data);
								}
							},
							{ "title": "Status", "className": "status", "data": "status" },
							{ "title": "Image", "className": "productPhoto", "data": "productPhoto",
								"render": function ( data, type, full, meta ) { 
									var link;
									return "<img src=" + Apps.defaultDirectory +((data == null) ? picture_default : data)+" width=100 height=100 />";
								}
							}
						],
						"fnDrawCallback": function() {
							$("#tblProduct tbody tr").off('click').click(function(e) {
								e.preventDefault();
								var productId = $(this).find('.productName label').attr('id'),
									productName = $(this).find('.productName label').text();
								Apps.item = {};
								Apps.item.productId = productId;
								Apps.item.productName = productName;
								if ( $(this).hasClass('selected') ) {
									$(this).removeClass('selected');
								}
								else {
									$('#tblProduct tbody tr.selected').removeClass('selected');
									$(this).addClass('selected');
								}
							})
						}
					});
					$('#popup').modal('show');
					$('.modal-body').css({ 'overflow-y': 'scroll', 'height': '500px' }).html($("#tblProduct"));
					$('.modal-body').append('<form role="form" class="form-horizontal" id="frmProduct">' +
						'<div class="form-group">' +
							'<label class="control-label col-sm-2">Price</label>' +
							'<div class="col-sm-10">' +
								'<input class="form-control" type="number" name="priceDetail" id="priceDetail" placeholder="Price" value=0 />' +
							'</div>' +
						'</div>'+
					'</form>');
					$("#btnSubmitPopup").off('click').click(function(e) {
						var elPrice = $("#frmProduct #priceDetail")[1];
						Apps.item.price = $(elPrice).val();
						var flag = true;
						$.map(Apps.props || [], function(value, index) {
							if (value.productId == Apps.item.productId) {
								flag = false;
								return;
							}
						});
						
						if (typeof Apps.item.productId == 'undefined')
							return;

						if (flag)
							Apps.props.push(Apps.item);
						else {
							$('html, body').animate({
								scrollTop: $('body').offset().top
							}, 'slow');
							$("#alert #alert-title").html("Error");
							$("#alert #alert-content").html("Product Already Exists");
							$("#alert").addClass('alert-danger').show();
							$("#alert").fadeOut(2000);
						}
						$('#popup').modal('hide');
						loadTable();
					});
				});
			}
			loadTable();
		}
	}, {
		mode: 'Input',
		loadAll: true,
		cls: 'col-lg-12',
		componentDidMount: function () {
			$("#btnSave").click(function(e) {
				e.preventDefault();
				var detail;
				detail = $.map(Apps.props, function(value, index) {
					var items = [];
					items.push({ productId: value.productId, price: value.price });
					return items;
				});
				var SalesPricingId = $("#SalesPricingID").val(),
					boolVal = true,
					error = $("#errorDisplay");

				$.each($(':input[required], select[required]'), function(value, index) {
					if ($(this).val() == "") {
						boolVal = false;
						return;
					}
				});

				if (!boolVal)
					return;

				if (detail.length == 0) {
					error.html("Product must have at least one item");
					error.css('display', 'block');
					return;
				}

				dataSalesPricing.details = JSON.stringify(detail);

				if (SalesPricingId == "") {
					RestCall.performRequest('/v1/sales-pricing', 'POST', dataSalesPricing, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								$('html, body').animate({
									scrollTop: $('body').offset().top
								}, 'slow');
								$("#alert #alert-title").html("Success");
								$("#alert #alert-content").html("Save Sales Pricing");
								$("#alert").addClass('alert-success').show();
								$("#alert").fadeOut(2000);
								setValue("", "", "");
							}
						}
					});
				}
				else {
					RestCall.performRequest('/v1/sales-pricing/'+SalesPricingId, 'PUT', dataSalesPricing, function(data) {
						if (typeof data.error != 'undefined') {
							error.html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
							error.css('display', 'block');
						}
						else {
							if (data.status == "ERROR") {
								error.html(data.message);
								error.css('display', 'block');
							}
							else {
								window.location.assign('/sales-pricing');
							}
						}
					});	
				}
			});
		},
		items:[{
			text: '',
			name: 'errorDisplay',
			type: 'label',
			color: 'red',
			display: 'none'
		}, {
			type: 'button',
			data: [{
				text: 'Save',
				type: 'submit',
				name: 'btnSave',
				cls: 'btn-primary',
			}, {
				text: 'Back',
				type: 'button',
				name: 'btnBack',
				cls: 'btn-default',
				event: function() {
					window.location.assign("/sales-pricing");
				}
			}]
		}]
	}];

@withStyles(styles)

class SalesPricingInput {
	render() {
		return (null)
	}
}

var Content = Apps.Widget({
	dataPageHeading: dataPageHeading,
	dataContent1: dataContent1,
	dataContent2: dataContent2
}).Body;

export default Content;
