import React, { PropTypes } from 'react';
import styles from './Dashboard.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';

var dataPageHeading = 
	{
		title: 'Dashboard',
		subtitle: ['Dashboard'],
		additionalTitle: 'Statistics Overview',
		breadcrumbsIcon: 'fa-dashboard' 
	};	
var dataContent1 = 
	[{
		panel: 'panel-primary',
		type: 'fa-comments',
		text: '26',
		desc: 'New Comments!',
		url: '#'
	},{
		panel: 'panel-green',
		type: 'fa-tasks',
		text: '12',
		desc: 'New Tasks!',
		url: '#'
	},{
		panel: 'panel-yellow',
		type: 'fa-shopping-cart',
		text: '124',
		desc: 'New Orders!',
		url: '#'
	}, {
		panel: 'panel-red',
		type: 'fa-support',
		text: '13',
		desc: 'Support Tickets!',
		url: '#'
	}];
var dataContent2 = 
	[{
		type: 'AreaChart',
		loadAll: true,
		cls: 'col-lg-8',
		componentDidMount: function() {
			Morris.Area({
				element: 'morris-area-chart',
				data: [{
					period: '2010 Q1',
					iphone: 2666,
					ipad: null,
					itouch: 2647
				}, {
					period: '2010 Q2',
					iphone: 2778,
					ipad: 2294,
					itouch: 2441
				}, {
					period: '2010 Q3',
					iphone: 4912,
					ipad: 1969,
					itouch: 2501
				}, {
					period: '2010 Q4',
					iphone: 3767,
					ipad: 3597,
					itouch: 5689
				}, {
					period: '2011 Q1',
					iphone: 6810,
					ipad: 1914,
					itouch: 2293
				}, {
					period: '2011 Q2',
					iphone: 5670,
					ipad: 4293,
					itouch: 1881
				}, {
					period: '2011 Q3',
					iphone: 4820,
					ipad: 3795,
					itouch: 1588
				}, {
					period: '2011 Q4',
					iphone: 15073,
					ipad: 5967,
					itouch: 5175
				}, {
					period: '2012 Q1',
					iphone: 10687,
					ipad: 4460,
					itouch: 2028
				}, {
					period: '2012 Q2',
					iphone: 8432,
					ipad: 5713,
					itouch: 1791
				}],
				xkey: 'period',
				ykeys: ['iphone', 'ipad', 'itouch'],
				labels: ['iPhone', 'iPad', 'iPod Touch'],
				pointSize: 2,
				hideHover: 'auto',
				resize: true
			});
		},
		menu: [{
			text: 'Action',
			url: '#',
			separated: false
		}, {
			text: 'Another action',
			url: '#',
			separated: false
		}, {
			text: 'Something else here',
			url: '#',
			separated: false
		}, {
			text: 'Separated link',
			url: '#',
			separated: true
		}]
	}, {
		mode: 'Notification',
		loadAll: true,
		title: 'Notifications Panel',
		cls: 'col-lg-4',
		iconHeader: 'fa-bell',
		items: [{
			text: 'New Comment',
			time: '4 minutes ago',
			icon: 'fa-comment',
			url: '#'
		}, {
			text: '3 New Followers',
			time: '12 minutes ago',
			icon: 'fa-twitter',
			url: '#'
		}, {
			text: 'Message Sent',
			time: '27 minutes ago',
			icon: 'fa-envelope',
			url: '#'
		}, {
			text: 'New Task',
			time: '43 minutes ago',
			icon: 'fa-tasks',
			url: '#'
		}, {
			text: 'Server Rebooted',
			time: '11:32 AM',
			icon: 'fa-upload',
			url: '#'
		}, {
			text: 'Server Crashed!',
			time: '11:13 AM',
			icon: 'fa-bolt',
			url: '#'
		}, {
			text: 'Server Not Responding',
			time: '10:57 AM',
			icon: 'fa-warning',
			url: '#'
		}, {
			text: 'New Order Placed',
			time: '9:49 AM',
			icon: 'fa-shopping-cart',
			url: '#'
		}]
	}, {
		type: 'BarChart',
		loadAll: true,
		cls: 'col-lg-8',
		table: [{
			typeTable: 'table-responsive',
			header: ['#', 'Date', 'Time', 'Amount'],
			body: [
				['3326', '10/21/2013', '3:29 PM', '$321.33'],
				['3325', '10/21/2013', '3:20 PM', '$234.34'],
				['3324', '10/21/2013', '3:03 PM', '$724.17'],
				['3323', '10/21/2013', '3:00 PM', '$23.71'],
				['3322', '10/21/2013', '2:49 PM', '$8345.23'],
				['3321', '10/21/2013', '2:23 PM', '$245.12'],
				['3320', '10/21/2013', '2:15 PM', '$5663.54'],
				['3319', '10/21/2013', '2:13 PM', '$943.45'],
			]	
		}],
		componentDidMount: function() {
			Morris.Bar({
				element: 'morris-bar-chart',
				data: [{
					y: '2006',
					a: 100,
					b: 90
				}, {
					y: '2007',
					a: 75,
					b: 65
				}, {
					y: '2008',
					a: 50,
					b: 40
				}, {
					y: '2009',
					a: 75,
					b: 65
				}, {
					y: '2010',
					a: 50,
					b: 40
				}, {
					y: '2011',
					a: 75,
					b: 65
				}, {
					y: '2012',
					a: 100,
					b: 90
				}],
				xkey: 'y',
				ykeys: ['a', 'b'],
				labels: ['Series A', 'Series B'],
				hideHover: 'auto',
				resize: true
			});
		},
		menu: [{
			text: 'Action',
			url: '#',
			separated: false
		}, {
			text: 'Another action',
			url: '#',
			separated: false
		}, {
			text: 'Something else here',
			url: '#',
			separated: false
		}, {
			text: 'Separated link',
			url: '#',
			separated: true
		}]
	}, {
		type: 'DonutChart',
		loadAll: true,
		cls: 'col-lg-4',
		componentDidMount: function() {
			Morris.Donut({
				element: 'morris-donut-chart',
				data: [{
					label: "Download Sales",
					value: 12
				}, {
					label: "In-Store Sales",
					value: 30
				}, {
					label: "Mail-Order Sales",
					value: 20
				}],
				resize: true
			});
		},
		menu: [{
			text: 'Action',
			url: '#',
			separated: false
		}, {
			text: 'Another action',
			url: '#',
			separated: false
		}, {
			text: 'Something else here',
			url: '#',
			separated: false
		}, {
			text: 'Separated link',
			url: '#',
			separated: true
		}]
	}, {
		mode: 'Timeline',
		loadAll: true,
		title: 'Responsive Timeline',
		cls: 'col-lg-8',
		iconHeader: 'fa-clock-o',
		items: [{
			header: 'Lorem ipsum dolor',
			time: '11 hours ago via Twitter',
			body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero laboriosam dolor perspiciatis omnis exercitationem. Beatae, officia pariatur? Est cum veniam excepturi. Maiores praesentium, porro voluptas suscipit facere rem dicta, debitis.',
			icon: 'timeline-badge',
			typeTimeline: 'timeline-badge',
			typeAlert: '',
			iconBadge: 'fa-check',
			inverted: false // left timeline (Default false)
		}, {
			header: 'Lorem ipsum dolor',
			time: '11 hours ago via Twitter',
			body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem dolorem quibusdam, tenetur commodi provident cumque magni voluptatem libero, quis rerum. Fugiat esse debitis optio, tempore. Animi officiis alias, officia repellendus. @ Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium maiores odit qui est tempora eos, nostrum provident explicabo dignissimos debitis vel! Adipisci eius voluptates, ad aut recusandae minus eaque facere.',
			icon: 'timeline-badge',
			typeTimeline: 'timeline-badge',
			typeAlert: 'warning',
			iconBadge: 'fa-credit-card',
			inverted: true // right timeline (Default false)
		}, {
			header: 'Lorem ipsum dolor',
			time: '11 hours ago via Twitter',
			body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus numquam facilis enim eaque, tenetur nam id qui vel velit similique nihil iure molestias aliquam, voluptatem totam quaerat, magni commodi quisquam.',
			icon: 'timeline-badge',
			typeTimeline: 'timeline-badge',
			typeAlert: 'danger',
			iconBadge: 'fa-bomb',
			inverted: false
		}, {
			header: 'Lorem ipsum dolor',
			time: '11 hours ago via Twitter',
			body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates est quaerat asperiores sapiente, eligendi, nihil. Itaque quos, alias sapiente rerum quas odit! Aperiam officiis quidem delectus libero, omnis ut debitis!',
			icon: 'timeline-panel',
			typeTimeline: '',
			typeAlert: '',
			iconBadge: '',
			inverted: true
		}, {
			header: 'Lorem ipsum dolor',
			time: '11 hours ago via Twitter',
			body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis minus modi quam ipsum alias at est molestiae excepturi delectus nesciunt, quibusdam debitis amet, beatae consequuntur impedit nulla qui! Laborum, atque.',
			icon: 'timeline-panel',
			typeTimeline: 'timeline-badge',
			typeAlert: 'info',
			iconBadge: 'fa-save',
			inverted: false
		}, {
			header: 'Lorem ipsum dolor',
			time: '11 hours ago via Twitter',
			body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt obcaecati, quaerat tempore officia voluptas debitis consectetur culpa amet, accusamus dolorum fugiat, animi dicta aperiam, enim incidunt quisquam maxime neque eaque.',
			icon: 'timeline-panel',
			typeTimeline: 'timeline-badge',
			typeAlert: 'success',
			iconBadge: 'fa-graduation-cap',
			inverted: true
		}]
	}, {
		mode: 'Table',
		loadAll: true,
		panel: true,
		cls: 'col-lg-4',
		title: 'Table',
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		header: ['#', 'Date', 'Time', 'Amount'],
		body: [
			['3326', '10/21/2013', '3:29 PM', '$321.33'],
			['3325', '10/21/2013', '3:20 PM', '$234.34'],
			['3324', '10/21/2013', '3:03 PM', '$724.17'],
			['3323', '10/21/2013', '3:00 PM', '$23.71'],
			['3322', '10/21/2013', '2:49 PM', '$8345.23'],
			['3321', '10/21/2013', '2:23 PM', '$245.12'],
			['3320', '10/21/2013', '2:15 PM', '$5663.54'],
			['3319', '10/21/2013', '2:13 PM', '$943.45'],
		]
	}];
@withStyles(styles)

class Dashboard {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading, 
	dataContent1: dataContent1, 
	dataContent2: dataContent2 
}).Body;

export default Content;