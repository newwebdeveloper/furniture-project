import React, { PropTypes } from 'react';
import styles from './Payment.css';
import withStyles from '../../decorators/withStyles';
import Apps from '../../core/Apps';
import RestCall from '../../api/RestCall';

var dataPageHeading = 
	{
		title: 'Payment',
		breadcrumbsIcon: 'fa-table',
		subtitle: ['Payment'],
		toolbar: [{
			type: "button",
			name: "btnAdd",
			text: "Add",
			cls: "btn-primary",
			event: function(e) {
				window.location.assign("/payment-input");
			}
		}]
	};
var dataContent2 = 
	[{
		mode: 'Table',
		cls: 'col-lg-12',
		title: 'User',
		name: 'table-payment',
		panel: false,
		iconHeader: 'fa-table',
		typeTable: 'table-responsive',
		componentDidMount: function () {
			var datatable;
			function loadTable() {
				datatable = $("#table-payment").DataTable({
					"retrieve": true,
					"bAutoWidth": true,
					"bProcessing": true,
					"bServerSide": false,
					"sAjaxSource": Apps.host + "/v1/payment",
					"columns": [
						{ "title": "Payment ID", "className": "paymentId", "data": "paymentId", "visible": false },
						{ "title": "Staff", "className": "staffId", "data": "staff",
							"render": function ( data, type, full, meta ) { 
								return (data != null) ? data.staffName : "";
							}
						},
						{ "title": "Payment Date", "className": "paymentDate", "data": "paymentDate",
							"render": function ( data, type, full, meta ) { 
								return moment(data).format(Apps.dateFormat)
							}
						},
						{ "title": "Payment Method", "className": "paymentMethod", "data": "paymentMethod" },
						{ "title": "Amount", "className": "amount", "data": "amount", "type": "num-fmt",
							"render": function ( data, type, full, meta ) { 
								return FormatToCurrency(data);
							}
						},
						{ "title": "Note", "className": "note", "data": "note" },
						{ 
							"title": "Action",
							"className": 'iAction',
							"sortable": false,
							"data": "paymentId",
							"render": function ( data, type, full, meta ) { 
								Apps.props[data] = full.salesInvoice;
								return "<label class='viewDetail' data-attr="+data+"><span class='glyphicon glyphicon-eye-open'></span> Detail</label> &nbsp;" +
								" <a class='btn btn-default iEdit' href='payment-input?id="+data+"'>Edit</a>" +
								" <a class='btn btn-danger iDelete' data-attr="+data+">Delete</a></td>"
							}
						}
					],
					"fnDrawCallback": function() {
						$("#table-payment").closest('.table-responsive').css('overflow', 'hidden');
						$(".iDelete").click(function(e) {
							e.preventDefault();
							var paymentId = $(this).attr('data-attr');
							$("#PopUpConfirm .modal-body").html('Are You Sure Want <b>Delete</b> ?');
							$("#PopUpConfirm").modal("show");
							$("#PopUpConfirm #btnYes").click(function(e) {
								RestCall.performRequest('/v1/payment/'+paymentId, 'DELETE', {}, function(data) {
									$('html, body').animate({
										scrollTop: $('body').offset().top
									}, 'slow');
									$("#PopUpConfirm").modal("hide");
									if (typeof data.error != 'undefined') {
										$("#alert #alert-title").html("Error");
										$("#alert #alert-content").html((typeof data.error_description == 'undefined') ? data.message : data.error_description);
										$("#alert").addClass('alert-danger').show();
										$("#alert").fadeOut(2000);
									}
									else {
										$("#alert #alert-title").html("Success");
										$("#alert #alert-content").html("Delete Payment");
										$("#alert").addClass('alert-success').show();
										$("#alert").fadeOut(2000);
										datatable.destroy();
										loadTable();
									}
								});
							});
						});
						$('label.viewDetail').css('cursor', 'pointer').click(function(e) {
							var selected = $(this).attr('data-attr'),
								data = Apps.props[selected];
							var row = "";
								row += '<tr>' +
									'<td>'+data.salesInvoiceId+'</td>' +
									'<td>'+moment(data.invoiceDate).format(Apps.dateFormat)+'</td>' +
									'<td>'+data.status+'</td>' +
									'<td>'+((data.note == null) ? " - " : data.note)+'</td>' +
								'</tr>';
							
							$('.modal-title').html('Order Detail');
							$('.modal-body').html('<div class="container-fluid">' +
								'<div class="row">' +
									'<table class="table">' +
										'<thead>' +
											'<tr>' +
												'<th>Invoice No</th>' + 
												'<th>Product</th>' + 
												'<th>Quantity</th>' +
												'<th>Note</th>' +
											'</tr>' +
										'</thead>' +
										'<tbody>' +
											row +
										'</tbody>' +
									'</table>' +
								'</div>' +
							'</div>');
							$('.modal-footer').html("")
							$('#popup').modal('show');
						});
					}
				});
			}

			loadTable();
		}
	}];

@withStyles(styles)

class Payment {
	render() {
		return (null);
	}
}

var Content = Apps.Widget({ 
	dataPageHeading: dataPageHeading,
	dataContent2: dataContent2
}).Body;

export default Content;