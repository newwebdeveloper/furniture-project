var isNode = typeof module !== 'undefined' && module.exports
	, React = isNode ? require('react') : window.React;

var reactCookie = require('./react-cookie');
var RestCall = require('../api/RestCall');
var Apps = {
	version: "1.0.0",
	baseUrl: "/",
	host: "https://ceramic-api.herokuapp.com",
	// host: "http://api.ceramic.dev:8080",
	selector: "body",
	timeFormat: "hh:mm:ss",
	dateFormat: "DD-MMM-YYYY",
	dateTimeFormat: "DD-MMM-YYYY hh:mm:ss",
	defaultDirectory: "/attachment/",
	defaultInformationMessage: "Your data has been saved.",
	defaultErrorMessage: "Sorry, we cannot processing your request.\nPlease, try again later!",
	optionSelectDefault: "-- SELECT ONE OPTION --",
	AllAlerts: "",
	props: [],
	item: {}
}

Apps.Widget = function (options) {
	var selector = Apps.selector + " .main";
	var dataMessages = options.dataMessages || [];
	var dataTask = options.dataTask || [];
	var dataNotif = options.dataNotif || [];
	var dataAccount = options.dataAccount || []; 
	var dataSideBar = options.dataSideBar || [];
	var dataPageHeading = options.dataPageHeading || [];
	var dataContent1 = options.dataContent1 || [];
	var dataContent2 = options.dataContent2 || [];
	var sessionUser = reactCookie.load('username');

	/* <!-- Top Menu Header --> */
	var NavHeader = React.createClass({
		render: function() {
			return(
				<div className="navbar-header">
					<button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span className="sr-only">Toggle navigation</span>
						<span className="icon-bar"></span>
						<span className="icon-bar"></span>
						<span className="icon-bar"></span>
					</button>
					<a className="navbar-brand" href="/">SB Admin</a>
				</div>
			);
		}
	});

	var Messages = React.createClass({
		componentDidMount: function() {
			{dataMessages.map(function(result, i) {
				if (typeof result.componentDidMount != 'undefined')
					result.componentDidMount();
			})};
		},
		loadHTML: function() {
			var rows = [], _this = this;
			{dataMessages.map(function(result, i) {
				result.id = "key-" + i;
				rows.push(
					<li key={result.id}>
						<a href="#">
							<div>
								<strong>{ result.text }</strong>
								<span className="pull-right text-muted">
									<em>{ result.time }</em>
								</span>
							</div>
							<div>{ result.desc }</div>
						</a>
					</li>
				);
				rows.push(<li key={ result.id + "-devider" } className="divider"></li>);
			})};
			rows.push(<li key={ "key-view" }><a className="text-center" href="#"><strong>Read All Messages</strong> <i className="fa fa-angle-right"></i></a></li>);
			return (<ul className="dropdown-menu dropdown-messages"> {rows} </ul>);
		},

		render: function() {
			return (this.loadHTML());
		}
	});

	var Task = React.createClass({
		componentDidMount: function() {
			{dataTask.map(function(result, i) {
				if (typeof result.componentDidMount != 'undefined')
					result.componentDidMount();
			})};
		},
		loadHTML: function() {
			var rows = [], style = [], className = "", _this = this;
			
			{dataTask.map(function(result, i) {
				result.id = "key-" + i;
				className = "progress-bar progress-bar-" + result.type;
				style[i] = {};
				style[i].width = result.percentage;
				rows.push(
					<li key={result.id}>
						<a href="#">
							<div>
								<p>
									<strong>{ result.text }</strong>
									<span className="pull-right text-muted">{ result.percentage } Complete</span>
								</p>
								<div className="progress progress-striped active">
									<div className={ className } role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style={ style[i] }>
									    <span className="sr-only">{ result.percentage } Complete ({ result.type })</span>
									</div>
								</div>
							</div>
						</a>
					</li>
				);
				rows.push(<li key={ result.id + "-divider" } className="divider"></li>);
			})};
			
			rows.push(<li key={ "key-view" }><a className="text-center" href="#"><strong>See All Tasks</strong> <i className="fa fa-angle-right"></i></a></li>);

			return(<ul className="dropdown-menu dropdown-tasks"> { rows } </ul>);
		},

		render: function() {
			return (this.loadHTML());
		}
	});

	var Notif = React.createClass({
		componentDidMount: function() {
			{dataNotif.map(function(result, i) {
				if (typeof result.componentDidMount != 'undefined')
					result.componentDidMount();
			})};
		},
		loadHTML: function() {
			var rows = [], className = "", _this = this;

			{dataNotif.map(function(result, i) {
				var url = result.url || "#";
				result.id = "key-" + i;
				className = "fa fa-fw "+ result.icon;
				if (typeof result.event == 'function') { _this.onHandler = result.event; }
				rows.push(<li key={result.id}><a href={ url } onClick={_this.onHandler}><i className= { className }></i> { result.text } <span className="pull-right text-muted small"> { result.time } </span></a></li>);
				rows.push(<li key={result.id + "-divider"} className="divider"></li>);
			})};
			rows.push(<li key={ "key-view" }><a className="text-center" href={ Apps.AllAlerts }><strong>See All Alerts</strong> <i className="fa fa-angle-right"></i></a></li>);
			return (<ul className="dropdown-menu dropdown-alerts"> {rows} </ul>);
		},

		render: function() {
			return (this.loadHTML());
		}
	});

	var Account = React.createClass({
		componentDidMount: function() {
			{dataAccount.map(function(result, i) {
				if (typeof result.componentDidMount != 'undefined')
					result.componentDidMount();
			})};
		},
		loadHTML: function() {
			var rows = [], _this = this;
			{dataAccount.map(function(result, i) {
				result.id = "key-" + i;
				var cls = "fa fa-fw " + result.cls;
				if (typeof result.event == 'function') { _this.onHandler = result.event; }
				rows.push(<li id={ result.id } key={ result.id }><a href={ result.url } onClick={_this.onHandler}><i className={ cls }></i> { result.text }</a></li>);
				if (result.separated == true)
					rows.push(<li key={result.id+"-divider"} className="divider"></li>);
			})};
			return (<ul className="dropdown-menu"> {rows} </ul>);
		},
		render: function() {
			return (this.loadHTML());
		}
	});

	/* <!-- Top Menu Items --> */
	var NavBar = React.createClass({
		loadHTML: function() {
			var html = [];
			var tab1 = 
				<li className="dropdown" key={ "key1" }>
					<a href="#" className="dropdown-toggle" data-toggle="dropdown"><i className="fa fa-envelope fa-fw"></i> <i className="fa fa-caret-down"></i></a>
					<Messages />
				</li>;
			// html.push(tab1);
			var tab2 = 
				<li className="dropdown" key={ "key2" }>
					<a href="#" className="dropdown-toggle" data-toggle="dropdown"><i className="fa fa-tasks fa-fw"></i> <i className="fa fa-caret-down"></i></a>
					<Task />
				</li>;
			// html.push(tab2);
			var tab3 = 
				<li className="dropdown" key={ "key3" }>
					<a href="#" className="dropdown-toggle" data-toggle="dropdown"><i className="fa fa-bell fa-fw"></i> <span className="label label-success" id="total-notif">10</span> <i className="fa fa-caret-down"></i></a>
					<Notif />
				</li>;
			html.push(tab3);
			var tab4 = 
				<li className="dropdown" key={ "key4" }>
					<a href="#" className="dropdown-toggle" data-toggle="dropdown"><i className="fa fa-user fa-fw"></i> { sessionUser } <i className="fa fa-caret-down"></i></a>
					<Account />
				</li>
			html.push(tab4);
			return (<ul className="nav navbar-top-links navbar-right"> {html} </ul>);
		},
		render: function() {
			return (this.loadHTML());
		}
	});

	/* <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens --> */
	var Sidebar = React.createClass({
		getInitialState: function () {
			var _this = this;
			{dataSideBar.map(function(result, i) {
				if (typeof result.data != 'undefined')
					_this.prop.data = result.data;
			})};
			return (null);
		},
		componentDidMount: function() {
			{dataSideBar.map(function(result, i) {
				if (typeof result.componentDidMount != 'undefined')
					result.componentDidMount()
			})};
			$('#side-menu').metisMenu();
		},
		loadHTML: function() {
			var rows = [], className = "", _this = this;
			rows.push(
				<li key={ "key-search-bar" } className="sidebar-search">
					<div className="input-group custom-search-form">
						<input type="text" className="form-control" placeholder="Search..." />
						<span className="input-group-btn">
							<button className="btn btn-default" type="button">
								<i className="fa fa-search"></i>
							</button>
						</span>
					</div>
				</li>
			);
			{dataSideBar.map(function(result, i) {
				result.id = "key-" + i;
				className = "fa fa-fw " + result.icon;
				if (typeof result.items !== 'undefined')
				{
					var Items = [], Items2 = [];
					{result.items.map(function(resultItem1, j) {
						resultItem1.id = "key-child1-" + j;
						if (typeof resultItem1.items  !== 'undefined')
						{
							{resultItem1.items.map(function(resultItem2, k) {
								resultItem2.id = "key-child2-" + k;
								Items2.push(<li key={ resultItem2.id }><a href= { resultItem2.url }>{ resultItem2.text }</a></li>);
							})};
							Items.push(
								<li key={ resultItem1.id }><a href= { resultItem1.url }>{ resultItem1.text } <span className="fa arrow"></span></a>
									<ul className="nav nav-third-level"> { Items2 } </ul>
								</li>
							);
						}
						else
						{
							Items.push(<li key={ resultItem1.id }><a href= { resultItem1.url }>{ resultItem1.text }</a></li>);
						}
					})};
					rows.push(
						<li key={ result.id } className={ result.class }><a href="#"><i className={ className }></i> { result.title } <span className="fa arrow"></span></a>
							<ul className="nav nav-second-level">{ Items }</ul>
						</li>
					);
				}
				else
				{
					rows.push(<li key={ result.id }><a href={ result.url }><i className={ className }></i> { result.title } </a></li>);
				}
			})};

			return (
				<div className="navbar-default sidebar" role="navigation">
					<div className="sidebar-nav navbar-collapse">
						<ul className="nav" id="side-menu"> {rows} </ul>
					</div>
				</div>
			);
		},
		render: function() {
			return (this.loadHTML());
		}
	});

	var Header = React.createClass({
		render: function() {
			return (
				<nav className="navbar navbar-default navbar-static-top" role="navigation">
					<NavHeader />
					<NavBar />
					<Sidebar />
				</nav>
			);
		}
	});

	var PageHeading = React.createClass({
		componentDidMount: function() {
			QueryString = function () {
				// This function is anonymous, is executed immediately and 
				// the return value is assigned to QueryString!
				var query_string = {};
				var query = window.location.search.substring(1);
				var vars = query.split("&");
				for (var i=0;i<vars.length;i++) {
					var pair = vars[i].split("=");
						// If first entry with this name
					if (typeof query_string[pair[0]] === "undefined") {
						query_string[pair[0]] = decodeURIComponent(pair[1]);
						// If second entry with this name
					} else if (typeof query_string[pair[0]] === "string") {
						var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
						query_string[pair[0]] = arr;
						// If third or later entry with this name
					} else {
						query_string[pair[0]].push(decodeURIComponent(pair[1]));
					}
				}
				return query_string;
			}();

			$.ajaxSetup({
 				beforeSend: function (xhr){
 					xhr.setRequestHeader("Authorization", reactCookie.load('auth'));
 					/* Loading */
 					$("body").append('<div id="loading" class="center" style="display:block;"><img src="/loading-x.gif" alt="" class="iLoading"/></div>');
 					var width = (parseInt($(window).width()) - ($(".iLoading").width() == 0 ? 480 : parseInt($(".iLoading").width()))) / 2;
 					var height = (parseInt($(window).height())-($(".iLoading").height() == 0 ? 320 : parseInt($(".iLoading").height()))) / 2;
 					$(".iLoading").css({
						'position' : 'fixed',
						'left': width,
						'top': height,
						'z-index' : 1000
					});
					$("body").css({
						opacity : .6,
						background : '#daecf4'
					});
					/***********/
 				},
 				complete: function(xhr, status) {
 					$("#loading").remove();
					$("body").css({
						opacity : 1,
						background : ''
					});
 				},
				statusCode: {
					401: function(){
						alert("Sorry, session expired. You must login again");
						location.reload();
					}
				}
 			});

			$(document).ajaxComplete(function(event, xhr, request, settings) {
				// console.log(xhr);
				var result = xhr.responseJSON;
			});
		},
		loadHTML: function() {
			var rows = [], className = "", data = dataPageHeading, _this = this, toolbar = [], pageHeader = [], breadcrumb = "";
			var buttonCollection = data.toolbar || [];

			{buttonCollection.map(function(result, i) {
				className = "btn " + result.cls;
				if (typeof result.event == 'function') { _this.onHandler = result.event; }
				if (result.type.toLowerCase() == "button") {
					toolbar.push(<button className={className} id={result.name} name={result.name} onClick={_this.onHandler}>{result.text}</button>);
				}
			})}

			if (buttonCollection.length > 0) {
				pageHeader.push(<h1 className="page-header" style={ { display: "flex" } }>{ data.title } <small>{ data.additionalTitle }</small> <div style={ { position: "absolute", right: "20px" } }>{toolbar}</div></h1>);
			}
			else {
				pageHeader.push(<h1 className="page-header">{ data.title } <small>{ data.additionalTitle }</small> </h1>);
			}

			className = "fa " + data.breadcrumbsIcon;

			if (typeof data.subtitle != 'undefined' && data.subtitle.length != 0) {
				var subtitle = data.subtitle || [];
				subtitle.map(function(result, i) {
					if (i == 0)
						breadcrumb += result;
					else
						breadcrumb += " / " + result;
				});
			};
		
			rows.push(
				<div key={ "key-page-heading" } className="col-lg-12">
					{pageHeader}
					<ol className="breadcrumb">
						<li>
							<i className={ className }></i> &nbsp;
							{ breadcrumb }
						</li>
					</ol>
				</div>
			);
			return(<div className="row"> { rows } </div>);
		},
		render: function() {
			return (this.loadHTML());
		}
	});

	var Content1 = React.createClass({
		componentDidMount: function() {
			{dataContent1.map(function(result, i) {
				if (typeof result.componentDidMount != 'undefined')
					result.componentDidMount();
			})};
		},
		loadHTML: function() {
			var className = {}, rows = [], _this = this;
			{dataContent1.map(function(result, i) {
				result.id = "key-" + i;
				className.panel = "panel " + result.panel;
				className.type = "fa fa-5x " + result.type;
				rows.push(
					<div key={ result.id } className="col-lg-3 col-md-6">
						<div className={ className.panel }>
							<div className="panel-heading">
								<div className="row">
									<div className="col-xs-3">
										<i className={ className.type }></i>
									</div>
									<div className="col-xs-9 text-right">
										<div className="huge">{ result.text }</div>
										<div>{ result.desc }</div>
									</div>
								</div>
							</div>
							<a href={ result.url }>
								<div className="panel-footer" key={ "view-all-" + i } onClick={_this.clickHandler}> 
									<span className="pull-left">View Details</span>
									<span className="pull-right"><i className="fa fa-arrow-circle-right"></i></span>
									<div className="clearfix"></div>
								</div>
							</a>
						</div>
					</div>
				);
			})};
			return (
			    <div className="row"> {rows} </div>
			);
		},
		render: function() {
			return (this.loadHTML());
		}
	});

	var Content2 = React.createClass({
		componentWillUnmount: function() {
			{dataContent2.map(function(result, i) {
				if (typeof result.componentWillUnmount != 'undefined')
					result.componentWillUnmount();
			})};
		},
		componentWillMount: function() {
			{dataContent2.map(function(result, i) {
				if (typeof result.componentWillMount != 'undefined')
					result.componentWillMount();
			})};
		},
		componentWillReceiveProps: function(nextProps) {},
		shouldComponentUpdate: function(nextProps, nextState) { return true; },
		componentWillUpdate: function(nextProps, nextState) {},
		componentDidMount: function() {
			$('#page-wrapper').append('<div class="alert alert-dismissible fade in" role="alert" id="alert" style="position: absolute; top: 50px; right: 30px; display:none; width:220px;">' +
					'<div style="display:inline-flex">' +
					'<h4 id="alert-title"></h4> &nbsp;' +
					'<p id="alert-content"></p>' +
					'</div>' +
				'</div>');

			$('body').append('<div class="modal fade" id="PopUpConfirm" tabindex="-1" role="dialog">'+
				'<div class="modal-dialog modal-sm">' +
					'<div class="modal-content">' +
						'<div class="modal-header">' +
							'<h4 class="modal-title">Confirmation</h4>' +
						'</div>' +
						'<div class="modal-body">' +
						'</div>' +
						'<div class="modal-footer">' +
							'<button type="button" class="btn btn-default" data-dismiss="modal">No</button>' +
							'<button type="button" class="btn btn-primary" id="btnYes">Yes</button>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>');


			$('body').append('<div class="modal fade" id="popup"></div>');
			$('#popup').html('<div class="modal-dialog">' +
					'<div class="modal-content">' +
						'<div class="modal-header">' +
							'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
							'<h4 class="modal-title">Modal title</h4>' +
						'</div>' +
						'<div class="modal-body">' +
							'<p>One fine body&hellip;</p>' +
						'</div>' +
						'<div class="modal-footer">' +
							'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
							'<button type="button" id="btnSubmitPopup" class="btn btn-primary">Save changes</button>' +
						'</div>' +
					'</div>' +
				'</div>');
			
			if (typeof $(".datepicker") !== 'undefined') {
				$(".datepicker").datepicker({
					autoclose: true,
					format: "dd M yyyy"
				});
			}

			if (typeof $(".input-daterange input") !== 'undefined') {
				$(".input-daterange input").each(function (){
					$(this).datepicker({
						autoclose: true,
						format: "dd M yyyy"
					});
				});
			}

			if (typeof $("input[type=radio]") !== 'undefined') {
				if ($("input[type=radio]").length > 0) {
					$("input[type=radio]").each(function() {
						var id = $(this).attr('id');
						$("#"+id+":first").attr('checked', 'checked');
					});
				}
			}
			
			$(':input[required], select[required]').change(function(e) {
				if ($(this).val() == "") {
					if ($(this).siblings().length == 0)
						$(this).parent().append("<span className='errorInput' id='error-"+$(this).attr('id')+"' style='color:red;font-weight:bold;'>This Field is Required!</span>");
				}
				else 
					$("#error-" + $(this).attr('id')).remove();
			}).trigger("change");
			
			{dataContent2.map(function(result, i) {
				if (typeof result.componentDidMount != 'undefined')
					result.componentDidMount();
			})};
		},

		loadHTML: function() {
			var rows = [], cls = "", objects = [], object = {}, classHeader = "", body = [], ColHead = 12, ColBody = 12, _this = this, chart = [];

			{dataContent2.map(function(result, i) {
				if (result.loadAll == true) rows = rows; else rows = [];
				result.id = "key-" + i;
				cls = result.cls || "col-lg-12";
				if (typeof result.type !== 'undefined'){
					objects = [], chart = [];
					if (typeof result.menu !== 'undefined') {
						{result.menu.map(function(resultItem1, j) {
							resultItem1.id = "key-child1-menu-" + j;
							if (resultItem1.separated == true)
								objects.push(<li key={resultItem1.id+"-divider"} className="divider"></li>);
							objects.push(<li key={resultItem1.id}><a href={ resultItem1.url }> { resultItem1.text } </a></li>);
						})};
					}
					if (result.type.toUpperCase() == 'AREACHART') {
						object.title = result.title || 'Area Chart';
						object.chartName = 'morris-area-chart';
					}
					else if (result.type.toUpperCase() == 'BARCHART') {
						object.title = result.title || 'Bar Chart';
						object.chartName = 'morris-bar-chart';
					}
					else if (result.type.toUpperCase() == 'DONUTCHART') {
						object.title = result.title || 'Donut Chart';
						object.chartName = 'morris-donut-chart';
					}

					if (typeof result.table != 'undefined') {
						var bodyTable = [], objectsTable = [], headerTable = [], cellTable = [], rowTable = [], classTable = "", classChart = "";
						{result.table.map(function(resultItem1, j) {
							if (typeof resultItem1.header !== 'undefined') {
								{resultItem1.header.map(function(resultItem2, k) {
									headerTable.push(<th key={"key-child1-header-" + k} > { resultItem2 } </th>);
								})};
								objectsTable.push(<thead><tr key={"key-child1-row-" + j}>{ headerTable }</tr></thead>);
							}
							if (typeof resultItem1.body !== 'undefined') {
								{resultItem1.body.map(function(resultItem2, k) {
									cellTable = [];
									{resultItem2.map(function(resultItem3, l) {
										cellTable.push(<td key={"key-child2-cell-" + k + "-" + l}> { resultItem3 } </td>);
									})};
									rowTable.push(<tr id={result.name + "-key-child2-row-" + k} key={"key-child2-row-" + k}>{ cellTable }</tr>);
								})};
								objectsTable.push(<tbody>{ rowTable }</tbody>);
							}

							bodyTable.push(
								<div key={result.id + "table-body"} className={ resultItem1.typeTable }>
									<table className="table table-bordered table-hover table-striped" id={ resultItem1.name } name={ resultItem1.name }>
										{ objectsTable }
									</table>
								</div>
							);
						})};
						if (typeof result.cols == 'undefined') {
							ColHead = 4;
							ColBody = 8;
						}
						classTable = "col-lg-" + ColHead;
						classChart = "col-lg-" + ColBody;
						chart.push(<div className={ classTable }>{ bodyTable }</div>);
					}
					else {
						classChart = "col-lg-12";
					}
					chart.push(<div className={ classChart }><div id={ object.chartName }></div></div>);

					rows.push(
						<div key={result.id} className={ cls }>
							<div className="panel panel-default">
								<div className="panel-heading">
									<i className="fa fa-bar-chart-o fa-fw"></i> { object.title } 
									<div className="pull-right">
										<div className="btn-group">
											<button type="button" className="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
												Actions 
												<span className="caret"></span>
											</button>
											<ul className="dropdown-menu pull-right" role="menu">
												{ objects }
											</ul>
										</div>
									</div>
								</div>
								<div className="panel-body">
									<div className="row">
										{ chart }
									</div>
								</div>
							</div>
						</div>
					);
				}
				else if (typeof result.mode !== 'undefined'){
					switch(result.mode.toUpperCase()) {
						case 'TIMELINE': 
							objects = [], body = [];
							if (typeof result.items !== 'undefined') {
								{result.items.map(function(resultItem1, j) {
									resultItem1.id = "key-child1-timeline-" + j;
									if (resultItem1.inverted)
										object.level1 = "timeline-inverted";
									else
										object.level1 = "";
									object.level2 = resultItem1.typeTimeline + " " + resultItem1.typeAlert;
									object.level3 = "fa " + resultItem1.iconBadge;

									objects.push(
										<li key={resultItem1.id} className={ object.level1 }>
											<div className={ object.level2 }><i className={ object.level3 }></i>
											</div>
											<div className="timeline-panel">
												<div className="timeline-heading">
													<h4 className="timeline-title">{ resultItem1.header }</h4>
													<p><small className="text-muted"><i className="fa fa-clock-o"></i> { resultItem1.time }</small>
													</p>
												</div>
												<div className="timeline-body">
													<p>{ resultItem1.body }</p>
												</div>
											</div>
										</li>
									);
								})};
								body.push(
									<div className="panel panel-default">
										<div className="panel-heading">
											<i className="fa fa-bar-chart-o fa-fw"></i> { result.title } 
										</div>
										<div className="panel-body">
											<ul className="timeline">
												{ objects }
											</ul>
										</div>
									</div>
								);
							}
						break;
						case 'NOTIFICATION':
							objects = [], body = [];
							var notif = [];
							if (typeof result.items !== 'undefined') {
								{result.items.map(function(resultItem1, j) {
									resultItem1.id = "key-child1-notification-" + j;
									object.class = "fa fa-fw " + resultItem1.icon;
									objects.push(
										<a key={resultItem1.id} href={ resultItem1.url } className="list-group-item">
											<i className={ object.class }></i> { resultItem1.text }
											<span className="pull-right text-muted small"><em>{ resultItem1.time }</em>
											</span>
										</a>
									);
								})};
								notif.push(
									<div className="list-group">
										{ objects }
									</div>
								);
								notif.push(<a key={ "key-view-alert-" + i } href="#" className="btn btn-default btn-block">View All Alerts</a>);
								body.push(
									<div className="panel panel-default">
										<div className="panel-heading">
											<i className="fa fa-bar-chart-o fa-fw"></i> { result.title } 
										</div>
										<div className="panel-body">
											{ notif }
										</div>
									</div>
								);
							}
						break;
						case 'TABLE':
							body = [], objects = [];
							var headerTable = [], cellTable = [], rowTable = [], tableEl = [];
							if (typeof result.header !== 'undefined') {
								{result.header.map(function(resultItem1, j) {
									headerTable.push(<th key={"key-child1-header-" + j} > { resultItem1 } </th>);
								})};
								objects.push(<thead><tr key={"key-child1-row-" + i}>{ headerTable }</tr></thead>);
							}
							if (typeof result.body !== 'undefined') {
								{result.body.map(function(resultItem1, j) {
									cellTable = [];
									{resultItem1.map(function(resultItem2, k) {
										cellTable.push(<td key={"key-child2-cell-" + j + "-" + k}> { resultItem2 } </td>);
									})};
									rowTable.push(<tr id={result.name + "-key-child2-row-" + j} key={"key-child2-row-" + j}>{ cellTable }</tr>);
								})};
								objects.push(<tbody>{ rowTable }</tbody>);
							}
							if (result.panel == true) {
								tableEl.push(
									<div key={result.id + "table-body"} className={ result.typeTable }>
										<table className="table table-bordered table-hover table-striped" id={ result.name } name={ result.name }>
											{ objects }
										</table>
									</div>
								);
								body.push(
									<div className="panel panel-default">
										<div className="panel-heading">
											<i className="fa fa-bar-chart-o fa-fw"></i> { result.title } 
										</div>
										<div className="panel-body">
											{ tableEl }
										</div>
									</div>
								);
							}
							else {
								body.push(
									<div key={result.id + "table-body"} className={ result.typeTable }>
										<table className="table table-bordered table-hover table-striped" id={ result.name } name={ result.name }>
											{ objects }
										</table>
									</div>
								);
							}
						break;
						case 'INPUT':
							var classNameHeader = [], classNameBody = [], style = [];
							body = [], objects = [];
							{result.items.map(function(resultItem1, j) {
								var placeholder = resultItem1.placeholder || resultItem1.text;
								var required = resultItem1.required || "", disabled = resultItem1.disabled || "";
								if (typeof resultItem1.cols == 'undefined') {
									ColHead = 2;
									ColBody = 10;
								}
								else {
									ColHead = resultItem1.cols[0];
									if (resultItem1.cols.length > 1)
										ColBody = resultItem1.cols[1];
									else
										ColBody = 12 - ColHead;
								}
								style[j] = {};
								style[j].color = resultItem1.color;
								style[j].display = resultItem1.display;
								classNameHeader.push("control-label col-sm-" + ColHead);
								classNameBody.push("col-sm-" + ColBody);
								if (resultItem1.type == 'hidden') {
									objects.push(<input type={ resultItem1.type } name={ resultItem1.name } id={ resultItem1.name } value={ resultItem1.value } />);
								}
								else if(resultItem1.type == 'label') {
									objects.push(
										<div className="form-group" key={ "group-" + j }>
											<label className={ classNameHeader[j] }>{ resultItem1.text }</label> 
											<div className={ classNameBody[j] }>
												<label ref={ resultItem1.name } name={ resultItem1.name } id={ resultItem1.name } style={ style[j] }>{ resultItem1.text }</label>
											</div>
										</div>
									);
								}
								else if (resultItem1.type == 'text' || resultItem1.type == 'email' || resultItem1.type == 'password' || resultItem1.type == 'number') {
									objects.push(
										<div className="form-group" key={ "group-" + j } style={ style[j] }>
											<label className={ classNameHeader[j] }>{ resultItem1.text }</label>
											<div className={ classNameBody[j] }>
												<input className="form-control" required={ required } disabled={ disabled } type={ resultItem1.type } name={ resultItem1.name } id={ resultItem1.name } placeholder={ placeholder } />
											</div>
										</div>
									);
								}
								else if (resultItem1.type == 'autocomplete') {
									objects.push(
										<div className="form-group" key={ "group-" + j }>
											<label className={ classNameHeader[j] }>{ resultItem1.text }</label>
											<div className={ classNameBody[j] }>
												<input className="form-control typeahead" required={ required } type="text" name={ resultItem1.name } id={ resultItem1.name } placeholder={ placeholder } />
											</div>
										</div>
									);
								}
								else if (resultItem1.type == 'area') {
									objects.push(
										<div className="form-group" key={ "group-" + j }>
											<label className={ classNameHeader[j] }>{ resultItem1.text }</label>
											<div className={ classNameBody[j] }>
												<textarea required={ required } className="form-control" name={ resultItem1.name } id={ resultItem1.name } rows={ (resultItem1.rows == 'undefined') ? 3 : resultItem1.rows }></textarea>
											</div>
										</div>
									);	
								}
								else if (resultItem1.type == 'file') {
									objects.push(
										<div className="form-group" key={ "group-" + j }>
											<label className={ classNameHeader[j] }>{ resultItem1.text }</label>
											<div className={ classNameBody[j] }>
												<input required={ required } type="file" name={ resultItem1.name } id={ resultItem1.name } accept="image/*" />
											</div>
										</div>
									);
								}
								else if (resultItem1.type == 'checkbox') {
									var checkbox = [];
									{resultItem1.data.map(function(resultItem2, k) {
										if (resultItem1.inline == false) {
											checkbox.push(
												<div className="checkbox" key={ "checkbox-" + resultItem2.text + k }>
													<label>
														<input type="checkbox" value={ resultItem2.value } name={ resultItem1.name } id={ resultItem1.name } /> { resultItem2.text }
													</label>
												</div>
											);
										}
										else {
											checkbox.push(
												<label className="checkbox-inline" key={ "checkbox-" + resultItem2.text + k }>
													<input type="checkbox" value={ resultItem2.value } name={ resultItem1.name } id={ resultItem1.name } /> { resultItem2.text }
												</label>
											);	
										}
									})};
									objects.push(
										<div className="form-group" key={ "group-" + j }>
											<label className={ classNameHeader[j] }>{ resultItem1.text }</label> 
											<div className={ classNameBody[j] }>
												{ checkbox } 
											</div>
										</div>
									);
								}
								else if (resultItem1.type == 'radio') {
									var radiogroup = [];
									{resultItem1.data.map(function(resultItem2, k) {
										if (resultItem1.inline == false) {
											radiogroup.push(
												<div className="radio" key={ "radio-" + resultItem2.text + k }>
													<label>
														<input type="radio" name={ resultItem2.name } id={ resultItem2.name } value={ resultItem2.value } /> { resultItem2.text }
													</label>
												</div>
											);
										}
										else {
											radiogroup.push(
												<label className="radio-inline" key={ "radio-" + resultItem2.text + k }>
													<input type="radio" name={ resultItem2.name } id={ resultItem2.name } value={ resultItem2.value } /> { resultItem2.text }
												</label>
											);
										}
									})};

									objects.push(
										<div className="form-group" key={ "group-" + j }>
											<label className={ classNameHeader[j] }>{ resultItem1.text }</label> 
											<div className={ classNameBody[j] }>
												{ radiogroup } 
											</div>
										</div>
									);
								}
								else if (resultItem1.type == 'select') {
									var selectoption = [], option = resultItem1.data || [];
									{option.map(function(resultItem2, k) {
										selectoption.push(
											<option value={ resultItem2.value }>{ resultItem2.text }</option>
										);
									})};
									if (resultItem1.multiple == true) {
										objects.push(
											<div className="form-group" key={ "group-" + j }>
												<label className={ classNameHeader[j] }>{ resultItem1.text }</label>
												<div className={ classNameBody[j] }>
													<select required={ required } multiple className="form-control" name={ resultItem1.name } id={ resultItem1.name }> { selectoption } </select>
												</div>
											</div>
										);
									}
									else {
										objects.push(
											<div className="form-group" key={ "group-" + j }>
												<label className={ classNameHeader[j] }>{ resultItem1.text }</label>
												<div className={ classNameBody[j] }>
													<select required={ required } className="form-control" name={ resultItem1.name } id={ resultItem1.name }> { selectoption } </select>
												</div>
											</div>
										);
									}
								}
								else if (resultItem1.type == 'button') {
									var buttongroup = [];
									{resultItem1.data.map(function(resultItem2, k) {
										var className = "btn " + resultItem2.cls;
										if (typeof resultItem2.event == 'function') { _this.onHandler = resultItem2.event; }
										buttongroup.push(
											<button type={ resultItem2.type } className= { className } name={ resultItem2.name } id={ resultItem2.name } onClick={_this.onHandler}> { resultItem2.text } </button> 
										);
									})};
									objects.push(
										<div className="form-group" key={ "group-" + j }>
											<label className={ classNameHeader[j] }>{ resultItem1.text }</label> 
											<div className={ classNameBody[j] }>
												{ buttongroup } 
											</div>
										</div>
									);
								}
								else if (resultItem1.type == 'datetime') {
									if (resultItem1.range == true) {
										objects.push(
											<div className="form-group" key={ "group-" + j }>
												<label className={ classNameHeader[j] }>{ resultItem1.text }</label> 
												<div required={ required } className={ "input-daterange " + classNameBody[j] }>
													<input name={ resultItem1.nameStart } id={ resultItem1.nameStart } type="text" className="input-small datepicker" />
													<span className="add-on">to</span>
													<input name={ resultItem1.nameEnd } id={ resultItem1.nameEnd } type="text" className="input-small datepicker" />
												</div>
											</div>
										);
									}
									else {
										objects.push(
											<div className="form-group" key={ "group-" + j }>
												<label className={ classNameHeader[j] }>{ resultItem1.text }</label> 
												<div className={ classNameBody[j] }>
													<input required={ required } name={ resultItem1.name } id={ resultItem1.name } className="datepicker" />
												</div>
											</div>
										);
									}	
								}
							})};
							body.push(
								<form role="form" className="form-horizontal" name={ result.name } id={ result.name }>
									{ objects }	
								</form>
							);
						break;
						case 'THUMBNAIL':
							objects = [];
							{result.items.map(function(resultItem1, j){
								var src = ((typeof resultItem1.url == 'undefined' || resultItem1.url == '') ? "/placeholder-640x480.png" : resultItem1.url);
								objects.push(
									<div key={ "thumbnail" + j } className={ resultItem1.col }>
										<div className="thumbnail">
											<img src={ src } alt="placeholder" />
											<div className="caption">
												<h3>{ resultItem1.caption }</h3>
												<p>{ resultItem1.desc }</p>
											</div>
										</div>
									</div>
								);
							})};
							if (result.panel == false) {
								body.push(
									<div className="row">
										{ objects }
									</div>
								);
							}
							else {
								classHeader = "fa fa-fw " + result.iconHeader
								body.push(
									<div key={result.id} className={ cls }>
										<div className="panel panel-default">
											<div className="panel-heading">
												<i className={ classHeader }></i> { result.title }
											</div>
											<div className="panel-body">
												{ objects }
											</div>
										</div>
									</div>
								);
							}
						break;
					}

					rows.push(
						<div key={result.id} className={ cls }>
							{ body }
						</div>
					);
				}
			})};
			return (
				<div className="row"> { rows } </div>
			);
		},
		render: function() {
			return (this.loadHTML());
		}
	});

	var Page = React.createClass({
		render: function() {
			return (
				<div id="page-wrapper">
					<PageHeading />
					<Content1 />
					<Content2 />
				</div>
			);
		}
	});

	return { Header: Header, Body: Page };
};

Apps.Users = function (options) {
	var dataField = options.dataField || [];
	var Form = React.createClass({
		componentWillUnmount: function() {
			{dataField.map(function(result, i) {
				if (typeof result.componentWillUnmount != 'undefined')
					result.componentWillUnmount();
			})};
		},
		componentWillMount: function() {
			{dataField.map(function(result, i) {
				if (typeof result.componentWillMount != 'undefined')
					result.componentWillMount();
			})};
		},
		componentDidMount: function() {
			$('#wrapper').append('<div class="alert alert-dismissible fade in" role="alert" id="alert" style="position: absolute; top: 50px; right: 30px; display:none; width:220px;">' +
					'<div style="display:inline-flex">' +
					'<h4 id="alert-title"></h4> &nbsp;' +
					'<p id="alert-content"></p>' +
					'</div>' +
				'</div>');
			{dataField.map(function(result, i) {
				if (typeof result.componentDidMount != 'undefined')
					result.componentDidMount();
			})};
		},
		componentWillReceiveProps: function(nextProps) {},
		shouldComponentUpdate: function(nextProps, nextState) { return true; },
		componentWillUpdate: function(nextProps, nextState) {},
		loadHTML: function() {
			var rows = [], cls = "", objects = [], signin = [], buttonCollection = [], _this = this, style = [];
			
			{dataField.map(function(result, i) {
				cls = result.cls;
				{result.items.map(function(resultItem1, j) {
					resultItem1.id = "key-" + j;
					style[i] = {};
					style[i].color = resultItem1.color;
					style[i].display = resultItem1.display;
					var Class = resultItem1.cls || "";
					if (typeof resultItem1.event == 'function') { _this.onHandler = resultItem1.event; }
					switch(resultItem1.type.toUpperCase()) {
						case 'LABEL':
							objects.push(
								<div className="form-group" key={ resultItem1.id }>
									<label ref={ resultItem1.name } name={ resultItem1.name } id={ resultItem1.name } style={ style[i] }>{ resultItem1.text }</label>
								</div>
							);
						break;
						case 'HIDDEN':
							objects.push(
								<div className="form-group" key={ resultItem1.id }>
									<input className="form-control" ref={ resultItem1.name } name={ resultItem1.name } id={ resultItem1.name } type="hidden" value={ resultItem1.value } />
								</div>
							);
						break;
						case 'TEXT':
							objects.push(
								<div className="form-group" key={ resultItem1.id }>
									<input className="form-control" placeholder={ resultItem1.text } ref={ resultItem1.name } name={ resultItem1.name } id={ resultItem1.name } type="text" style={ style[i] } />
								</div>
							);
						break;
						case 'PASSWORD':
							objects.push(
								<div className="form-group" key={ resultItem1.id }>
									<input className="form-control" placeholder={ resultItem1.text } ref={ resultItem1.name } name={ resultItem1.name } id={ resultItem1.name } type="password" style={ style[i] } />
								</div>
							);
						break;
						case 'EMAIL':
							objects.push(
								<div className="form-group" key={ resultItem1.id }>
									<input className="form-control" placeholder={ resultItem1.text } ref={ resultItem1.name } name={ resultItem1.name } id={ resultItem1.name } type="email" style={ style[i] } />
								</div>
							);
						break;
						case 'CHECKBOX':
							objects.push(
								<div className="form-group" key={ resultItem1.id }>
									<label><input name={ resultItem1.name } id={ resultItem1.name } ref={ resultItem1.name } value={ resultItem1.value } type="checkbox" style={ style[i] } /> { resultItem1.text }</label>
								</div>
							);
						break;
						case 'BUTTON':
							if (typeof resultItem1.event == 'function') { _this.onHandler = resultItem1.event; }

							objects.push(
								<div className="form-group" key={ resultItem1.id } onClick={_this.onHandler}>
									<a href={ resultItem1.url } className={ Class } id={ resultItem1.name } name={ resultItem1.name }>{ resultItem1.text }</a>
								</div>
							);
							if (typeof resultItem1.additional != 'undefined') {
								if (resultItem1.additional.length > 0) {
									{resultItem1.additional.map(function(resultItem2, k){
										var Class = "btn btn-block btn-social btn-" + resultItem2.toLowerCase();
										var Icon = "fa fa-" + resultItem2.toLowerCase();
										var SignAuth = resultItem2.charAt(0).toUpperCase() + resultItem2.slice(1).toLowerCase();
										signin.push(
											<a className={ Class }>
												<i className={ Icon }></i> Sign in with { SignAuth }
											</a>
										);
									})};

									buttonCollection.push(<hr />);
									buttonCollection.push(
										<div>
											{ signin }
										</div>
									);
								}
							}
						break;
						case 'SELECT':
							var option = [];
							if (typeof resultItem1.options != 'undefined') {
								{resultItem1.options.map(function(resultItem2, k) {
									option.push(<option value={ resultItem2.value }>{ resultItem2.text }</option>)
								})};
							}
							objects.push(
								<div className="form-group" key={ resultItem1.id }>
									<select className="form-control" ref={ resultItem1.name } name={ resultItem1.name } id={ resultItem1.name } style={ style[i] }><option>{ (typeof resultItem1.opt_text == 'undefined') ? Apps.optionSelectDefault : resultItem1.opt_text }</option>{ option }</select>
								</div>
							);
						break;
						case 'RADIO':
							var radiogroup = [];
							{resultItem1.data.map(function(resultItem2, k) {
								if (resultItem1.inline == false) {
									radiogroup.push(
										<div className="radio" key={ "radio-" + resultItem2.text + k }>
											<label>
												<input type="radio" ref={ resultItem1.name } name={ resultItem2.name } id={ resultItem2.name } value={ resultItem2.value } /> { resultItem2.text }
											</label>
										</div>
									);
								}
								else {
									radiogroup.push(
										<label className="radio-inline" key={ "radio-" + resultItem2.text + k }>
											<input type="radio" ref={ resultItem1.name } name={ resultItem2.name } id={ resultItem2.name } value={ resultItem2.value } /> { resultItem2.text }
										</label>
									);
								}
							})};

							objects.push(
								<div className="form-group" key={ "group-" + j }>
									<label>{ resultItem1.text }</label> 
									<div>
										{ radiogroup } 
									</div>
								</div>
							);
						break;
					}
				})};
				rows.push(
					<div className="panel-heading" key={ "pnl-heading" + i }>
						<h3 className="panel-title">{ result.title }</h3>
					</div>
				);
				rows.push(
					<div className="panel-body" key={ "pnl-body" + i }>
						<form role="form" id={ result.name } name={ result.name }>
							<fieldset>
								{ objects }
							</fieldset>
						</form>
						{ buttonCollection }
					</div>
				);
			})};
			
			return (
				<div className={ cls }>
					<div className="login-panel panel panel-default">
						{ rows }
					</div>
				</div>
			);
		},
		render: function() {
			return (this.loadHTML());
		}
	});

	var Page = React.createClass({
		render: function() {	
			return (
				<div className="row">
					<Form />
				</div>
			)
		}
	});

	var EventHandler = React.createClass({
		clickHandler: function() {
			console.log(React.findDOMNode(this.refs.myTextInput).value);
		},
		render: function() {
			return(
				<div>
					<input type="text" ref="myTextInput" />
					<button id="btnClickMe" onClick={this.clickHandler}>Click Me</button>
					<div onClick={this.clickHandler}>Click Me</div>
				</div>
			);
		}
	});

	return Page;
};

module.exports = Apps;