import 'babel/polyfill';
import _ from 'lodash';
import fs from 'fs';
import path from 'path';
import express from 'express';
import ReactDOM from 'react-dom/server';
import router from './router';
import reactCookie from './core/react-cookie';
import request from 'superagent';

const server = global.server = express();

server.set('port', (process.env.PORT || 5000));
server.use(express.static(path.join(__dirname, 'public')));

// Include static assets. Not advised for production
/* CSS */
server.use(express.static(path.join(__dirname, '../bower_components/bootstrap/dist/fonts')));
server.use(express.static(path.join(__dirname, '../bower_components/bootstrap/dist/css')));
server.use(express.static(path.join(__dirname, '../bower_components/metisMenu/dist')));
server.use(express.static(path.join(__dirname, '../bower_components/font-awesome/fonts')));
server.use(express.static(path.join(__dirname, '../bower_components/font-awesome/css')));
server.use(express.static(path.join(__dirname, '../bower_components/bootstrap-social')));
server.use(express.static(path.join(__dirname, '../dist/css')));
server.use(express.static(path.join(__dirname, '../bower_components/bootstrap-social')));
server.use(express.static(path.join(__dirname, '../bower_components/morrisjs')));

/* JS */
server.use(express.static(path.join(__dirname, '../bower_components/jquery/dist')));
server.use(express.static(path.join(__dirname, '../bower_components/bootstrap/dist/js')));
server.use(express.static(path.join(__dirname, '../bower_components/typeaheadjs/dist')));
server.use(express.static(path.join(__dirname, '../bower_components/metisMenu/dist')));
server.use(express.static(path.join(__dirname, '../bower_components/raphael')));
server.use(express.static(path.join(__dirname, '../bower_components/morrisjs')));
server.use(express.static(path.join(__dirname, '../bower_components/datatables/media/js')));
server.use(express.static(path.join(__dirname, '../bower_components/datatables-plugins/integration/bootstrap/3')));

//
// Register API middleware
// -----------------------------------------------------------------------------
server.use('/api/content', require('./api/content'));
//
// Register server-side rendering middleware
// -----------------------------------------------------------------------------

server.get('/sales-invoice/:salesInvoiceId/print', (req, res, next) => {
	const salesInvoiceId = req.params.salesInvoiceId;

	reactCookie.plugToRequest(req, res);
	const auth = reactCookie.load('auth');

	request
		.get(`https://ceramic-api.herokuapp.com/v1/sales-invoice/${salesInvoiceId}/print`)
		.set('Authorization', auth)
		.pipe(res);

	// prompt to download
	res.attachment('invoice-' + Date.now() + '.pdf');
});

server.get('/shipping-order/:shippingOrderId/print', (req, res, next) => {
	const shippingOrderId = req.params.shippingOrderId;

	reactCookie.plugToRequest(req, res);
	const auth = reactCookie.load('auth');

	request
		.get(`https://ceramic-api.herokuapp.com/v1/shipping-order/${shippingOrderId}/print`)
		.set('Authorization', auth)
		.pipe(res);

	// prompt to download
	res.attachment('shipping-' + Date.now() + '.pdf');
});

// The top-level React component + HTML template for it
const templateFile = path.join(__dirname, 'templates/index.html');
const template = _.template(fs.readFileSync(templateFile, 'utf8'));

server.get('*', async (req, res, next) => {
	try {
		let statusCode = 200;
		const data = { title: '', description: '', css: '', body: '' };
		const css = [];
		const context = {
			onInsertCss: value => css.push(value),
			onSetTitle: value => data.title = value,
			onSetMeta: (key, value) => data[key] = value,
			onPageNotFound: () => statusCode = 404
		};

		reactCookie.plugToRequest(req, res);

		await router.dispatch({ path: req.path, context }, (state, component) => {
			data.body = ReactDOM.renderToString(component);
			data.css = css.join('');
		});

		const html = template(data);
		res.status(statusCode).send(html);
	}
	catch (err) {
		next(err);
	}
});

//
// Launch the server
// -----------------------------------------------------------------------------

server.listen(server.get('port'), () => {
	if (process.send) {
		process.send('online');
	}
	else {
		console.log('The server is running at http://localhost:' + server.get('port'));
	}
});
