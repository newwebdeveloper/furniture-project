// http://rapiddg.com/blog/calling-rest-api-nodejs-script
var querystring = require('querystring');
var http = require('http');
var https = require('https');
var reactCookie = require('../core/react-cookie');

// var host = 'www.thegamecrafter.com';
// var host = 'api.ceramic.dev:8080'
var host = 'ceramic-api.herokuapp.com'
var username = 'admin';
var password = 'admin';
var apiKey = '*****';
var sessionId = null;
var deckId = '68DC5A20-EE4F-11E2-A00C-0858C0D5C2ED';

var init = function(host, username, password, apiKey, sessionId, deckId) {
	this.host = host;
	this.username = username;
	this.password = password;
	this.apiKey = apiKey;
	this.sessionId = sessionId;
	this.deckId = deckId;
}

var performRequest = (endpoint, method, data, success, file) => {
	var dataString = JSON.stringify(data);
	var headers = {};

	headers.Authorization = reactCookie.load('auth');

	if (method == 'GET') {
		// endpoint += '?' + querystring.stringify(data);
	}
	else {
		if (file)
			headers["Content-Type"] = 'multipart/form-data';
		else
			headers["Content-Type"] = 'application/json';
		headers["Content-Length"] = dataString.length;
	}
	var options = {
		host: host,
		path: endpoint,
		method: method,
		headers: headers,
		withCredentials: false
	};
	
	var req = https.request(options, (res) => {
		// res.setEncoding('utf8');

		var responseString = '';

		res.on('data', function(data) {
			responseString += data;
		});

		res.on('end', function() {
			var responseObject = {};

			responseObject = JSON.parse(responseString)

			success(responseObject);
		});
	});

	req.write(dataString);
	req.end();
}
module.exports.init = init;
module.exports.performRequest = performRequest;