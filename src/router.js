import React from 'react';
import Router from 'react-routing/src/Router';
import http from './core/http';
/* Component */
import App from './components/App';
import ContentPage from './components/ContentPage';
import NotFoundPage from './components/NotFoundPage';
import ErrorPage from './components/ErrorPage';
/* Page */
import LoginPage from './components/LoginPage';
import Dashboard from './components/Dashboard';
import Customer from './components/Customer';
import CustomerInput from './components/CustomerInput';
import Staff from './components/Staff';
import StaffInput from './components/StaffInput';
import Supplier from './components/Supplier';
import SupplierInput from './components/SupplierInput';
import Department from './components/Department';
import Product from './components/Product';
import ProductInput from './components/ProductInput';
import ProductAdjustment from './components/ProductAdjustment';
import ProductAdjustmentInput from './components/ProductAdjustmentInput';
import ProductCategory from './components/ProductCategory';
import SalesInvoice from './components/SalesInvoice';
import SalesInvoiceInput from './components/SalesInvoiceInput';
import SalesOrder from './components/SalesOrder';
import SalesOrderInput from './components/SalesOrderInput';
import SalesPricing from './components/SalesPricing';
import SalesPricingInput from './components/SalesPricingInput';
import SalesReturn from './components/SalesReturn';
import SalesReturnInput from './components/SalesReturnInput';
import ShippingOrder from './components/ShippingOrder';
import ShippingOrderInput from './components/ShippingOrderInput';
import Payment from './components/Payment';
import PaymentInput from './components/PaymentInput';
import CashIn from './components/CashIn';
import CashInInput from './components/CashInInput';
import CashOut from './components/CashOut';
import CashOutInput from './components/CashOutInput';
import CostCategory from './components/CostCategory';
import Profit from './components/Profit';
import Transaction from './components/Transaction';
import Receivable from './components/Receivable';
import User from './components/User';
import Role from './components/Role';
import StaffPosition from './components/StaffPosition';
import Thumbnail from './components/Thumbnail';
import RegisterPage from './components/RegisterPage';

var reactCookie = require('./core/react-cookie');

const router = new Router(on => {
	on('*', async (state, next) => {
		const component = await next();
		if (state.path == '/login') {
			if (typeof reactCookie.load('auth') != 'undefined') {
				return component && <App context={state.context}>{<Dashboard />}</App>;
			}
			else return <LoginPage />;
		}
		else if (state.path == '/register') {
			return <RegisterPage />
		}
		else {
			if (typeof reactCookie.load('auth') == 'undefined') {
				return <LoginPage />;
			} else {
				return component && <App context={state.context}>{component}</App>;
			}
		}
	});
	on('/login', async () => <LoginPage />);
	on('/register', async () => <RegisterPage />);
	on('/', async () => <Dashboard />);
	on('/staff', async () => <Staff />);
	on('/staff-input', async () => <StaffInput />);
	on('/customer', async () => <Customer />);
	on('/customer-input', async () => <CustomerInput />);
	on('/supplier', async () => <Supplier />);
	on('/supplier-input', async () => <SupplierInput />);
	on('/product', async () => <Product />);
	on('/product-input', async () => <ProductInput />);
	on('/sales-invoice', async () => <SalesInvoice />);
	on('/sales-invoice-input', async () => <SalesInvoiceInput />);
	on('/sales-order', async () => <SalesOrder />);
	on('/sales-order-input', async () => <SalesOrderInput />);
	on('/sales-pricing', async () => <SalesPricing />);
	on('/sales-pricing-input', async () => <SalesPricingInput />);
	on('/sales-return', async () => <SalesReturn />);
	on('/sales-return-input', async () => <SalesReturnInput />);
	on('/product-adjustment', async () => <ProductAdjustment />);
	on('/product-adjustment-input', async () => <ProductAdjustmentInput />);
	on('/product-category', async () => <ProductCategory />);
	on('/user', async () => <User />);
	on('/department', async () => <Department />);
	on('/role', async () => <Role />);
	on('/staff-position', async () => <StaffPosition />);
	on('/shipping-order', async () => <ShippingOrder />);
	on('/shipping-order-input', async () => <ShippingOrderInput />);
	on('/payment', async () => <Payment />);
	on('/payment-input', async () => <PaymentInput />);
	on('/cash-in', async () => <CashIn />);
	on('/cash-in-input', async () => <CashInInput />);
	on('/cash-out', async () => <CashOut />);
	on('/cash-out-input', async () => <CashOutInput />);
	on('/cost-category', async () => <CostCategory />);
	on('/profit', async () => <Profit />);
	on('/transaction', async () => <Transaction />);
	on('/receivable', async () => <Receivable />);

	on('*', async (state) => {
		// const content = await http.get(`/api/content?path=${state.path}`);
		const content = await http.get(`/api/content`);
		return content && <ContentPage {...content} />;
	});

	on('error', (state, error) => state.statusCode === 404 ?
		<App context={state.context} error={error}><NotFoundPage /></App> :
		<App context={state.context} error={error}><ErrorPage /></App>);
});

export default router;